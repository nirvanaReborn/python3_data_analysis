#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6860093613674758668/
# Gvcode库：一个更简单的、华人开源的、自动生成验证码的python库
# gvcode全称：graphic-verification-code。
# https://pypi.org/project/graphic-verification-code/
# https://github.com/vcodeclub/graphic-verification-code
# https://pypi.org/user/Hackathon/
'''可能显示不全
对源码进行修改：
#本机位置，root下file:///usr/local/python3.8/lib/python3.8/site-packages/gvcode文件夹
#打开main.py  第77行修改
[修改前] xy = ((width - font_width) / 3, (height - font_height) / 3)
[修改后] xy = (0,0)
'''
# pip install graphic-verification-code  #注意不是gvcode
# 注意，这个库依赖于PIL
import gvcode


def test_1():
    # 实例化自动生成图片和验证码
    img, code = gvcode.generate()
    # 终端打印验证码
    print(code)
    # 展示生成验证码的图片
    img.show()
    # 保存图片
    # img.save('/home/xgj/Desktop/gvcode/1.jpg')


def test_2():
    # 自定义：图片大小，背景颜色和字体颜色，字体大小，字符串长度
    img, code = gvcode.generate(size=(480, 120), bg_color=(255, 255, 255), fg_color=(55, 110, 0),
                                font_size=30, length=6)
    print(code)
    img.show()



def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
