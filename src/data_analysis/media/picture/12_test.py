#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.7624.net/iyule/5408094/20180408A1O2MR00.html
# Open CV是处理图像的神级工具

import random

# pip install opencv-contrib-python
import cv2
import numpy as np


# 颜色反转
def test_1():
    img = cv2.imread('1.jpg', 1)
    imgInfo = img.shape()
    height = imgInfo[0]
    width = imgInfo[1]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    dst = np.zeros((height, width, 1), np.uint8)  # 1 代表一个像素是由几个颜色组成的
    for i in range(0, height):
        for j in range(0, width):
            grayPixel = gray[i, j]
            dst[i, j] = 255 - grayPixel
    cv2.imshow('dst', dst)
    cv2.waitKey(0)


# 彩色图片 反转
def test_2():
    img = cv2.imread('2.jpg', 1)
    imgInfo = img.shape
    height = imgInfo[0]
    width = imgInfo[1]
    dst = np.zeros((height, width, 3), np.uint8)  # 1 代表一个像素是由几个颜色组成的
    for i in range(0, height):
        for j in range(0, width):
            (b, g, r) = img[i, j]
            dst[i, j] = (255 - b, 255 - g, 255 - r)
    cv2.imshow('dst', dst)
    cv2.waitKey(0)


# 马赛克
def test_3():
    img = cv2.imread('2.jpg', 1)
    imgInfo = img.shape
    height = imgInfo[0]
    width = imgInfo[1]

    for m in range(100, 300):
        for n in range(50, 200):
            if m % 10 == 0 and n % 10 == 0:
                for i in range(0, 10):
                    for j in range(0, 10):
                        (b, g, r) = img[m, n]
                        img[i + m, j + n] = (b, g, r)
    cv2.imshow('dst', img)
    cv2.waitKey(0)


# 图片融合
def test_4():
    img0 = cv2.imread('2.jpg', 1)
    img1 = cv2.imread('3.jpg', 1)
    imgInfo = img0.shape
    height = imgInfo[0]
    width = imgInfo[1]
    roiH = int(height / 2)
    roiW = int(width / 2)
    img0ROI = img0[0:roiH, 0:roiW]
    img1ROI = img1[0:roiH, 0:roiW]
    dst = np.zeros((roiH, roiW, 3), np.uint8)
    dst = cv2.addWeighted(img0ROI, 0.5, img1ROI, 0.5, 0)
    cv2.imshow('dst', dst)
    cv2.waitKey(0)


# 边缘检测
def test_5():
    img = cv2.imread('2.jpg', 1)
    imgInfo = img.shape
    height = imgInfo[0]
    width = imgInfo[1]
    cv2.imshow('src', img)
    # canny 1 gray 2 高斯 3 canny
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    imgG = cv2.GaussianBlur(gray, (3, 3), 0)
    dst = cv2.Canny(img, 50, 50)
    cv2.imshow('dst', dst)
    cv2.waitKey(0)


# 浮雕效果
def test_6():
    img = cv2.imread('2.jpg', 1)
    imgInfo = img.shape
    height = imgInfo[0]
    width = imgInfo[1]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # newP = gray0 - gray1 + 150
    dst = np.zeros((height, width, 1), np.uint8)
    for i in range(0, height):
        for j in range(0, width - 1):
            grayP0 = int(gray[i, j])
            grayP1 = int(gray[i, j + 1])
            newP = grayP0 - grayP1 + 150
            if newP > 255:
                newP = 255
            if newP < 0:
                newP = 0
            dst[i, j] = newP
    cv2.imshow('dst', dst)
    cv2.waitKey(0)


def main():
    print(cv2.__version__)
    test_1()


if __name__ == "__main__":
    main()
