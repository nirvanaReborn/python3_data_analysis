#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.codeobj.com/2019/06/python3%E7%9A%84pil-image-image%E5%9B%BE%E7%89%87webp%E6%A0%BC%E5%BC%8F%E8%BD%AC%E6%8D%A2%EF%BC%8Curl-image-byte%E5%AD%97%E8%8A%82%E6%B5%81%E6%93%8D%E4%BD%9C%EF%BC%8C%E8%BD%AC%E6%8D%A2%E6%88%90byte/
# Python3的PIL.Image.Image图片webp格式转换，URL Image Byte字节流操作，转换成byte格式


def test_1():
    import io
    from PIL import Image  # 注意我的Image版本是pip3 install Pillow==4.3.0
    import requests

    # 获取字节流最好加stream这个参数,原因见requests官方文档
    res = requests.get('http://p1.pstatp.com/list/300x196/pgc-image/152923179745640a81b1fdc.webp', stream=True)

    # 把请求到的数据转换为Bytes字节流(这样解释不知道对不对，
    # 可以参照[廖雪峰](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/001431918785710e86a1a120ce04925bae155012c7fc71e000)的教程看一下)
    byte_stream = io.BytesIO(res.content)

    roiImg = Image.open(byte_stream)  # Image打开Byte字节流数据
    roiImg.show()  # 弹出 显示图片

    imgByteArr = io.BytesIO()  # 创建一个空的Bytes对象
    roiImg.save(imgByteArr, format='PNG')  # PNG就是图片格式，我试过换成JPG/jpg都不行
    imgByteArr = imgByteArr.getvalue()  # 这个就是保存的图片字节流

    # 下面这一步只是本地测试， 可以直接把imgByteArr，当成参数上传到七牛云
    with open("./abc.png", "wb") as f:
        f.write(imgByteArr)


def test_2():
    import io
    import qrcode

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data('{0}/reffer?id={1}'.format(CURRENT_URL, id))
    qr.make(fit=True)

    img = qr.make_image(fill_color="black", back_color="white")
    print(img)  # 类型PIL.Image.Image
    imgByteArr = io.BytesIO()
    img.save(imgByteArr, format='PNG')  # PNG就是图片格式，我试过换成JPG/jpg都不行
    imgByte = imgByteArr.getvalue()  # 这里转换后就是b''格式的数据


def main():
    test_1()


if __name__ == "__main__":
    main()
