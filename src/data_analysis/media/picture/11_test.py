#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6536837632456917507/
# python实现图片转换成手绘图像
'''
为什么要求梯度值呢？
因为梯度值表示的是图像各离散像素点的变化率，也就能够找出变化最快的地方
（变化率最快的地方，一般差异最明显，也就是我们要找的物体的边界）。
'''

import os

import numpy
from PIL import Image


def test_1(source_pic, dest_pic):
    # 通过PIL模块的convert函数，把图像转换为灰度图像，再转化为numpy中的矩阵形式
    a = numpy.asanyarray(Image.open(source_pic).convert('L')).astype('float')
    depth = 10  # (0-100)
    grad = numpy.gradient(a)  # 取象灰的梯度值
    grad_x, grad_y = grad  # 分別取横纵图象梯度值
    grad_x = grad_x * depth / 100
    grad_y = grad_y * depth / 100
    A = numpy.sqrt(grad_x ** 2 + grad_y ** 2 + 1)
    uni_x = grad_x / A
    uni_y = grad_y / A
    uni_z = 1 / A

    vec_el = numpy.pi / 2.2  # 光源的俯视角度，弧度制
    vec_az = numpy.pi / 4  # 光源的方位角度，弧度制
    dx = numpy.cos(vec_el) * numpy.cos(vec_az)  # 光源对X轴的影响
    dy = numpy.cos(vec_el) * numpy.sin(vec_az)  # 光源对Y轴的影响
    dz = numpy.sin(vec_el)  # 光源对Z轴的影响

    b = 255 * (dx * uni_x + dy * uni_y + dz * uni_z)
    b = b.clip(0, 255)

    im = Image.fromarray(b.astype('uint8'))  # 重构图像
    im.save(dest_pic)


def main():
    source_pic = r"风景 - 2.jpg"
    dest_pic = os.path.join(os.path.dirname(source_pic), "test.jpg")
    test_1(source_pic, dest_pic)


if __name__ == "__main__":
    main()
