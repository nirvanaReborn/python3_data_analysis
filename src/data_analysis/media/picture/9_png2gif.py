#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/dcb3688/p/4608048.html
# python 将png图片格式转换生成gif动画
'''
注：loop循环次数在浏览器有效果，用看图软件不起作用
'''

import os
import sys

import imageio
from PIL import Image

from public_function import GLOBAL_WORK_DIR


# 从gif提取frame
def test_1():
    def processImage(infile):
        try:
            im = Image.open(infile)
        except IOError:
            print("Cant load", infile)
            sys.exit(1)
        i = 0
        mypalette = im.getpalette()

        try:
            while 1:
                im.putpalette(mypalette)
                new_im = Image.new("RGBA", im.size)
                new_im.paste(im)
                new_im.save('images/a' + str(i) + '.png')

                i += 1
                im.seek(im.tell() + 1)

        except EOFError:
            pass  # end of sequence

    source_file = os.path.join(GLOBAL_WORK_DIR, r"9.gif")
    processImage(source_file)


# 但从frame生成gif
def test_2():
    images = []
    filenames = sorted((fn for fn in os.listdir('.') if fn.endswith('.png')))
    for filename in filenames:
        # print(filename)
        images.append(imageio.imread("images/" + filename))
    imageio.mimsave('gif.gif', images, duration=0.5, loop=1)  # duration 每帧间隔时间，loop 循环次数
    # imageio查看参数： http://imageio.readthedocs.io/
    # imageio.help('gif')


# 其实，PIL自身也有一个save方法，里面有一个‘save_all’ 参数，意思就是save多个，当format指定为gif时，生成的便是gif的动画
def test_3():
    im = Image.open("images/a0.png")
    images = []
    # filenames = sorted((fn for fn in os.listdir('.') if fn.endswith('.png')))
    # for filename in filenames:
    #     images.append(Image.open(filename))
    images.append(Image.open('images/a1.png'))
    images.append(Image.open('images/a2.png'))
    im.save('gif.gif', save_all=True, append_images=images, loop=1, duration=1, comment=b"aaabb")


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
