#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/linjiqin/p/4140455.html
# Python使用QRCode模块生成二维码
# python-qrcode是个用来生成二维码图片的第三方模块，依赖于 PIL 模块和 qrcode 库。
'''
version：值为1~40的整数，控制二维码的大小（最小值是1，是个12×12的矩阵）。 如果想让程序自动确定，将值设置为 None 并使用 fit 参数即可。

error_correction：控制二维码的错误纠正功能。可取值下列4个常量。
　　ERROR_CORRECT_L：大约7%或更少的错误能被纠正。
　　ERROR_CORRECT_M（默认）：大约15%或更少的错误能被纠正。
　　ROR_CORRECT_H：大约30%或更少的错误能被纠正。

box_size：控制二维码中每个小格子包含的像素数。

border：控制边框（二维码与图片边界的距离）包含的格子数（默认为4，是相关标准规定的最小值）
'''

import qrcode
from PIL import Image  # PIL是Python 中最著名的2D 图片处理库。

from public_function import GLOBAL_PNG_FILE


def test_2(pic):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data('hello, qrcode')
    qr.make(fit=True)
    img = qr.make_image()
    img.save(pic)


def test_1(pic):
    img = qrcode.make('hello, qrcode')
    img.save(pic)


def main():
    pic = GLOBAL_PNG_FILE
    dict_choice = {
        "1": "test_1(pic)",
        "2": "test_2(pic)",
        "3": "test_3(pic)",
        "4": "test_4(pic)",
        "5": "test_5(pic)",
        "6": "test_6(pic)",
        "7": "test_7(pic)",
        "8": "test_8(pic)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)

    image = Image.open(pic)  # 打开验证码图片
    image.load()  # 加载一下图片，防止报错，此处可省略
    image.show()  # 调用show来展示图片，调试用，可省略


if __name__ == "__main__":
    main()
