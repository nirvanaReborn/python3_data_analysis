#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qiushi_1990/article/details/78041375
# https://www.toutiao.com/a6486621845314339342/
# 通过pytesseract识别图片中文文本

# tesseract-ocr是惠普公司开源的一个文字识别项目，通过它可以快速搭建图文识别系统，帮助我们开发出能识别图片的ocr系统。
# 通过官方github的wiki：https://github.com/tesseract-ocr/tesseract/wiki 选择对应版本进行安装。
#
# 这里介绍的是windows环境下的安装步骤：
# 1. 下载windows安装包：https://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-setup-4.00.00dev.exe
#     下载好的语言包放入到安装目录中的testdata下即可。
# 2.选择中文训练数据
# 3.配置环境变量：
#     在PATH中新增：C:\Program Files (x86)\Tesseract-OCR
#     新增环境变量TESSDATA_PREFIX：C:\Program Files (x86)\Tesseract-OCR
# 4.安装完成tesseract-ocr后，我们还需要做一下配置
#     在C:\Users\huxiu\AppData\Local\Programs\Python\Python35\Lib\site-packages\pytesseract找到pytesseract.py打开后做如下操作
#     tesseract_cmd = 'D:/ProgramFiles/Tesseract-OCR/tesseract.exe'
# 5.PIP安装依赖包
#     pip install pillow
#     pip install pytesseract

# 百度云OCR：https://cloud.baidu.com/doc/OCR/OCR-Python-SDK.html
# 微软Azure 图像识别：https://azure.microsoft.com/zh-cn/services/cognitive-services/computer-vision/
# 有道智云文字识别：http://aidemo.youdao.com/ocrdemo
# 阿里云图文识别：https://www.aliyun.com/product/cdi/
# 腾讯OCR文字识别： https://cloud.tencent.com/product/ocr


import os

from PIL import Image

from public_function import GLOBAL_WORK_DIR


class Languages:
    CHS = 'chi_sim'
    CHT = 'chi_tra'
    ENG = 'eng'


def test_1(pic):
    import pytesseract

    image = Image.open(pic)  # 打开验证码图片
    image.load()  # 加载一下图片，防止报错，此处可省略
    # image.show()  # 调用show来展示图片，调试用，可省略
    text = pytesseract.image_to_string(image, lang='chi_sim')  # lang='chi_sim'表明使用简体中文训练包。
    print(text)


def test_2(pic):
    import tesserocr

    image = Image.open(pic)  # 打开验证码图片
    result = tesserocr.image_to_text(image)
    print(result)


def main():
    pic = os.path.join(GLOBAL_WORK_DIR, "杜甫_登高.jpg")
    test_1(pic)


if __name__ == "__main__":
    main()
