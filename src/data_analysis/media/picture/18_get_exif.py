#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/qianxinggz/p/11402602.html
# python实现提取图片exif信息

import optparse
import os
import urllib.error
import urllib.parse
import urllib.request
from os.path import basename
from urllib.parse import urlsplit

import exifread
from PIL import Image
from PIL.ExifTags import TAGS
from bs4 import BeautifulSoup


def findImages(url):  # 找到该网页的所有图片标签
    print('[+] Finding images of ' + str(urlsplit(url)[1]))
    resp = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(resp, "lxml")
    imgTags = soup.findAll('img')
    return imgTags


def downloadImage(imgTag):  # 根据标签从该网页下载图片
    try:
        print('[+] Downloading image...')
        imgSrc = imgTag['src']
        imgContent = urllib.request.urlopen(imgSrc).read()
        imgName = basename(urlsplit(imgSrc)[2])
        f = open(imgName, 'wb')
        f.write(imgContent)
        f.close()
        return imgName
    except:
        return ''


def delFile(imgName):  # 删除该目录下下载的文件
    os.remove('/mnt/hgfs/temp/temp/python/exercise/' + str(imgName))
    print("[+] Del File")


def exifImage(imageName):  # 提取exif信息，若无则删除
    if imageName.split('.')[-1] == 'png':
        imageFile = open(imageName, 'rb')
        Info = exifread.process_file(imageFile)
    elif imageName.split('.')[-1] == 'jpg' or imageName.split('.')[-1] == 'jpeg':
        imageFile = Image.open(imageName)
        Info = imageFile._getexif()
    else:
        pass
    try:
        exifData = {}
        if Info:
            for (tag, value) in Info:
                TagName = TAGS.get(tag, tag)
                exifData[TagName] = value
            exifGPS = exifData['GPSInfo']
            if exifGPS:
                print('[+] GPS: ' + str(exifGPS))
            else:
                print('[-] No GPS information')
                delFile(imageName)
        else:
            print('[-] Can\'t detecated exif')
            delFile(imageName)
    except Exception as e:
        print(e)
        delFile(imageName)
        pass


def main():
    parser = optparse.OptionParser('-u <target url>')
    parser.add_option('-u', dest='url', type='string', help='specify the target url')
    (options, args) = parser.parse_args()
    url = options.url
    # url=r""
    if url == None:
        print(parser.usage)
        exit(0)
    else:
        print(url)

    imgTags = findImages(url)
    for imgTag in imgTags:
        imgFile = downloadImage(imgTag)
        exifImage(imgFile)


if __name__ == '__main__':
    main()
