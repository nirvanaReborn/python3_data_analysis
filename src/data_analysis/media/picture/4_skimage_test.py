#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jianshu.com/p/f2e88197e81d
# python skimage图像处理(一)
#
# skimage官网
# https://scikit-image.org/
# 原文：https://blog.csdn.net/qq_41185868/article/details/80330262
# 子模块名称　                主要实现功能
# io                            读取、保存和显示图片或视频
# data                       提供一些测试图片和样本数据
# color                           颜色空间变换
# filters             图像增强、边缘检测、排序滤波器、自动阈值等
# draw               操作于numpy数组上的基本图形绘制，包括线条、矩形、圆和文本等
# transform          几何变换或其它变换，如旋转、拉伸和拉东变换等
# morphology          形态学操作，如开闭运算、骨架提取等
# exposure              图片强度调整，如亮度调整、直方图均衡等
# feature                        特征检测与提取等
# measure                  图像属性的测量，如相似性或等高线等
# segmentation                          图像分割
# restoration                           图像恢复
# util                                  通用函数

# pip install Pillow
from PIL import Image  # PIL是Python 中最著名的2D 图片处理库。
from matplotlib import pyplot as plot
from skimage import io, transform, data

from public_function import GLOBAL_JPG_FILE
from public_function import dict_choice


# Image读出来的是PIL的类型，而skimage.io读出来的数据是numpy格式的
# 输出可以看出Img读图片的大小是图片的(width, height)；而skimage的是(height,width, channel),
#  这也是为什么caffe在单独测试时要要在代码中设置：transformer.set_transpose('data',(2,0,1))，
#  因为caffe可以处理的图片的数据格式是(channel,height,width)，所以要转换数据
def test_1():
    # Image和skimage读图片
    img_file1 = Image.open(GLOBAL_JPG_FILE)
    img_file2 = io.imread(GLOBAL_JPG_FILE)

    # 读图片后数据的大小：
    print("the picture's size: ", img_file1.size)
    print("the picture's shape: ", img_file2.shape)
    # 输出可以看出Img读图片的大小是图片的(width, height)；
    # 而skimage的是(height,width, channel),
    # 这也是为什么caffe在单独测试时要要在代码中设置：transformer.set_transpose('data',(2,0,1))，
    # 因为caffe可以处理的图片的数据格式是(channel,height,width)，所以要转换数据

    # 得到像素：
    print(img_file1.getpixel((500, 1000)), img_file2[500][1000])
    print(img_file1.getpixel((500, 1000)), img_file2[1000][500])
    print(img_file1.getpixel((1000, 500)), img_file2[500][1000])


# 获取skimage图片信息
def test_2():
    # 为了方便练习，也提供一个data模块，里面嵌套了一些示例图片，我们可以直接使用。
    img = data.chelsea()
    io.imshow(img)
    print(type(img))  # 显示类型
    print(img.shape)  # 显示尺寸
    print(img.shape[0])  # 图片高度
    print(img.shape[1])  # 图片宽度
    print(img.shape[2])  # 图片通道数
    print(img.size)  # 显示总像素个数
    print(img.max())  # 最大像素值
    print(img.min())  # 最小像素值
    print(img.mean())  # 像素平均值
    print(img[0][0])  # 图像的像素值

    # PIL image 查看图片信息，可用如下的方法
    img = Image.open(GLOBAL_JPG_FILE)
    print(type(img))  # 显示类型
    print(img.size)  # 图片的尺寸
    print(img.mode)  # 图片的模式
    print(img.format)  # 图片的格式
    print(img.getpixel((0, 0)))  # 得到像素：
    # img读出来的图片获得某点像素用getpixel((w,h))可以直接返回这个点三个通道的像素值

    # 获取图像的灰度值范围
    width = img.size[0]
    height = img.size[1]

    # 输出图片的像素值
    count = 0
    for i in range(0, width):
        for j in range(0, height):
            if img.getpixel((i, j)) >= 0 and img.getpixel((i, j)) <= 255:
                count += 1
    print(count)
    print(height * width)


# 从外部读取图片并显示
def test_3():
    img = io.imread(GLOBAL_JPG_FILE)
    io.imshow(img)

    # 读取单张灰度图片
    img = io.imread(GLOBAL_JPG_FILE, as_grey=True)
    io.imshow(img)

    # 程序自带图片
    # skimage程序自带了一些示例图片，如果我们不想从外部读取图片，就可以直接使用这些:
    #
    # astronaut     航员图片      coffee     一杯咖啡图片
    # lena          lena美女图片   camera   拿相机的人图片
    # coins           硬币图片     moon    月亮图片
    # checkerboard   棋盘图片       horse   马图片
    # page   书页图片              chelsea   小猫图片
    # hubble_deep_field    星空图片   text   文字图片
    # clock    时钟图片   immunohistochemistry   结肠图片

    img = data.chelsea()
    io.imshow(img)

    # 打印示例图片存放路径
    print(data.data_dir)

    # 保存图片
    io.imsave('d:/cat.jpg', img)


# 图像像素的访问与裁剪
def test_4():
    pass


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
