#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/eedlrj/archive/2016/08/27/5813211.html
# 调用第三方库压缩png或者转换成webp
import os


# full path
def check_file(file):
    dic = os.path.splitext(file)
    if dic[1] == ".png":
        return True
    return False


def is_file(path):
    return os.path.isfile(path)


def do_common(path, cmd):
    if cmd == "1":  # png压缩
        exe_path = "E:\workSpace\pngTest\pngquant.exe"
        common = exe_path + " -f --quality 100 --ext .png --speed 1 " + path
        print("*" * 10 + "\n" + path)
        os.system(common)
    else:  # png转webp
        exe_path = '''E:\workSpace\pngTest\webp\libwebp-0.3.1-windows-x64\cwebp.exe'''
        dir_name = os.path.dirname(path)
        file = os.path.basename(path)
        dic = os.path.splitext(file)
        file_name = dic[0]
        file_out_name = file_name + "_webp"
        ext = dic[1]
        file_out = os.path.join(dir_name, file_out_name + ".webp")

        common = exe_path + " -preset drawing -q 100 -alpha_q 100 -quiet " + path + " -o " + file_out
        print("*" * 10 + "\n" + path)
        os.system(common)

        os.remove(path)
        print("#" * 10 + "\n" + file_out)
        os.rename(file_out, path)


def walk_dir(root_path, cmd):
    for root, dir_names, file_names in os.walk(root_path):
        for file_name in file_names:
            full_path = os.path.join(root, file_name)
            if is_file(full_path):
                if check_file(full_path):
                    do_common(full_path, cmd)
            else:
                walk_dir(full_path, cmd)


def main():
    root_path = os.getcwd() + os.sep
    cmd = str(input("Enter a positive integer to choice: "))
    walk_dir(root_path, cmd)


if __name__ == "__main__":
    main()
