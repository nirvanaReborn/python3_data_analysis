#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 判断一个文件是否是图片
import os


def is_img(ext):
    ext = ext.lower()
    if ext in ['.jgp', '.png', '.jpeg', '.bmp', '.webp']:
        return True
    else:
        return False


def check_pic(pic):
    import imghdr
    with open(pic, 'rb') as img_file:
        img_format = imghdr.what(img_file)
        if img_format == None:
            print("不是图片或图片已损坏", pic)
            return False
        elif img_format == "webp":
            print("不识别:", img_format, pic)
            return False
    return True


def main():
    root_dir = os.path.join(r"D:/", os.environ['USERNAME']).replace('/', '\\')
    source_dir = os.path.join(root_dir, r"Pictures\Picture\美女\待整理")
    for pic in os.listdir(source_dir):
        if is_img(os.path.splitext(pic)[1]):
            if check_pic(pic):
                print("这是一张图片：", pic)


if __name__ == "__main__":
    main()
