#!/usr/bin/env python
# -*- coding:utf-8 -*-

# pip install Wand
# 你可以使用wand 做许多Pillow 能做到的事情：
# wand 包含旋转、调整大小、添加文本、画线、格式转化等强大的功能，这些功能你也可以在Pillow 中找到。
# Wand 和Pillow 的API 和文档都非常规范好用。
# http://docs.wand-py.org/en/latest/guide/install.html#install-imagemagick-on-windows
# https://zh.osdn.net/projects/sfnet_imagemagick/releases/

from wand.display import display
from wand.image import Image

from public_function import GLOBAL_JPG_FILE


def main():
    img = Image(filename=GLOBAL_JPG_FILE)
    print(img.size, img.format)
    display(img)


if __name__ == "__main__":
    main()

# ImportError: MagickWand shared library not found.
