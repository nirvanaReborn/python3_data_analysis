#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/kevinelstri/article/details/61921812
# 使用python读取数据科学最常用的文件格式

from scipy import misc

from public_function import dict_choice


def test_1():
    f = misc.face()
    misc.imsave('face.png', f)  # uses the Image module (PIL)
    import matplotlib.pyplot as plt
    plt.imshow(f)
    plt.show()


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == '__main__':
    main()
