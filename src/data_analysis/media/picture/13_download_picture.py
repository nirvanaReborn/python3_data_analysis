#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/xiangxiang613/article/details/80494643?utm_source=blogxgwz0
# 网页图片下载


def test_1(url, out_filename):
    import requests

    img = requests.get(url)  # 请求链接，有防爬的要加headers
    # 保存图片
    with open(out_filename, 'ab') as file:
        file.write(img.content)


def test_2(url, out_filename):
    import wget

    wget.download(url, out=out_filename)


def main():
    url = "http://pic29.nipic.com/20130511/9252150_174018365301_2.jpg"
    test_1(url, r'D:\img_1.jpg')
    test_2(url, r'D:\img_2.jpg')


if __name__ == "__main__":
    main()
