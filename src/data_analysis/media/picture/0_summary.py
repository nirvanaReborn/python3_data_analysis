#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6675560802004501006/
# https://www.toutiao.com/i6950465448420999711/
# https://opensource.com/article/19/3/python-image-manipulation-tools
# 10 个 Python 图像编辑工具

from public_function import GLOBAL_JPG_FILE
from public_function import dict_choice


# scikit-image 是一个结合 NumPy 数组使用的开源 Python 工具，它实现了可用于研究、教育、工业应用的算法和应用程序。
# 即使是对于刚刚接触 Python 生态圈的新手来说，它也是一个在使用上足够简单的库。
# 同时它的代码质量也很高，因为它是由一个活跃的志愿者社区开发的，并且通过了 同行评审(peer review)。
# 图像滤波(image filtering)
# 使用 match_template() 方法实现 模板匹配(template matching)：
def test_1():
    import matplotlib.pyplot as plt
    from skimage import data, filters

    image = data.coins()  # ... or any other NumPy array!
    edges = filters.sobel(image)
    plt.imshow(edges, cmap='gray')


# NumPy 提供了对数组的支持，是 Python 编程的一个核心库。图像的本质其实也是一个包含像素数据点的标准 NumPy 数组，
# 因此可以通过一些基本的 NumPy 操作（例如切片、 掩膜(mask)、 花式索引(fancy indexing)等），就可以从像素级别对图像进行编辑。
# 通过 NumPy 数组存储的图像也可以被 skimage 加载并使用 matplotlib 显示。
# 使用 NumPy 对图像进行 掩膜(mask)操作：
def test_2():
    import numpy as numpy
    from skimage import data
    import matplotlib.pyplot as plt

    image = data.camera()
    type(image)
    numpy.ndarray  # Image is a NumPy array:
    mask = image < 87
    image[mask] = 255
    plt.imshow(image, cmap='gray')


# 像 NumPy 一样， SciPy 是 Python 的一个核心科学计算模块，也可以用于图像的基本操作和处理。
# 尤其是 SciPy v1.1.0 中的 scipy.ndimage 子模块，它提供了在 n 维 NumPy 数组上的运行的函数。
# SciPy 目前还提供了 线性和非线性滤波(linear and non-linear filtering)、
# 二值形态学(binary morphology)、 B 样条插值(B-spline interpolation)、
# 对象测量(object measurements)等方面的函数。
def test_3():
    from scipy import misc, ndimage
    import matplotlib.pyplot as plt

    face = misc.face()
    blurred_face = ndimage.gaussian_filter(face, sigma=3)
    very_blurred = ndimage.gaussian_filter(face, sigma=5)
    # Results
    plt.imshow()


# PIL (Python Imaging Library) 是一个免费 Python 编程库，它提供了对多种格式图像文件的打开、编辑、保存的支持。
# 但在 2009 年之后 PIL 就停止发布新版本了。幸运的是，还有一个 PIL 的积极开发的分支 Pillow ，
# 它的安装过程比 PIL 更加简单，支持大部分主流的操作系统，并且还支持 Python 3。
# Pillow 包含了图像的基础处理功能，包括像素点操作、使用内置卷积内核进行滤波、颜色空间转换等等。
# 使用 Pillow 中的 ImageFilter 模块实现图像增强：
def test_4():
    from PIL import Image, ImageEnhance
    # Read image
    im = Image.open(GLOBAL_JPG_FILE)
    # Display image
    im.show()
    enh = ImageEnhance.Contrast(im)
    enh.enhance(1.8).show("30% more contrast")


# OpenCV（Open Source Computer Vision 库）是计算机视觉领域最广泛使用的库之一，
# OpenCV-Python 则是 OpenCV 的 Python API。
# OpenCV-Python 的运行速度很快，这归功于它使用 C/C++ 编写的后台代码，
# 同时由于它使用了 Python 进行封装，因此调用和部署的难度也不大。
# 这些优点让 OpenCV-Python 成为了计算密集型计算机视觉应用程序的一个不错的选择。
# 入门之前最好先阅读 OpenCV2-Python-Guide 这份文档。
# 使用 OpenCV-Python 中的 金字塔融合(Pyramid Blending)将苹果和橘子融合到一起：
def test_5():
    # pip install opencv-contrib-python
    import cv2
    import numpy as np

    A = cv2.imread('apple.jpg')
    B = cv2.imread('orange.jpg')

    # generate Gaussian pyramid for A
    G = A.copy()
    gpA = [G]
    for i in range(6):
        G = cv2.pyrDown(G)
        gpA.append(G)

    # generate Gaussian pyramid for B
    G = B.copy()
    gpB = [G]
    for i in range(6):
        G = cv2.pyrDown(G)
        gpB.append(G)

    # generate Laplacian Pyramid for A
    lpA = [gpA[5]]
    for i in range(5, 0, -1):
        GE = cv2.pyrUp(gpA[i])
        L = cv2.subtract(gpA[i - 1], GE)
        lpA.append(L)

    # generate Laplacian Pyramid for B
    lpB = [gpB[5]]
    for i in range(5, 0, -1):
        GE = cv2.pyrUp(gpB[i])
        L = cv2.subtract(gpB[i - 1], GE)
        lpB.append(L)

    # Now add left and right halves of images in each level
    LS = []
    for la, lb in zip(lpA, lpB):
        rows, cols, dpt = la.shape
        ls = np.hstack((la[:, 0:cols / 2], lb[:, cols / 2:]))
        LS.append(ls)

    # now reconstruct
    ls_ = LS[0]
    for i in range(1, 6):
        ls_ = cv2.pyrUp(ls_)
        ls_ = cv2.add(ls_, LS[i])

    # image with direct connecting each half
    real = np.hstack((A[:, :cols / 2], B[:, cols / 2:]))

    cv2.imwrite('Pyramid_blending2.jpg', ls_)
    cv2.imwrite('Direct_blending.jpg', real)


# SimpleCV 是一个开源的计算机视觉框架。它支持包括 OpenCV 在内的一些高性能计算机视觉库，
# 同时不需要去了解 位深度(bit depth)、文件格式、 色彩空间(color space)之类的概念，
# 因此 SimpleCV 的学习曲线要比 OpenCV 平缓得多，正如它的口号所说，“将计算机视觉变得更简单”。
# SimpleCV 的优点还有：即使是刚刚接触计算机视觉的程序员也可以通过 SimpleCV 来实现一些简易的计算机视觉测试;
# 录像、视频文件、图像、视频流都在支持范围内.
def test_6():
    pass


# Mahotas 是另一个 Python 图像处理和计算机视觉库。
# 在图像处理方面，它支持滤波和形态学相关的操作；
# 在计算机视觉方面，它也支持 特征计算(feature computation)、 兴趣点检测(interest point detection)、
# 局部描述符(local descriptors)等功能。
# Mahotas 的接口使用了 Python 进行编写，因此适合快速开发，而算法使用 C++ 实现，并针对速度进行了优化。
# Mahotas 尽可能做到代码量少和依赖项少，因此它的运算速度非常快。
def test_7():
    from pylab import imshow, show
    import mahotas
    import numpy

    wally = mahotas.demos.load('Wally')
    imshow(wally)
    show()

    wfloat = wally.astype(float)
    r, g, b = wfloat.transpose((2, 0, 1))
    w = wfloat.mean(2)
    pattern = numpy.ones((24, 16), float)
    for i in range(2):
        pattern[i::4] = -1
    v = mahotas.convolve(r - w, pattern)
    mask = (v == v.max())
    mask = mahotas.dilate(mask, numpy.ones((48, 24)))
    numpy.subtract(wally, .8 * wally * ~mask[:, :, None], out=wally, casting='unsafe')
    imshow(wally)
    show()


# ITK （Insight Segmentation and Registration Toolkit）是一个为开发者提供普适性图像分析功能的开源、跨平台工具套件，
# SimpleITK 则是基于 ITK 构建出来的一个简化层，旨在促进 ITK 在快速原型设计、教育、解释语言中的应用。
# SimpleITK 作为一个图像分析工具包，它也带有 大量的组件 ，可以支持常规的滤波、图像分割、 图像配准(registration)功能。
# 尽管 SimpleITK 使用 C++ 编写，但它也支持包括 Python 在内的大部分编程语言。
# http://insightsoftwareconsortium.github.io/SimpleITK-Notebooks/
# 使用 Python + SimpleITK 实现的 CT/MR 图像配准过程：
# https://github.com/InsightSoftwareConsortium/SimpleITK-Notebooks/blob/master/Utilities/intro_animation.py
def test_8():
    pass


# pgmagick 是使用 Python 封装的 GraphicsMagick 库。
# GraphicsMagick 通常被认为是图像处理界的瑞士军刀，
# 因为它强大而又高效的工具包支持对多达 88 种主流格式图像文件的读写操作，
# 包括 DPX、GIF、JPEG、JPEG-2000、PNG、PDF、PNM、TIFF 等等。
# pgmagick 的 GitHub 仓库 中有相关的安装说明、依赖列表，以及详细的 使用指引 。
# https://github.com/hhatto/pgmagick
# https://pgmagick.readthedocs.io/en/latest/
def test_9():
    # 图像缩放
    # https://pgmagick.readthedocs.io/en/latest/cookbook.html#scaling-a-jpeg-image
    from pgmagick import Image, Blob

    img = Image(Blob(open('lena_std.jpg').read()), Geometry(200, 200))
    img.scale('200x200')
    img.write('lena_scale.jpg')

    # 边缘提取
    from pgmagick.api import Image

    img = Image('lena.jpg')
    img.edge(2)
    img.write('lena_edge.jpg')


# Cairo 是一个用于绘制矢量图的二维图形库，而 Pycairo 是用于 Cairo 的一组 Python 绑定。
# 矢量图的优点在于做大小缩放的过程中不会丢失图像的清晰度。
# 使用 Pycairo 可以在 Python 中调用 Cairo 的相关命令。
# Pycairo 的 GitHub 仓库 提供了关于安装和使用的详细说明，以及一份简要介绍 Pycairo 的 入门指南 。
# https://github.com/pygobject/pycairo
# https://pycairo.readthedocs.io/en/latest/tutorial.html
# 使用 Pycairo 绘制线段、基本图形、 径向渐变(radial gradients)：
# http://zetcode.com/gfx/pycairo/basicdrawing/
def test_10():
    pass


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
