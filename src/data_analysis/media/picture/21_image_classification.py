#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 自定义图像分类
'''
jpg是jpeg的简称，是目前网络上最为流行的图片格式呢，jpg格式的图片可以将图像文件压缩到最小格式，
而png图片的特性就是体积小，节约空间，与jpg图片相比，png图片是无损压缩，不支持压缩。

png格式的图片可以编辑，比如图片中的字体，线条等，可以通过ps等软件更改。
但是jpg格式的图片则不可更改。

png与jpg图片相比png格式的图片更大。
'''

import imghdr
import os
import shutil
# Python PIL 读取报 UserWarning 如何定位
import warnings

from PIL import Image

warnings.filterwarnings("error", category=UserWarning)


# from PIL import features
# print(features.check_module('webp'))  # False 说明不支持webp格式图片

# import libwebp

# def remove_exif(pic):
#     # 使用piexif库删除exif信息
#     import piexif
#     try:
#         # 某些错误的exif信息会造成keras读取图像文件失败
#         piexif.remove(pic)
#     except:
#         print('save img error', pic)


def check_pic(pic):
    with open(pic, 'rb') as img_file:
        img_format = imghdr.what(img_file)
        if img_format == None:
            print("不是图片或图片已损坏", pic)
            new_pic = None
        elif img_format == "webp":
            print("不识别:", img_format, pic)
            new_pic = None
        else:
            filename, type = os.path.splitext(pic)
            suffix = "." + str(img_format).lower()
            if type != suffix:
                new_pic = filename + suffix
            else:
                new_pic = pic
    return new_pic


def get_img_size(pic):
    try:
        img = Image.open(pic)
        img_size = img.size
        print(pic, img.size, img.format, img.mode, img.palette, img.info)
        img.close()
    except Exception as e:
        print(pic, "报错:", e)
        # remove_exif(pic)
        # shutil.move(pic, os.path.join(root_dir, os.path.basename(pic)))
        img_size = (0, 0)
    return img_size


def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(string)
        return True
    except (TypeError, ValueError):
        pass

    return False


def classify_dir(dest_dir, size, split_flag="-"):
    dict_dir = {}
    dir_list = os.listdir(dest_dir)
    for item in dir_list:
        fi_d = os.path.join(dest_dir, item)
        if os.path.isdir(fi_d):
            dir_size = item.split(split_flag)[0]
            if is_number(dir_size):
                dict_dir[int(dir_size)] = fi_d
    tuple_dir = sorted(dict_dir.items(), key=lambda d: d[0])
    # for i in range(len(tuple_dir)):
    #     print(tuple_dir[i][0], '--->', tuple_dir[i][1])
    width = size[0]
    for i in range(len(tuple_dir)):
        # print(tuple_dir[i][0], '--->', tuple_dir[i][1])
        if tuple_dir[i][0] <= width < tuple_dir[i + 1][0]:
            return tuple_dir[i][1]
        elif width >= tuple_dir[len(tuple_dir) - 1][0]:
            return tuple_dir[len(tuple_dir) - 1][1]
        i += 1
    return None


def classify_single_picture(dest_dir, pic, img_size):
    dir_1 = classify_dir(dest_dir, img_size)
    if dir_1:
        dir_2 = classify_dir(dir_1, img_size, split_flag="X")
        if dir_2:
            new_pic = os.path.join(dir_2, os.path.basename(pic))
            # 判断目标是否存在
            if not os.path.exists(new_pic):
                shutil.move(pic, new_pic)
                print('%s ------move--------> %s' % (pic, new_pic))


# 遍历filepath下所有文件，包括子目录
def traverse(source_dir, dest_dir):
    file_list = os.listdir(source_dir)
    for item in file_list:
        fi_d = os.path.join(source_dir, item)
        if os.path.isdir(fi_d):
            traverse(fi_d, dest_dir)
        else:
            file = os.path.join(source_dir, fi_d).encode('utf-8').decode('utf-8')
            # if len(os.path.basename(file)) < 32: # 不是md5命名
            #     print(file)
            new_pic = check_pic(file)
            if new_pic != file:
                os.rename(file, new_pic)
            # print(new_pic)
            if new_pic:
                img_size = get_img_size(new_pic)
                classify_single_picture(dest_dir, new_pic, img_size)
            else:
                pass


def main():
    source_dir = os.path.join(root_dir, r"Pictures\Picture\美女\待整理")
    dest_dir = os.path.join(root_dir, r"Pictures\Picture\美女\分辨率分类")
    traverse(source_dir, dest_dir)


if __name__ == "__main__":
    root_dir = os.path.join(r"D:/", os.environ['USERNAME']).replace('/', '\\')
    main()
