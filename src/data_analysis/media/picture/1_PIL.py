#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
# http://blog.csdn.net/chenriwei2/article/details/42071517
# Python-Image 基本的图像处理操作
# Python 里面最常用的图像操作库是Image library（PIL），功能上，虽然还不能跟Matlab比较，但是还是比较强大的。

1. 首先需要导入需要的图像库：
     import Image

2. 读取一张图片：
     im=Image.open('/home/Picture/test.jpg')

3. 显示一张图片：
    im.show()

4. 保存图片：
    im.save("save.gif","GIF")      #保存图像为gif格式

5. 创建新图片：
    Image.new(mode, size)  
    Image.new(mode, size, color)  
    例子：newImg = Image.new("RGBA",(640,480),(0,255,0))
               newImg.save("newImg.png","PNG")

6. 两张图片相加：
    Image.blend(img1, img2, alpha)    # 这里alpha表示img1和img2的比例参数

7. 点操作：
    im.point(function) #,这个function接受一个参数，且对图片中的每一个点执行这个函数
    比如：out = im.point(lambda i : i*1.5)#对每个点进行50%的加强

8. 查看图像信息：
    im.format, im.size, im.mode

9.  图片裁剪：
    box = (100,100,500,500)  #设置要裁剪的区域 
    region = im.crop(box)     #此时，region是一个新的图像对象。

10. 图像黏贴（合并）
    im.paste(region, box)#粘贴box大小的region到原先的图片对象中。 

11. 通道分离：
   r,g,b = im.split()#分割成三个通道  ，此时r,g,b分别为三个图像对象。

12. 通道合并：
   im = Image.merge("RGB", (b, g, r))#将b,r两个通道进行翻转。

13. 改变图像的大小：
   out = img.resize((128, 128))#resize成128*128像素大小

14. 旋转图像：
   out = img.rotate(45) #逆时针旋转45度  
   有更方便的：
   region = region.transpose(Image.ROTATE_180）

15. 图像转换：
   out = im.transpose(Image.FLIP_LEFT_RIGHT)#左右对换。
  out= im.transpose(Image.FLIP_TOP_BOTTOM)#上下对换

16. 图像类型转换：
    im=im.convert("RGBA")

17. 获取某个像素位置的值：
   im.getpixel((4,4))

18.  写某个像素位置的值：
   img.putpixel((4,4),(255,0,0))
————————————————————————————————————————————————
# format : 识别图像的源格式，如果该文件不是从文件中读取的，则被置为 None 值。
# size : 返回的一个元组，有两个元素，其值为象素意义上的宽和高（以像素为单位）。
# mode : RGB（true color image），此外还有，L（luminance），CMTK（pre-press image）。
#         mode 属性定义图像的色彩通道的数量与名字，同时也包括像素的类型和颜色深度信息。
#         通常来说，灰度图像的mode是"L" (luminance)，
#         真彩色图像的mode是 "RGB" （true color image），
#         而用来打印的图像的mode是"CMYK"（pre-press image）。

# UserWarning: image file could not be identified because WEBP support not installed
# https://pillow.readthedocs.io/en/latest/installation.html#external-libraries
'''
import io
import logging
import os

import requests
# 注意：在python3中，请使用from PIL import Image，不要使用import Image
# pip install Pillow
from PIL import Image  # PIL是Python 中最著名的2D 图片处理库。

logging.getLogger("requests").setLevel(logging.WARNING)
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# logging.disable(logging.DEBUG) # 禁用日志
from public_function import GLOBAL_WORK_DIR


# 查看本地图像信息
def test_1():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"test_PIL.jpg")
    # 1.最基本的图像文件读取方式:
    img = Image.open(source_file)

    # 2.类文件读取：
    # fp = open(source_file, "rb")
    # img = Image.open(fp)

    # 3.从归档文件读取：
    # import TarIO
    # fp = TarIo.TarIO("Image.tar", "Image/test/lena.ppm")
    # img = Image.open(fp)

    print(img.format, img.size, img.mode, img.palette, img.info, sep="\n")


# 查看网页图像信息
def test_2():
    url = 'http://www.jb51.net/images/logo.gif'

    # response  = urllib.request.urlopen(url)
    # logging.debug(response)
    # file = response.read()
    # logging.debug(file)

    response = requests.get(url)
    # logging.debug(response)
    file = response.content
    # logging.debug(file)

    tmpImg = io.BytesIO(file)
    img = Image.open(tmpImg)
    print(img.format, img.size, img.mode, img.palette, img.info, sep="\n")


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
