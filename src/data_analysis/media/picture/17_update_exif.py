#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/lijia168/p/6693862.html
# Python 批量修改图片exif属性

import os
import time

# pyexiv2模块于2020年2月发布了2.0版本，API有所变动，请看新教程
# https://github.com/LeoHsiao1/pyexiv2/blob/master/docs/Tutorial-cn.md
import pyexiv2


class Exif():
    def __init__(self, dict_config):
        self.dict_config = dict_config

    def imgSave(self, dirname):
        for filename in os.listdir(dirname):
            path = dirname + filename
            if os.path.isdir(path):
                path += '/'
                self.imgSave(path)
            else:
                self.imgExif(path)

    def imgExif(self, path):
        try:
            if self.dict_config.get("DateTimeOriginal") == "now":
                mytime = time.strftime('%Y:%m:%d %H:%M:%S', time.localtime(time.time()))
            else:
                mytime = self.dict_config.get("DateTimeOriginal")

            exiv_image = pyexiv2.ImageMetadata(path)
            exiv_image.read()
            exiv_image["Exif.Image.Artist"] = self.dict_config.get("Artist")
            exiv_image["Exif.Photo.DateTimeOriginal"] = mytime
            exiv_image["Exif.Image.Software"] = self.dict_config.get("Software")
            exiv_image.write()
            print('图片:', path, '操作成功')
        except Exception as e:
            print('图片:', path, '操作失败', e)

    def star(self):
        path = input('请输入图片路径：')
        # newpath = unicode(path, 'utf8')
        self.imgSave(path + '/')
        self.star()


def main():
    dict_config = {
        "Artist": "www.blmm.com",
        "DateTimeOriginal": "now",
        "Software": "Adobe Photoshop CS6 Windows"
    }
    print('#------------------------------------')
    print('# 程序:批量修改图片exif信息')
    print('#------------------------------------')
    Exif(dict_config).star()


if __name__ == "__main__":
    main()
