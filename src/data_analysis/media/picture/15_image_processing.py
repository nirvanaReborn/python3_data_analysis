#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.cnblogs.com/way_testlife/archive/2011/04/17/2019013.html
# Python 之 使用 PIL 库做图像处理

from PIL import Image, ImageFilter  # PIL是Python 中最著名的2D 图片处理库。

from public_function import dict_choice


def test_1():
    im = Image.open("1.jpg")
    print(im.format, im.size, im.mode)
    # JPEG (440, 330) RGB
    im.show()

    box = (100, 100, 200, 200)
    # crop() : 从图像中提取出某个矩形大小的图像。
    # 它接收一个四元素的元组作为参数，各元素为（left, upper, right, lower），坐标系统的原点（0, 0）是左上角。
    region = im.crop(box)
    region.show()
    region = region.transpose(Image.ROTATE_180)
    region.show()
    im.paste(region, box)
    im.show()


# 旋转一幅图片
def roll(image, delta):
    xsize, ysize = image.size

    delta = delta % xsize
    if delta == 0:
        return image

    part1 = image.crop((0, 0, delta, ysize))
    part2 = image.crop((delta, 0, xsize, ysize))
    image.paste(part2, (0, 0, xsize - delta, ysize))
    image.paste(part1, (xsize - delta, 0, xsize, ysize))

    return image


# 简单的几何变换
def test_2():
    im = Image.open("1.jpg")
    out = im.resize((128, 128))  #
    out = im.rotate(45)  # 逆时针旋转 45 度角。
    out = im.transpose(Image.FLIP_LEFT_RIGHT)  # 左右对换。
    out = im.transpose(Image.FLIP_TOP_BOTTOM)  # 上下对换。
    out = im.transpose(Image.ROTATE_90)  # 旋转 90 度角。
    out = im.transpose(Image.ROTATE_180)  # 旋转 180 度角。
    out = im.transpose(Image.ROTATE_270)  # 旋转 270 度角。


# 色彩空间变换
# convert() : 该函数可以用来将图像转换为不同色彩模式。
def test_3():
    im = Image.open("1.jpg")


# 图像增强
# Filters : 在 ImageFilter 模块中可以使用 filter 函数来使用模块中一系列预定义的增强滤镜。
def test_4():
    im = Image.open("1.jpg")
    imfilter = im.filter(ImageFilter.DETAIL)
    imfilter.show()


# 序列图像
# 即我们常见到的动态图，最常见的后缀为 .gif ，另外还有 FLI / FLC 。PIL 库对这种动画格式图也提供了一些基本的支持。
# 当我们打开这类图像文件时，PIL 自动载入图像的第一帧。我们可以使用 seek 和 tell 方法在各帧之间移动。
def test_5():
    im = Image.open("9.gif")
    im.seek(1)  # skip to the second frame

    try:
        while 1:
            im.seek(im.tell() + 1)
            # do something to im
    except EOFError:
        pass


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
