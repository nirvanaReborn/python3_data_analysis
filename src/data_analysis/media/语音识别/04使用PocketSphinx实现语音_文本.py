'''
https://blog.csdn.net/jiaweide123/article/details/118655470
https://blog.csdn.net/liuhuanheng/article/details/96379147
python安装pocketsphinx模块（包）时报错：command ‘swig.exe‘ failed: No such file or directory的解决方法：
报错的原因是电脑中缺少swig这个东西，所以要下载并安装。
第一步：下载符合系统的swig
第二步：把swig.exe拷贝到python的安装文件目录下 （例如：C:/python373）
第三步：打开swigwin-3.0.12/Lib 文件夹，把所有带swg后缀名的文件（*.swg）拷贝到python的安装文件目录下的lib文件目录中 （ C:/python373/lib）
第四步：打开swigwin-3.0.12/lib/python文件目录， 拷贝所有文件到python的安装文件目录下的lib文件目录中  (C:/python373/lib)
第五步：打开swigwin-3.0.12/lib/文件目录， 拷贝typemaps文件夹到python的安装文件目录下的lib文件目录中  (C:/python373/lib)

注意：
□ 安装完speech_recognition 之后是不支持中文的，需要在Sphinx 语音识别工具包里
   面下载对应的普通话升学和语言模型。下载地址:
   https://sourceforge.net/projects/cmusphinx/files/Acoustic%20and%20Language%20Models/
□ 将下载好的普通话升学和语言模型放到安装speech_recognition 模块的pocketsphinx-data 目录下

PocketSphinx 是一个用于语音转换文本的开源API。它是一个轻量级的语音识别引擎，
尽管在桌面端也能很好地工作，它还专门为手机和移动设备做过调优。

https://blog.csdn.net/qq_32643313/article/details/99936268


'''
# pip install PocketSphinx
# pip install SpeechRecognition
import speech_recognition as sr

audio_file = 'demo_audio.wav'
r = sr.Recognizer()
# 打开语音文件
with sr.AudioFile(audio_file) as source:
    audio = r.record(source)

try:
    # 将语音转换为文本
    # print('文本内容：', r.recognize_sphinx(audio))  # 默认识别英文
    print('文本内容：', r.recognize_sphinx(audio, language="zh-cn"))
except Exception as e:
    print(e)
