#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 判断一个文件是否是音频格式
# https://blog.csdn.net/jy692405180/article/details/63262680
'''whathdr函数返回元组类型的数据包括5个属性：filetype、framerate、nchannels、nframes和sampwidth。
filetype代表音频格式。值为：’aifc’, ‘aiff’, ‘au’, ‘hcom’, ‘sndr’, ‘sndt’, ‘voc’, ‘wav’, ‘8svx’, ‘sb’, ‘ub’, ‘ul’或者None中之一。常见的几个格式反倒不支持。
framerate代表音频文件的帧率。如果音频文件难以解码或者未知，该值会返回0。
nchannels代表通道数。如果音频文件难以解码或者未知，该值会返回0。
nframes代表帧数。如果无法确定则返回-1。
sampwidth代表返回样本的长度（比特），值为8的倍数，或者返回A（A-LAW格式）、u（u-LAW格式）。
'''
import sndhdr

from public_function import GLOBAL_MP3_FILE as source_file


def main():
    audio = sndhdr.what(source_file)  # 无法检测，返回None
    if audio == None:
        print("不是音频文件或音频已损坏", source_file)
    else:
        filetype, framerate, nchannels, nframes, sampwidth = audio
        print(audio)


if __name__ == "__main__":
    main()
