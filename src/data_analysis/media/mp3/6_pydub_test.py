#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/baidu_29198395/article/details/86694365
# 最好用的python音频库之一：pydub的中文文档（含API）

# https://github.com/jiaaro/pydub
# https://zhuanlan.zhihu.com/p/91257681
# pydub直接支持wav和raw格式音频读取，如果需要处理其它格式音频（如MP3，ogg等）需要安装ffmpeg或avconv。

import os
import sys
# 运行报错：RuntimeWarning: Couldn't find ffmpeg or avconv - defaulting to ffmpeg, but may not work
# 【解决办法】https://www.pianshen.com/article/5739874938/
# pip install pydub
import pydub
from pydub.playback import play

from public_function import IMAGEIO_FFMPEG_EXE
from public_function import GLOBAL_MP3_FILE as source_file

# https://www.it1352.com/1828731.html
if os.name == "posix":  # linux系统
    pydub.AudioSegment.converter = os.path.expanduser(r"~/.imageio/ffmpeg/ffmpeg")
    pydub.AudioSegment.ffmpeg = IMAGEIO_FFMPEG_EXE
    pydub.AudioSegment.ffprobe = os.path.join(os.path.basename(IMAGEIO_FFMPEG_EXE), "ffprobe.exe")
elif os.name == "nt":  # windows系统
    # pydub.AudioSegment.converter = os.path.expanduser(r"~\AppData\Local\imageio\ffmpeg\ffmpeg.win32.exe")
    pydub.AudioSegment.converter = IMAGEIO_FFMPEG_EXE
    pydub.AudioSegment.ffmpeg = IMAGEIO_FFMPEG_EXE
    pydub.AudioSegment.ffprobe = os.path.join(os.path.basename(IMAGEIO_FFMPEG_EXE), "ffprobe.exe")
else:
    pydub.AudioSegment.converter = ""
    print("未知系统:", os.name)


def test_1():
    # 打开一个MP3文件：
    song = pydub.AudioSegment.from_mp3(source_file)
    play(song)  # PermissionError: [Errno 13] Permission denied:
    # 打开一个WAV文件：
    # song = pydub.AudioSegment.from_wav("never_gonna_give_you_up.wav")
    # ogg_version = pydub.AudioSegment.from_ogg("never_gonna_give_you_up.ogg")
    # flv_version = pydub.AudioSegment.from_flv("never_gonna_give_you_up.flv")
    # mp4_version = pydub.AudioSegment.from_file("never_gonna_give_you_up.mp4", "mp4")
    # wma_version = pydub.AudioSegment.from_file("never_gonna_give_you_up.wma", "wma")
    # aac_version = pydub.AudioSegment.from_file("never_gonna_give_you_up.aiff", "aac")


# 对音频段切片
def test_2():
    dest_file = sys._getframe().f_code.co_name + ".mp3"
    # 打开一个MP3文件：
    song = pydub.AudioSegment.from_mp3(source_file)

    # pydub做任何操作的时间尺度都是毫秒
    ten_seconds = 10 * 1000

    first_10_seconds = song[:ten_seconds]
    last_5_seconds = song[-5 * 1000:]

    # 使开头十秒的声音变得更响并使结束的五秒声音变弱：
    # 声音增益6dB
    beginning = first_10_seconds + 6
    # 声音减弱3dB
    end = last_5_seconds - 3

    # 连两个接音频段(把一个文件接在另一个后面)
    without_the_middle = beginning + end
    # without_the_middle.duration_seconds == 15.0

    # 音频不可以被修改
    backwards = song.reverse()

    # 交叉淡化（再一次强调，beginning和end都是不可变的）
    # 1.5秒的淡入淡出
    with_style = beginning.append(end, crossfade=1500)

    # 将片段重复两遍
    do_it_over = with_style * 2

    # 2秒淡入, 3秒淡出
    awesome = do_it_over.fade_in(2000).fade_out(3000)

    # 保存编辑的结果（再说一下，支持所有ffmpeg支持的格式）
    awesome.export(dest_file, format="mp3")

    # 保存带有标签的结果（元数据）
    # awesome.export(dest_file, format="mp3", tags={'artist': 'Various artists', 'album': 'Best of 2011', 'comments': 'This album is awesome!'})

    # 你也可以通过指定任意ffmpeg支持的比特率来导出你的结果
    # awesome.export(dest_file, format="mp3", bitrate="192k")

    # 更多其他的ffmpeg所支持的参数可以通过给’parameters’参数传递一个列表来实现
    # 特别注意一下，这些参数没有得到确认，支持的参数可能会受限于你所使用的特定的 ffmpeg / avlib 构建

    # 使用预设MP3质量0(相当于lame -V0)
    # ——lame是个MP3编码器，-V设置的是VBR压缩级别,品质从0到9依次递减（译者注）
    # awesome.export(dest_file, format="mp3", parameters=["-q:a", "0"])

    # 混合到双声道并设置输出音量百分比（放大为原来的150%）
    # awesome.export(dest_file, format="mp3", parameters=["-ac", "2", "-vol", "150"])


def test_3():
    file_1 = r"D:\%s\Music\Music\音乐专辑\纯音乐\古筝\古筝.mp3" % os.environ['USERNAME']
    file_2 = r"D:\%s\Music\Music\歌曲\DJ\大家一起来 - 孙悦.mp3" % os.environ['USERNAME']

    # 加载MP3文件
    song1 = pydub.AudioSegment.from_file(file_1, format="mp3")  # 默认mp3格式
    song2 = pydub.AudioSegment.from_mp3(file_2)

    # 取得两个MP3文件的声音分贝
    db1 = song1.dBFS
    db2 = song2.dBFS

    # pydub做任何操作的时间尺度都是毫秒
    song1 = song1[300:]  # 从300ms开始截取英文MP3

    # 调整两个MP3的声音大小，防止出现一个声音大一个声音小的情况
    dbplus = db1 - db2
    if dbplus < 0:  # song1的声音更小
        song1 += abs(dbplus)
    elif dbplus > 0:  # song2的声音更小
        song2 += abs(dbplus)

    # 拼接两个音频文件
    song = song1 + song2

    # 导出音频文件
    dest_file = sys._getframe().f_code.co_name + ".mp3"
    song.export(dest_file, format="mp3")  # 导出为MP3格式


# 百度短语音识别接口支持1分钟以内的音频，这时候需要对音频进行分割处理
def test_4():
    from pydub import AudioSegment
    from pydub.utils import make_chunks
    list_chunk_name = []
    audio_file = r"D:\ARESHelper\思勰LDP沟通.wav"
    audio = AudioSegment.from_file(audio_file, "wav")
    size = 25 * 60 * 1000  # 每块切割的毫秒数，即25min
    chunks = make_chunks(audio, size)
    filepath, basename = os.path.split(audio_file)
    shotname, extension = os.path.splitext(basename)
    output_dir = filepath + os.sep + shotname
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)  # 创建目录
    for i, chunk in enumerate(chunks):
        # 枚举，i是索引，chunk是切割好的文件
        chunk_name = output_dir + os.sep + shotname + "_{0}.wav".format(i)
        print(chunk_name)
        list_chunk_name.append(chunk_name)
        # 保存文件
        chunk.export(chunk_name, format="wav")
    return list_chunk_name


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
