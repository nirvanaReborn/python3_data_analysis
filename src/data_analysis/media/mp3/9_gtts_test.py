#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 没学python之前，我们可能会想到讯飞语音，只记得其听音阁确实厉害。
# 在代码编写方面，我简单查了下，百度、讯飞的语音转换API都是收费的，因此无视体验。
# 谷歌gTTS可以直接拿来用，而且python仓库有这个项目。因此我们直接拿来使用就行。
# gTTS 使用 translate.google.com 翻译API ：需要科学上网
# gtts.tts.gTTSError: Connection error during token calculation: HTTPSConnectionPool(host='translate.google.com', port=443)

import sys

# pip install gTTS (谷歌文字转语音api)
from gtts import gTTS


def test_1():
    dest_file = sys._getframe().f_code.co_name + ".mp3"
    txt2speech = gTTS(text="Hello Python", lang="en")
    txt2speech.save(dest_file)


# https://baiyue.one/archives/1520.html
def test_2():
    dest_file = sys._getframe().f_code.co_name + ".mp3"
    # demo.txt 内容不限制语言，支持英语、中文、法语等等。
    with open("demo.txt", 'r', encoding="utf8") as f:
        audio = gTTS(text=f.read(), lang="zh-cn")
        audio.save(dest_file)


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
