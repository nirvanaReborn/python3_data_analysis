#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qq_35164554/article/details/105846824
# 这是一个文字转语音的python模块
'''
# 语音引擎工厂
# 类似于设计模式中的“工厂模式”，pyttsx3通过初始化来获取语音引擎。
# 当我们第一次调用init操作的时候，会返回一个pyttsx3的engine对象，
# 再次调用的时候，如果存在engine对象实例，就会使用现有的，否则再重新创建一个。
pyttsx.init([driverName : string, debug : bool]) → pyttsx.Engine
    从方法声明上来看，第一个参数指定的是语音驱动的名称，这个在底层适合操作系统密切相关的。如下：
        1.drivename：由pyttsx3.driver模块根据操作系统类型来调用，默认使用当前操作系统可以使用的最好的驱动。
            sapi5 - SAPI5 on Windows
            nsss - NSSpeechSynthesizer on Mac OS X
            espeak - eSpeak on every other platform
        2.debug: 这第二个参数是指定要不要以调试状态输出，建议开发阶段设置为True。

'''
import pyttsx3


def test_1():
    # 模块初始化
    engine = pyttsx3.init()
    print('准备开始语音播报...')
    engine.say('我高冷，我并不想说话～')
    # 等待语音播报完毕
    engine.runAndWait()
    engine.stop()


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
