#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# https://blog.csdn.net/royzdr/article/details/84950311
# python的Mutagen模块入门

# https://mutagen.readthedocs.io/en/latest/
# pip install mutagen
import mutagen
from mutagen.mp3 import MP3
from mutagen.id3 import ID3
from mutagen.easyid3 import EasyID3
from public_function import GLOBAL_MP3_FILE as source_file


# http://www.jb51.net/article/67834.htm
def test_1():
    def getID3(filename):
        fp = open(filename, 'rb')
        fp.seek(-128, 2)
        fp.read(3)  # TAG iniziale
        title = fp.read(30).decode('gb2312').replace("\x00", "").strip()
        artist = fp.read(30).decode('gb2312').replace("\x00", "").strip()
        album = fp.read(30).decode('gb2312').replace("\x00", "").strip()
        anno = fp.read(4).decode('gb2312').replace("\x00", "").strip()
        comment = fp.read(28).decode('gb2312').replace("\x00", "").strip()
        fp.close()
        return {'title': title, 'artist': artist, 'album': album, 'anno': anno, 'comment': comment}

    print(getID3(source_file))


# http://www.oschina.net/code/snippet_16840_1623
def test_2():
    EasyID3.valid_keys["comment"] = "COMM::'XXX'"
    id3info = mutagen.mp3.MP3(source_file, ID3=EasyID3)
    for k, v in id3info.items():
        print(k, v)


# https://www.jianshu.com/p/ed39a9ffea8e
def test_3():
    from mutagen.id3 import ID3, APIC, TIT2, TPE1, TALB

    # 传入mp3、jpg的本地路径以及其他字符串
    def setSongInfo(songfilepath, songtitle, songartist, songalbum, songpicpath):
        audio = ID3(songfilepath)
        img = open(songpicpath, 'r')
        audio.update_to_v23()  # 把可能存在的旧版本升级为2.3
        audio['APIC'] = APIC(  # 插入专辑图片
            encoding=3,
            mime='image/jpeg',
            type=3,
            desc=u'Cover',
            data=img.read()
        )
        audio['TIT2'] = TIT2(  # 插入歌名
            encoding=3,
            text=[songtitle]
        )
        audio['TPE1'] = TPE1(  # 插入第一演奏家、歌手、等
            encoding=3,
            text=[songartist]
        )
        audio['TALB'] = TALB(  # 插入专辑名称
            encoding=3,
            text=[songalbum]
        )
        audio.save()  # 记得要保存
        img.close()


def test_4():
    print(mutagen.File(source_file))


def test_5():
    audio = mutagen.mp3.MP3(source_file)
    print(audio.info.length)  # 获得mp3文件的长度
    print(audio.info.bitrate)  # 获得mp3文件的比特率
    print(audio.info.pprint())


# 删除mp3文件的id3标签
def test_6():
    audio = mutagen.id3.ID3(source_file)
    audio.delete()


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
