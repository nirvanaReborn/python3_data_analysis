#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.cnblogs.com/pcode/archive/2013/04/01/2992690.html

'''
# http://blog.csdn.net/kevinelstri/article/details/61921812
# 使用python读取数据科学最常用的文件格式
MP3文件格式来自于多媒体文件格式，多媒体文件格式类似于图像文件格式，但它们恰好都是最复杂的文件格式。
在多媒体文件格式中，您可以存储各种数据，如文字图像，图形，视频和音频数据。
例如，一个多媒体格式可以允许文本被存储为富文本格式（RTF）的数据而非ASCII数据，这是一个纯文本格式。
MP3是一种最常见的音频编码格式的数字音频。一个MP3文件格式采用的MPEG-1编码格式为视频和音频压缩标准。
在有损压缩中，一旦压缩原始文件，则无法恢复原始数据。
一个MP3文件格式过滤掉人类无法听到的声音，压缩了音频的质量。
MP3压缩通常减小达到75%至95%，从而节省了大量的空间。
一个MP3文件有许多框架。框架可以进一步分为标题和数据块。我们称这些序列的框架是基本流。
在MP3的头部，找出有效的框架和一个数据块的开始包含（压缩）在频率和振幅的音频信息。
下面是MP3文件结构：http://download.csdn.net/detail/kevinelstri/9779577
'''
import getpass
import os
import struct


def GetFiles(path):
    """
    读取指定目录的文件
    """
    FileDic = []
    files = []
    for source_file in os.listdir(path):
        suffix = os.path.splitext(source_file)[1]
        if suffix == ".mp3":
            file_name = source_file[:-4]
            FileDic.append(file_name)
            files.append(source_file)
    return FileDic, files


def _GetLast128K(path, file):
    ff1 = open(os.path.join(path, file), "rb")
    ff1.seek(-128, 2)
    id3v1data = ff1.read()
    ff1.close()
    return id3v1data


def _GetAllBinData(path, file):
    ff1 = open(os.path.join(path, file), "rb")
    data = ff1.read()
    ff1.close()
    return data


def SetTag(path, file, title, artist, album, year, comment, genre):
    """
    设置mp3的ID3 v1中的部分参数
    char Header[3]; /*标签头必须是"TAG"否则认为没有标签*/
    char Title[30]; /*标题*/
    char Artist[30]; /*作者*/
    char Album[30]; /*专集*/
    char Year[4]; /*出品年代*/
    char Comment[30]; /*备注*/
    char Genre; /*类型*/
    mp3文件尾部128字节为id3v1的数据，如果有数据则读取修改，无数据则补充
    """
    header = 'TAG'  # 组合出最后128K的id3V1的数据内容
    str = struct.pack('3s30s30s30s4s30ss', header, title, artist, album, year, comment, genre)  # 获取原始全部数据
    print(str)
    # data=_GetAllBinData(path,file)#获取末尾的128字节数据
    # print(data)
    input()
    # id3v1data=_GetLast128K(path,file)#打开原文件准备写入
    # print(id3v1data)
    # ff=open(os.path.join(path,file),"wb")
    # try:#判断是否有id3v1数据
    #     if id3v1data[0:3]!=header:#倒数128字节不是以TAG开头的说明没有#按照id3v1的结构补充上去
    #         ff.write(data+str)
    #     else:#有的情况下要换一下
    #         ff.write(data[0:-128]+str)
    #     ff.close()
    #     print("OK"+title)
    # except:
    #     ff.write(data)
    #     print("Error "+title)
    # finally:
    #     if ff :
    #         ff.close()


def main():
    # 我存放mp3文件的目录
    from public_function import GLOBAL_WORK_DIR as source_dir
    names, files = GetFiles(source_dir)  # 苦力代码
    print(names, files)

    for i in range(len(files)):  # 注意编码解码
        title = names[i].encode('gbk')
        artist = u'梁冬 吴伯凡'.encode('gbk')
        album = u'东吴相对论'.encode('gbk')
        year = ''
        comment = ''
        genre = ''
        # 调用函数处理
        SetTag(source_dir, files[i], title, artist, album, year, comment, genre)


if __name__ == "__main__":
    main()
