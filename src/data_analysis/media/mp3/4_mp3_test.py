#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://blog.sina.com.cn/s/blog_80ab598b0102vbao.html
# Python3 下 MP3格式文件的ID3v2标签与压缩信息读取

from public_function import GLOBAL_MP3_FILE as source_file


# 获取MP3的信息，包括比特率，取样率，等等。
class MP3Info:
    MP3Version = {0b00: 'MPEG 2.5', 0b01: 'UNDEFINED', 0b10: 'MPEG 2', 0b11: 'MPEG 1'}
    MP3Layer = {0b00: 'UNDEFINED', 0b01: 'Layer 3', 0b10: 'Layer 2', 0b11: 'Layer 1'}
    MP3CRC = {0b0: '校检', 0b1: '非校检'}
    MP3Bitrate = {0b0000: 'free',
                  0b0001: 32,
                  0b0010: 40,
                  0b0011: 48,
                  0b0100: 56,
                  0b0101: 64,
                  0b0110: 80,
                  0b0111: 96,
                  0b1000: 112,
                  0b1001: 128,
                  0b1010: 160,
                  0b1011: 192,
                  0b1100: 224,
                  0b1101: 256,
                  0b1110: 320,
                  0b1111: 'bad'}
    MP3Samp_freq = {0b00: 441000, 0b01: 48000, 0b10: 32000, 0b11: 'UNdefined'}
    MP3Frame_mod = {0: '无需调整', 1: '调整'}
    MP3Trackmod = {0b00: '立体声', 0b01: '联合立体声', 0b10: '双声道', 0b11: '单声道'}
    MP3Copyright = {0b0: '不合法', 0b1: '合法'}
    MP3Original = {0b0: '非原版', 0b1: '原版'}

    def __init__(self, file):
        self.name = file
        try:
            self.file = open(file, 'rb')

        except IOError as msg:
            print('{0:s} open Error! {1:s}'.format(self.fname, msg))
            return

        if self.file.read(3) == b'ID3':
            HDR = self.file.read(7)
            tagsz = HDR[-1] + HDR[-2] * 0x80 + HDR[-3] * 0x4000 + HDR[-4] * 0x200000 + 10
            self.file.seek(tagsz, 0)
        else:
            self.file.seek(0)

        framehdr = self.file.read(4)
        vbrinfo = self.file.read(32)

        self.file.close()

        self.d = {}

        self.version = self.MP3Version[(framehdr[1] & 0b00011000) >> 3] + ' - ' + self.MP3Layer[(framehdr[1] & 0b00000110) >> 1]
        self.bitrate = self.MP3Bitrate[framehdr[2] >> 4]
        self.sample_freq = self.MP3Samp_freq[(framehdr[2] & 0b00001100) >> 2]
        self.padding = (framehdr[2] & 0b00000010) >> 1
        self.frame_mod = self.MP3Frame_mod[self.padding]
        self.trackmod = self.MP3Trackmod[framehdr[3] >> 6]
        self.copyright = self.MP3Copyright[(framehdr[3] & 0b00001000) >> 3]
        self.original = self.MP3Original[(framehdr[3] & 0b00000100) >> 2]

        if self.version: self.d['Version'] = self.version
        if self.bitrate: self.d['Bitrate'] = self.bitrate
        if self.sample_freq: self.d['Sample Frequency'] = self.sample_freq
        if self.frame_mod: self.d['Frame Mode'] = self.frame_mod
        if self.trackmod: self.d['Track Mode'] = self.trackmod
        if self.copyright: self.d['Copyright'] = self.copyright
        if self.original: self.d['Original'] = self.original


# 获取MP3的信息包含id3v2信息
class MP3:
    def __init__(self, file, name='unknown'):
        self.name = file

        self.artist = ''
        self.title = ''
        self.album = ''

        self.bitrate = ''
        self.trackmod = ''
        self.sample_freq = ''
        self.d = {}

        try:
            mp3info = MP3Info(file)

            self.bitrate = mp3info.bitrate
            self.trackmod = mp3info.trackmod
            self.sample_freq = mp3info.sample_freq

            id3 = ID3(file)

            self.artist = id3.artist
            self.title = id3.title
            self.album = id3.album
            self.d = dict(mp3info.d, **id3.d)

        except NoID3v2Error as msg:
            self.d = mp3info.d
            print(msg)


##        except :
##            print('Some Error!')
##


def trip_space(s):
    while len(s) > 0 and s[-1] == '\x00':
        s = s[:-1]
    ##    while len(s) > 0 and s[:2]  == b'\x00\x00':
    ##        s = s[2:]

    return s


class NoID3v2Error(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


# Module for manipulating ID3 informational tags in MP3 audio files
# 鉴于现在适用与python3的ID3软件包很少，遂自己写了一个。现在网上的mp3一般都有ID3v2标签，
# 本模块功能就是读取mp3文件的ID3v2标签，把标签记录在一个集合里面。
# 特别的，对于最常见的歌手，曲名和专辑名，可以通过一些内部属性直接调用访问。
class ID3:
    def __init__(self, file):
        self.fname = file
        try:
            self.file = open(file, 'rb')

        except IOError as msg:
            print('{0:s} open Error! {1:s}'.format(self.fname, msg))
            return

        if self.file.read(3) != b'ID3':
            # print('No ID3v2 Tag')
            raise NoID3v2Error('No ID3v2 Tag')
            self.file.close()
            return

        self.d = {}
        self.tagversion = 0
        self.tagsize = 0
        self.haspic = False
        self.artist = ''
        self.title = ''
        self.album = ''

        HDR = self.file.read(7)

        self.tagv = 'v%d.%d' % (2, HDR[0])
        self.tagsize = HDR[-1] + HDR[-2] * 0x80 + HDR[-3] * 0x4000 + HDR[-4] * 0x200000 + 10
        # print (self.tagsize)

        while True:
            a = self.file.read(1)
            if a == b'\x00' or a == b'\xff': break

            self.file.seek(-1, 1)
            fhdr = self.file.read(10)  # ;print (fhdr)
            sz = fhdr[-3] + fhdr[-4] * 0x100 + fhdr[-5] * 0x10000 + fhdr[-6] * 0x1000000
            kind = fhdr[0:4].decode()

            if kind != 'APIC':
                info = self.file.read(sz)  # ;print (info)

                try:
                    st = info.rfind(b'\xff\xfe')
                    if st != -1:  # \x01\xff\xfe.....\xff\xfe
                        self.d[kind] = trip_space(info[st + 2:].decode('utf16'))

                    elif info.startswith(b'\x03'):
                        self.d[kind] = trip_space(info[1:].decode())

                    else:  # \x00
                        self.d[kind] = info[1:-1].replace(b'\x00', b'\x20').decode('gbk')

                except UnicodeDecodeError as msg:
                    # print('Decode Error @%s, Content is %s\nMsg:%s'%(kind,info, msg))
                    pass

            else:
                self.haspic = True
                break
                # self.file.seek(sz,1)

                # print(self.file.tell())

        if 'TPE1' in self.d.keys(): self.artist = self.d['TPE1']
        if 'TIT2' in self.d.keys(): self.title = self.d['TIT2']
        if 'TALB' in self.d.keys(): self.album = self.d['TALB']

        self.file.close()


def test_ID3v2(file):
    a = ID3(file)
    print(a.d)
    print(a.artist, a.title, a.album)


def test_MP3(file):
    a = MP3(file)
    print(a.d)
    print(a.artist, a.title, a.bitrate)


def test_MP3Info(file):
    a = MP3Info(file)
    print(a.d)
    print(a.bitrate)


def main():
    test_ID3v2(source_file)
    test_MP3(source_file)
    test_MP3Info(source_file)


if __name__ == "__main__":
    main()
