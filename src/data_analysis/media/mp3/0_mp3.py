#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 用来操作音频的库
# audiolazy：Python 的数字信号处理包。
# audioread：交叉库 (GStreamer + Core Audio + MAD + FFmpeg) 音频解码。
# beets：一个音乐库管理工具及 MusicBrainz 标签添加工具。
# dejavu：音频指纹提取和识别。
# django-elastic-transcoder：Django + Amazon Elastic Transcoder。
# eyeD3：一个用来操作音频文件的工具，具体来讲就是包含 ID3 元信息的 MP3 文件。
# id3reader：一个用来读取 MP3 元数据的 Python 模块。
# m3u8：一个用来解析 m3u8 文件的模块。
# mutagen：一个用来处理音频元数据的 Python 模块。
# pydub：通过简单、简洁的高层接口来操作音频文件。
# pyechonest：Echo Nest API 的 Python 客户端。
# talkbox：一个用来处理演讲/信号的 Python 库。
# TimeSide：开源 web 音频处理框架。
# tinytag：一个用来读取 MP3, OGG, FLAC 以及 Wave 文件音乐元数据的库。
# mingus：一个高级音乐理论和曲谱包，支持 MIDI 文件和回放功能。

# https://www.questarter.com/q/how-to-play-mp3-27_36347786.html
# 如何播放MP3
import getpass


def test_1(source_file):
    import subprocess

    sound_program = r"C:\Program Files (x86)\Windows Media Player\wmplayer.exe"  # Windows Media Player
    # sound_program = r"D:\ProgramFiles\1_FreeInstallation\vlc-3.0.7\vlc.exe"
    subprocess.call([sound_program, source_file])


def test_2(source_file):
    # webbrowser：是Python 自带的，打开浏览器获取指定页面。
    import webbrowser
    webbrowser.open(source_file)


# https://www.cnblogs.com/XingzhiDai/p/11654484.html
# 安装后需要手工修改windows.py 中的 class _mci 两处地方
# https://blog.csdn.net/qq_39039017/article/details/80100322?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param
# mp3play遇到的一个不友好的问题是，它要加上time.sleep(), 才能听到音乐，而且音乐时长是由自己定的.
def test_3(source_file):
    # pip install  mp3play
    import mp3play
    import time

    clip = mp3play.load(source_file)
    clip.play()
    duration = clip.milliseconds()  # 返回mp3文件共多少毫秒，注意这里的单位是毫秒
    # 如果mp3文件的长度小于30少时，全部播放完，否则仅播放30秒。
    time.sleep(min(30, clip.seconds()))
    clip.stop()


# https://www.zhihu.com/question/263989196
def test_4(source_file):
    import pygame  # pygame 1.9.6

    pygame.init()
    pygame.mixer.init()
    screen = pygame.display.set_mode([640, 480])
    pygame.time.delay(1000)
    # pygame.mixer.music.load(source_file)
    # pygame.mixer.music.set_volume(0.2)  # 设置音量为 0.2
    # pygame.mixer.music.play()
    pygame.mixer.Sound(source_file).play()
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
    pygame.quit()


def test_5(source_file):
    # pip install pyglet
    import pyglet
    # Download installed here Link to AVbin(http://avbin.github.io/AVbin/Download.html) setup ( it's a must )

    music = pyglet.media.load(source_file)
    music.play()
    pyglet.app.run()


def test_6(source_file):
    # https://zhuanlan.zhihu.com/p/138073103
    import simpleaudio

    wave_obj = simpleaudio.WaveObject.from_wave_file(source_file)
    play_obj = wave_obj.play()
    play_obj.wait_done()


def test_7(source_file):
    import os
    # 设置VLC库路径，需在import vlc之前
    os.environ['PYTHON_VLC_MODULE_PATH'] = r"D:\ProgramFiles\1_FreeInstallation\vlc\vlc-3.0.11"
    # pip install python-vlc
    import vlc  # OSError: [WinError 126] 找不到指定的模块。
    # 下载VLC库：http://download.videolan.org/pub/videolan/vlc/last/win64/

    p = vlc.MediaPlayer(source_file)
    p.play()
    p.stop()


def test_8(source_file):
    pass


# 从列表中随机选择一首音乐播放
def test_9(music_dir):
    import random, os
    songs = os.listdir(music_dir)
    for item in songs[:]:
        if not str(item).endswith('.mp3'):
            songs.remove(item)
    index = random.randint(0, len(songs))
    print(songs[index])  ## Prints The Song Name
    # 需要注意的是 os.startfile 仅支持 Windows 系统。
    os.startfile(os.path.join(music_dir, songs[0]))


def main():
    source_file = r"D:\%s\Music\江上清风游 - 变奏的梦想.mp3" % getpass.getuser()
    dict_choice = {}
    for i in range(0, 100):
        if i == 9:
            source_file = r"D:\%s\Music" % getpass.getuser()
        dict_choice[str(i)] = "test_" + str(i) + "(source_file)"

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
