#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
OpenCV 的全称是Open Source Computer Vision Library，是一个跨平台的计算机视觉库。
OpenCV 是由英特尔公司发起并参与开发，以BSD 许可证授权发行，可以在商业和研究领域中免费使用。
OpenCV 用C++语言编写，它的主要接口也是C++语言，但是依然保留了大量的C 语言接口。
该库也有大量的Python、Java and MATLAB/OCTAVE（版本2.5）的接口。
官方文档：https://opencv-python-tutroals.readthedocs.io/en/latest/
项目地址：https://github.com/opencv/opencv-python
'''
# pip install opencv-python
# pip install opencv-contrib-python
import cv2 as cv

# 读取图片
img = cv.imread('lena.jpg', cv.IMREAD_UNCHANGED)  # 路径中不能有中文，否则加载图片失败
print(type(img), img.dtype)
# 显示图片
cv.imshow('read_img', img)
# 等待键盘输入 单位毫秒  传入0 则就是无限等待
cv.waitKey(3000)
# 释放内存  由于OpenCV底层是C++编写的
cv.destroyAllWindows()
