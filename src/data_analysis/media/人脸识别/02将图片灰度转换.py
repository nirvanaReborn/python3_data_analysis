#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
OpenCV 中有数百种关于在不同色彩空间之间转换的方法。
当前，在计算机视觉中有三种常用的色彩空间：灰度、BGR、以及HSV（Hue，Saturation，Value）。
（1）灰度色彩空间是通过去除彩色信息来将其转换成灰阶，灰度色彩空间对中间处理特别有效，比如人脸识别。
（2）BGR 及蓝、绿、红色彩空间，每一个像素点都由一个三元数组来表示，分别代表蓝、绿、红三种颜色。
网页开发者可能熟悉另一个与之相似的颜色空间：RGB 它们只是颜色顺序上不同。
（3）HSV，H（Hue）是色调，S（Saturation）是饱和度，V（Value）表示黑暗的程度（或光谱另一端的明亮程度）。

https://www.cnblogs.com/silence-cho/p/10926248.html
1、对于只有黑白颜色的灰度图，为单通道，一个像素块对应矩阵中一个数字，
   数值为0到255, 其中0表示最暗（黑色） ，255表示最亮（白色）
2、对于采用RGB模式的彩色图片，为三通道图，Red、Green、Blue三原色，按不同比例相加，
   一个像素块对应矩阵中的一个向量, 如[24,180, 50]，分别表示三种颜色的比列, 即对应深度上的数字。
   需要注意的是，由于历史遗留问题，opencv采用BGR模式，而不是RGB。

灰度转换的作用就是：转换成灰度的图片的计算强度得以降低。
'''
import cv2 as cv

img = cv.imread('lena.jpg')
cv.imshow('BGR_img', img)

# cv2 读取图片的通道是BGR（蓝绿红）
# PIL 读取图片的通道是RGB
# 将图片灰度转换
gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv.imshow('gray_img', gray_img)
# 保存图片
cv.imwrite('gray_lena.jpg', gray_img)
# 等待键盘输入 单位毫秒  传入0 则就是无限等待
cv.waitKey(0)
# 释放内存  由于OpenCV底层是C++编写的
cv.destroyAllWindows()
