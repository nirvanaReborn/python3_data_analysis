'''
Haar 级联的概念
摄影作品可能包含很多令人愉悦的细节。但是，由于灯光、视角、视距、摄像头抖动以及数字噪声的变化，
图像细节变得不稳定。人们在分类时不会受这些物理细节方面差异的影响。
以前学过，在显微镜下没有两片看起来很像的雪花。
幸运的是，作者生长在加拿大，已经学会如何不用显微镜来识别雪花。
因此，提取出图像的细节对产生稳定分类结果和跟踪结果很有用。
这些提取的结果被称为特征，专业的表述为：从图像数据中提取特征。
虽然任意像素都可以能影响多个特征，但特征应该比像素少得多。
两个图像的相似程度可以通过它们对应特征的欧氏距离来度量。
Haar 特征是一种用于实现实时人脸跟踪的特征。
每一个Haar 特征都描述了相邻图像区域的对比模式。
例如，边、顶点和细线都能生成具有判别性的特征。

获取Haar 级联数据
首先我们要进入OpenCV 官网：https://opencv.org/releases 下载你需要的版本。
然后双击下载的文件，进行安装，实质就是解压一下，解压完出来一个文件夹，其他什么也没发生。
安装完后的目录结构如下。其中build 是OpenCV 使用时要用到的一些库文件，
而sources 中则是OpenCV 官方为我们提供的一些demo示例源码。
在sources 的一个文件夹data/haarcascades。该文件夹包含了所有OpenCV 的人脸检测的XML 文件，
这些可用于检测静止图像、视频和摄像头所得到图像中的人脸。
人脸检测器（默认）：haarcascade_frontalface_default.xml
人脸检测器（快速Harr）：haarcascade_frontalface_alt2.xml
人脸检测器（侧视）：haarcascade_profileface.xml
眼部检测器（左眼）：haarcascade_lefteye_2splits.xml
眼部检测器（右眼）：haarcascade_righteye_2splits.xml
嘴部检测器：haarcascade_mcs_mouth.xml
鼻子检测器：haarcascade_mcs_nose.xml
身体检测器：haarcascade_fullbody.xml
人脸检测器（快速LBP）：lbpcascade_frontalface.xml
'''
import cv2 as cv


def face_detect_demo():
    # 将图片转换为灰度图片
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # 加载特征数据
    face_detector = cv.CascadeClassifier('D:/ProgramFiles/1_FreeInstallation/opencv/sources/data/haarcascades/haarcascade_frontalface_default.xml')
    faces = face_detector.detectMultiScale(gray)
    for x, y, w, h in faces:
        cv.rectangle(img, (x, y), (x + w, y + h), color=(0, 0, 255), thickness=2)
        cv.circle(img, center=(x + w // 2, y + h // 2), radius=w // 2, color=(0, 255, 0), thickness=2)
    cv.imshow('result', img)


# 加载图片
img = cv.imread('lena.jpg')
face_detect_demo()
cv.waitKey(0)
cv.destroyAllWindows()


'''
运行后报错：
libpng warning: iCCP: cHRM chunk does not match sRGB
【解决】避免这个报错极其简单和没头没脑，Ctrl+shift 将输入法切换，不使用QQ输入法下运行，报错就没了。
'''