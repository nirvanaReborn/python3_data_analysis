#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 视频提取文字

import os
from ffmpy3 import FFmpeg
from public_function import IMAGEIO_FFMPEG_EXE


# 1.视频提取音频
def video2audio(video_file):
    output_dir = os.path.dirname(video_file)
    audio_file = output_dir + os.sep + os.path.basename(video_file)\
        .replace('mp4', 'wav').replace('mkv', 'wav') \
        .replace('rmvb', 'wav').replace('3gp', 'wav').replace('avi', 'wav') \
        .replace('mpeg', 'wav').replace('mpg', 'wav').replace('dat', 'asf') \
        .replace('wmv', 'wav').replace('flv', 'wav').replace('mov', 'wav') \
        .replace('ogg', 'wav').replace('ogm', 'wav').replace('rm', 'wav')

    ff = FFmpeg(
        executable=IMAGEIO_FFMPEG_EXE,
        inputs={video_file: None},
        # 因为百度接口只能识别16 kHz的音频
        # outputs={audio_file: '-vn -ar 16000 -ac 2 -ab 192 -f wav'},
        outputs={audio_file: '-vn -ar 44100 -ac 2 -ab 192 -f wav'},
    )
    print(ff.cmd)
    ff.run()
    return audio_file


# 2.百度短语音识别接口支持1分钟以内的音频，这时候需要对音频进行分割处理
def split_audio(audio_file):
    from pydub import AudioSegment
    from pydub.utils import make_chunks
    list_chunk_name = []
    audio = AudioSegment.from_file(audio_file, "wav")
    size = 120  # 切割的秒数
    chunks = make_chunks(audio, size * 1000)  # 将文件切割为30s一块

    filepath, basename = os.path.split(audio_file)
    shotname, extension = os.path.splitext(basename)
    output_dir = filepath + os.sep + shotname
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)  # 创建目录
    for i, chunk in enumerate(chunks):
        # 枚举，i是索引，chunk是切割好的文件
        chunk_name = output_dir + os.sep + shotname + "_{0}.wav".format(i)
        # print(chunk_name)
        list_chunk_name.append(chunk_name)
        # 保存文件
        chunk.export(chunk_name, format="wav")
    return list_chunk_name


# 3.音频转文字(在线)
def audio2word_online(list_audio_file):
    from aip import AipSpeech
    from public_function import GetConfigInfo

    login_file = os.path.join(r"D:\share\git\gitee\nirvanaReborn_python\python3_AI\src\baidu_AI\resource", r"获取登录信息.xml")
    dict_AI_baidu_all = GetConfigInfo(login_file, r"百度AI").get_dict_config()
    dict_AI_baidu = dict_AI_baidu_all["百度语音识别"]

    # 初始化AipSpeech对象
    APP_ID = dict_AI_baidu["APP_ID"]
    API_KEY = dict_AI_baidu["API_KEY"]
    SECRET_KEY = dict_AI_baidu["SECRET_KEY"]
    client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)

    # 读取文件
    def get_file_content(file_path):
        with open(file_path, 'rb') as fp:
            return fp.read()

    for i, audio_file in enumerate(list_audio_file):
        print(audio_file)
        # 识别本地文件
        result = client.asr(get_file_content(audio_file), 'wav', 16000, {
            'dev_pid': 1537  # 默认1537（普通话 输入法模型），dev_pid参数见本节开头的表格
        })

        if result['err_no'] == 0:
            print(result['result'])
        else:
            # https://cloud.baidu.com/doc/SPEECH/s/Dk4o0bmkl
            print(result)


# 3.音频转文字(离线)
def audio2word(list_audio_file):
    import speech_recognition as sr
    for i, audio_file in enumerate(list_audio_file):
        print(audio_file)
        r = sr.Recognizer()
        # 打开语音文件
        with sr.AudioFile(audio_file) as source:
            audio = r.record(source)

        try:
            # 将语音转换为文本
            # print('文本内容：', r.recognize_sphinx(audio))  # 默认识别英文
            print('文本内容：', r.recognize_sphinx(audio, language="zh-cn"))
        except Exception as e:
            print(e)


def main(flag=True):
    video_file = r"D:\BaiduNetdiskWorkspace\启林沟通20220509.mp4"  # 待转换视频存放的路径
    if flag:
        audio_file = video2audio(video_file)
        list_audio_file = split_audio(audio_file)
    else:
        source_dir = r"D:\BaiduNetdiskWorkspace\启林沟通20220509"
        list_file = os.listdir(source_dir)
        list_audio_file = []
        for file in list_file:
            list_audio_file.append(os.path.join(source_dir, file))
    audio2word(list_audio_file)
    # audio2word_online(list_audio_file)


if __name__ == "__main__":
    main()
