#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 用来操作视频和 GIF 的库。
# moviepy：一个用来进行基于脚本的视频编辑模块，适用于多种格式，包括动图 GIFs。
# scikit-video：SciPy 视频处理常用程序。


def test_1():
    # import moviepy
    pass


def test_2():
    # import scikit-video
    pass


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
