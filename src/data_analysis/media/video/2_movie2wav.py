#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jb51.net/article/173032.htm
# 利用Python小工具实现3秒钟将视频转换为音频
'''
当你运行代码时，Windows 系统可能会出现 ffmpeg 无法找到之类的报错
https://ffmpeg.zeranoe.com/builds/
访问上面的地址，根据自身电脑版本下载相应安装包。解压 ffmpeg 文件，打开 ffmpy3.py 文件(按住Ctrl键，点击ffmpy3，快速跳转到该模块)，
将下面代码中参数 executable 的值改为 ffmpeg.exe 可执行文件的绝对路径。
'''
# pip install ffmpy3
import os
from ffmpy3 import FFmpeg
from public_function import IMAGEIO_FFMPEG_EXE


def mkdir_output(output_dir):
    existence = os.path.exists(output_dir)
    if not existence:
        print('创建音频存放目录')
        os.makedirs(output_dir)  # 创建目录
        os.chdir(output_dir)  # 切换到创建的文件夹
        return True
    else:
        print('目录已存在,即将保存！')
        return False


def main():
    source_dir = r"D:\BaiduNetdiskWorkspace"  # 待转换视频存放的路径
    os.chdir(source_dir)  # 切换到改路径下
    filename = os.listdir(source_dir)  # 得到文件夹下的所有文件名称
    output_dir = r"D:\ARESHelper"
    mkdir_output(output_dir)
    for i in range(len(filename)):
        changefile = source_dir + os.sep + filename[i]
        outputfile = output_dir + os.sep + filename[i].replace('mp4', 'wav').replace('mkv', 'wav') \
            .replace('rmvb', 'wav').replace('3gp', 'wav').replace('avi', 'wav') \
            .replace('mpeg', 'wav').replace('mpg', 'wav').replace('dat', 'asf') \
            .replace('wmv', 'wav').replace('flv', 'wav').replace('mov', 'wav') \
            .replace('ogg', 'wav').replace('ogm', 'wav').replace('rm', 'wav')

        ff = FFmpeg(
            executable=IMAGEIO_FFMPEG_EXE,
            inputs={changefile: None},
            # https://blog.csdn.net/weiyuefei/article/details/52053776
            # -vn: v代表video,n代表不要视频
            # -ar: a代表audio,r代表read,采样率44100 44.1k
            # -ac: a代表audio,c2代表双声道还有立体声,环绕立体声
            # -ab: <比特率> 设定声音比特率，前面-ac设为立体声时要以一半比特率来设置，比如192kbps的就设成96，转换君默认比特率都较小，要听到较高品质声音的话建议设到160kbps（80）以上
            # -f: 音频抽取出来的存取格式, s16 s代表有符号的,每一位16位表示
            # outputs={outputfile: '-vn -ar 44100 -ac 2 -ab 192 -f wav'},
            # 因为百度接口只能识别16 kHz的音频
            outputs={outputfile: '-vn -ar 16000 -ac 2 -ab 192 -f wav'},
        )
        print(ff.cmd)
        ff.run()


if __name__ == "__main__":
    main()
