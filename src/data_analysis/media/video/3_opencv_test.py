#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/iamjingong/article/details/107508035?utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduend~default-2-107508035.nonecase&utm_term=python%20vlc%20%E4%BD%BF%E7%94%A8&spm=1000.2123.3001.4430
# python使用opencv、vlc对直播连接进行视频播放


import ctypes
import time

# pip install opencv-contrib-python
import cv2
import numpy
import vlc
from PIL import Image

vlcInstance = vlc.Instance()
# 机场内
# m = vlcInstance.media_new("")
# 机场外
# 记得换url,最好也和上面一样进行测试一下
url = ""
m = vlcInstance.media_new(url)
mp = vlc.libvlc_media_player_new_from_media(m)

# ***如果显示不完整，调整以下宽度和高度的值来适应不同分辨率的图像***
video_width = 1080
video_height = 640

size = video_width * video_height * 4
buf = (ctypes.c_ubyte * size)()
buf_p = ctypes.cast(buf, ctypes.c_void_p)

VideoLockCb = ctypes.CFUNCTYPE(ctypes.c_void_p, ctypes.c_void_p, ctypes.POINTER(ctypes.c_void_p))


@VideoLockCb
def _lockcb(opaque, planes):
    # print("lock", file=sys.stderr)
    planes[0] = buf_p


@vlc.CallbackDecorators.VideoDisplayCb
def _display(opaque, picture):
    img = Image.frombuffer("RGBA", (video_width, video_height), buf, "raw", "BGRA", 0, 1)
    opencv_image = cv2.cvtColor(numpy.array(img), cv2.COLOR_RGB2BGR)
    cv2.imshow('image', opencv_image)
    cv2.waitKey(10)


def main():
    vlc.libvlc_video_set_callbacks(mp, _lockcb, None, _display, None)
    mp.video_set_format("BGRA", video_width, video_height, video_width * 4)
    while True:
        mp.play()
        time.sleep(1)


if __name__ == "__main__":
    main()
