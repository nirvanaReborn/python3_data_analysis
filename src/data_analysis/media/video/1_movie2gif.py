#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/kevinelstri/article/details/61921812
# 使用python读取数据科学最常用的文件格式

# MP4文件格式用于存储视频和电影。它包含多幅图像（称为帧），从而起到在一个视频形式为每一个特定的时间。
# 两种方法来解释mp4，一个是一个封闭的实体，其中整个视频被认为是一个单一的实体。
# 另一个是马赛克的图像，其中在视频中的每个图像被认为是作为一个不同的实体，这些图像从视频进行采样。

# 问题
# http://www.360doc.com/content/17/0324/09/10408243_639673213.shtml
# http://blog.csdn.net/meijie770342/article/details/77934614?locationNum=9&fps=1
# https://stackoverflow.com/questions/41402550/raise-needdownloaderrorneed-ffmpeg-exe-needdownloaderror-need-ffmpeg-exe
# 解决方法
# Download from https://github.com/imageio/imageio-binaries/raw/master/ffmpeg/ffmpeg.win32.exe
# File saved as C:\Users\Administrator\AppData\Local\imageio\ffmpeg\ffmpeg.win32.exe

# Moviepy是一个Python模块，可以用来做基于脚本的电影编辑。
# 它可以读写很多格式，包括GIF，并且支持一些基本操作如剪切、级联、标题插入等。


import os
from public_function import GLOBAL_GIF_FILE


# 从视频中截图gif动图
def save_gif(source_file):
    import imageio
    from moviepy.editor import VideoFileClip

    current_dir = os.path.dirname(os.path.abspath(source_file))
    dest_file = os.path.join(current_dir, GLOBAL_GIF_FILE)
    print(imageio.plugins.ffmpeg.get_exe())
    clip = VideoFileClip(source_file).subclip(10, 15)  # 代表识别视频中10-15s这一时间段。
    clip.write_gif(dest_file)


def save_wav(source_file):
    from moviepy.editor import AudioFileClip

    # 导入视频
    my_audio_clip = AudioFileClip("一行玩Python/1012 视频转文字/11.mp4")
    # 提取音频并保存
    my_audio_clip.write_audiofile("一行玩Python/1012 视频转文字/11.wav")


def main():
    source_file = r"D:\share\test\1\demo.flv"
    save_gif(source_file)


if __name__ == "__main__":
    main()
