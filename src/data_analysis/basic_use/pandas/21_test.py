#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import pandas as pd
import time

"""
iat[] 和 at[] 通常比 iloc[] 和 loc[] 更快，因为 iat[] 和 at[] 专门设计用于单个元素访问，没有复杂的解析过程。
iloc[] 和 loc[] 功能更强大，因为它们支持更多类型的索引（如切片、布尔数组等），但在处理单个元素时可能相对较慢。

通过运行下面的代码，你可以看到 iat[] 和 at[] 在反复操作单个元素时通常会比 iloc[] 和 loc[] 更快。
结论：
如果频繁操作单个元素并且知道具体的位置，建议使用 iat[] 以获得最佳性能。
如果更习惯使用标签索引或你需要通过标签来访问单个元素，使用 at[] 也是很好的选择。
"""

# 创建一个较大的DataFrame
df = pd.DataFrame({
    'A': range(10000),
    'B': range(10000, 20000),
    'C': range(20000, 30000)
})


def main():
    # 使用%timeit进行简单的性能测试

    # 测试iat
    start_time = time.time()
    for _ in range(10000):
        df.iat[5000, 0] = 999
    end_time = time.time()
    print(f"iat took {end_time - start_time:.4f} seconds")

    # 测试at
    start_time = time.time()
    for _ in range(10000):
        df.at[5000, 'A'] = 999
    end_time = time.time()
    print(f"at took {end_time - start_time:.4f} seconds")

    # 测试iloc
    start_time = time.time()
    for _ in range(10000):
        df.iloc[5000, 0] = 999
    end_time = time.time()
    print(f"iloc took {end_time - start_time:.4f} seconds")

    # 测试loc
    start_time = time.time()
    for _ in range(10000):
        df.loc[5000, 'A'] = 999
    end_time = time.time()
    print(f"loc took {end_time - start_time:.4f} seconds")


if __name__ == '__main__':
    main()
