#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import pandas as pd
import numpy as np
import timeit

# 创建一个示例 DataFrame
df = pd.DataFrame({
    'A': np.random.randint(0, 100, size=1000000),
    'B': np.random.choice(['a', 'b', 'c', 'd', 'e'], size=1000000)
})

# 性能测试
print(timeit.timeit("df.loc[df['A'] == 3, 'B']", globals=globals(), number=100))
print(timeit.timeit("df[df['A'] == 3]", globals=globals(), number=100))
print(timeit.timeit("df.query('A == 3')", globals=globals(), number=100))
print(timeit.timeit("df[df['B'].isin(['c', 'd'])]", globals=globals(), number=100))
print(timeit.timeit("df.applymap(lambda x: x == 'c')", globals=globals(), number=10))  # 注：由于速度较慢，迭代次数较低
