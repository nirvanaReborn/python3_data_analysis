#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""# https://blog.csdn.net/weixin_44214830/article/details/117773082
1.数据框字段类型查看：df.dtypes
2.维度查看df.shape
3.数据框的策略基本信息df.info()
4.某一列格式df['列名'].dtype
5.数据类型转换.astype
"""

import numpy as np
import pandas as pd
import copy

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
# pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)

header = ['m_lOpen', 'm_lClose', 'm_lHigh', 'm_lLow', 'm_lVolume', 'm_lMoney', 'm_lPaused', 'm_lPreClose', 'm_lUp', 'm_lDown', 'm_lTotalHold',
          'm_lSettle', 'm_lPreSettle', 'm_lTurnOverRate']


def test_1():
    df = pd.DataFrame(columns=header)
    print(df)
    df.index = df.index.astype('datetime64[ns]')
    print(df)


def test_2():
    n = 56

    df_index = np.arange(n / 14, dtype=np.int64)
    kline_datas = np.arange(n, dtype=np.int64)
    print(type(df_index))
    print(df_index)
    df_index[:] = [20211115, 20211116, 20211117, 20211118]
    print(df_index)
    # 在python3中，‘整数/整数 = 浮点数’，使用 '//'就可以达到原python2中'/'的效果。
    z = np.reshape(kline_datas, (n // 14, 14))  # 将一维修改为二维
    df = pd.DataFrame(z, columns=header, index=df_index.astype('M8[s]'))
    # df = pd.DataFrame(z, columns=header, index=df_index)
    print(df)


def test_3():
    s = pd.Series([0, 1, 2])
    print(s.astype('M8[D]'))
    '''
    0   1970-01-01
    1   1970-01-02
    2   1970-01-03
    dtype: datetime64[ns]
    '''

    print(s.astype('M8[s]'))
    '''
    0   1970-01-01 00:00:00
    1   1970-01-01 00:00:01
    2   1970-01-01 00:00:02
    dtype: datetime64[ns]
    '''


def test_4():
    # suppress=True 表示取消科学计数法
    # np.set_printoptions(suppress=True)
    # 科学计数法改为浮点型展示
    # pd.set_option('display.float_format', lambda x: '%.0f' % x)
    data = {
        'state': ['ok', 'ok', 'good', 'bad'],
        'year': [2000, 2001, 2002, 2003.6],
        'business_time': [20220412151014123, 20220412151014001, 20220412151014300, 20220412151014400]
    }
    df = pd.DataFrame(data)
    df_new = copy.deepcopy(df)
    df_new['business_time'] = df['business_time'].astype('int64')
    df['business_time'] = df['business_time'].astype('int')
    print(df)
    print(df_new)


def test_5():
    pass


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    # main()
    test_3()
