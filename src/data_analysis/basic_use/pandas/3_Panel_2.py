#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import pandas as pd


class Panel(pd.DataFrame):
    """
    Pandas Panel class simulation
    """

    def __init__(self, data=None, items=None, major_axis=None, minor_axis=None,
                 dtype=None, copy=False):
        """
        Constructor of Panel class
        """
        if data is None:
            data = {}
        if items is None:
            items = []
        if major_axis is None:
            major_axis = []
        if minor_axis is None:
            minor_axis = []

        self.items = items
        self.major_axis = major_axis
        self.minor_axis = minor_axis
        self.data = data

        # construct MultiIndex
        row_index = pd.MultiIndex.from_product([self.items, self.major_axis], names=['items', 'major'])
        col_index = pd.MultiIndex.from_product([self.minor_axis], names=['minor'])

        # initialize DataFrame
        super().__init__(self.data, index=row_index, columns=col_index)

    @property
    def _constructor(self):
        return Panel

    def __getitem__(self, key):
        if isinstance(key, list) or isinstance(key, tuple):
            return Panel(super().__getitem__(key))
        else:
            return super().__getitem__(key)



