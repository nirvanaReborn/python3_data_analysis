#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import pandas as pd


def main():
    """
    我们将DataFrame df按每10万行分割，并将其保存到单独的CSV文件中。
    每个文件的名称是output_chunk_0.csv, output_chunk_1.csv, 等等。
    """
    # 生成示例数据
    data = {
        'A': range(1000000),  # 生成一百万行数据
        'B': range(1000000, 2000000)
    }
    df = pd.DataFrame(data)

    # 定义分割大小，例如每10万行一个文件
    chunk_size = 100000

    # 确定需要多少个文件
    num_chunks = (len(df) // chunk_size) + 1

    for i in range(num_chunks):
        start_row = i * chunk_size
        end_row = (i + 1) * chunk_size
        chunk_df = df.iloc[start_row:end_row]

        # 将chunk_df写入一个CSV文件
        chunk_df.to_csv(f'output_chunk_{i}.csv', index=False)


if __name__ == '__main__':
    main()
