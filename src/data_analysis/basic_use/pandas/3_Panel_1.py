#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Pandas官方推荐使用其他数据结构，如MultiIndex DataFrame或者xarray来替代Panel。
import pandas as pd


class Panel:
    def __init__(self, data, items, major_axis, minor_axis):
        self.data = pd.DataFrame(data)
        self.data.index = pd.MultiIndex.from_product([items, major_axis, minor_axis])

    def __getitem__(self, key):
        return self.data.loc[key]

    def __setitem__(self, key, value):
        self.data.loc[key] = value

    def to_frame(self):
        return self.data

    def to_xarray(self):
        return self.data.to_xarray()


if pd.__version__ > "0.23.4":
    pd.Panel = Panel


def main():
    import numpy as np

    # 创建一个Panel对象
    data = np.random.randn(2, 3, 4)
    items = ['Item1', 'Item2']
    major_axis = pd.date_range('2021-01-01', periods=3)
    minor_axis = ['A', 'B', 'C', 'D']
    panel = Panel(data, items, major_axis, minor_axis)

    # 通过索引获取数据
    print(panel['Item1'])

    # 设置某个位置的值
    panel['Item1', '2021-01-01', 'A'] = 0

    # 转换为DataFrame
    panel_df = panel.to_frame()
    print(panel_df)

    # 转换为xarray
    panel_xr = panel.to_xarray()
    print(panel_xr)


if __name__ == "__main__":
    main()
