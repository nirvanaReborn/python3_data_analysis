#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/cxmscb/article/details/54632492
# python之pandas的基本使用（1）
'''
# pd ：pannel data analysis（面板数据分析）。
# pandas是基于numpy构建的，为时间序列分析提供了很好的支持。
# pandas中有三个主要的数据结构：1.是Series， 2.是DataFrame， 3.是Panel。
# Series一维、Dataframe二维、Panel三维
'''
'''
Series 类似于一维数组与字典(map)数据结构的结合。
它由一组数据和一组与数据相对应的数据标签（索引index）组成。
这组数据和索引标签的基础都是一个一维ndarray数组。可将index索引理解为行索引。
Series的表现形式为：索引在左，数据在右。

Series与Numpy中的一维array类似。
二者与Python基本的数据结构List也很相近，
其区别是：List中的元素可以是不同的数据类型，而Array和Series中则只允许存储相同的数据类型，这样可以更有效的使用内存，提高运算效率。

'''

import numpy as np
import pandas as pd


# 用一维数组生成Series
def test_1():
    ps = pd.Series([1, 2, 3, 4])
    print(ps)
    '''
    0    1
    1    2
    2    3
    3    4
    dtype: int64
    '''

    print(ps.index)  # RangeIndex(start=0, stop=4, step=1)
    print(ps.values)  # [1 2 3 4]


# 指定Series的index
def test_2():
    ps = pd.Series([1, 2, 3, 4], index=['a', 'b', 'd', 'c'])
    print(ps)
    '''
    a    1
    b    2
    d    3
    c    4
    dtype: int64
    '''

    print(ps.index)  # Index(['a', 'b', 'd', 'c'], dtype='object')
    print(ps["a"])  # 1
    ps["d"] = 6

    print(ps[['c', 'a', 'd']])  # 类似于numpy的花式索引
    '''
    c    4
    a    1
    d    6
    dtype: int64
    '''

    print(ps[ps > 2])  # 类似于numpy的布尔索引
    '''
    d    6
    c    4
    dtype: int64
    '''

    print('b' in ps)  # 类似于字典的使用：是否存在该索引：True
    print('e' in ps)  # False


# 使用字典来生成Series
def test_3():
    data = {'a': 1, 'b': 2, 'd': 3, 'c': 4}
    x = pd.Series(data)
    print(x)
    '''
    a    1
    b    2
    c    4
    d    3
    dtype: int64
    '''

    # 使用字典生成Series,并指定额外的index，不匹配的索引部分数据为NaN。
    exindex = ['a', 'b', 'c', 'e']
    y = pd.Series(data, index=exindex)  # 类似替换索引
    print(y)
    '''
    a    1.0
    b    2.0
    c    4.0
    e    NaN
    dtype: float64
    '''

    # Series相加，相同行索引相加，不同行索引则数值为NaN
    print(x + y)
    '''
    a    2.0
    b    4.0
    c    8.0
    d    NaN
    e    NaN
    dtype: float64
    '''

    #  指定Series/索引的名字
    y.name = 'weight of letters'
    y.index.name = 'letter'
    print(y)
    '''
    letter
    a    1.0
    b    2.0
    c    4.0
    e    NaN
    Name: weight of letters, dtype: float64
    '''

    # 替换index
    y.index = ['a', 'b', 'c', 'f']
    print(y)  # 不匹配的索引部分数据为NaN
    '''
    a    1.0
    b    2.0
    c    4.0
    f    NaN
    Name: weight of letters, dtype: float64
    '''


# 索引对象
def test_4():
    x = pd.Series(range(3), index=['a', 'b', 'c'])
    index = x.index
    print(index)  # Index(['a', 'b', 'c'], dtype='object')
    print(index[0:2])  # Index(['a', 'b'], dtype='object')

    # Index对象是不可修改的，Series和DataFrame中的索引都是Index对象。
    try:
        index[0] = 'd'
    except:
        print("Index is immutable")

    index = pd.Index(np.arange(3))
    obj2 = pd.Series([1.5, -2.5, 0], index=index)
    print(obj2)
    print(obj2.index is index)


# 对列/行索引重新指定索引（删除/增加：行/列）：reindex()函数
'''
reindex的method选项：
ffill 或 pad       前向填充(或搬运)值
bfill 或 backfill  后向填充(或搬运)值
'''


def test_5():
    print("重新指定索引及NaN填充值")
    x = pd.Series([4, 7, 5], index=['a', 'b', 'c'])
    y = x.reindex(['a', 'b', 'c', 'd'])
    print(y)

    # fill_value 指定不存在元素NaN的默认值
    print(x.reindex(['a', 'b', 'c', 'd'], fill_value=0))

    x = pd.Series(['blue', 'purple'], index=[0, 2])
    print(x.reindex(range(4), method='ffill'))


# 删除（丢弃）整一行/列的元素：drop()函数
def test_6():
    print('Series根据行索引删除行')

    x = pd.Series(np.arange(4), index=['a', 'b', 'c', 'd'])
    print(x.drop('c'))  # Series根据行索引删除行
    print(x.drop(['a', 'b']))  # 花式删除
    print(x)


# 处理空值
def test_7():
    series = pd.Series([1, 2, np.nan, 4])
    series_filled = series.fillna('')
    print("\nSeries after replacing NaN with empty strings:")
    print(series_filled)


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
