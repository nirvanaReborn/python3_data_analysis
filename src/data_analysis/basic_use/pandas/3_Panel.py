#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/u010030977/article/details/79916787
# Pandas Panel 按不同维度添加数据的方法
'''
Panel相当于一个存储DataFrame的字典
Series一维、Dataframe二维、Panel三维Panel

可用以下函数构造Panel
     pandas.Panel(data, items, major_axis, minor_axis, dtype, copy)
data为数据。可通过多种方式构造。
items相当于上图中的分类标签。在item确定之后，就可以将其看作dataframe
Major_axis即为dataframe中的index。
Minor_axis为dataframe中的columns。

自版本0.20.0 起pandas库已弃用数据结构Panel，使用MultiIndex的DataFrame结构替代:https://zhuanlan.zhihu.com/p/139045553
或者使用xarray 包:Pandas 提供了一种to_xarray（）方法来自动执行此转换。
'''
import os
import pandas as pd
from public_function import GLOBAL_WORK_DIR


if pd.__version__ > "0.20.0":
    print("自版本0.20.0 起pandas库已弃用数据结构Panel")
    exit(0)


# https://blog.csdn.net/qq_41455398/article/details/81355377
def test_1():
    dict_df = {"one": pd.DataFrame([1, 2, 3], index=["a", "b", "c"], columns=["hh"])}
    dp = pd.Panel(dict_df)
    print(dp)
    dest_file = os.path.join(GLOBAL_WORK_DIR, "demo.xlsx")
    # pandas.concat(da).to_excel(dest_file, index=False)

    writer = pd.ExcelWriter(dest_file, engine='openpyxl')
    for key in dp:
        df = dp[key]
        df.to_excel(writer, sheet_name=key, index=False, header=True)

    writer.save()
    writer.close()


def test_2():
    import xarray
    dict_df = {"one": pd.DataFrame([1, 2, 3], index=["a", "b", "c"], columns=["hh"])}
    da = xarray.DataArray(dict_df)

    print(type(da))
    print(da)


def test_3():
    # 转置
    dict_df = {"one": pd.DataFrame([1, 2, 3], index=["a", "b", "c"], columns=["hh"])}
    dp = pd.Panel(dict_df)
    dp = dp.swapaxes("minor_axis", "items")
    print(dp)


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
