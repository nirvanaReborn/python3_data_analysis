#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import numpy as np
import pandas as pd


# https://blog.csdn.net/coco3600/article/details/100232609
# pandas的dataFrame输出不换行
def test_1():
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    # pd.set_option('display.height', 1000)
    pd.set_option('display.width', 1000)
    df = pd.DataFrame(np.random.randn(1, 20))
    print(df)


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
