#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/cxmscb/article/details/54632492
# python之pandas的基本使用（1）
"""
pd ：pannel data analysis（面板数据分析）。
pandas是基于numpy构建的，为时间序列分析提供了很好的支持。
pandas中有三个主要的数据结构：1.是Series， 2.是DataFrame， 3.是Panel。

DataFrame 是一个类似表格的数据结构，索引包括列索引和行索引，
包含有一组有序的列，每列可以是不同的值类型（数值、字符串、布尔值等）。
DataFrame的每一行和每一列都是一个Series，这个Series的name属性为当前的行索引名/列索引名。
可以将DataFrame理解为Series的容器。

通过列索引获取列数据（Series类型 ）：df_obj[col_idx] 或 df_obj.col_idx

.ix，标签与位置混合索引

# http://blog.csdn.net/claroja/article/details/71598572
# 构造函数：DataFrame([data, index, columns, dtype, copy])
"""

import numpy as np
import pandas as pd

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
# pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)

data = {
    'state': ['ok', 'ok', 'good', 'bad'],
    'year': [2000, 2001, 2002, 2003],
    'pop': [3.7, 3.6, 2.4, 0.9]
}


def test_0():
    # 方式一：直接初始化一个空的 DataFrame
    df = pd.DataFrame()
    print(df)
    # 方式二：指定列名和索引但没有数据
    df = pd.DataFrame(columns=['A', 'B', 'C'], index=[])
    print(df)


# 使用字典生成DataFrame，key为列名字。
def test_1():
    df = pd.DataFrame(data)
    print(df)  # 行索引index默认为0，1，2，3
    '''
       pop state  year
    0  3.7    ok  2000
    1  3.6    ok  2001
    2  2.4  good  2002
    3  0.9   bad  2003
    '''
    row_count = df.shape[0]
    col_count = df.shape[1]
    print("行数:", row_count, len(df))
    print("列数:", col_count)


# 遍历Dataframe的3种方式
def test_2():
    df = pd.DataFrame(data)
    # iterrows(): 按行遍历，将DataFrame的每一行迭代为(index, Series)对，可以通过row[name]对元素进行访问。
    for index, row in df.iterrows():
        print(index)  # 输出每行的索引值
        print('-' * 50)
        print(row['state'], row['year'], row['pop'])  # 访问对应的元素
    print('*' * 50)

    # itertuples(): 按行遍历，将DataFrame的每一行迭代为元祖，可以通过row[name]对元素进行访问，比iterrows()效率高。
    for row in df.itertuples():
        print(getattr(row, 'state'), getattr(row, 'year'))  # 输出每一行
    print('*' * 50)

    # iteritems():按列遍历，将DataFrame的每一列迭代为(列名, Series)对，可以通过row[index]对元素进行访问。
    for column, col in df.items():
        print(column, col)  # 输出列名
        print('-' * 50)
        print(col[0], col[1], col[2], col[3])  # 输出各列


def test_3():
    # 指定列索引columns,不匹配的列为NaN
    print(pd.DataFrame(data, columns=['year', 'state', 'pop', 'debt']))

    # 指定行索引index
    df = pd.DataFrame(data,
                      columns=['year', 'state', 'pop', 'debt'],
                      index=['one', 'two', 'three', 'four'])
    print(df)
    '''
           year state  pop debt
    one    2000    ok  3.7  NaN
    two    2001    ok  3.6  NaN
    three  2002  good  2.4  NaN
    four   2003   bad  0.9  NaN
    '''

    print(df.columns)  # Index(['year', 'state', 'pop', 'debt'], dtype='object')


# DataFrame元素的索引与修改
def test_4():
    df = pd.DataFrame(data,
                      columns=['year', 'state', 'pop', 'debt'],
                      index=['one', 'two', 'three', 'four'])
    print(df['state'])  # 列索引,返回一个名为state的Series
    print(df['state'].tolist())
    # print(df.state) # 也可直接用.进行列索引
    '''
    one        ok
    two        ok
    three    good
    four      bad
    Name: state, dtype: object
    '''

    print(df.loc['three'])  # 用.loc[]进行行索引
    '''
    year     2002
    state    good
    pop       2.4
    debt      NaN
    Name: three, dtype: object
    '''

    df['debt'] = 16.5  # 修改一整列数据
    print(df)
    '''
           year state  pop  debt
    one    2000    ok  3.7  16.5
    two    2001    ok  3.6  16.5
    three  2002  good  2.4  16.5
    four   2003   bad  0.9  16.5
    '''

    # 用numpy数组修改元素
    df.debt = np.arange(4)
    print(df)
    '''
           year state  pop  debt
    one    2000    ok  3.7     0
    two    2001    ok  3.6     1
    three  2002  good  2.4     2
    four   2003   bad  0.9     3
    '''

    # 用Series修改元素，没有指定的默认数据用NaN
    val = pd.Series([-1.2, -1.5, -1.7, 0], index=['one', 'two', 'five', 'six'])
    df.debt = val  # DataFrame的行索引不变
    print(df)
    '''
           year state  pop  debt
    one    2000    ok  3.7  -1.2
    two    2001    ok  3.6  -1.5
    three  2002  good  2.4   NaN
    four   2003   bad  0.9   NaN
    '''

    # 给DataFrame添加新列
    df['gain'] = (df.debt > 0)  # 如果debt大于0为True
    print(df)
    '''
           year state  pop  debt   gain
    one    2000    ok  3.7  -1.2  False
    two    2001    ok  3.6  -1.5  False
    three  2002  good  2.4   NaN  False
    four   2003   bad  0.9   NaN  False
    '''


# DataFrame转置
def test_5():
    index_colums = ['year', 'state', 'pop', 'debt']
    index_row = ['one', 'two', 'three', 'four']
    df = pd.DataFrame(data, columns=index_colums, index=index_row)
    print(df.T)
    # 或者
    # df_T = pd.DataFrame(df.values.T, columns=index_row, index=index_colums)
    # print(df_T)


# 使用切片初始化数据,未被匹配的数据为NaN
def test_6():
    df = pd.DataFrame(data,
                      columns=['year', 'state', 'pop', 'debt'],
                      index=['one', 'two', 'three', 'four'])
    pdata = {
        'state': df['state'][0:3],
        'pop': df['pop'][0:2]
    }
    y = pd.DataFrame(pdata)
    print(y)
    '''
           pop state
    one    3.7    ok
    three  NaN  good
    two    3.6    ok
    '''

    # 指定索引和列的名称 # 与Series的index.name相区分
    y.index.name = '序号'
    y.columns.name = '信息'
    print(y)
    '''
    信息 pop state
    序号              
    one    3.7    ok
    three  NaN  good
    two    3.6    ok
    '''

    print(y.values)
    '''
    [[3.7 'ok']
     [nan 'good']
     [3.6 'ok']]
    '''


# 索引对象
def test_7():
    dict_data = {'pop': {2.4, 2.9},
                 'year': {2001, 2002}}
    df = pd.DataFrame(dict_data)
    print(df)
    print('pop' in df.columns)  # True
    print(1 in df.index)  # True


# 对列/行索引重新指定索引（删除/增加：行/列）：reindex()函数
'''
reindex的method选项：
ffill 或 pad       前向填充(或搬运)值
bfill 或 backfill  后向填充(或搬运)值
'''


def test_8():
    print('对DataFrame重新指定行/列索引')

    df = pd.DataFrame(np.arange(9).reshape(3, 3),
                      index=['a', 'c', 'd'],
                      columns=['A', 'B', 'C'])
    print(df)

    df = df.reindex(['a', 'b', 'c', 'd'], method='bfill')
    print(df)

    print('重新指定column')
    states = ['A', 'B', 'C', 'D']
    df = df.reindex(columns=states, fill_value=0)
    print(df)
    print(df.ix[['a', 'b', 'd', 'c'], states])


def test_9():
    df = pd.DataFrame(np.arange(16).reshape((4, 4)),
                      index=['a', 'b', 'c', 'd'],
                      columns=['A', 'B', 'C', 'D'])
    print(df)
    print(df.drop(['A', 'B'], axis=1))  # 在列的维度上删除AB两行
    print(df.drop('a', axis=0))  # 在行的维度上删除行
    print(df.drop(['a', 'b'], axis=0))  # 在行的维度上删除行


def test_10():
    list_data = [
        {
            'state': 'bad',
            'year': 2009,
            'pop': 3.9,
        },
        {
            'state': 'good',
            'year': 2001,
            'pop': 3.6,
        }
    ]
    df = pd.DataFrame(list_data)
    print(df)  # 行索引index默认为0，1，2，3


# https://blog.csdn.net/stone0823/article/details/89447982
def test_11():
    import sqlite3
    con = sqlite3.connect('my-test.db')
    df = pd.DataFrame(data)
    df.set_index(['year'], inplace=True)
    df.to_sql('test', con)  # 存入数据库

    # 假设我们要连接表USER和SKILL，并将结果读入Pandas数据框。它也是无缝的。
    df = pd.read_sql('''
        SELECT s.user_id, u.name, u.age,s.skill
        FROM USER u LEFT JOIN SKILL s ON u.id = s.user_id
        ''', con)

    # 让我们把结果写到一个名为USER_SKILL的新表中
    df.to_sql('USER_SKILL', con)


# https://www.cnblogs.com/wqbin/p/11845042.html
# 获取DataFrame列名的4种方法
def test_12():
    df = pd.DataFrame(data)
    print(df.columns.tolist())
    print(df.columns.values.tolist())
    print([column for column in df])  # 链表推倒式
    print(list(df))


def test_13():
    df = pd.DataFrame(data)
    print(df.index.tolist())


def test_14():
    df = pd.DataFrame(data)
    list_sheet = df.values.tolist()  # 已经跳过标题行
    # list_sheet = list(np.array(df).tolist())  # 已经跳过标题行
    print(list_sheet)


# 把dataframe的一列设为索引
def test_15():
    df = pd.DataFrame(data)
    df.set_index(['year'], inplace=True, drop=False)
    print(df)


# dataframe排序
def test_16():
    df = pd.DataFrame(data)
    print(df)
    # df = df.sort_index(axis=0, ascending=True)  # 默认按“行标签”升序排序
    # df = df.sort_index(axis=1, ascending=True) #
    df = df.sort_values(by=['pop'], axis=0, ascending=True)
    print(df)


def test_17():
    import datetime
    data_dict = {
        'c_time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'id': 56,
        'name': '牛逼',
        'sex': 0,
        'qq': 154745845,
        'tel': 15748586589,
        'period': '第四期',
        'course': '机器学习',
        'is_delete': 0
    }
    # df = pd.DataFrame(data=data_dict) # 报错：ValueError: If using all scalar values, you must pass an index
    df = pd.DataFrame(data=data_dict, index=[0])
    print(df)
    # df2 = pd.DataFrame(data=data_dict, index=range(len(data_dict)))
    df2 = pd.DataFrame(data=data_dict, index=range(1))
    print(df2.T)


# 使用多种方法来查找和匹配 DataFrame 中的某个值
def test_18():
    df = pd.DataFrame({
        'A': [1, 2, 3, 4, 5],
        'B': ['a', 'b', 'c', 'd', 'e']
    })

    # 1. 使用 loc 方法: 查找 'A' 列中值为 3 的行，并选择 'B' 列
    result = df.loc[df['A'] == 3, 'B']
    print(result, '*' * 50, sep='\n')

    # 2. 使用布尔索引（Boolean Indexing）: 查找 'A' 列中值为 3 的行
    result = df[df['A'] == 3]
    print(result, '*' * 50, sep='\n')

    # 3. 使用 query 方法: 查找 'A' 列中值为 3 的行
    result = df.query('A == 3')
    print(result, '*' * 50, sep='\n')

    # 4. 使用 isin 方法: 查找 'B' 列中值为 'c' 或 'd' 的行
    result = df[df['B'].isin(['c', 'd'])]
    print(result, '*' * 50, sep='\n')

    # 5. 查找整个 DataFrame 中的值: 字符串 'c' 是否在这个 DataFrame 中
    result = df.applymap(lambda x: x == 'c')
    print(result)


# 处理空值
def test_19():
    # 创建一个示例 DataFrame 包含 NaN
    df = pd.DataFrame({
        'A': [1, 2, np.nan],
        'B': [4, np.nan, 6],
        'C': [np.nan, 8, 9]
    })

    print("Original DataFrame:")
    print(df)

    # 1. 对于整个DataFrame将 NaN 替换为空字符串
    df_filled = df.fillna('')

    print("\nDataFrame after replacing NaN with empty strings:")
    print(df_filled)

    # 2. 如果你只想替换某一列中的 NaN 值，也可以使用 fillna 方法指定列名。
    df['A'] = df['A'].fillna('')
    print("\nDataFrame after replacing NaN in column 'A' with empty strings:")
    print(df)

    # 3. 原地操作：
    df.fillna('', inplace=True)
    print("\nOriginal DataFrame after in-place NaN replacement with empty strings:")
    print(df)


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
