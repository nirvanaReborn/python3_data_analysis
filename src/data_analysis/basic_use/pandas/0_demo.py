#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://pandas.pydata.org/pandas-docs/version/0.23.4/whatsnew.html
import pandas as pd


def test_1():
    print(pd.__version__)
    ps = pd.Series([1, 2, 3, 4])
    print(ps)


def test_2():
    # pip install pandasgui
    import pandasgui
    print(pandasgui.__version__)
    df = pd.read_excel(
        io=r'../../../../doc/Python3.xlsx',
        engine="openpyxl", sheet_name="Python3标准库",
        skiprows=3, usecols="B:R",
        nrows=1000
    )
    pandasgui.show(df)


def test_3():
    pass
    # https://www.datalearner.com/blog/1051638704435903
    # PandasTutor: 一个用于可视化pandas操作的网站
    # https://pandastutor.com/
    # 为了让初学者能够充分理解pandas中的操作，Pandas Tutor将pandas的操作变成可视化的过程，让我们充分理解这个过程。


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
