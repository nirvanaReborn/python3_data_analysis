#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/cxmscb/article/details/54670113
# python之pandas的基本使用（2）

import os
import openpyxl
import numpy
import pandas
from public_function import GLOBAL_WORK_DIR

'''
excel的写入函数为pd.DataFrame.to_excel()；必须是DataFrame写入excel, 即Write DataFrame to an excel sheet。
to_excel(self, excel_writer, sheet_name='Sheet1', na_rep='', float_format=None,
        columns=None,  header=True,  index=True, index_label=None,
        startrow=0, startcol=0, engine=None,  merge_cells=True, 
        encoding=None, inf_rep='inf', verbose=True, freeze_panes=None)

excel_writer ： ExcelWriter目标文件路径
sheet_name   ： excel表中sheet页命名
na_rep       ： 缺失值填充，可以设置为字符串    # 如果na_rep设置为bool值，则写入excel时改为0和1；也可以写入字符串或数字
columns      ： 选择输出的的列存入。
header       :  指定作为列名的行，默认0，即取第一行，数据为列名行以下的数据；若数据不含列名，则设定 header = None；
index        ： 显示行索引（名字），默认为True，显示index，当index=False 则不显示行索引（名字）
index_label  ： 设置索引列的列名
startcol     ： 
'''


# 读取xlsx文件
def test_1():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xls")
    df = pandas.read_excel(source_file, sheet_name='登录信息', converters={u'密码': str})
    print(df)
    # print([[c, *dataframe[c].tolist()] for c in dataframe.columns])
    list_head = df.columns.tolist()  # 标题行
    list_sheet = df.values.tolist()  # 已经跳过标题行
    # list_sheet = list(numpy.array(df).tolist())  # 已经跳过标题行
    list_sheet.insert(0, list_head)
    print(list_sheet)


# https://www.jianshu.com/p/b5e8eab578bd
# 读写xlsx文件
def test_2():
    # 使用pandas读取excel文件
    xls_file = pandas.ExcelFile('workbook.xls')
    print(xls_file.sheet_names)  # 显示出读入excel文件中的表名字
    table1 = xls_file.parse('first_sheet')
    table2 = xls_file.parse('second_sheet')

    xlsx_file = pandas.ExcelFile(os.path.join(GLOBAL_WORK_DIR, "demo.xlsx"))
    data1 = xlsx_file.parse(0)
    data2 = xlsx_file.parse(1)

    # 使用pandas写入excel文件
    # data.to_excel("abc.xlsx",sheet_name="abc",index=False,header=True)
    # 该条语句会运行失败，原因在于写入的对象是np数组而不是DataFrame对象,只有DataFrame对象才能使用to_excel方法。
    list_head = [i for i in range(len(list(data1)))]
    pandas.DataFrame(data1, columns=list_head).to_excel("abc.xlsx", sheet_name="123", index=False, header=True)
    # excel文件和pandas的交互读写，主要使用到pandas中的两个函数,一个是pd.ExcelFile函数,一个是to_excel函数


def test_3():
    #  读写 excel主要通过read_excel 、 to_excel函数实现，除了pandas还需要安装第三方库  xlrd、 openpyxl

    # 读取excel
    df = pandas.DataFrame(pandas.read_excel('name.xlsx',
                                            skiprows=1,  # 跳过前几行
                                            na_values=['NA', '–', 'n/a'],  # 将这些值替换为NaN
                                            header=None,
                                            names=['A', 'B', 'C']))
    print(df)

    df2 = pandas.DataFrame([["王五", 21],
                            ["赵六", 32]], columns=['name', 'age'])
    combined_df = pandas.concat([df, df2], ignore_index=True)
    # combined_df = pandas.concat([pandas.read_excel(file) for file in file_names], ignore_index=True)
    print(combined_df)

    # 写入excel
    combined_df.to_excel('E:\\excel_to_python.xlsx', sheet_name='bluewhale_cc')

    # 写入Excel
    writer = pandas.ExcelWriter('E:\\output.xlsx')
    df3 = pandas.DataFrame(data={'col1': [1, 1], 'col2': [2, 2]})
    print(df3)
    df3.to_excel(writer, 'Sheet12')
    writer.save()


# https://blog.csdn.net/qq_21578125/article/details/81111651
def test_4():
    # create some Pandas DateFrame from some data
    df1 = pandas.DataFrame({'Data1': [1, 2, 3, 4, 5, 6, 7]})
    df2 = pandas.DataFrame({'Data2': [8, 9, 10, 11, 12, 13]})
    df3 = pandas.DataFrame({'Data3': [14, 15, 16, 17, 18]})
    All = [df1, df2, df3]
    # create a Pandas Excel writer using xlswriter
    writer = pandas.ExcelWriter('test.xlsx')

    df1.to_excel(writer, sheet_name='Data1', startcol=0, index=False)
    df2.to_excel(writer, sheet_name='Data1', startcol=1, index=False)
    df3.to_excel(writer, sheet_name='Data3', index=False)


def test_5():
    # 依次读取多个相同结构的Excel文件并创建DataFrame
    dfs = []
    for fn in ('1.xlsx', '2.xlsx', '3.xlsx', '4.xlsx'):
        dfs.append(pandas.read_excel(fn))
    # 将多个DataFrame合并为一个
    df = pandas.concat(dfs)
    # 写入Excel文件，不包含索引数据
    df.to_excel('result.xlsx', index=False)


def test_6():
    raw_data = pandas.read_excel("raw_data.xlsx")
    all_need = []
    need_list = ["vickey", "wu"]
    for n in need_list:
        # find need data
        need = raw_data[raw_data["filed"].str.contains(n)]
        all_need.append(need)
    # write all need data into excel
    pandas.concat(all_need).to_excel("sample.xlsx", index=False)


# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.ExcelWriter.html#pandas.ExcelWriter
def test_7():
    df1 = pandas.DataFrame({'Data1': [1, 2, 3, 4, 5, 6, 7]})
    df2 = pandas.DataFrame({'Data2': [8, 9, 10, 11, 12, 13]})
    with pandas.ExcelWriter('path_to_file.xlsx',
                            mode='a',
                            date_format='YYYY-MM-DD',
                            datetime_format='YYYY-MM-DD HH:MM:SS') as writer:
        df1.to_excel(writer, sheet_name='Sheet1')
        df2.to_excel(writer, sheet_name='Sheet2')


def test_8():
    list_2D = []
    source_file = r"D:\SVN\PBOX-FLY\trunk\Sources\fly_IQ_trunk\fly_server\download\asset.pk"
    dest_file = r"listStatus.csv"
    list_pk = pandas.read_pickle(source_file)
    # print(len(list_pk))  # 6
    line_count = 1  # 表示标题行
    for row in list_pk:
        # print(type(row))  # <class 'list'>
        # print(len(row))  # 6043
        line_count += len(row)
        for cell in row:
            # print(type(cell))  # <class 'IQEngine.core.asset.StockAsset'>
            # print(cell)
            # print(cell.symbol, cell.name, cell.listed_date.strftime('%Y%m%d'), cell.delisted_date.strftime('%Y%m%d'))
            list_2D.append([cell.symbol, cell.name, cell.listed_date.strftime('%Y%m%d'), cell.delisted_date.strftime('%Y%m%d')])
    df = pandas.DataFrame(list_2D, columns=['stock_code', 'stock_name', 'List_date', 'Delisting_date'])
    # 写入Excel文件，不包含索引数据
    df.to_csv(dest_file, header=True, index=False)
    print(line_count)


# --------------------------------------------------------------------------------------------------
# https://www.jianshu.com/p/7ce4bbf72639
# 关于pandas写入excel不同sheet的方法(写入excel时显示0开头数字)

# 获取列名，返回列表，组装到存有数据的dataframe；
def get_header():
    """
    获取列名
    return: list
    """
    df1 = pandas.read_clipboard(header=None, dtype=str)
    headers = [i.pop() for i in numpy.array(df1).tolist()]
    return headers


# 获取有空值的数据，并将空值置为null；
def gen_df():
    """针对复制源数据有空值的"""
    df3 = pandas.read_clipboard(header=None, sep=r'\s', dtype=str).replace('nan', '')
    return df3


# 将数据写入excel，可以写入不同sheet
def write_excel(headers, sheet, df_piece=None):
    """
    数据写入到Excel,可以写入不同的sheet
    """
    df2 = pandas.read_clipboard(header=None, dtype=str)
    if df_piece is not None:  # 主要针对有空值得时候，需要拼接df
        df2 = df2.append(df_piece)
    print(df2)

    writer = pandas.ExcelWriter(r'C:\Users\hand\Desktop\result\data.xlsx', engine='openpyxl')
    if os.path.exists(r'C:\Users\hand\Desktop\result\data.xlsx') != True:
        df2.to_excel(writer, sheet_name=sheet, index=None)
    else:
        book = openpyxl.load_workbook(writer.path)
        writer.book = book

    df2.to_excel(writer, index=False, sheet_name=sheet, header=headers)
    writer.save()
    writer.close()


# --------------------------------------------------------------------------------------------------


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
