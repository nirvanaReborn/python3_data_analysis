#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6517191920060465677/
# Pandas数据分析包


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
