# -*- coding: utf-8 -*-

'''numpy 的基本数据类型:
类型            类型表示符                                类型字符码(数据类型简写)
---------------------------------------------------------------------------------------------------
布尔型          bool                                      ?
有符号整数型    int8 / int16 / int32 / int64              i1 / i2 / i4 / i8
无符号整数型    uint8 / uint16 / uint32 / uint64          u1 / u2 / u4 / u8
浮点型          float16 / float32 / float64               f2 / f4 / f8
复数型          complex64 / complex128                    c8 / c16
字符型          str，每个字符用 32 位 Unicode 编码表示    U
日期            datatime64                                M8[Y] / M8[M] / M8[D] / M8[h] / M8[m] / M8[s]
'''
import numpy as np
print(np.__version__)


def test_0():
    # numpy 自定义复合数据类型
    data = [
        ('zs', [99, 98, 90], 17),
        ('ls', [95, 95, 92], 16),
        ('ww', [97, 92, 91], 18)
    ]
    # 姓名 2 个字符
    # 成绩 3 个 int32 类型的数据
    # 年龄 1 个 int32 类型的数据
    arr = np.array(data, dtype='2str, 3int32, int32')
    print(arr)
    print(arr.dtype)  # 打印数组元素的类型: [('f0', '<U2'), ('f1', '<i4', (3,)), ('f2', '<i4')]
    # 可以通过索引访问
    print(arr[0], arr[0][2])
    # ------------------------------------------------
    # 当数据量大时，采用上述方法不便于数据的访问。

    # 采用字典定义列名和元素的数据类型
    arr = np.array(data, dtype={
        # 设置每列的别名
        'names': ['name', 'scores', 'age'],
        # 设置每列数据元素的数据类型
        # 'formats': ['2str', '3int32', 'int32']
        'formats': ['2U', '3i4', 'i4']
    })

    print(arr, arr[0]['age'])

    # 采用列表定义列名和元素的数据类型
    arr = np.array(data, dtype=[
        # 第一列
        ('name', 'str', 2),
        # 第二列
        ('scores', 'int32', 3),
        # 第三列
        ('age', 'int32', 1)
    ])

    print(arr, arr[1]['scores'])

    # 直接访问数组的一列
    print(arr['scores'])
    print(arr.dtype)  # [('name', '<U2'), ('scores', '<i4', (3,)), ('age', '<i4')]


def test_1():
    print('使用列表生成一维数组')
    data = [1, 2, 3, 4, 5, 6]
    x = np.array(data)
    print(type(x))  # <class 'numpy.ndarray'>
    print(x.dtype)  # 打印数组元素的类型: int32
    print(x)  # 打印数组


def test_2():
    print('使用列表生成二维数组')
    data = [[1, 2], [3, 4], [5, 6]]
    x = np.array(data)
    print(type(x))  # <class 'numpy.ndarray'>
    print(x.dtype)  # 打印数组元素的类型: int32
    print(x)  # 打印数组
    print(x.ndim)  # 打印数组的维度
    print(x.shape)  # 打印数组各个维度的长度。shape是一个元组


def test_3():
    print('使用zero/ones/empty创建数组:根据shape来创建')
    x = np.zeros(6)  # 创建一维长度为6的，元素都是0一维数组
    print(x)
    x = np.zeros((2, 3))  # 创建一维长度为2，二维长度为3的二维0数组
    print(x)
    x = np.ones((2, 3))  # 创建一维长度为2，二维长度为3的二维1数组
    print(x)
    x = np.empty((3, 3))  # 创建一维长度为2，二维长度为3,未初始化的二维数组
    print(x)


def test_4():
    print('使用arrange生成连续元素')
    print(np.arange(6))  # [0,1,2,3,4,5,] 开区间
    print(np.arange(0, 6, 2))  # [0, 2，4]


def test_5():
    print('生成指定元素类型的数组:设置dtype属性')
    x = np.array([1, 2.6, 3], dtype=np.int64)
    print(x)  # 元素类型为int64
    print(x.dtype)
    x = np.array([1, 2, 3], dtype=np.float64)
    print(x)  # 元素类型为float64
    print(x.dtype)


def test_6():
    print('使用astype复制数组，并转换类型')
    x = np.array([1, 2.6, 3], dtype=np.float64)
    y = x.astype(np.int32)
    print(y)  # [1 2 3]
    print(x)  # [ 1.   2.6  3. ]
    z = y.astype(np.float64)
    print(z)  # [ 1.  2.  3.]


def test_7():
    print('将字符串元素转换为数值元素')
    x = np.array(['1', '2', '3'], dtype=np.string_)
    y = x.astype(np.int32)
    print(x)  # ['1' '2' '3']
    print(y)  # [1 2 3] 若转换失败会抛出异常


def test_8():
    print('使用其他数组的数据类型作为参数')
    x = np.array([1., 2.6, 3.], dtype=np.float32)
    y = np.arange(3, dtype=np.int32)
    print(y)  # [0 1 2]
    print(y.astype(x.dtype))  # [ 0.  1.  2.]


def test_9():
    print('ndarray数组与标量/数组的运算')
    x = np.array([1, 2, 3])
    print(x * 2)  # [2 4 6]
    print(x > 2)  # [False False  True]
    y = np.array([3, 4, 5])
    print(x + y)  # [4 6 8]
    print(x > y)  # [False False False]


def test_10():
    print('ndarray的基本索引')
    x = np.array([[1, 2], [3, 4], [5, 6]])
    print(x[0])  # [1,2]
    print(x[0][1])  # 2,普通python数组的索引
    print(x[0, 1])  # 同x[0][1]，ndarray数组的索引
    x = np.array([[[1, 2], [3, 4]], [[5, 6], [7, 8]]])
    print(x[0])  # [[1 2],[3 4]]
    y = x[0].copy()  # 生成一个副本
    z = x[0]  # 未生成一个副本
    print(y)  # [[1 2],[3 4]]
    print(y[0, 0])  # 1
    y[0, 0] = 0
    z[0, 0] = -1
    print(y)  # [[0 2],[3 4]]
    print(x[0])  # [[-1 2],[3 4]]
    print(z)  # [[-1 2],[3 4]]


def test_11():
    print('ndarray的切片')
    x = np.array([1, 2, 3, 4, 5])
    print(x[1:3])  # [2,3] 右边开区间
    print(x[:3])  # [1,2,3] 左边默认为 0
    print(x[1:])  # [2,3,4,5] 右边默认为元素个数
    print(x[0:4:2])  # [1,3] 下标递增2
    x = np.array([[1, 2], [3, 4], [5, 6]])
    print(x[:2])  # [[1 2],[3 4]]
    print(x[:2, :1])  # [[1],[3]]
    x[:2, :1] = 0
    print(x)  # [[0,2],[0,4],[5,6]]
    x[:2, :1] = [[8], [6]]
    print(x)  # [[8,2],[6,4],[5,6]]


def test_12():
    print('ndarray的布尔型索引')
    x = np.array([3, 2, 3, 1, 3, 0])
    # 布尔型数组的长度必须跟被索引的轴长度一致
    y = np.array([True, False, True, False, True, False])
    print(x[y])  # [3,3,3]
    print(x[y == False])  # [2,1,0]
    print(x >= 3)  # [ True False  True False  True  False]
    print(x[~(x >= 3)])  # [2,1,0]
    print((x == 2) | (x == 1))  # [False  True False  True False False]
    print(x[(x == 2) | (x == 1)])  # [2 1]
    x[(x == 2) | (x == 1)] = 0
    print(x)  # [3 0 3 0 3 0]


def test_13():
    print('ndarray的花式索引:使用整型数组作为索引')
    x = np.array([1, 2, 3, 4, 5, 6])
    print(x[[0, 1, 2]])  # [1 2 3]
    print(x[[-1, -2, -3]])  # [6,5,4]
    x = np.array([[1, 2], [3, 4], [5, 6]])
    print(x[[0, 1]])  # [[1,2],[3,4]]
    print(x[[0, 1], [0, 1]])  # [1,4] 打印x[0][0]和x[1][1]
    print(x[[0, 1]][:, [0, 1]])  # 打印01行的01列 [[1,2],[3,4]]
    # 使用numpy.ix_()函数增强可读性
    print(x[np.ix_([0, 1], [0, 1])])  # 同上 打印01行的01列 [[1,2],[3,4]]
    x[[0, 1], [0, 1]] = [0, 0]
    print(x)  # [[0,2],[3,0],[5,6]]


def test_14():
    print('ndarray数组的转置和轴对换')
    k = np.arange(9)  # [0,1,....8]
    m = k.reshape((3, 3))  # 改变数组的shape复制生成2维的，每个维度长度为3的数组
    print(k)  # [0 1 2 3 4 5 6 7 8]
    print(m)  # [[0 1 2] [3 4 5] [6 7 8]]
    # 转置(矩阵)数组：T属性 : mT[x][y] = m[y][x]
    print(m.T)  # [[0 3 6] [1 4 7] [2 5 8]]
    # 计算矩阵的内积 xTx
    print(np.dot(m, m.T))  # np.dot点乘
    # 高维数组的轴对象
    k = np.arange(8).reshape(2, 2, 2)
    print(k)  # [[[0 1],[2 3]],[[4 5],[6 7]]]
    print(k[1][0][0])
    # 轴变换 transpose
    m = k.transpose((1, 0, 2))  # m[y][x][z] = k[x][y][z]
    print(m)  # [[[0 1],[4 5]],[[2 3],[6 7]]]
    print(m[0][1][0])
    # 轴交换 swapaxes (axes：轴)
    m = k.swapaxes(0, 1)  # 将第一个轴和第二个轴交换 m[y][x][z] = k[x][y][z]
    print(m)  # [[[0 1],[4 5]],[[2 3],[6 7]]]
    print(m[0][1][0])
    # 使用轴交换进行数组矩阵转置
    m = np.arange(9).reshape((3, 3))
    print(m)  # [[0 1 2] [3 4 5] [6 7 8]]
    print(m.swapaxes(1, 0))  # [[0 3 6] [1 4 7] [2 5 8]]


def test_15():
    print('一元ufunc示例')
    x = np.arange(6)
    print(x)  # [0 1 2 3 4 5]
    print(np.square(x))  # [ 0  1  4  9 16 25]
    x = np.array([1.5, 1.6, 1.7, 1.8])
    y, z = np.modf(x)
    print(y)  # [ 0.5  0.6  0.7  0.8]
    print(z)  # [ 1.  1.  1.  1.]


def test_16():
    print('二元ufunc示例')
    x = np.array([[1, 4], [6, 7]])
    y = np.array([[2, 3], [5, 8]])
    print(np.maximum(x, y))  # [[2,4],[6,8]]
    print(np.minimum(x, y))  # [[1,3],[5,7]]


def test_17():
    print('where函数的使用')
    cond = np.array([True, False, True, False])
    x = np.where(cond, -2, 2)
    print(x)  # [-2  2 -2  2]
    cond = np.array([1, 2, 3, 4])
    x = np.where(cond > 2, -2, 2)
    print(x)  # [ 2  2 -2 -2]
    y1 = np.array([-1, -2, -3, -4])
    y2 = np.array([1, 2, 3, 4])
    x = np.where(cond > 2, y1, y2)  # 长度须匹配
    print(x)  # [1,2,-3,-4]


def test_18():
    print('where函数的嵌套使用')
    y1 = np.array([-1, -2, -3, -4, -5, -6])
    y2 = np.array([1, 2, 3, 4, 5, 6])
    y3 = np.zeros(6)
    cond = np.array([1, 2, 3, 4, 5, 6])
    x = np.where(cond > 5, y3, np.where(cond > 2, y1, y2))
    print(x)  # [ 1.  2. -3. -4. -5.  0.]


def test_19():
    print('numpy的基本统计方法')
    x = np.array([[1, 2], [3, 3], [1, 2]])  # 同一维度上的数组长度须一致
    print(x.mean())  # 2
    print(x.mean(axis=1))  # 对每一行的元素求平均
    print(x.mean(axis=0))  # 对每一列的元素求平均
    print(x.sum())  # 同理 12
    print(x.sum(axis=1))  # [3 6 3]
    print(x.max())  # 3
    print(x.max(axis=1))  # [2 3 2]
    print(x.cumsum())  # [ 1  3  6  9 10 12]
    print(x.cumprod())  # [ 1  2  6 18 18 36]


def test_20():
    print('用于布尔数组的统计方法')
    x = np.array([[True, False], [True, False]])
    print(x.sum())  # 2
    print(x.sum(axis=1))  # [1,1]
    print(x.any(axis=0))  # [True,False]
    print(x.all(axis=1))  # [False,False]
    print('.sort的就地排序')
    x = np.array([[1, 6, 2], [6, 1, 3], [1, 5, 2]])
    x.sort(axis=1)
    print(x)  # [[1 2 6] [1 3 6] [1 2 5]]
    # 非就地排序：np.sort()可产生数组的副本


def test_21():
    print('ndarray的唯一化和集合运算')
    x = np.array([[1, 6, 2], [6, 1, 3], [1, 5, 2]])
    print(np.unique(x))  # [1,2,3,5,6]
    y = np.array([1, 6, 5])
    print(np.in1d(x, y))  # [ True  True False  True  True False  True  True False]
    print(np.setdiff1d(x, y))  # [2 3]
    print(np.intersect1d(x, y))  # [1 5 6]


def test_22():
    print('ndarray的存取')
    x = np.array([[1, 6, 2], [6, 1, 3], [1, 5, 2]])
    np.save('file1', x)  # 以二进制.npy保存
    np.savetxt('filetxt', x, delimiter=',')  # 以文本保存
    y = np.load('file1.npy')
    print(y)  # [[1 6 2] [6 1 3] [1 5 2]]
    y = np.loadtxt('filetxt', delimiter=',')  # delimiter为分隔符
    print(y)  # [[1 6 2] [6 1 3] [1 5 2]]
    # 压缩保存
    np.savez('filezip', a=x, b=y)
    print(np.load('filezip.npz')['a'])  # 按字典索引  [[1 6 2] [6 1 3] [1 5 2]]


def test_23():
    print('线性代数')
    import numpy.linalg as nla
    print('矩阵点乘')
    x = np.array([[1, 2], [3, 4]])
    y = np.array([[1, 3], [2, 4]])
    print(x.dot(y))  # [[ 5 11][11 25]]
    print(np.dot(x, y))  # # [[ 5 11][11 25]]
    print('矩阵求逆')
    x = np.array([[1, 1], [1, 2]])
    y = nla.inv(x)  # 矩阵求逆（若矩阵的逆存在）
    print(x.dot(y))  # 单位矩阵 [[ 1.  0.][ 0.  1.]]
    print(nla.det(x))  # 求行列式


def test_24():
    print('np.random随机数生成')
    import numpy.random as npr

    x = npr.randint(0, 2, size=100000)  # 抛硬币
    print((x > 0).sum())  # 正面的结果
    print(npr.normal(size=(2, 2)))  # 正态分布随机数数组 shape = (2,2)


def test_25():
    print('ndarray数组重塑')
    x = np.arange(0, 6)  # [0 1 2 3 4]
    print(x)  # [0 1 2 3 4]
    print(x.reshape((2, 3)))  # [[0 1 2][3 4 5]]
    print(x)  # [0 1 2 3 4]
    print(x.reshape((2, 3)).reshape((3, 2)))  # [[0 1][2 3][4 5]]
    y = np.array([[1, 1, 1], [1, 1, 1]])
    x = x.reshape(y.shape)
    print(x)  # [[0 1 2][3 4 5]]
    print(x.flatten())  # [0 1 2 3 4 5]
    x.flatten()[0] = -1  # flatten返回的是拷贝
    print(x)  # [[0 1 2][3 4 5]]
    print(x.ravel())  # [0 1 2 3 4 5]
    x.ravel()[0] = -1  # ravel返回的是视图（引用）
    print(x)  # [[-1 1 2][3 4 5]]
    print("维度大小自动推导")
    arr = np.arange(15)
    print(arr.reshape((5, -1)))  # 15 / 5 = 3


def test_26():
    print('数组的合并与拆分')
    x = np.array([[1, 2, 3], [4, 5, 6]])
    y = np.array([[7, 8, 9], [10, 11, 12]])
    print(np.concatenate([x, y], axis=0))  # 竖直组合 [[ 1  2  3][ 4  5  6][ 7  8  9][10 11 12]]
    print(np.concatenate([x, y], axis=1))  # 水平组合 [[ 1  2  3  7  8  9][ 4  5  6 10 11 12]]
    print('垂直stack与水平stack')
    print(np.vstack((x, y)))  # 垂直堆叠:相对于垂直组合
    print(np.hstack((x, y)))  # 水平堆叠：相对于水平组合
    # dstack：按深度堆叠
    print(np.split(x, 2, axis=0))  # 按行分割 [array([[1, 2, 3]]), array([[4, 5, 6]])]
    print(np.split(x, 3, axis=1))  # 按列分割 [array([[1],[4]]), array([[2],[5]]), array([[3],[6]])]


def test_27():
    print('数组的元素重复操作')
    x = np.array([[1, 2], [3, 4]])
    print(x.repeat(2))  # 按元素重复 [1 1 2 2 3 3 4 4]
    print(x.repeat(2, axis=0))  # 按行重复 [[1 2][1 2][3 4][3 4]]
    print(x.repeat(2, axis=1))  # 按列重复 [[1 1 2 2][3 3 4 4]]
    x = np.array([1, 2])
    print(np.tile(x, 2))
    print(np.tile(x, (2, 2)))  # 指定从低位到高位依次复制的次数。


def test_28():
    '''意思是一个正态分布
    参数loc(float)：正态分布的均值，对应着这个分布的中心。loc=0说明这一个以Y轴为对称轴的正态分布，
    参数scale(float)：正态分布的标准差，对应分布的宽度，scale越大，正态分布的曲线越矮胖，scale越小，曲线越高瘦。
    参数size(int 或者整数元组)：输出的值赋在shape里，默认为None。
    :return:返回包含 size 个元素的一维数组
    '''
    arr = np.random.normal(loc=0, scale=1, size=1000)
    print(arr)


# 使用 ndarray 保存日期数据类型
def test_29():
    '''
    1.日期字符串支持不支持 2011/11/11，使用空格进行分隔日期也不支持 2011 11 11，支持 2011-11-11
    2.日期与时间之间需要有空格进行分隔 2011-04-01 10:10:10
    3.时间的书写格式 10:10:10
    '''
    dates = [
        '2011',
        '2011-02',
        '2011-02-03',
        '2011-04-01 10:10:10'
    ]

    ndates = np.array(dates)
    print(ndates, ndates.dtype)

    # 数据类型为日期类型，采用 64 位二进制进行存储，D 表示日期精确到天
    ndates = ndates.astype('datetime64[D]')
    print(ndates, ndates.dtype)

    # 日期运算
    print(ndates[-1] - ndates[0])


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
