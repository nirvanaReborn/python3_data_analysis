#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/frostime/article/details/104809060
# 将HTML以树形结构打印

import json
import re
from queue import Queue
from pprint import pprint

from lxml import etree


class Node:
    def __init__(self, text, level):
        self.text = text
        self.level = level
        self.children = []

    def addChild(self, node):
        self.children.append(node)

    def empty(self):
        return len(self.children) == 0

    def __str__(self):
        return self.text


def printTree(node, indent: list, final_node=True):
    """打印树结构的算法
        Parameters:
            node: 节点
            indent: 记录了节点之前需要打印的信息
            final_node: node 是否是最后一个节点（i.e. 没有下一个 sibling 了）
    """
    for i in range(node.level):
        print(indent[i], end='')
    if final_node:
        print('└──', end='')
    else:
        print('├──', end='')
    print(node.text)

    if node.empty():
        return
    else:
        cnt = len(node.children)
        for i, n in enumerate(node.children):
            c = '    ' if final_node else '│   '
            indent.append(c)
            last_node = i == cnt - 1
            printTree(n, indent, last_node)
            del indent[-1]


def inspectElem(elem: etree.Element, level=0):
    """递归地建立 HTML 树"""
    tag = str(elem.tag)
    node = Node(tag, level)
    for child in elem.iterchildren():
        sub_node = inspectElem(child, level + 1)
        node.addChild(sub_node)
    return node


if __name__ == "__main__":
    f_path = r"8_sample.html"
    with open(f_path, encoding='utf-8') as f:
        html = f.read()
    tree = etree.HTML(html)
    node = inspectElem(tree)
    indent = []
    print('.')
    printTree(node, indent, True)
