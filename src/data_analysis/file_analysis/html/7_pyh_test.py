#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 生成 HTML 的方法
# https://blog.csdn.net/weixin_41697143/article/details/87607578
import pyh


def main():
    page = pyh.PyH('My wonderful PyH page')
    page.addCSS('myStylesheet1.css', 'myStylesheet2.css')
    page.addJS('myJavascript1.js', 'myJavascript2.js')
    page << pyh.h1('My big title', cl='center')
    page << pyh.div(cl='myCSSclass1 myCSSclass2', id='myDiv1') << pyh.p('I love PyH!', id='myP1')
    mydiv2 = page << pyh.div(id='myDiv2')
    mydiv2 << pyh.h2('A smaller title') + pyh.p('Followed by a paragraph.')
    page << pyh.div(id='myDiv3')
    page.myDiv3.attributes['cl'] = 'myCSSclass3'
    page.myDiv3 << pyh.p('Another paragraph')
    page.printOut('a.html')


if __name__ == "__main__":
    main()
