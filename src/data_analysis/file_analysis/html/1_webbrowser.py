#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import webbrowser


def test_1():
    dest_file = r"1.html"
    message = """
    <html>
    <head></head>
    <body>
    <div style="width:1700;border:1px red solid;">
    %s
    </div>
    </body>
    </html>
    """ % ('test')

    with open(dest_file, 'w') as fw:
        fw.write(message)

    # 自动在网页中显示
    webbrowser.open(dest_file, new=1)


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
