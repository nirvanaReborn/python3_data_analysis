#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/masako/p/5868367.html
'''
HtmlParser是一个类，在使用时一般继承它然后重载它的方法，来达到解析出需要的数据的目的。
　　1.常用属性：
　　　　lasttag，保存上一个解析的标签名，是字符串。

　　2.常用方法：　
　　　　handle_starttag(tag, attrs) ，处理开始标签，比如<div>；这里的attrs获取到的是属性列表，属性以元组的方式展示
　　　　handle_endtag(tag) ，处理结束标签,比如</div>
　　　　handle_startendtag(tag, attrs) ，处理自己结束的标签，如<img />
　　　　handle_data(data) ，处理数据，标签之间的文本
　　　　handle_comment(data) ，处理注释，<!-- -->之间的文本
'''
import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# logging 库获取日志 requests 和 urllib3(requests 依赖) 对象，调低他们的日志级别，
# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)
from html.parser import HTMLParser
import codecs
import requests


class MyHTMLParser(HTMLParser):

    def handle_starttag(self, tag, attrs):
        """
        recognize start tag, like <div>
        :param tag:
        :param attrs:
        :return:
        """
        print("Encountered a start tag:", tag)

    def handle_endtag(self, tag):
        """
        recognize end tag, like </div>
        :param tag:
        :return:
        """
        print("Encountered an end tag :", tag)

    def handle_data(self, data):
        """
        recognize data, html content string
        :param data:
        :return:
        """
        # print("Encountered some data  :", data)
        # 获取所有p标签的文本，最简单方法只修改handle_data
        if self.lasttag == 'p':
            print("Encountered p data  :", data)

    def handle_startendtag(self, tag, attrs):
        """
        recognize tag that without endtag, like <img />
        :param tag:
        :param attrs:
        :return:
        """
        print("Encountered startendtag :", tag)

    def handle_comment(self, data):
        """
        :param data:
        :return:
        """
        print("Encountered comment :", data)

    def _attr(self, attrlist, attrname):
        for attr in attrlist:
            if attr[0] == attrname:
                return attr[1]
        return None


class CommentHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)

    def handle_comment(self, data):
        cparser = MyHTMLParser()
        cparser.feed(data)


def main():
    html_file = "5.html"
    html = codecs.open(html_file, "r", 'UTF-8').read()
    # html = requests.get(url).text
    parser = CommentHTMLParser()
    parser.handle_comment(html)


if __name__ == "__main__":
    main()
