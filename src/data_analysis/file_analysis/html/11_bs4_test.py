#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 企业员工法律风险及安全意识题库合并

from bs4 import BeautifulSoup
import collections
import pprint

dict_question = collections.defaultdict(list)
dict_option = {
    0: "A. ",
    1: "B. ",
    2: "C. ",
    3: "D. ",
    4: "E. ",
    5: "F. ",
    6: "G. ",
}


def bs4_paraser(html):
    all_value = []
    value = {}
    soup = BeautifulSoup(html, 'html.parser')
    all_div = soup.find_all('div', attrs={'class': 'rt'})
    for row in all_div:
        div_question = row.find('div', attrs={'class': 'tit'})
        if div_question.find('span'):
            temp = div_question.find('span').string  # 正确答案
            key = str(div_question.contents).replace(str(div_question.find('span')), "") + temp
        else:
            key = div_question.string
        div_option = row.find('div', attrs={'class': 'as'})
        if div_option:
            all_div_option = div_option.find_all('li')
            for i, item in enumerate(all_div_option):
                # print(div_question, dict_option[i] + item.string)
                dict_question[key].append(dict_option[i] + item.string)
        else:
            dict_question[key].append("")
    return dict_question


def main():
    list_file = [
        r"D:\2018考试结果-1.html",
        r"D:\2018考试结果-0.html",
        r"D:\2019考试结果-0.html"
    ]

    for source_file in list_file:
        with open(source_file, 'r', encoding='UTF-8') as fr:
            html = fr.read()
            bs4_paraser(html)

    pprint.pprint(dict_question)


if __name__ == "__main__":
    main()
