#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 利用正则表达式解析html文件

import re
import os
from public_function import Excel_Pandas


def analysis_html(html):
    list_result = []
    with open(html, 'r', encoding='UTF-8') as f_origin:
        list_re = re.findall('<table.*class="legendTable">(.*?)</table>', f_origin.read(), re.S)
        # print('Total:' + str(len(list_re)))
        for line in list_re:
            # print(line)
            list_header = re.findall('<tr class="legendHeader">(.*?)</tr>', line, re.S)
            # print(list_header)
            for header in list_header:
                list_head = re.findall('<td>(.*?)</td>', header, re.S)
                if len(list_head) > 0:
                    list_head.pop(0)
                    list_result.append(list_head)

            list_body = re.findall('<tr class="legendRow">(.*?)</tr>', line, re.S)
            for body in list_body:
                list_row = re.findall('<td>(.*?)</td>', body, re.S)
                if len(list_row) > 0:
                    list_result.append(list_row)
    for i, fileLine in enumerate(list_result):
        for j, item in enumerate(fileLine):
            list_result[i][j] = str(list_result[i][j]).strip("\r\n")
    return list_result


def main():
    root_dir = r"D:\share\git\gitee\zhangtao2016\course_python\src\other\shencong\Report"
    list_file = [r'Report4.html', r"Report5.html"]

    dict_file = {}

    for item in list_file:
        key = item.split(".")[0]
        dict_file[key] = os.path.join(root_dir, item)

    dest_file = r"Report.xlsx"
    str_header = "分类 网址 新增日期 名称"
    Excel_Pandas(dest_file).create_excel_by_function(dict_file, str_header, analysis_html)


if __name__ == "__main__":
    main()
