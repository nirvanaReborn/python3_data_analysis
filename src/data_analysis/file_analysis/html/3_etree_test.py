#!/usr/bin/env python
# -*- coding: UTF-8 -*-


'''
# http://blog.csdn.net/u010154424/article/details/52273868
# Python解析html的几种操作方式

# http://www.jianshu.com/p/2ae6d51522c3
# 学习lxml解析html

# https://www.cnblogs.com/xieqiankun/p/lxmlencoding.html
# 使用etree.HTML的编码问题
'''
import gzip
import io
import re
import urllib.request
from lxml import etree


# lxml.html的方式进行解析
def lxml_parser(page):
    data = []
    doc = etree.HTML(page)
    all_div = doc.xpath('//div[@class="yingping-list-wrap"]')
    for row in all_div:
        # 获取每一个影评，即影评的item
        all_div_item = row.xpath('.//div[@class="item"]')  # find_all('div', attrs={'class': 'item'})
        for r in all_div_item:
            value = {}
            # 获取影评的标题部分
            title = r.xpath('.//div[@class="g-clear title-wrap"][1]')
            value['title'] = title[0].xpath('./a/text()')[0]
            value['title_href'] = title[0].xpath('./a/@href')[0]
            score_text = title[0].xpath('./div/span/span/@style')[0]
            score_text = re.search(r'\d+', score_text).group()
            value['score'] = int(score_text) / 20
            # 时间
            value['time'] = title[0].xpath('./div/span[@class="time"]/text()')[0]
            # 多少人喜欢
            value['people'] = int(
                re.search(r'\d+', title[0].xpath('./div[@class="num"]/span/text()')[0]).group())
            data.append(value)
    return data


# 把传递解析函数，便于下面的修改
def get_html(url, paraser=lxml_parser):
    headers = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Host': 'www.360kan.com',
        'Proxy-Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
    }
    request = urllib.request.Request(url, headers=headers)
    response = urllib.request.urlopen(request)
    response.encoding = 'utf-8'
    if response.code == 200:
        content = response.read()  # content是压缩过的数据
        # data = io.StringIO(response.read())
        data = io.BytesIO(content)  # 把content转为文件对象
        gzipper = gzip.GzipFile(fileobj=data)
        data = gzipper.read()
        value = paraser(data)  # open('E:/h5/haPkY0osd0r5UB.html').read()
        return value
    else:
        pass


def main():
    url = 'http://www.360kan.com/m/haPkY0osd0r5UB.html'
    value = get_html(url, paraser=lxml_parser)
    for row in value:
        print(row)


if __name__ == '__main__':
    main()
