#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
https://www.cnblogs.com/AlwinXu/p/5492033.html
标准库中的定义
class html.parser.HTMLParser(*, convert_charrefs=True)
1.HTMLParser主要是用来解析HTML文件（包括HTML中无效的标记）
2.参数convert_charrefs表示是否将所有的字符引用自动转化为Unicode形式，Python3.5以后默认是True
3.HTMLParser可以接收相应的HTML内容，并进行解析，遇到HTML的标签会自动调用相应的handler（处理方法）来处理，用户需要自己创建相应的子类来继承HTMLParser，并且复写相应的handler方法
4.HTMLParser不会检查开始标签和结束标签是否是一对

常用方法
HTMLParser.feed(data)：接收一个字符串类型的HTML内容，并进行解析
HTMLParser.close()：当遇到文件结束标签后进行的处理方法。如果子类要复写该方法，需要首先调用HTMLParser累的close()
HTMLParser.reset():重置HTMLParser实例，该方法会丢掉未处理的html内容
HTMLParser.getpos()：返回当前行和相应的偏移量
HTMLParser.handle_starttag(tag, attrs)：对开始标签的处理方法。例如<div id="main">，参数tag指的是div，attrs指的是一个（name,Value)的列表
HTMLParser.handle_endtag(tag)：对结束标签的处理方法。例如</div>，参数tag指的是div
HTMLParser.handle_data(data)：对标签之间的数据的处理方法。<tag>test</tag>,data指的是“test”
HTMLParser.handle_comment(data)：对HTML中注释的处理方法。
'''

import codecs
import json
# For python 3.x
from html.parser import HTMLParser


# 定义HTMLParser的子类,用以复写HTMLParser中的方法
class MyHTMLParser(HTMLParser):
    # 构造方法,定义data数组用来存储html中的数据
    def __init__(self):
        HTMLParser.__init__(self)
        self.data = []

    # 覆盖starttag方法,可以进行一些打印操作
    def handle_starttag(self, tag, attrs):
        pass
        # print("Start Tag: ",tag)
        # for attr in attrs:
        #   print(attr)

    # 覆盖endtag方法
    def handle_endtag(self, tag):
        pass

    # 覆盖handle_data方法,用来处理获取的html数据,这里保存在data数组
    def handle_data(self, data):
        if data.count('\n') == 0:
            self.data.append(data)


def main():
    # 读取本地html文件.(当然也可以用urllib.request中的urlopen来打开网页数据并读取,这里不做介绍)
    htmlFile = codecs.open(r"6_TFS.html", 'r', "utf-8")
    content = htmlFile.read()

    # 创建子类实例
    parser = MyHTMLParser()

    # 将html数据传给解析器进行解析
    parser.feed(content)

    # 对解析后的数据进行相应操作并打印
    for item in parser.data:
        if item.startswith("{\"columns\""):
            payloadDict = json.loads(item)
            list_2D = payloadDict["payload"]["rows"]
            for backlog in list_2D:
                if backlog[1] == "Product Backlog Item" or backlog[1] == "Bug":
                    print(backlog[2], "       Point: ", backlog[3])


if __name__ == "__main__":
    main()
