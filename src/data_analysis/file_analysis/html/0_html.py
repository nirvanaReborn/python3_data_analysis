#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#
import os

from public_function import GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE


def test_0():
    import webbrowser
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "webbrowser_test.py")
    print(call_file)
    os.popen(call_file)


def test_1():
    # pip install beautifulsoup4
    from bs4 import BeautifulSoup
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "beautifulsoup4_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    from lxml import etree
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "lxml_test.py")
    print(call_file)
    os.popen(call_file)


def test_3():
    from html.parser import HTMLParser
    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "html_test.py")
    print(call_file)
    os.popen(call_file)


def test_4():
    import pyh
    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "pyh_test.py")
    print(call_file)
    os.popen(call_file)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
