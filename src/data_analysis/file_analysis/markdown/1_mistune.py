#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/harrylyx/p/12395259.html
# Mistune是所有纯Python实现中最快的一个。
# 在纯Python环境中，几乎比Python- markdown快4倍，在Cython的帮助下，几乎快5倍。
# pip install mistune
import mistune
import os


def test_1():
    # mistune.markdown('I am using **mistune markdown parser**')
    # 如果关心性能，官方推荐使用对象实例化后在进行调用
    markdown = mistune.Markdown()
    markdown('I am using **mistune markdown parser**')


def test_2():
    source_file = 'chrome_bookmarks_20220827_225057.md'
    with open(source_file, 'r', encoding='UTF-8') as f:
        text = f.read()

    # escape: 设置未false，则所有的HTML将不会被转义.
    # hard_wrap: 设置为True，它将支持GFM换行特性。所有的新行将被替换为tag
    # use_xhtml: 如果设置为True，所有的标签必须符合xhtml规范
    # parse_block_html: 仅在块级别HTML中分析文本
    # parse_inline_html: 仅在内联级别HTML中分析文本
    renderer = mistune.Renderer(escape=True, hard_wrap=True)
    # use this renderer instance
    markdown = mistune.Markdown(renderer=renderer)
    html = markdown(text)

    # 使用默认的解析器，可以用如下方法：
    # mistune.markdown(text, escape=True, hard_wrap=True)
    # markdown = mistune.Markdown(escape=True, hard_wrap=True)
    # html = markdown(text)

    dest_file = os.path.splitext(source_file)[0] + ".html"
    print(dest_file)
    with open(dest_file, 'w', encoding='UTF-8') as f:
        f.write(html)


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
