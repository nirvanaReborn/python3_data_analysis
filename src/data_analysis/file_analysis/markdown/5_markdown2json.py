#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/sinat_33718563/article/details/125266379
# Markdown转Json
import os

# pip3 install markdown-to-json
# md_to_json -h
PYTHON_MD2JSON_PATH = os.path.expanduser(r"~\AppData\Roaming\Python\Python37\Scripts\md_to_json.exe")
import markdown_to_json.scripts.md_to_json as md2json


# 需要改造 markdown_to_json/scripts/md_to_json.py中的 jsonify_markdown 函数


def test_1():
    source_file = 'chrome_bookmarks_20220827_225057.md'
    dest_file = os.path.splitext(source_file)[0] + ".json"
    list_cmd = [
        PYTHON_MD2JSON_PATH,
        source_file,
        "-o",
        dest_file,
    ]
    os.system(" ".join(list_cmd))


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
