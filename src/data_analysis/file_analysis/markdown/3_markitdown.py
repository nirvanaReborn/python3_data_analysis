#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/20383472123
# https://github.com/microsoft/markitdown
# MarkItDown不仅支持常见的Office文档格式，还支持图像、音频、HTML等多种文件类型。
# 通过使用该工具，用户可以轻松地将复杂的文档转换为简洁的Markdown格式，便于后续处理和分享。
# 支持多种文件格式：PDF、Word、Excel、PowerPoint、图像、音频、HTML等。
# OCR和语音转文字：对于图像和音频文件，MarkItDown支持提取EXIF元数据，并进行OCR或语音转文字。
# 大型语言模型支持：用户可以配置MarkItDown使用OpenAI等大型语言模型来描述图像内容。
"""外部库集成
mammoth: DOCX 转 HTML
markdownify: HTML 转 Markdown
pandas: 处理 Excel 文件
pdfminer: 提取 PDF 文本
BeautifulSoup: 解析 HTML 和 XML

markitdown 在线工具：https://markitdown.pro/
"""
# pip install markitdown
import markitdown

def test_1():
    pass
    # 命令行工具
    # markitdown path-to-file.pdf > document.md


def test_2():
    md = markitdown.MarkItDown()
    result = md.convert("test.xlsx")
    print(result.text_content)


def test_3():
    from openai import OpenAI

    client = OpenAI()
    md = markitdown.MarkItDown(llm_client=client, llm_model="gpt-4o")
    result = md.convert("example.jpg")
    print(result.text_content)


def test_4():
    pass
    # Docker 支持
    # md = markitdown.MarkItDown(llm_client=None, llm_model="gpt-4o")
    # 构建镜像：docker build -t markitdown:latest .
    # 运行镜像: docker run --rm -i markitdown:latest < ~/your-file.pdf > output.md


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
