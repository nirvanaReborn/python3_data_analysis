#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 用Python实现浏览器书签导出到excel

import re
# from public_function import Excel_Pandas
from public_function import CSV


# read the original bookmarks html, filter the link and text of <a>
def analysis_bookmarks(html):
    list_link = []
    with open(html, 'r', encoding='UTF-8') as f_origin:
        html_file = f_origin.read()
        lines = re.findall('<DT><H3 ADD_DATE="(\d+)" LAST_MODIFIED="(\d+)">(.*?)</H3>.*?<DL><p>(.*?)</DL><p>', html_file, re.S)
        for line in lines:
            # print(line[3])
            links = re.findall('<DT><A HREF="(.*?)" ADD_DATE="(\d+)".*?>(.*?)</A>', line[3], re.S)
            for link in links:
                # print(link)
                list_temp = []
                list_temp.append(line[2])
                list_temp.extend(link)
                list_link.append(list_temp)
    return list_link


def main():
    list_html = [
        r'Chrome_bookmarks_2018_2_25.html',
        r"Chrome_bookmarks_2018_3_16.html",
        r"Chrome_bookmarks_2021_10_31.html",
        r"edge_favorites_2021_10_31.html",
        r"Firefox_bookmarks.html",
        r"UC书签备份_2018_1_7.html",
        r"UC书签备份_2018_2_25.html",
    ]

    dict_source_file = {}
    dict_head = {}
    dict_sheet = {}
    str_header = "分类 网址 新增日期 名称"
    for html in list_html:
        key = html.split(".")[0]
        dict_source_file[key] = html
        dict_head[key] = str_header
        dict_sheet[key] = analysis_bookmarks(html)

    dest_file = r"bookmarks.xlsx"
    # Excel_Pandas(dest_file).create_excel_by_function(dict_source_file, str_header, analysis_bookmarks)
    # CSV("bookmarks").create_csv(dict_sheet, dict_head)


if __name__ == "__main__":
    main()
