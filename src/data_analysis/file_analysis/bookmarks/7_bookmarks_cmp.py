#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import os
import platform
import json
import sqlite3
import time
import re


def process_html(source_file):
    list_1D = []
    with open(source_file, 'r', encoding='utf-8') as f:
        for i, fileLine in enumerate(f.readlines()):
            newFileLine = re.sub('ADD_DATE=\"\d+\"', 'ADD_DATE=\"0\"', fileLine)
            newFileLine = re.sub('LAST_MODIFIED=\"\d+\"', 'LAST_MODIFIED=\"0\"', newFileLine)
            newFileLine = re.sub('ICON=\"(.*?)\"', '', newFileLine)
            list_1D.append(newFileLine)
    dest_file = source_file + "_new.html"
    with open(dest_file, 'w', encoding='utf-8') as f:
        f.writelines(list_1D)


def main():
    list_html = [
        r'D:\bookmarks_{0}_{1}_{2}.html'.format(time.strftime("%Y"), int(time.strftime("%m")), int(time.strftime("%d"))),
        r'D:\favorites_{0}_{1}_{2}.html'.format(time.strftime("%Y"), int(time.strftime("%m")), int(time.strftime("%d"))),
    ]

    for source_file in list_html:
        if os.path.exists(source_file):
            process_html(source_file)
        else:
            print("文件不存在", source_file)


if __name__ == "__main__":
    main()
