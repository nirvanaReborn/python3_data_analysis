#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jb51.net/article/236904.htm
# 解析 Chrome 书签并写入到markdown文件
# https://raw.githubusercontent.com/wxnacy/study/master/python/simple/print_chrome_bookmarks.py
'''
打开 chrome浏览器,输入 chrome://version，进入浏览器版本信息页面,找到"个人资料路径";
浏览器的收藏夹的数据，记录在Bookmarks文件里面,而Bookmark文件的内容是json格式。
'''

import os
import platform
import json
import sqlite3


def get_bookmarks_path(webdriver='chrome'):
    def get_chrome_bookmarks_path():
        '''获取chrome浏览器书签地址'''
        if platform.system() == "Darwin":
            path = os.path.expanduser(r"~/Library/Application Support/Google/Chrome/Default")
        elif platform.system() == "Linux":
            path = os.path.expanduser(r"~/.config/google-chrome/Default")
        elif platform.system() == "Windows":
            path = os.path.expanduser(r"~\AppData\Local\Google\Chrome\User Data\Default")
            # path = os.environ["LOCALAPPDATA"] + r"\Google\Chrome\User Data\Default"
            # input_filename = os.environ["LOCALAPPDATA"] + r"\Packages\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\AC\MicrosoftEdge\User\Default\Favorites"
        else:
            path = None
            print('Your system ("{}") is not recognized. Please specify the input file manually.'.format(platform.system()))
        return path

    def get_edge_bookmarks_path():
        '''获取edge浏览器书签地址'''
        if platform.system() == "Darwin":
            path = None
        elif platform.system() == "Linux":
            path = None
        elif platform.system() == "Windows":
            path = os.path.expanduser(r"~\AppData\Local\Microsoft\Edge\User Data\Profile 1")
            # path = os.environ["LOCALAPPDATA"] + r"\Microsoft\Edge\User Data\Profile 1"
        else:
            path = None
            print('Your system ("{}") is not recognized. Please specify the input file manually.'.format(platform.system()))
        return path

    dict_bookmarks_path = {
        "chrome": get_chrome_bookmarks_path(),
        "edge": get_edge_bookmarks_path(),
    }
    return dict_bookmarks_path.get(webdriver, None)


# chrome browser bookmark
class BookMark:
    def __init__(self, chromePath=get_bookmarks_path()):
        # chromepath
        self.chromePath = chromePath
        self.chromeBookmarksPath = os.path.join(self.chromePath, 'Bookmarks')
        # parse bookmarks
        with open(self.chromeBookmarksPath, encoding='utf-8') as f:
            bookmarks = json.loads(f.read())
        self.bookmarks = bookmarks
        # folders
        self.folders = self.get_folders()

    def get_folders(self):
        # folders
        names = [
            (i, self.bookmarks['roots'][i]['name'])
            for i in self.bookmarks['roots']
        ]
        return names

    def get_folder_data(self, folder=0):
        return self.bookmarks['roots'][self.folders[folder][0]]['children']

    def set_chrome_path(self, chromePath):
        self.chromePath = chromePath

    def refresh(self):
        'update chrome data from chrome path'
        # parse bookmarks
        with open(self.chromeBookmarksPath, encoding='utf-8') as f:
            bookmarks = json.loads(f.read())
        self.bookmarks = bookmarks


# History
class History:
    def __init__(self, chromePath=get_bookmarks_path()):
        self.chromePath = chromePath
        self.chromeHistoryPath = os.path.join(self.chromePath, 'History')

    def connect(self):
        self.conn = sqlite3.connect(self.chromeHistoryPath)
        self.cousor = self.conn.cursor()

    def close(self):
        self.conn.close()

    def get_history(self):
        cursor = self.conn.execute("SELECT id,url,title,visit_count  from urls")
        rows = []
        for _id, url, title, visit_count in cursor:
            row = {}
            row['id'] = _id
            row['url'] = url
            row['title'] = title
            row['visit_count'] = visit_count
            rows.append(row)
        return rows


def main():
    b = BookMark()
    b.get_folder_data()

    h = History()
    h.connect()
    h.get_history()
    h.close()


if __name__ == "__main__":
    main()
