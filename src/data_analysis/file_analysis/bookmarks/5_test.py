#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import json
import argparse
import platform
import re
import os
import sys

# 过滤name
filter_name_list = {'My work', '书签栏', 'websites'}

html_escape_table = {
    "&": "&amp;",
    '"': "&quot;",
    "'": "&#39;",
    ">": "&gt;",
    "<": "&lt;",
}

output_file_template = """
<h3>书签目录</h3>
{catelog}
{bookmark_bar}

{other}
"""

# 目录
catelog = list()


def html_escape(text):
    return ''.join(html_escape_table.get(c, c) for c in text)


def html_for_node(node):
    # 判断url和children即判断是否包含在文件夹中
    if 'url' in node:
        return html_for_url_node(node)
    elif 'children' in node:
        return html_for_parent_node(node)
    else:
        return ''


def html_for_url_node(node):
    if not re.match("javascript:", node['url']):
        return '- [{}]({})\n'.format(node['name'], node['url'])
    else:
        return ''


def html_for_parent_node(node):
    return '{0}\n\n{1}\n'.format(filter_catelog_name(node),
                                 ''.join([filter_name(n) for n in node['children']]))


# 过滤文件夹
def filter_name(n):
    if n['name'] in filter_name_list:
        return ''
    else:
        return html_for_node(n)


# 过滤目录名
def filter_catelog_name(n):
    if n['name'] in filter_name_list:
        return ''
    else:
        catelog.append('- [{0}](#{0})\n'.format(n['name']))
        return '<h4 id={0}>{0}</h4>'.format(n['name'])


def get_chrome_bookmarks():
    if platform.system() == "Darwin":
        input_filename = os.path.expanduser("~/Library/Application Support/Google/Chrome/Default/Bookmarks")
    elif platform.system() == "Linux":
        input_filename = os.path.expanduser("~/.config/google-chrome/Default/Bookmarks")
    elif platform.system() == "Windows":
        input_filename = os.environ["LOCALAPPDATA"] + r"\Google\Chrome\User Data\Default\Bookmarks"
    else:
        input_filename = None
        print('Your system ("{}") is not recognized. Please specify the input file manually.'.format(platform.system()))
        exit(1)
    return input_filename


def get_bookmarks_data(input_filename):
    '''获取书签 json 数据'''
    with open(input_filename, 'r', encoding='UTF-8') as f:
        return json.loads(f.read())


def main():
    if len(sys.argv) > 2:
        # 如需本地调试可注释掉这一段 START
        parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                         description="python导出chrome书签到markdown文件.")
        parser.add_argument("input_file", type=argparse.FileType('r', encoding='utf-8'), nargs="?",
                            help="读取书签的位置,可以指定文件位置(相对路径，绝对路径都可以),非必填,默认为Chrome的默认书签位置")
        parser.add_argument("output_file", type=argparse.FileType('w', encoding='utf-8'),
                            help="读取书签的位置,可以指定文件位置(相对路径，绝对路径都可以),必填")

        args = parser.parse_args()
        if args.input_file:
            input_file = args.input_file
        else:
            input_file = get_chrome_bookmarks()

        if args.output_file:
            output_file = args.output_file
        else:
            output_file = 'test2.md'
    else:
        input_file = get_chrome_bookmarks()
        output_file = 'test2.md'

    try:
        contents = get_bookmarks_data(input_file)
    except IOError as e:
        contents = None
        if e.errno == 2:
            print("The bookmarks file could not be found in its default location ({}). ".format(e.filename) +
                  "Please specify the input file manually.")
            exit(1)

    bookmark_bar = html_for_node(contents['roots']['bookmark_bar'])
    other = html_for_node(contents['roots']['other'])
    catelog_str = ''.join(a for a in catelog)
    with open(output_file, 'w', encoding='utf-8') as fw:
        fw.write(output_file_template.format(catelog=catelog_str, bookmark_bar=bookmark_bar, other=other))


if __name__ == "__main__":
    main()
