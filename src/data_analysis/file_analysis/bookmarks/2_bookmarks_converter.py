#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 浏览器书签（bookmarks）结构格式分析
# https://github.com/radam9/bookmarks-converter
# pip install bookmarks-converter
from bookmarks_converter import BookmarksConverter
import json
import time
import os


def convert_to_markdown(input_filename):
    def get_md(dict_items, list_md, time_stamp, tree_level=1):
        '''打印列表'''
        str_md = "#" * tree_level + " " + str(dict_items['title'])
        if tree_level == 1:
            str_md += "[" + time.strftime("%Y-%m-%d %H:%M:%S", time_stamp) + "]"
        list_md.append('\n' + str_md + '\n')
        if 'children' in dict_items.keys():
            for item in dict_items['children']:
                type = item.get('type', '')
                name = item.get('title', '')
                url = item.get('url', '')
                # print('\t' * tree_level, name, url)
                if type == 'url':
                    list_md.append('- [{0}]({1})\n'.format(name, url))
                else:  # type == 'folder':
                    # 遇到文件夹循环调用
                    get_md(item, list_md, time_stamp, tree_level + 1)

    list_md = []
    time_stamp = time.localtime()
    # 获取书签 json 数据
    with open(input_filename, 'r', encoding='UTF-8') as f:
        dict_items = json.loads(f.read())
    get_md(dict_items, list_md, time_stamp)
    # get_md(dict_items['roots']['bookmark_bar'], list_md, time_stamp)
    output_file = '{0}_{1}.md'.format(os.path.splitext(input_filename)[0],
                                      time.strftime('%Y%m%d_%H%M%S', time_stamp))
    with open(output_file, 'w', encoding='utf-8') as fw:
        fw.writelines(list_md)
    print("save to {}".format(output_file))


def convert_bookmarks(source_file, dest_file_suffix):
    # initialize the class passing in the path to the bookmarks file to convert
    bookmarks = BookmarksConverter(source_file)

    # parse the file passing the format of the source file; "db", "html" or "json"
    bookmarks.parse("html")
    # source_file_suffix = os.path.splitext(source_file)[1]
    # bookmarks.parse('''{0}'''.format(source_file_suffix[1:]))

    if dest_file_suffix == "md":
        convert_to_markdown(source_file)
    else:
        # convert the bookmarks to the desired format by passing the fomrat as a string; "db", "html", or "json"
        bookmarks.convert("json")
        # bookmarks.convert('''{0}'''.format(dest_file_suffix))

    # at this point the converted bookmarks are stored in the 'bookmarks' attribute.
    # which can be used directly or exported to a file.
    bookmarks.save()


def main():
    list_html = [
        # r'bookmarks_2021_12_26.html',
        r'favorites_2021_12_26.html',
    ]

    for source_file in list_html:
        convert_bookmarks(source_file, "json")
        # json_source_file = "output_{0}_001.json".format(os.path.splitext(source_file)[0])
        # convert_bookmarks(json_source_file, "md")
    # json_source_file = r"chrome_bookmarks_20220827_225057.json"
    # convert_bookmarks(json_source_file, "html")


if __name__ == "__main__":
    main()
