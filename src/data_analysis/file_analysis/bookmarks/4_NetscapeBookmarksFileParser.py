#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 浏览器书签（bookmarks）结构格式分析
# https://github.com/FlyingWolFox/Netscape-Bookmarks-File-Parser
from NetscapeBookmarksFileParser import NetscapeBookmarksFile
from NetscapeBookmarksFileParser.parser import parse


def main():
    source_file = r'Chrome_bookmarks_2018_2_25.html'
    # with open(source_file, 'r', encoding='UTF-8') as fd:
    #     bookmarks = NetscapeBookmarksFile(fd.read())
    bookmarks = NetscapeBookmarksFile(source_file).parse()
    print(bookmarks)


if __name__ == "__main__":
    main()
