#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# pip install bookmarks-converter
from bookmarks_converter import BookmarksConverter
import os
import re
import sqlite3 as sl
import pandas as pd

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
# pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)


def parse_md(source_file_1, source_file_2):
    with open(source_file_1, 'r', encoding='utf-8') as fp1, open(source_file_2, 'r', encoding='utf-8') as fp2:
        list_context_1 = fp1.readlines()
        list_context_2 = fp2.readlines()
        for fileLine in list_context_1:
            list_four_1 = re.findall('^#### (.*)', fileLine)
            # list_four_2 = re.findall('####$ (.*?)\n\n', fileLine, re.S)
            if list_four_1:
                print(list_four_1[0])


def parse_db(source_file_1, source_file_2):
    def print_tree(parentId, tree, spaceStr='\t'):
        for index, row in tree.iterrows():
            if not pd.isna(row['parent_id']):
                if int(row['parent_id']) == int(parentId):
                    print(spaceStr, row['title'], row['url'])
                    print_tree(row['id'], tree, spaceStr + '\t')

    def db_to_html(dest_file, df):
        if os.path.exists(dest_file):
            os.remove(dest_file)
        df.to_sql('bookmark', sl.connect(dest_file), index=False)
        bookmarks = BookmarksConverter(dest_file)
        bookmarks.parse("db")
        bookmarks.convert("html")
        bookmarks.save()

    dict_bookmark = {}
    with sl.connect(source_file_1) as con1, sl.connect(source_file_2) as con2:
        df1 = pd.read_sql('SELECT * FROM bookmark', con1)
        df2 = pd.read_sql('SELECT * FROM bookmark', con2)
        # df3 = pd.merge(df1, df2, how='outer', on=['title', 'index', 'parent_id', 'type', 'url', 'icon', 'icon_uri', 'tags'])
        # df3 = pd.concat([df1, df2], axis=0, ignore_index=True)
        # print(df3.groupby(['index', 'parent_id']).apply(lambda x: print(x)))
        # df4 = df3.groupby(['index', 'parent_id'])['title'].apply(lambda x:','.join(x.values)).to_frame().reset_index()
        # print(df3)
        print_tree(3, df1)

        list_four_1 = df1.query(' parent_id=="3" ')['title'].values.tolist()
        list_four_2 = df2.query(' parent_id=="3" ')['title'].values.tolist()
        for item in set(list_four_1) | set(list_four_2):
            dict_bookmark[item] = ""

        list_five_1 = df1.query(' parent_id=="4" ')['title'].values.tolist()
        list_five_2 = df2.query(' parent_id=="4" ')['title'].values.tolist()
        # print(list_five_1, list_five_2)
    # dest_file = r"bookmark_merge.db"
    # db_to_html(dest_file, df3)


def main():
    # source_file_1 = r"output_bookmarks_2021_12_26_001_20220204_154611.md"
    # source_file_2 = r"output_favorites_2021_12_26_001_20220204_154611.md"
    # parse_md(source_file_1, source_file_2)

    source_file_1 = r"output_bookmarks_2021_12_26_001.db"
    source_file_2 = r"output_favorites_2021_12_26_001.db"
    parse_db(source_file_1, source_file_2)


if __name__ == "__main__":
    main()
