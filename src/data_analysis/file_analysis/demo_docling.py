#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/866619817
# https://github.com/DS4SD/docling
# 只需几行 Python 代码即可将任何文档转换为 LLM 可用数据！
# 兼容 macOS、Linux 和 Windows 环境。x86_64 和 arm64 架构。
"""pip install docling
成功安装后提示如下信息
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
conda-repo-cli 1.0.41 requires clyent==1.2.1, but you have clyent 1.2.2 which is incompatible.
conda-repo-cli 1.0.41 requires nbformat==5.4.0, but you have nbformat 5.7.0 which is incompatible.
conda-repo-cli 1.0.41 requires requests==2.28.1, but you have requests 2.32.3 which is incompatible.
numba 0.57.0 requires numpy<1.25,>=1.21, but you have numpy 1.26.4 which is incompatible.
s3fs 2023.3.0 requires fsspec==2023.3.0, but you have fsspec 2024.12.0 which is incompatible.
"""

import warnings

# 忽略所有的 UserWarning
warnings.simplefilter(action='ignore', category=UserWarning)

import os
import time
# import docling
from docling import document_converter
from docling.datamodel.base_models import ConversionStatus
from docling.datamodel.pipeline_options import PipelineOptions, EasyOcrOptions, TesseractOcrOptions


def test_1(choice=1):
    if choice is None:
        choice = int(input("Enter a positive integer to choice: "))

    dict_output = {
        1: '.md',
        2: '.json',
        3: '.html',
        4: '.xml',
    }
    source_list = [
        # "https://arxiv.org/pdf/2408.09869",  # document per local path or URL
        # r'D:\任务明细.xlsx',
        r'D:\zhangtao12887\周报\PTrade量化团队研发周报-i私募-2025 - 本周.xlsx',
        # r'D:\Q&A.xlsx',
        # r"http://221.204.19.240:7766/hub/help/api"
    ]
    for source in source_list:
        converter = document_converter.DocumentConverter()
        result = converter.convert(source)
        match choice:
            case 1:
                content = result.document.export_to_markdown()
            case 2:
                content = result.document.export_to_json()
            case 3:
                content = result.document.export_to_html()
            case 4:
                content = result.document.export_to_xml()
            case _:
                content = None
                print("请输入一个有效数字！")
                exit(0)
        # print(content)
        if os.path.isfile(source):
            (filepath, filename) = os.path.split(source)
            (shotname, extension) = os.path.splitext(filename)
            dest_file = os.path.join(filepath, shotname) + dict_output[choice]
        else:
            dest_file = time.strftime("%Y%m%d_%H%M%S") + dict_output[choice]
        print(dest_file)
        with open(dest_file, 'w', encoding='utf-8') as f:
            f.write(content)


def test_2():
    # 转换图片文件（支持 PNG/JPG 等格式）
    source = r"D:\1.png"  # 本地图片路径或 URL

    pipeline_options = PipelineOptions()
    pipeline_options.do_ocr = True
    pipeline_options.ocr_options = TesseractOcrOptions()  # Use Tesseract
    converter = document_converter.DocumentConverter(
        pipeline_options=pipeline_options,
    )
    result = converter.convert(source)
    # 输出为 Markdown（含 OCR 识别文字）
    print(result.document.export_to_markdown())

    # 输出为 JSON（含元数据和结构化内容）
    # import json
    # print(json.dumps(result.document.export_to_dict(), indent=4))

    # 提取表格数据
    tables = result.document.get_tables()
    for table in tables:
        print(f"表格标题: {table['caption']}")
        print("表头:", table['headers'])
        print("数据行:", table['rows'])


def test_3():
    pass


def test_4():
    pass


def main():
    choice = "1"
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    if choice is None:
        choice = str(input("Enter a positive integer to choice: "))

    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
