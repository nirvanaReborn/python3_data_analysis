#!/usr/bin/env python
# -*- coding:utf-8 -*-

# PDF 表示Portable Document Format，使用.pdf 文件扩展名。

import os
from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE
from public_function import dict_choice


def test_1():
    # PyPDF2 写入PDF 的能力，仅限于从其他PDF 中拷贝页面、旋转页面、重叠页面和加密文件。
    # PyPDF2 不能在PdfFileWriter 对象中间插入页面，addPage()方法只能够在末尾添加页面。
    # pip install PyPDF2
    import PyPDF2

    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "PyPDF2_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    import camelot


def test_3():
    import ReportLab


def test_4():
    # pip install pdfminer3k
    import PDFMiner


def main():
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
