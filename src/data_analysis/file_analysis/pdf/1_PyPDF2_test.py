#!/usr/bin/env python
# -*- coding:utf-8 -*-

# PDF 表示Portable Document Format，使用.pdf 文件扩展名。
# PyPDF2 写入PDF 的能力，仅限于从其他PDF 中拷贝页面、旋转页面、重叠页面和加密文件。
# PyPDF2 不能在PdfFileWriter 对象中间插入页面，add_page()方法只能够在末尾添加页面。

# pip install PyPDF2
import PyPDF2


# 从PDF 提取文本
def test_1(source_file):
    pdfFileObj = open(source_file, 'rb')
    pdfReader = PyPDF2.PdfReader(pdfFileObj)
    print("文档总页数:", len(pdfReader.pages))

    # PyPDF2 在取得页面时使用从0 开始的下标：第一页是0 页，第二页是1 页，以此类推。
    pageObj = pdfReader.pages[0]
    print(pageObj.extract_text())


# 加密PDF
def test_2(source_file, user_pwd="123456"):
    pdfFile = open('meetingminutes.pdf', 'rb')
    pdfReader = PyPDF2.PdfReader(pdfFile)
    pdfWriter = PyPDF2.PdfWriter()
    for pageNum in range(len(pdfReader.pages)):
        pdfWriter.add_page(pdfReader.pages[pageNum])

    # PDF 可以有一个用户口令（允许查看这个PDF）和一个拥有者口令（允许设置打印、注释、提取文本和其他功能的许可）。
    # 用户口令和拥有者口令分别是encrypt()的第一个和第二个参数。
    # 如果只传入一个字符串给encrypt()，它将作为两个口令。
    pdfWriter.encrypt(user_pwd)
    resultPdf = open('encryptedminutes.pdf', 'wb')
    pdfWriter.write(resultPdf)
    resultPdf.close()


# 解密PDF
def test_3(source_file, user_pwd="123456"):
    pdfFileObj = open(source_file, 'rb')
    pdfReader = PyPDF2.PdfReader(pdfFileObj)
    print(pdfReader.isEncrypted)  # 判断是否加密
    print(pdfReader.decrypt(user_pwd))  # 解密
    pageObj = pdfReader.getPage(0)
    print(pageObj.extract_text())


# 拷贝页面
def test_4(source_file_1, source_file_2, dest_file):
    pdfWriter = PyPDF2.PdfWriter()
    source_file_list = [source_file_1, source_file_2]
    for source_file in source_file_list:
        with open(source_file, 'rb') as pdf_file:
            pdfReader = PyPDF2.PdfReader(pdf_file)
            for pageNum in range(len(pdfReader.pages)):
                pageObj = pdfReader.getPage(pageNum)
                pdfWriter.add_page(pageObj)

    with open(dest_file, 'wb') as pdfOutputFile:
        pdfWriter.write(pdfOutputFile)


# 叠加页面
def test_5(source_file):
    minutesFile = open('meetingminutes.pdf', 'rb')
    pdfReader = PyPDF2.PdfReader(minutesFile)
    minutesFirstPage = pdfReader.getPage(0)

    pdfWatermarkReader = PyPDF2.PdfReader(open('watermark.pdf', 'rb'))
    minutesFirstPage.mergePage(pdfWatermarkReader.getPage(0))

    pdfWriter = PyPDF2.PdfWriter()
    pdfWriter.add_page(minutesFirstPage)
    for pageNum in range(1, len(pdfReader.pages)):
        pageObj = pdfReader.getPage(pageNum)
        pdfWriter.add_page(pageObj)
    resultPdfFile = open('watermarkedCover.pdf', 'wb')
    pdfWriter.write(resultPdfFile)
    minutesFile.close()
    resultPdfFile.close()


# 旋转页面
def test_6(source_file):
    minutesFile = open('meetingminutes.pdf', 'rb')
    pdfReader = PyPDF2.PdfReader(minutesFile)
    page = pdfReader.getPage(0)
    page.rotateClockwise(90)
    pdfWriter = PyPDF2.PdfWriter()
    pdfWriter.add_page(page)
    resultPdfFile = open('rotatedPage.pdf', 'wb')
    pdfWriter.write(resultPdfFile)
    resultPdfFile.close()
    minutesFile.close()


# 将 PDF 转换为音频文件:首先用 PyPDF 提取 pdf 中的文本，然后用 Pyttsx3 将文本转语音。
def test_7(source_file):
    import pyttsx3
    pdfreader = PyPDF2.PdfReader(open('story.pdf', 'rb'))
    speaker = pyttsx3.init()
    for page_num in range(len(pdfreader.pages)):
        text = pdfreader.getPage(page_num).extract_text()  ## extracting text from the PDF
        cleaned_text = text.strip().replace('\n', ' ')  ## Removes unnecessary spaces and break lines
        print(cleaned_text)  ## Print the text from PDF
        # speaker.say(cleaned_text) ## Let The Speaker Speak The Text
        speaker.save_to_file(cleaned_text, 'story.mp3')  ## Saving Text In a audio file 'story.mp3'
        speaker.runAndWait()
    speaker.stop()


def test_8(source_file):
    pdfWriter = PyPDF2.PdfWriter()
    with open(source_file, 'rb') as pdf_file:
        pdfReader = PyPDF2.PdfReader(pdf_file)
        for pageNum in range(len(pdfReader.pages)):
            if 1 < pageNum < 9:
                pdfWriter.add_page(pdfReader.pages[pageNum])
    dest_file = r'1.pdf'
    with open(dest_file, 'wb') as pdfOutputFile:
        pdfWriter.write(pdfOutputFile)


def main():
    # source_file = r'D:\share\git\gitee\nirvanaReborn_python\course_python\doc\book\Python3-廖雪峰.pdf'
    source_file = r'D:\360Downloads\1-PTrade仿真系统升级资料汇总.pdf'

    dict_choice = {}
    for i in range(0, 100):
        # if i == 3:
        #     source_file = r'helloworld.pdf'
        dict_choice[str(i)] = "test_" + str(i) + "(source_file)"

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
