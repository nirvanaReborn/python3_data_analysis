#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.jb51.net/article/171814.htm
# ython新工具：用三行代码提取PDF表格数据
'''
Camelot 是一个 Python 工具，用于将 PDF 文件中的表格数据提取出来。
项目地址：https://github.com/camelot-dev/camelot
# 对于合并的单元格，Camelot 在抽取后做了空行处理，这是一个稳妥的方法。

1.使用 Conda 进行安装: conda install -c conda-forge camelot-py
2.使用 pip 安装: pip install camelot-py[cv]
3.使用源码安装:
    git clone https://www.github.com/camelot-dev/camelot
    cd camelot
    pip install ".[cv]"
'''
import camelot


def main():
    tables = camelot.read_pdf('foo.pdf')  # 类似于Pandas打开CSV文件的形式
    print(tables[0].df)  # get a pandas DataFrame!
    tables.export('foo.csv', f='csv', compress=True)  # json, excel, html, sqlite，可指定输出格式
    tables[0].to_csv('foo.csv')  # to_json, to_excel, to_html, to_sqlite， 导出数据为文件
    print(tables)
    print(tables[0])
    print(tables[0].parsing_report)


if __name__ == "__main__":
    main()
