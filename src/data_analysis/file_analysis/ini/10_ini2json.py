#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qq_24822271/article/details/129046206
#  ini转json dict转ini

from configparser import ConfigParser
import json


def ini2json(ini_path):
    """
    :param ini_path: ini文件路径
    :return:ini文件转变成json
    """
    d = {}
    cfg = ConfigParser()
    cfg.read(ini_path)
    for s in cfg.sections():
        print(s)
        d[s] = dict(cfg.items(s))
    return json.dumps(d)


def dic2ini(dic, ini_path):
    """
    :param dic: python字典
    :param ini_path: ini文件路径
    :return: None
    """
    config = ConfigParser()
    for key in dic:
        config.add_section(key)
        for key1 in dic[key]:
            config.set(key, key1, dic[key][key1])
    config.write(open(ini_path, "w"))
