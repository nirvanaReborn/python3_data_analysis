#!/usr/bin/env python
# -*- coding:utf-8 -*-

import configparser


def main():
    config_file = r'config.ini'
    config = configparser.ConfigParser()
    config.read(config_file, 'gbk')
    # 获取所有的section节点
    print(config.sections())

    # 获取指定的section下的option的名称
    r = config.options("global")
    print(r)

    # 获取指点section下指点option的值
    Artist = config.get("global", "Artist")
    DateTimeOriginal = config.get("global", "DateTimeOriginal")
    Software = config.get("global", "Software")
    print(Artist, DateTimeOriginal, Software)

    # 获取指点section的所用配置信息
    r = config.items("global")
    print(r)

    # 检查section或option是否存在，bool值
    config.has_section("section")  # 是否存在该section
    config.has_option("section", "option")  # 是否存在该option

    # 添加section 和 option
    if not config.has_section("default"):  # 检查是否存在section
        config.add_section("default")
    if not config.has_option("default", "db_host"):  # 检查是否存在该option
        config.set("default", "db_host", "1.1.1.1")
    config.write(open(config_file, "w"))

    # 删除section 和 option
    config.remove_section("default")  # 整个section下的所有内容都将删除
    config.write(open(config_file, "w"))

    # 修改某个option的值，如果不存在则会创建
    config.set("global", "db_port", "69")  # 修改db_port的值为69
    config.write(open(config_file, "w"))


if __name__ == "__main__":
    main()
