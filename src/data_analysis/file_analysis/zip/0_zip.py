#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
from public_function import GLOBAL_RELATIVE_PATH_BUILTIN_MODULE


format = ['rar', 'zip', '7z', 'ace', 'arj', 'bz2', 'cab', 'gz', 'iso', 'jar', 'lzh', 'tar', 'uue', 'z']


# 解压
def test_0(source_file, rar_dir):
    suffix = os.path.splitext(source_file)[1]
    # 需要WinRAR软件提供的UnRAR.exe文件：将winrar的目录中的UnRAR.exe拷贝到python脚本目录下即可
    if suffix == "rar":
        import rarfile
        rf = rarfile.RarFile(source_file)  # 待解压文件
        rf.extractall(rar_dir)  # 指定文件解压路径
    elif suffix == "zip":
        import zipfile
        archive = zipfile.ZipFile(source_file, 'r')
        df = archive.extractall(r".")  # 当前目录
        archive.close()



def test_1():
    import zipfile

    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "zipfile_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    import tarfile

    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "tarfile_test.py")
    print(call_file)
    os.popen(call_file)


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
