#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/kevinelstri/article/details/61921812
# 使用python读取数据科学最常用的文件格式

import zipfile


# 压缩
def test_1(source_zipfile):
    archive = zipfile.ZipFile(source_zipfile, 'w')
    df = archive.write("1.txt")
    archive.close()


# 解压
def test_2(source_zipfile):
    archive = zipfile.ZipFile(source_zipfile, 'r')
    df = archive.extractall(r".")  # 当前目录
    archive.close()
    print(df)


def test_3(source_zipfile):
    archive = zipfile.ZipFile(source_zipfile, 'r')
    df = archive.read(r'automate_online-materials/hello.py').decode('utf-8')
    archive.close()
    print(df)


def main():
    source_zipfile = r"D:\share\git\gitee\zhangtao2016\course_python\doc\book\Python编程快速上手——让繁琐工作自动化.zip"
    test_1(source_zipfile)


if __name__ == "__main__":
    main()
