#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# python解压 tar.gz文件
import os
import tarfile

from public_function import GLOBAL_WORK_DIR


def main():
    source_tarfile = os.path.join(GLOBAL_WORK_DIR, r"pic.tar.gz")
    tar = tarfile.open(source_tarfile)
    names = tar.getnames()
    for name in names:
        tar.extract(name, path=r".")
    tar.close()


if __name__ == "__main__":
    main()
