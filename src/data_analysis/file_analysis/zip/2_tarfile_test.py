#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# python直接读取tar文件中的图片

import os
import tarfile

from public_function import GLOBAL_WORK_DIR


def get_file_list(source_tarfile):
    # 读取文件名，并放到list中
    name_list = []
    with tarfile.open(source_tarfile, "r") as fp:
        for tarinfo in fp.getmembers():
            # print(dir(type(tarinfo)))
            name_list.append(tarinfo.name)
    # print(name_list)
    return name_list


# 利用PIL模块与 numpy 模块读入图片并转为numpy array
def show_pic(source_tarfile, dest_pic_file):
    import numpy as np
    from matplotlib import pyplot as plt
    from PIL import Image, TarIO

    # 利用Image类读入图片
    fp = TarIO.TarIO(source_tarfile, dest_pic_file)
    img = Image.open(fp)
    print(img.format, img.size, img.mode)

    # 从Image类转化为numpy array
    im = np.asarray(img)
    im.flags.writeable = True
    plt.imshow(im)
    plt.show()


def main():
    source_tarfile = os.path.join(GLOBAL_WORK_DIR, r"pic.tar")
    dest_pic_file = "1.jpg"
    print(get_file_list(source_tarfile))
    show_pic(source_tarfile, dest_pic_file)


if __name__ == "__main__":
    main()
