#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/maxzheng/p/10498479.html
# python利用7z批量解压rar
# 一开始我使用了rarfile这个库，奈何对于含有密码的压缩包支持不好，在linux上不抛出异常；之后有又尝试了unrar，比rarfile还费劲。
# 所以用了调用系统命令的方法，用7z来解压
# 通过apt可以安装上7z-full和7z的rar插件
# 有一个地方要注意 -o 和-p与后面的目录、密码之间没有空格！！
# 同样可以用这个代码暴力破解压缩包密码

import getopt
import os
import shutil
import sys
import time
import traceback
import zipfile


# for op,value in opts:
#    if op in ("-p","--password"):
#        psds.add(value)


def mkdirs(dirpath):
    global succeedpath, unsucceedpath
    succeedpath = os.path.join(dirpath, "0.succeed")
    unsucceedpath = os.path.join(dirpath, "0.unsucceed")
    if (not os.path.exists(succeedpath)):
        os.makedirs(succeedpath)
    if (not os.path.exists(unsucceedpath)):
        os.makedirs(unsucceedpath)


def extractdir(path, psds):
    for filename in os.listdir(path):
        filepath = os.path.join(path, filename)
        if (os.path.isfile(filepath)):
            extractfile(filepath, psds)


def extractfile(path, psds):
    (dirpath, filen) = os.path.split(path)
    print("解压" + filen + "     ing...")
    try:
        if (filen.endswith(".rar") or filen.endswith(".zip")):
            path = os.path.abspath(path)
            dirpath = os.path.abspath(dirpath)
            cmd = "7z x " + path + " -o" + dirpath + " -y " + " -p"
            state = False
            if (os.system(cmd) == 0):
                state = True
            else:
                for psd in psds:
                    cmdp = "7z x " + path + " -p" + psd + " -y " + " -o" + dirpath
                    if (os.system(cmdp) == 0):
                        state = True
                        break
        else:
            return
        if (state):
            shutil.move(path, succeedpath)
        else:
            shutil.move(path, unsucceedpath)
            with open(os.path.join(dirpath, "errorinfo.txt"), "a+") as errorinfo:
                errorinfo.write("time:" + str(time.time()) + "\n")
                errorinfo.write("解压出错！\n可能是密码错误！\n")
    except:
        with open(os.path.join(dirpath, "errorinfo.txt"), "a+") as errorinfo:
            errorinfo.write("time:" + str(time.time()) + "\n")
            errorinfo.write("解压出错！\n")
            traceback.print_exc(file=errorinfo)
        shutil.move(path, unsucceedpath)


def main():
    succeedpath = ""
    unsucceedpath = ""
    psds = {"2018", "123456"}

    opts, args = getopt.getopt(sys.argv[1:], "p:")
    path = args[0]
    for psd in args[1:]:
        psds.add(psd)

    if (os.path.isdir(path)):
        dirpath = path
        mkdirs(dirpath)
        extractdir(dirpath, psds)
    elif (os.path.isfile(path)):
        filepath = path
        (dirpath, filename) = os.path.split(path)
        mkdirs(dirpath)
        extractfile(path, psds)
    else:
        print("输入错误！请重新输入！")


if __name__ == "__main__":
    main()
