#!/usr/bin/env python
# -*- coding:utf-8 -*-

# shutil 模块是python 标准库中提供的，主要用来做文件和文件夹的拷贝、移动、删除等；还可以做文件和文件夹的压缩、解压缩操作。
# os 模块提供了对目录或文件的一般操作。shutil 模块作为补充，提供了移动、复制、压缩、解压等操作，这些os 模块都没有提供。

import shutil


def test_1():
    '''
    make_archive(base_name, format, root_dir, …)： 生成压缩文件
        base_name：压缩文件的文件名，不允许有扩展名，因为会根据压缩格式生成相应的扩展名
        format：压缩格式
        root_dir：将制定文件夹进行压缩
    '''
    # 获取支持的压缩文件格式。目前支持的有：tar、zip、gztar、bztar、xztar。
    print(shutil.get_archive_formats())
    # 压缩
    shutil.make_archive("目的压缩文件", "zip", "源文件路径")


def test_2():
    '''
    unpack_archive(filename, extract_dir=None, format=None)： 解压操作
        filename：文件路径
        extract_dir：解压至的文件夹路径。文件夹可以不存在，会自动生成
        format：解压格式，默认为None，会根据扩展名自动选择解压格式

    '''
    # 获取支持的解压文件格式。目前支持的有：tar、zip、gztar、bztar、xztar。
    print(shutil.get_unpack_formats())
    # 解压
    shutil.unpack_archive("源压缩文件路径", "目的解压文件路径")


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
