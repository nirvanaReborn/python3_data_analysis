#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
import glob
import os

import rarfile

from public_function import GLOBAL_WORK_DIR


# 批量解压缩
def decompress_file(source_dir):
    # sys_str = platform.system()
    # if sys_str == 'Windows':
    #     os.environ["PATH"] += r";d:\programfiles\winrar\unrar.exe"
    #     print(os.environ["PATH"].split(';'))

    list_file = glob.glob(os.path.join(source_dir, "*" + ".rar"))
    # print(list_file)
    for source_file in list_file:
        (filepath, filename) = os.path.split(source_file)
        (shotname, extension) = os.path.splitext(filename)
        # print(filepath, filename, shotname, extension)
        rar_dir = os.path.join(filepath, shotname)
        if not os.path.exists(rar_dir):
            os.mkdir(rar_dir)

        # 需要WinRAR软件提供的UnRAR.exe文件：将winrar的目录中的UnRAR.exe拷贝到python脚本目录下即可
        rf = rarfile.RarFile(source_file)  # 待解压文件
        rf.extractall(rar_dir)  # 指定文件解压路径

        # rar_command = r'"d:\programfiles\winrar\unrar.exe" x %s %s' % (source_file, rar_dir)
        # if os.system(rar_command) == 0:
        #     print('Successful')
        # else:
        #     print('FAILED')


def main():
    source_dir = GLOBAL_WORK_DIR
    decompress_file(source_dir)


if __name__ == "__main__":
    main()
