#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#

import codecs
import os
import re


def deal_cs_file(text):
    line_pattern = r'\s*\d+\s?(.*)'
    c = re.compile(line_pattern)
    lists = []
    lines = text.split('\r\n')
    # print(lines)
    for line in lines:
        r = c.findall(line)
        if r:
            lists.append(r[0])
    return '\n'.join(lists)


def get_FormControl(source_file):
    list_1D = []
    with codecs.open(source_file, 'r', 'utf-8') as fr:
        s = fr.readlines()

    for fileLine in s:
        list_re = re.findall("this.(.*)? = new System.Windows.Forms.(.*)?", fileLine, re.S)
        if list_re:
            # print(list_re)
            result = list_re[0]
            # print(result)
            if str(result[0])[0].islower():
                list_1D.append(result[0])
    # print(list_1D)
    return list_1D


def create_list1D_to_txt(dest_file, list_file_content):
    with open(dest_file, 'w', encoding='UTF-8') as fw:
        for i, fileLine in enumerate(list_file_content):
            if fileLine:
                fw.write(str(fileLine) + "\n")
            else:
                # logging.debug("%s第%s行文件为空" % (self.file_name, i + 1))
                pass


# 遍历source_dir下所有子文件,并过滤文件后缀
def get_file_by_suffix(source_dir, suffix=None):
    list_file = []
    for parent, dirnames, filenames in os.walk(source_dir):
        for filename in filenames:
            source_file = os.path.join(parent, filename).encode('utf-8').decode('utf-8')
            # 文件不为空
            if os.path.getsize(source_file):
                if ((suffix != None and str(source_file).endswith(suffix))
                        or suffix == None):
                    list_file.append(source_file)
            else:
                print("空文件:%s" % source_file)
                continue
    return list_file


def main():
    list_shell_cmd = []
    source_dir = r"D:\share\git\gitee\nirvanaReborn\course_cSharp\src\WinForm\ARESHelper"
    list_file = get_file_by_suffix(source_dir, ".Designer.cs")
    for source_file in list_file:
        list_1D = get_FormControl(source_file)
        for item in list_1D:
            shell_cmd = r"sed -i 's/" + item + "/" + item[0].capitalize() + item[1:] + "/g'" + " *.cs"
            print(shell_cmd)
            list_shell_cmd.append(shell_cmd)

    dest_file = os.path.join(source_dir, "ARESHelper/src/1.sh")
    create_list1D_to_txt(dest_file, list_shell_cmd)
    # cd /mnt/d/share/git/gitee/nirvanaReborn/course_cSharp/src/WinForm/ARESHelper/ARESHelper/src
    # sed -i 's/btGetTest/BtGetTest/g' *.cs


if __name__ == "__main__":
    main()
