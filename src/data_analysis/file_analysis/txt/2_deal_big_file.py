#!/usr/bin/env python
# -*- coding:utf-8 -*-

# python 读取大文件
# http://blog.csdn.net/lanchunhui/article/details/51581540
#
# https://www.cnblogs.com/yu-zhang/p/5949696.html
# 我们在处理小的文本文件时一般使用.read()、.readline() 和 .readlines()，
# 当我们的文件有10个G甚至更大时，用上面的方法内存就直接爆掉了。
#
# http://blog.csdn.net/notback/article/details/73134824


from public_function import GLOBAL_TXT_FILE


# 文件对象fr视为一个迭代器，会自动的采用缓冲IO和内存管理，所以你不必担心大文件。
# rb方式最快，100w行全遍历2.7秒。基本能满足中大型文件处理效率需求。如果rb改为r，慢6倍。
# 虽然此方式处理文件，fLine为bytes类型，但是python自动断行，仍旧能很好的以行为单位处理读取内容。
# 如果建立索引，以便后续查找定位，不建议使用传统win编程的行号定位,最好记录偏移量来进行定位。
def test_1(source_file=GLOBAL_TXT_FILE):
    import itertools
    with open(source_file, "rb") as fr:
        # for lineNum, line in enumerate(fr):
        for lineNum, line in itertools.dropwhile(lambda x: x[0] < 1073109, enumerate(fr)):
            print(line)
            # <do something with line>


def test_2(source_file=GLOBAL_TXT_FILE):
    import fileinput
    for line in fileinput.input([source_file]):
        print(line)
        # <do something with line>


# 利用read进行大文件切割
def test_3(source_file=GLOBAL_TXT_FILE):
    def read_in_chunks(file_object, chunk_size=1024 * 1024):
        """把文件切割成一块块的读，默认是1M"""
        while True:
            # chunk_data = file_object.readline()
            chunk_data = file_object.read(chunk_size)
            if not chunk_data:
                break
            yield chunk_data

    with open(source_file, "rb") as file_object:
        for chunk in read_in_chunks(file_object):
            print(chunk)
            # <do something with chunk>


'''https://www.toutiao.com/i6793597382673039880/
现在数据科学家们要面对 50GB，甚至 500GB 数据集的场景变得越来越普遍。
这些数据集处理起来有点麻烦。就大小而言，它们可以放进你笔记本电脑的硬盘里，但却无法装入内存。
所以，仅仅打开和查看它们就很困难，更何况进一步探索和分析。

处理这样的数据集时，一般有 3 种策略：
第 1 种是对数据进行子抽样，但它有一个明显缺点：可能因忽略部分数据而错失关键信息，甚至误解数据表达的含义。
第 2 种是使用分布式计算。虽然在某些情况下这是一种有效的方法，但是管理和维护集群会带来巨大开销。
        想象一下，要为一个刚超出内存大小、大概 30-50GB 的数据集就建立一套集群，对我来说，这似乎有点“用力过猛”。
第 3 种是租用一个内存大小等同于数据集大小的强大云服务实例，例如，AWS 提供了 TB 级内存的云服务实例。
        但这种情况还需要管理云数据存储空间，并且在每次实例启动时都要等待数据从存储空间传输到实例。
        另外还需要应对数据上云的合规性问题，以及忍受在远程机器上工作带来的不便。
        更别提成本，虽然开始会比较低，但随着时间推移会快速上涨。

本文向你展示一种全新方法，它更快、更安全，可以更方便、全面地对几乎任意大小的数据集进行数据科学研究，
只要这个数据集能装进你的笔记本电脑、台式机或者服务器的硬盘里就行。
'''


def test_4():
    '''原文链接：https://blog.csdn.net/qq_37085158/article/details/126799856
    常规文件操作为了提高读写效率和保护磁盘，使用了页缓存机制。
    这样造成读文件时需要先将文件页从磁盘拷贝到页缓存中，由于页缓存处在内核空间，不能被用户进程直接寻址，
    所以还需要将页缓存中数据页再次拷贝到内存对应的用户空间中。
    这样，通过了两次数据拷贝过程，才能完成进程对文件内容的获取任务。
    写操作也是一样，待写入的buffer在内核空间不能直接访问，
    必须要先拷贝至内核空间对应的主存，再写回磁盘中（延迟写回），也是需要两次数据拷贝。

    常规文件操作需要从磁盘到页缓存再到用户主存的两次数据拷贝。
    而mmap操控文件，只需要从磁盘到用户主存的一次数据拷贝过程。
    说白了，mmap的关键点是实现了用户空间和内核空间的数据直接交互而省去了空间不同数据不通的繁琐过程。
    因此在某些场景下，mmap效率更高。

    mmap是一种虚拟内存映射文件的方法，即将一个文件或者其它对象映射到进程的地址空间，
    实现文件磁盘地址和进程虚拟地址空间中一段虚拟地址的一一映射关系。
    它省掉了内核态和用户态页copy这个动作（两态间copy），直接将用户态的虚拟地址与内核态空间进行映射，
    进程直接读取内核空间，速度提高了，内存占用也少了。
    简单点来说，mmap函数实现的是内存共享：同一块物理内存被映射到两个进程的各自的进程地址空间。

    mmap的应用场景:
    1.将一个普通文件映射到内存中，通常在需要对文件进行频繁读写时使用，这样用内存映射读写取代I/O缓存读写，以获得较高的性能；
    2.进程间的内存共享。多个进程将同一个文件map到同一段内核地址上，即实现了相互之间的共同访问。
    '''
    import mmap

    def read_data(file_path):
        with open(file_path, "r+") as f:
            m = mmap.mmap(f.fileno(), 0)
            g_index = 0
            for index, char in enumerate(m):
                if char == b"\n":
                    yield m[g_index:index + 1].decode()
                    g_index = index + 1

    file_path = ""
    for content in read_data(file_path):
        print(content)


def test_5(source_file=GLOBAL_TXT_FILE):
    '''
    Vaex 是一个开源的 DataFrame 库，对于和你硬盘空间一样大小的表格数据集，它可以有效进行可视化、探索、分析乃至实践机器学习。
    为实现这些功能，Vaex 采用内存映射、高效的核外算法和延迟计算等概念。
    所有这些都封装为类 Pandas 的 API，因此，任何人都能快速上手。

    第一步是将数据转换为内存映射文件格式，如 Apache Arrow 、 Apache Parquet 或 HDF5 。
    关于如何把 CSV 数据转为 HDF5 的例子请看这里。
    一旦数据存为内存映射格式，即便它的磁盘大小超过 100GB，用 Vaex 也可以在瞬间打开它（0.052 秒）：

    为什么这么快？
    在用 Vaex 打开内存映射文件时，实际上并没有读取数据。
    Vaex 只读取了文件元数据，比如数据在磁盘上的位置、数据结构（行数、列数、列名和类型）、文件描述等等。
    '''
    import vaex
    df = vaex.open(source_file)


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
