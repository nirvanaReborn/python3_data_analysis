#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 简单地解析visual studio的buildlog：
# 有的时候log文件太长，但是我们只是关心warning和error，
# 通过该程序可以直接得到所有的warning和error的行

import codecs
import re

warninglist = []
# append() 方法向列表的尾部添加一个新的元素
warninglist.append("warning CS\d{4}")
errorlist = []
errorlist.append("error lnkd{4}")
errorlist.append("error CS\d{4}")


def parse(logfile, regexlist):
    resultlines = []
    with codecs.open(logfile, "r", "gbk") as log:
        for line in log:
            for regex in regexlist:
                m = re.search(regex, line)
                if m != None:
                    resultlines.append(line)
    return resultlines


def parse_info(logfile, regexlist, info):
    info_list = []
    info_list = parse(logfile, regexlist)
    print(info + ":" + str(len(info_list)))
    for info in info_list:
        print(info)
    print("=" * 50)


def main():
    source_file = "1.txt"
    parse_info(source_file, warninglist, "warnings")
    parse_info(source_file, errorlist, "errors")


if __name__ == "__main__":
    main()
