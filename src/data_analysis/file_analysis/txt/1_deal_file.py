#!/usr/bin/env python
# -*- coding: UTF-8 -*-


# python 文件操作
# http://www.cnblogs.com/rollenholt/archive/2012/04/23/2466179.html
# http://www.cnblogs.com/xinchrome/p/5011304.html
# http://www.jb51.net/article/48001.htm
# http://www.jb51.net/article/91416.htm
# 三种基本的文件操作模式：r(only-read)、w(only-write)、a(append)
# 文本处理，一般分为分隔符处理，定长处理。
# 其实就是看重python3.x系列将string固化为unicode字符串，其他使用bytes类型。
# 不是经常处理ascII，unicode不知道这种改进是多好。这也是抛弃2.7直接学3.x的原因。


# 如果文件很小，read()一次性读取最方便；
# 如果不能确定文件大小，反复调用read(size)比较保险；
# 如果是配置文件，调用readlines()最方便：
def test_1():
    f = open('file1', 'r')
    # read是逐字符地读取,read可以指定参数，设定需要读取多少字符，无论一个英文字母还是一个汉字都是一个字符。
    f_read = f.read()
    print(f_read)
    f.close()

    f = open('file1', 'r')
    # readline只能读取第一行代码，原理是读取到第一个换行符就停止。
    f_read = f.readline()
    print(f_read)
    f.close()

    f = open('file1', 'r')
    # readlines会把内容以列表的形式输出。
    f_read = f.readlines()
    print(f_read)
    f.close()

    f = open('file1', 'r')
    # 使用for循环可以把内容按字符串输出。
    # 输出一行内容输出一个空行，一行内容一行空格...
    # 因为文件中每行内容后面都有一个换行符，而且print()语句本身就可以换行。
    # 如果不想输出空行，就需要使用下面的语句：print(line.strip())
    for line in f.readlines():
        print(line.strip())
    f.close()


# w模式 : 在进行操作前，文件中所有内容会被清空。
def test_2():
    # 由于Python3的默认编码方式是Unicode，所以在写入文件的时候需要调用utf8，以utf8的方式保存，
    # 这时pycharm（默认编码方式是utf8）才能正确读取，当读取文件时，文件是utf8格式，pycharm也是utf8，就不需要调用了。
    f = open('file1', 'w', encoding='utf8')
    f_w = f.write('hello world')
    print(f_w)  # 有意思的是，这里并不打印'hello world'，只打印写入多少字符
    f.close()


# a模式 : 与w模式不同的是，a模式不会把原来内容清空，而是光标移到内容最后位置，继续写入新内容。
def test_3():
    f = open('file1', 'a')
    f_a = f.write('hello world')
    print(f_a)  # 还是会打印写入的字符数
    f.close()


# 打开文件，把全部内容读入内存，然后再打印输入。
# 当文件很大时，这种读取方式就不靠谱了，甚至会使机器崩溃。我们需要及时关闭文件，
def test_4():
    f = open('file', 'r')
    data = f.readlines()  # 注意及时关闭文件
    f.close()

    num = 0
    for i in data:
        num += 1
        if num == 5:
            i = ''.join([i.strip(), 'hello world'])  # 不要使用“+”进行拼接
        print(i.strip())


# 对于大数据文件，要使用下面的方法
def test_5():
    num = 0
    f = open('file', 'r')
    for i in f:  # for内部把f变为一个迭代器，用一行取一行。
        num += 1
        if num == 5:
            i = ''.join([i.strip(), 'hello world'])
        print(i.strip())
    f.close()


#  tell：查询文件中光标位置
#  seek：光标定位
def test_6():
    f = open('file', 'r')
    print(f.tell())  # 光标默认在起始位置
    f.seek(10)  # 把光标定位到第10个字符之后
    print(f.tell())  # 输出10
    f.close()

    f = open('file', 'w')
    print(f.tell())  # 先清空内容，光标回到0位置
    f.seek(10)
    print(f.tell())
    f.close()

    f = open('file', 'a')
    print(f.tell())  # 光标默认在最后位置
    f.write('你好 世界')
    print(f.tell())  # 光标向后9个字符，仍在最后位置
    f.close()


# flush 同步将数据从缓存转移到磁盘
# 示例，实现进度条功能
def test_7():
    def progress_bar_1():
        import sys, time  # 导入sys和time模块
        for i in range(40):
            sys.stdout.write('*')
            sys.stdout.flush()  # flush的作用相当于照相，拍一张冲洗一张
            time.sleep(0.2)

    # 下面代码也能够实现相同的功能
    def progress_bar_2():
        import time
        for i in range(40):
            print('*', end='', flush=True)  # print中的flush参数
            time.sleep(0.2)


# truncate 截断
# 只能工作在a模式下，截断指定位置后的内容。
def test_8():
    f = open('file', 'a')
    f.truncate(6)  # 只显示6个字节的内容（6个英文字符或三个汉字），后面的内容被清空。


# 光标位置总结
# 一个汉字两个字节，涉及光标位置的方法有4个：read、tell、seek、truncate。
def test_9():
    f = open('file', 'r')
    print(f.read(6))  # 6个字符
    print(f.tell())  # 位置12字节，一个汉字两个字节
    f.close()

    f = open('file', 'r')
    f.seek(6)  # 6个字节
    print(f.tell())
    f.close()

    f = open('file', 'a')
    print(f.tell())  # 光标默认在最后位置
    f.write('你好 世界')
    print(f.tell())  # 光标向后9个字节，一个汉字两个字节，仍在最后位置 182-->191
    f.close()

    f = open('file', 'a', encoding='utf-8')
    print(f.truncate(6))  # 由于需要光标定位位置，所以也是字节。只显示6个字节的内容（6个英文字母或三个汉字,一个汉字两个字节），后面的内容被清空。
    f.close()


# 另外3种模式：r+、w+、a+
# r+：读写模式，光标默认在起始位置，当需要写入的时候，光标自动移到最后
# w+：写读模式，先清空原内容，再写入，也能够读取
# a+：追加读模式，光标默认在最后位置，直接写入，也能够读取。
def test_10():
    f = open('file', 'a')
    print(f.tell())  # 末尾207位置
    f.close()

    f = open('file', 'r+')
    print(f.tell())  # 0位置
    print(f.readline())  # 读取第一行
    f.write('羊小羚')  # 光标移到末尾207位置并写入
    print(f.tell())  # 213位置
    f.seek(0)  # 光标移到0位置
    print(f.readline())  # 读取第一行
    f.close()


# with语句可以同时对多个文件同时操作。
# 当with代码块执行完毕时，会自动关闭文件释放内存资源，不用特意加f.close()
def test_11():
    num = 0
    with open('file', 'r') as f1, open('file2', 'w', encoding='utf8') as f2:
        for line in f1:
            num += 1
            if num == 5:
                line = ''.join([line.strip(), '羊小羚'])
            f2.write(line)


def test_12():
    try:
        with open('致橡树.txt', 'r', encoding='utf-8') as f:
            print(f.read())
    except FileNotFoundError:
        print('无法打开指定的文件!')
    except LookupError:
        print('指定了未知的编码!')
    except UnicodeDecodeError:
        print('读取文件时解码错误!')


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
