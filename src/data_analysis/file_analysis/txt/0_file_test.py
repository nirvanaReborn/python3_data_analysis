#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
名称                                说明
io                                  模块文件流的输入和输出操作input output
os                                  模块基本操作系统功能，包括文件操作
glob                                模块查找符合特定规则的文件路径名
fnmatch                             模块使用模式来匹配文件路径名
fileinput                           模块处理多个输入文件
filecmp                             模块用于文件的比较
cvs                                 模块用于csv 文件处理
xml                                 用于XML 数据处理
pickle 和cPickle                    用于序列化和反序列化
bz2、gzip、zipfile、zlib、tarfile   用于处理压缩和解压缩文件（分别对应不同的算法）
'''


# Python 中，一切皆对象，对象本质上就是一个“存储数据的内存块”。
# 有时候，我们需要将“内存块的数据”保存到硬盘上，或者通过网络传输到其他的计算机上。
# 这时候，就需要“对象的序列化和反序列化”。对象的序列化机制广泛的应用在分布式、并行系统上。
# 序列化指的是：将对象转化成“串行化”数据形式，存储到硬盘或通过网络传输到其他地方。
# 反序列化是指相反的过程，将读取到的“串行化数据”转化成对象。
# 在Python中要实现序列化和反序列化除了使用json模块之外，
# 还可以使用pickle和shelve模块，但是这两个模块是使用特有的序列化协议来序列化数据，
# 因此序列化后的数据只能被Python识别。


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
