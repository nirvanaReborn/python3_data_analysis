#!/usr/bin/env python
# -*- coding:utf-8 -*-

# python-magic：文件类型检测的第三方库 libmagic 的 Python 接口。
# 官网: https://github.com/ahupp/python-magic
#
import magic


def test_1():
    file_type = magic.from_file("testdata/test.pdf")
    print(file_type)  # 'PDF document, version 1.2'

    file_type = magic.from_buffer(open("testdata/test.pdf").read(1024))
    print(file_type)  # 'PDF document, version 1.2'

    file_type = magic.from_file("testdata/test.pdf", mime=True)
    print(file_type)  # 'application/pdf'


def test_2():
    f = magic.Magic(uncompress=True)
    file_type = f.from_file('testdata/test.gz')
    print(file_type)  # 'ASCII text (gzip compressed data, was "test", last modified: Sat Jun 28 21:32:52 2008, from Unix)'

    f = magic.Magic(mime=True, uncompress=True)
    file_type = f.from_file('testdata/test.gz')
    print(file_type)  # 'text/plain'


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
