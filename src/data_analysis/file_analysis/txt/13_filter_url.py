#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 过滤文本中的网址

import codecs
import os
import pandas
from public_function import GLOBAL_WORK_DIR


def test_1(source_file):
    list_2D = []
    with codecs.open(source_file, 'r', 'utf-8') as fr:
        file = fr.readlines()
        for i, fileLine in enumerate(file):
            if (fileLine.lstrip().startswith("http")) and i > 1:
                list_2D.append([file[i - 1], fileLine])
    return list_2D


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, "网址.txt")
    list_sheet = test_1(source_file)
    dest_file = r"网址.xlsx"
    list_header = "说明 网址".split()
    df = pandas.DataFrame(list_sheet, columns=list_header)
    df.to_excel(dest_file, sheet_name='网址', index=False, header=True)


if __name__ == "__main__":
    main()
