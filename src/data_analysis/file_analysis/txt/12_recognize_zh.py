#!/usr/bin/env python
# -*- coding: utf-8 -*-

# http://www.cnblogs.com/yourstars/p/5892754.html
# Python读取文本输出指定中文（字符串）

import codecs
import re


def func(text):
    line_pattern = r'\s*\d+\s?(.*)'
    c = re.compile(line_pattern)
    lists = []
    lines = text.split('\r\n')
    # print(lines)
    for line in lines:
        r = c.findall(line)
        if r:
            lists.append(r[0])
    return '\n'.join(lists)


def main():
    source_file = "12.txt"
    fr = codecs.open(source_file, 'r', 'gb2312')
    s = fr.readlines()
    fr.flush()
    fr.close()

    list = []
    for fileLine in s:
        # print(fileLine)
        if u'检查' in fileLine:
            result = func(fileLine)
            # print(result)
            list.append(result)
    print(list)


if __name__ == "__main__":
    main()
