#!/usr/bin/env python
# -*- coding:utf-8 -*-

import fileinput
import os
import re
import chardet
import openpyxl
import pandas
import time

import logging
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# logging.disable(logging.DEBUG) # 禁用日志
from public_function import dict_choice
from public_function import Excel_Pandas


def is_exist_keyword(target, source):
    if isinstance(target, str):
        if target in source:
            return True
    elif isinstance(target, list):
        for item in target:
            if item in source:
                return True
    elif isinstance(target, dict):
        for key in target:
            if target[key] in source:
                return True
    return False


def file_analysis_git_code(code_file, line_pattern):
    line_no = ""
    line_result = ""
    testdata = open(code_file, "rb").read()
    encoding = chardet.detect(testdata)['encoding']
    try:
        for fileLine in fileinput.input(code_file, openhook=fileinput.hook_encoded(encoding)):
            # print(fileinput.filename(), '|', 'Line Number:', fileinput.lineno(), '|: ', fileLine)
            # if (fileLine.lstrip().startswith("//")
            #     or fileLine.lstrip().startswith("#")):  # 注释
            #     # print(objectId, fileinput.filelineno())
            #     pass
            # else:
            c = re.compile(line_pattern)
            r = c.findall(fileLine)
            if r:
                line_no += str(fileinput.filelineno()) + ";"
                line_result += r[0] + ";"
    except:
        logging.debug("[8]读取文件时出错%s" % code_file)
    fileinput.close()
    list = [line_no, line_result, code_file]
    return list


def file_classification(source_file, line_pattern):
    list_except_suffix = [
        ".zip", ".rar", ".7z", ".gz",
        ".jpg", ".jpeg", ".png", ".gif", ".ico", ".bmp",
        ".wav", ".mp3",
        ".doc", ".docx",
        ".xls", ".xlsx",
        ".ppt",
        ".vsd",
        ".pdf",
        ".chm",
        ".dll", ".so", ".a", ".exe", ".depend", ".layout", ".bin", ".out", ".class", ".o", ".lib",
        ".dcu", ".dof", ".dpr", ".drc", ".map", ".ddp", ".res", ".dfm", ".pas", "rtf", ".local", "identcache",
        ".sln", ".suo", ".user", ".resx", ".settings", ".sdf", ".vcxproj", ".filters", ".rc",
        ".prj", ".tmw", ".pro", ".ui", ".5pre1", ".iml", ".kotlin_module", ".internal", ".includecache",
        ".bak", ".old", ".dat", ".db",
        ".url", ".xml", ".xaml",
    ]

    list_txt_suffix = [
        ".prefs", ".txt", ".log", ".ini", ".t", ".supp", ".dasc", ".yml", ".gyp", ".1", ".in",
        ".h", ".c", ".cpp", ".bat", ".cs", ".java", ".sh", ".kt", ".py", ".lua", ".php",
        ".pc", ".sql", ".vb", ".js", ".m", ".exp", ".pl", ".pdc",
    ]

    suffix = os.path.splitext(source_file)[1]
    if len(suffix) > 0:
        if is_exist_keyword(list_txt_suffix, suffix):
            # logging.debug("待处理的文件:%s" % source_file)
            list = file_analysis_git_code(source_file, line_pattern)
            if list != None and len(list) > 0:
                return list
        else:
            if is_exist_keyword(list_except_suffix, suffix):
                # logging.debug("[7]过滤掉的后缀:%s" % source_file)
                pass
            else:
                # logging.debug("[6]未识别后缀的文件:%s" % source_file)
                pass
    else:
        # logging.debug("[5]没有后缀的文件:%s" % source_file)
        return


def traverse(filepath, line_pattern):
    list_2D = []
    list_except_dir = [
        ".vscode",
        ".git",
        "obj",
        "bin",
        "Debug",
        "Release",
        ".vs",
        ".idea"
    ]
    list_except_file = [
        ".gitignore",
        "README.md",
        "wtmpx",
    ]
    list_except_key_word = [
        "（作废）",
        "(作废)",
    ]

    for parent, dirnames, filenames in os.walk(filepath):
        for file in filenames:
            source_file = os.path.join(parent, file).encode('utf-8').decode('utf-8')
            if is_exist_keyword(list_except_dir, parent):
                # logging.debug("[1]过滤掉的目录:%s" % parent)
                continue
            if file in list_except_file:
                # logging.debug("[2]过滤掉的文件:%s" % source_file)
                continue
            # 文件不为空
            if os.path.getsize(source_file):
                if is_exist_keyword(list_except_key_word, source_file):
                    # logging.debug("[4]过滤掉的文件%s" % source_file)
                    continue
                else:
                    # logging.debug("待处理的文件:%s" % source_file)
                    list = file_classification(source_file, line_pattern)
                    if list != None and len(list) > 0:
                        list_2D.append(list)
            else:
                logging.debug("[3]空文件:%s" % source_file)
                continue
    return list_2D


def test_1():
    list_sheet_1 = traverse(r"D:\share\git\gitee\zhangtao2016\course_python\src", "转置")
    dict_sheet_name = {
        "0": "搜索",
    }

    dict_head = {
        "0": "行号 内容 路径".split(),
    }

    list3D_sheet = []
    if list_sheet_1 != None and len(list_sheet_1) > 0:
        list3D_sheet.append(list_sheet_1)

    dest_file = r"文件内容搜索.xlsx"
    Excel_Pandas(dest_file).create_excel_pandas(dict_sheet_name, dict_head, list3D_sheet)


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == '__main__':
    main()
