#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
regex = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
'''
# http://blog.csdn.net/hk2291976/article/details/41545881

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# logging 库获取日志 requests 和 urllib3(requests 依赖) 对象，调低他们的日志级别，
# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
logging.getLogger("requests").setLevel(logging.WARNING)
import re
import os
from public_function import Excel_Pandas


def get_list_file_content(source_file):
    # pip install chardet chardet2
    import chardet

    with open(source_file, "rb") as fd:
        encoding = chardet.detect(fd.read(1024))["encoding"]

    with open(source_file, 'r', encoding=encoding) as fr:
        list_file_content = fr.readlines()

    return list_file_content


def test_1(dict_source_file, split_flag):
    def txt2excel_1(source_file, split_flag):
        list_sheet = []
        for fileLine in get_list_file_content(source_file):
            for item in fileLine.split(split_flag):
                logging.debug(item)
                # logging.debug(list(filter(lambda x: x not in '0123456789', item)))
                # logging.debug(re.compile("\d{1,3}$").findall(str(item)))
                str_temp = re.sub(r"\d{1,3}", "", str(item))
                # logging.debug(type(str_temp), str_temp)
                list_sheet.append([str_temp])
        return list_sheet

    str_header = "音乐"
    dest_file = r"音乐1.xlsx"
    Excel_Pandas(dest_file).create_excel_by_function(dict_source_file, str_header, txt2excel_1, (split_flag,))


def test_2(dict_source_file, split_flag):
    def txt2excel_2(source_file, split_flag):
        list_sheet = []
        for fileLine in get_list_file_content(source_file):
            list_re = re.findall("\d{1,3}\. (.*?)\n", fileLine, re.S)
            if list_re:
                newLine = list_re[0]
                # logging.debug(newLine)
                list_sheet.append(newLine.split(split_flag))
        return list_sheet

    str_header = "命令 描述"
    dest_file = r"音乐2.xlsx"
    Excel_Pandas(dest_file).create_excel_by_function(dict_source_file, str_header, txt2excel_2, (split_flag,))


def test_3(dict_source_file, split_flag):
    def txt2excel_3(source_file, split_flag):
        list_sheet = []
        for fileLine in get_list_file_content(source_file):
            logging.debug(fileLine)
            list_sheet.append(fileLine.split(split_flag[0]))
        return list_sheet

    str_header = "命令 描述 路径"
    dest_file = r"音乐3.xlsx"
    Excel_Pandas(dest_file).create_excel_by_function(dict_source_file, str_header, txt2excel_3, (split_flag,))


def test_4(dict_source_file, split_flag):
    def txt2excel_4(source_file, split_flag):
        list_sheet = []
        for fileLine in get_list_file_content(source_file):
            list_temp = []
            for item in fileLine.split(split_flag):
                if ("作词：" in item
                        or "作曲：" in item
                        or "编曲：" in item
                        or "演唱：" in item):
                    list_temp.append(str(item).split("：")[1])
                else:
                    list_temp.append(str(item))
            logging.debug(list_temp)
            list_sheet.append(list_temp)
        return list_sheet

    str_header = "序号 音乐 作曲 编曲"
    dest_file = r"音乐4.xlsx"
    Excel_Pandas(dest_file).create_excel_by_function(dict_source_file, str_header, txt2excel_4, (split_flag,))


def test_5(dict_source_file):
    def txt2excel_5(source_file, split_flag=None):
        list_sheet = []
        for fileLine in get_list_file_content(source_file):
            list_temp = []
            # logging.debug(fileLine)
            regular = r"(.*?)(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*,]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)(.*?)\n"
            pattern = re.compile(regular)
            list_temp = pattern.findall(fileLine)
            logging.debug(list_temp)
            if list_temp:
                list_sheet.extend(list_temp)

            # python 验证一个网址是否存在
            # requestscode = requests.get(list_temp[0][1]).status_code
            # if requestscode == 200:
            #     list_sheet.extend(list_temp)
            # else:
            #     logging.debug(requestscode)
        return list_sheet

    str_header = "序号 音乐 作曲 编曲"
    dest_file = r"音乐5.xlsx"
    Excel_Pandas(dest_file).create_excel_by_function(dict_source_file, str_header, txt2excel_5)


def test_6(dict_source_file, split_flag):
    str_sql = ""
    source_file = dict_source_file["1"]
    for fileLine in get_list_file_content(source_file):
        # 将多个空格换为一个空格
        list_temp = str(re.sub(' +', ' ', fileLine)).split(split_flag)
        # logging.debug(list_temp)
        str_sql += "--" + list_temp[1] + "\n"
        str_sql += "select a.* from " + list_temp[0] + " a;" + "\n\n"

    print(str_sql)


def test_7(dict_source_file, split_flag):
    def txt2excel_7(source_file, split_flag):
        list_sheet = []
        for fileLine in get_list_file_content(source_file):
            logging.debug(fileLine)
            list_sheet.append(fileLine.split(split_flag[0]))
        return list_sheet

    str_header = "参数  配置"
    dest_file = r"lscpu.xlsx"
    Excel_Pandas(dest_file).create_excel_by_function(dict_source_file, str_header, txt2excel_7, (split_flag,))


def test_8(dict_source_file, split_flag):
    def txt2excel_8(source_file, split_flag):
        list_sheet = []
        for fileLine in get_list_file_content(source_file):
            # logging.debug(fileLine)
            list_sheet.append(fileLine.split(split_flag[0]))

        for i, row in enumerate(list_sheet):
            for j, col in enumerate(row):
                list_sheet[i][j] = str(col).replace('“', '').replace('”\n', '')

        return list_sheet

    dest_file = r"高智商回答.xlsx"
    import pandas
    df = pandas.DataFrame(txt2excel_8(dict_source_file['1'], split_flag))
    df.to_excel(dest_file, index=False)


def get_file_suffix(source_file):
    (filepath, filename) = os.path.split(source_file)
    (shotname, extension) = os.path.splitext(filename)
    return (filepath, filename, shotname, extension)[2]


def main():
    dict_source_file = {
        # "0": r"D:\share\git\gitee\nirvanaReborn\python3_course\doc\Python3标准库.txt",
        "1": os.path.expanduser(r"~\Desktop\1.txt"),
    }
    # dict_source_file = glob.glob(r'D:\share\test\*.txt')

    list_split_flag = [
        ":",
        "：",
        ",",
        "，",
        "\t",
        " ",
        "  ",
        ". ",
        "->",
        "|",
        "”“",
    ]

    dict_choice = {
        "1": "test_1(dict_source_file, list_split_flag[3])",
        "2": "test_2(dict_source_file, list_split_flag[1])",
        "3": "test_3(dict_source_file, list_split_flag[8])",
        "4": "test_4(dict_source_file, list_split_flag[1])",
        "5": "test_5(dict_source_file)",
        "6": "test_6(dict_source_file, list_split_flag[5])",
        "7": "test_7(dict_source_file, list_split_flag[0])",
        "8": "test_8(dict_source_file, list_split_flag[10])",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
