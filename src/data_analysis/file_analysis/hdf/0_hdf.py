#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/kevinelstri/article/details/61921812
# 使用python读取数据科学最常用的文件格式
'''
在分层数据格式（HDF），您可以很容易存储大量的数据。它不仅用于存储高容量或复杂的数据，而且还用于存储小体积或简单的数据。
使用HDF的优点：
    1.它可用于各种规模和类型的系统
    2.它具有灵活，高效的存储和快速I/O.
    3.多格式支持HDF
现在有多个HDF格式。但是，HDF5是最新的版本，它用来解决一些老的HDF文件格式的限制。
HDF5格式与XML相似，像XML，HDF5文件都是自描述的，允许用户指定复杂的数据关系和依赖关系。
以一个HDF5文件格式为例，可以识别以.h5为扩展的文件： 
'''
import os
import sys

from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE
from public_function import dict_choice


def test_1():
    import pandas
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "pandas_test.py")
    print(call_file)
    os.popen(call_file)


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
