#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://blog.csdn.net/kevinelstri/article/details/61921812
import pandas


# 读取HDF5文件
def test_4():
    pf = pandas.read_hdf('train.h5')
    print(pf)


def main():
    test_4()


if __name__ == "__main__":
    main()
