#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-json.html
# Python3 JSON 数据解析
'''
在Python中要实现序列化和反序列化除了使用json模块之外，
还可以使用pickle和shelve模块，但是这两个模块是使用特有的序列化协议来序列化数据，
因此序列化后的数据只能被Python识别。
'''
import os

from public_function import GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE


def test_1():
    try:
        import simplejson as json
    except ImportError:
        import json
    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "json_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    # import demjson
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "demjson_test.py")
    print(call_file)
    os.popen(call_file)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
