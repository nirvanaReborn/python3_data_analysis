#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://my.oschina.net/reter/blog/3154767
# https://my.oschina.net/reter/blog/3154767
tree_dict = {
    'data': 'Root',
    'children': [
        {
            'data': 'Child 1',
            'children': []
        },
        {
            'data': 'Child 2',
            'children': [
                {
                    'data': 'Child 2.1',
                    'children': [
                        {
                            'data': 'Child 2.1.1',
                            'children': []
                        }
                    ]
                },
                {
                    'data': 'Child 2.2',
                    'children': [
                        {
                            'data': 'Child 2.2.1',
                            'children': []
                        }
                    ]
                },
                {
                    'data': 'Child 2.3',
                    'children': [
                        {
                            'data': 'Child 2.3.1',
                            'children': []
                        }
                    ]
                }
            ]
        },
        {
            'data': 'Child 3',
            'children': [
                {
                    'data': 'Child 4.1',
                    'children': []
                },
                {
                    'data': 'Child 4.1',
                    'children': []
                },
                {
                    'data': 'Child 4.1',
                    'children': []
                },
                {
                    'data': 'Child 4.1',
                    'children': []
                },
                {
                    'data': 'Child 4.1',
                    'children': [
                        {
                            'data': 'Child 4.1',
                            'children': []
                        },
                        {
                            'data': 'Child 4.1',
                            'children': []
                        },
                        {
                            'data': 'Child 4.1',
                            'children': []
                        },
                        {
                            'data': 'Child 4.1',
                            'children': []
                        },
                        {
                            'data': 'Child 4.1',
                            'children': []
                        },
                    ]
                },
                {
                    'data': 'Child 4.1',
                    'children': [
                        {
                            'data': 'Child 4.1',
                            'children': []
                        }, {
                            'data': 'Child 4.1',
                            'children': []
                        }, {
                            'data': 'Child 4.1',
                            'children': []
                        }, {
                            'data': 'Child 4.1',
                            'children': []
                        },
                    ]
                },
                {
                    'data': 'Child 4.6',
                    'children': []
                },
            ]
        },
        {
            'data': 'Child 4',
            'children': [
                {
                    'data': 'Child 4.1',
                    'children': []
                },
                {
                    'data': 'Child 4.2',
                    'children': []
                }
            ]
        }
    ]
}


def print_tree():
    VLine = '│'  # 垂直线
    HLine = '─'  # 水平线
    VHLine = '├'  # 垂直+水平线
    URLine = '└'  # 转弯线，用于最后一个结点

    def helper(node):

        # 先打印前面的字符
        i = 0
        while i < len(parents) - 1:
            # 从左边往右边遍历，检查每个父结点在祖父结点中是不是最后一个
            if parents[i]['children'][-1] is parents[i + 1]:
                # 是最后一个，打印空白
                print(f'  ', end='')
            else:
                # 不是，打印竖线
                print(f'{VLine} ', end='')
            i += 1

        # 我们想比较两个结点是不是同一个对象，而不是比较两个结点的内容是否相等
        # 用 is 判断
        is_last = parents[-1]['children'][-1] is node

        if is_last:
            # 当前结点是最后一个，结束
            print(f'{URLine}{HLine}', end='')
        else:
            # 还有兄弟结点
            print(f'{VHLine}{HLine}', end='')

        print(node['data'], end='\n')
        parents.append(node)
        children = node['children']
        for child in children:
            helper(child)
        parents.pop()

    root = tree_dict
    parents = [root]
    print(root['data'])
    children = root['children']
    for child in children:
        helper(child)
    parents.pop()
    assert len(parents) == 0


if __name__ == "__main__":
    print_tree()
