#!/usr/bin/env python
# -*- coding:utf-8 -*-


# https://www.toutiao.com/a6519847374913798659/
# 十行代码就能实现json文件的格式化！
try:
    import simplejson as json
except ImportError:
    import json


def format_json(source_file, dest_file):
    with open(source_file, "r", encoding='utf-8') as fr:
        data = json.load(fr)
        # ensure_ascii=False, 就不会用 ASCII 编码，中文就可以正常显示了
        # sort_keys: 是否排序（默认False–不排序）,参数sort_keys是设置输出是否按照[a-z]顺序输出
        # indent: 缩进（一般填2，缩进2格）；
        newjson = json.dumps(data, ensure_ascii=False, sort_keys=True, indent=2, separators=(',', ':'))
        print(newjson)

    with open(dest_file, "w", encoding="utf-8") as fw:
        fw.write('%s\n\n' % newjson)


def main():
    source_file = r"listen1_backup.json"
    dest_file = r"listen1_backup_format.json"
    format_json(source_file, dest_file)


if __name__ == "__main__":
    main()
