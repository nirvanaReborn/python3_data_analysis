#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#

import demjson


def main():
    data = [{'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}]
    # 将 Python 对象编码成 JSON 字符串。
    json = demjson.encode(data)  # json.dumps()
    print(json)

    json = '{"a":1,"b":2,"c":3,"d":4,"e":5}'
    # 对数据进行解码,将已编码的 JSON 字符串解码为 Python 对象。
    text = demjson.decode(json)  # json.loads()
    print(text)


if __name__ == "__main__":
    main()
