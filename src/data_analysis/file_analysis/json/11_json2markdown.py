#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/sinat_33718563/article/details/125266379
# Json转Markdown
# pip3 install torsimany
# python3 torsimany.py [JSON_FILE].json

import json


class Json2Markdown(object):
    """
    # json转markdown形式
    """
    def __init__(self):
        self.markdown = ""
        self.tab = "  "
        self.list_tag = '* '
        self.htag = '#'

    def loadJSON(self, file):
        """
        :param file:
        :return:
        """
        with open(file, 'r') as f:
            data = f.read()
        return json.loads(data)

    def parseJSON(self, json_block, depth):
        """
        :param json_block:
        :param depth:
        :return:
        """
        if isinstance(json_block, dict):
            self.parseDict(json_block, depth)
        if isinstance(json_block, list):
            self.parseList(json_block, depth)

    def parseDict(self, d, depth):
        """
        :param d:
        :param depth:
        :return:
        """
        for k in d:
            if isinstance(d[k], (dict, list)):
                self.addHeader(k, depth)
                self.parseJSON(d[k], depth + 1)
            else:
                self.addValue(k, d[k], depth)

    def parseList(self, l, depth):
        """
        :param l:
        :param depth:
        :return:
        """
        for value in l:
            if not isinstance(value, (dict, list)):
                index = l.index(value)
                self.addValue(index, value, depth)
            else:
                self.parseDict(value, depth)

    def buildHeaderChain(self, depth):
        """
        :param depth:
        :return:
        """
        chain = self.list_tag * (bool(depth)) + self.htag * (depth + 1) + \
                ' value ' + (self.htag * (depth + 1) + '\n')
        return chain

    def buildValueChain(self, key, value, depth):
        """
        :param key:
        :param value:
        :param depth:
        :return:
        """
        chain = self.tab * (bool(depth - 1)) + self.list_tag + \
                str(key) + ": " + str(value) + "\n"
        return chain

    def addHeader(self, value, depth):
        """
        :param value:
        :param depth:
        :return:
        """
        chain = self.buildHeaderChain(depth)
        self.markdown += chain.replace('value', value.title())

    def addValue(self, key, value, depth):
        """
        :param key:
        :param value:
        :param depth:
        :return:
        """
        chain = self.buildValueChain(key, value, depth)
        self.markdown += chain

    def json2markdown(self, json_data):
        """
        :param json_data:
        :return:
        """
        depth = 0
        self.parseJSON(json_data, depth)
        self.markdown = self.markdown.replace('#######', '######')
        return self.markdown

    def save(self, dest_file):
        with open(dest_file, 'w', encoding='UTF-8') as f:
            f.write(self.markdown)


def test_1():
    json_data = [
        {
            "scene": "气泡触发次数过高(1小时)",
            "data": [
            ]
        },
        {
            "scene": "重点人触发气泡过多(1小时)",
            "data": [
            ]
        },
        {
            "scene": "某一气泡CTR过低(1天)",
            "data": [
                "马家春慢#0#60#0.0",
                "法曲献仙音#1#135#0.007",
                "月宫春#0#91#0.0",
                "海棠花令#0#97#0.0"
            ]
        }
    ]

    # 实例
    json2markdown_ins = Json2Markdown()

    # json转markdown
    markdown_data = json2markdown_ins.json2markdown(json_data)
    print(markdown_data)
    json2markdown_ins.save("11_test.md")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
