#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
# tabulate默认没有考虑中文字符宽度，因此无法对齐，如要实现对齐，需要wcwidth 包。
from tabulate import tabulate
import wcwidth

tabulate.PRESERVE_WHITESPACE = True  # 保留空格
tabulate.WIDE_CHARS_MODE = False  # 宽（全角中日韩）符号
import pandas as pd
import os
try:
    import simplejson as json
except ImportError:
    import json

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
# pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)


def test_1():
    source_file = r"D:\SVN\PBOX-FLY\branches\PBOXQT1.0V202101.06.001\Sources\fly_IQ_trunk\fly_server\download\stock_paused.json"
    dest_file = os.path.join(os.path.dirname(source_file), "secu_paused.csv")
    dict_data = {}
    with open(source_file, 'r', encoding='utf-8') as fd:
        dict_data.update(json.load(fd))
    source_file = r"D:\SVN\PBOX-FLY\branches\PBOXQT1.0V202101.06.001\Sources\fly_IQ_trunk\fly_server\download\lof_paused.json"
    with open(source_file, 'r', encoding='utf-8') as fd:
        dict_data.update(json.load(fd))
    source_file = r"D:\SVN\PBOX-FLY\branches\PBOXQT1.0V202101.06.001\Sources\fly_IQ_trunk\fly_server\download\etf_paused.json"
    with open(source_file, 'r', encoding='utf-8') as fd:
        dict_data.update(json.load(fd))
    new_data = {}
    for k, v in dict_data.items():
        new_data[k] = [';'.join(v)]
    # pprint(new_data)
    df = pd.DataFrame(new_data)
    df = df.T  # 转置
    print(df)
    # index=False表示csv文件不加行序号
    df.to_csv(dest_file, index=True, header=False)


def test_2():
    source_file = r"D:\1.json"
    # 从JSON文件中加载数据
    with open(source_file, 'r', encoding='utf-8') as f:
        data = json.load(f)
    # 创建DataFrame对象
    df = pd.DataFrame(data['data'])
    # 打印输出结果
    print(df)
    # https://blog.csdn.net/The_Time_Runner/article/details/88791587
    # print(tabulate(df, headers='keys', tablefmt='fancy_grid'))


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
