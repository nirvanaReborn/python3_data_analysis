#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-json.html
# Python3 JSON 数据解析
'''
json 类型转换到 python 的类型对照表：

JSON	        Python
object	        dict
array	        list
string	        unicode
number (int)	int, long
number (real)	float
true	        True
false	        False
null	        None


1、json.dumps()和json.loads()是json格式处理函数（可以这么理解，json是字符串），针对内存对象：
　　(1)json.dumps()函数是将一个Python数据类型列表进行json格式的编码（可以这么理解，json.dumps()函数是将字典转化为字符串）
　　(2)json.loads()函数是将json格式数据转换为字典（可以这么理解，json.loads()函数是将字符串转化为字典）

2、json.dump()和json.load()针对文件句柄，主要用来读写json文件函数。
　　(1)json.dump()是将python数据保存成json格式文件
　　(2)json.load()是读取json格式文件

Python的json是simplejson。
也就是Python采用了simplejson的一个版本，并将其合并到每个发行版中。
所以呢，相当于simplejson是个大容器，Python只是用了其中的一个而已。

使用simplejson具有一些优点：
1.它适用于更多Python版本。
2.它具有用C编写的部分，因此非常快速。
'''

try:
    import simplejson as json
except ImportError:
    import json


# json.dump：将python数据保存成json格式文件
def test_1():
    import collections
    dict_data = collections.OrderedDict([
        ('书签栏', 'A wise guy, huh?'),
        ('娱乐', 'Ow!'),
        ('影视', 'Nyuk nyuk!'),
    ])
    # print("数据类型：" + str(type(dict_data)))  # <class 'collections.OrderedDict'>
    dest_file = 'data.json'
    with open(dest_file, 'w', encoding='UTF-8') as fw:
        # ensure_ascii=False: 解决中文乱码问题
        json.dump(dict_data, fw, ensure_ascii=False)


# json.load：读取json格式文件
def test_2():
    # 设置以utf-8解码模式读取文件，encoding参数必须设置，
    # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
    source_file = 'data.json'
    with open(source_file, 'r', encoding='utf-8') as fd:
        data = json.load(fd)
    return data


# json.dumps()：将字典转化为json字符串
def test_3():
    dict1 = {"age": "12"}
    json_info = json.dumps(dict1)
    print("dict1的类型：" + str(type(dict1)))  # <class 'dict'>
    print("通过json.dumps()函数处理：")
    print("json_info的类型：" + str(type(json_info)))  # <class 'str'>


# json.loads()：将json字符串转化为字典
def test_4():
    json_info = '{"age": "12"}'
    dict_data = json.loads(json_info)
    print("json_info的类型：" + str(type(json_info)))  # <class 'str'>
    print("通过json.dumps()函数处理：")
    print("转换后的数据类型：" + str(type(dict_data)))  # <class 'dict'>
    # dest_file = 'data.json'
    # with open(dest_file, 'w', encoding='UTF-8') as fw:
    #     # ensure_ascii=False: 解决中文乱码问题
    #     json.dump(dict_data, fw, ensure_ascii=False)


def test_5():
    data1 = {'no': 1, 'name': 'Runoob', 'url': 'http://www.runoob.com'}

    # Python 字典类型转换为 JSON 对象
    json_str = json.dumps(data1)
    print("Python 原始数据：", repr(data1))
    print("JSON 对象：", json_str)

    # 将 JSON 对象转换为 Python 字典
    data2 = json.loads(json_str)
    print("data2['name']: ", data2['name'])
    print("data2['url']: ", data2['url'])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
