#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 遍历本地音乐

import collections
import os
import shutil
import numpy
import pandas
import re
from public_function import Excel_Pandas


def deal_music_info(music_info):
    if "(" in music_info:
        music_info = re.sub("\(.*?\)", "", music_info)
    elif "（" in music_info:
        music_info = re.sub("（.*?）", "", music_info)
    elif "·" in music_info:
        music_info = re.sub("·", "", music_info)
    elif "." in music_info:
        music_info = re.sub("\.", "", music_info)
    elif "：" in music_info:
        music_info = re.sub("：", "", music_info)
    elif ":" in music_info:
        music_info = re.sub(":", "", music_info)
    elif "*" in music_info:
        music_info = re.sub("\*", "_", music_info)
    else:
        music_info = str(music_info).replace("/", "")
    return str(music_info).replace("/", "").strip()


def get_dict_music(source_file, sheet_name):
    dict_music = {}
    list_sheet = numpy.array(pandas.read_excel(source_file, sheet_name=sheet_name)).tolist()
    for i, fileLine in enumerate(list_sheet):
        try:
            fileLine[2] = deal_music_info(str(fileLine[2]))  # 歌曲名称
            fileLine[3] = deal_music_info(str(fileLine[3]))  # 作者
            if sheet_name == "listen1_music":  #
                # if len(fileLine[0]) > 5:  # 过滤掉专辑
                #     continue

                if "-" in fileLine[2]:  # 歌曲名称
                    print("歌名带有-:", fileLine[2])
                    fileLine[2] = re.sub("-", "", fileLine[2]).replace(' ', '')

                if "-" in fileLine[3]:  # 作者
                    print("作者带有-:", fileLine[2])
                    fileLine[3] = re.sub("-.*", "", fileLine[3])
                if fileLine[3] == "董贞":  # 作者
                    fileLine[3] = "董真"

                if fileLine[2]:
                    dict_music[(fileLine[2], fileLine[3])] = fileLine
                else:
                    print("歌曲名称不规范:", fileLine)
            else:
                # (歌名，作者) = 文件路径
                dict_music[(fileLine[2], fileLine[3])] = fileLine
        except:
            print(fileLine)
    return dict_music


def commpare_music(source_file, sheet_name_1, sheet_name_2):
    list_sheet = [("歌名", "作者", "平台")]
    dict_music_1 = get_dict_music(source_file, sheet_name_1)
    dict_music_2 = get_dict_music(source_file, sheet_name_2)
    for key in dict_music_1:
        if key in dict(dict_music_2).keys():
            # print("共有", dict_music_1[key])
            pass
        else:
            # print(sheet_name_1, "独有:", dict_music_1[key])
            pass

    for key in dict_music_2:
        list_temp = []
        list_temp.extend(key)
        if key in dict(dict_music_1).keys():
            pass
        else:
            print(sheet_name_2, "独有:", dict_music_2[key])
            list_temp.append(dict_music_2[key])
            list_sheet.append(list_temp)
    return list_sheet


# 遍历source_dir下所有子文件,并过滤文件后缀
def get_file_by_suffix(source_dir):
    list_file = []
    for parent, dirnames, filenames in os.walk(source_dir):
        for filename in filenames:
            source_file = os.path.join(parent, filename).encode('utf-8').decode('utf-8')
            # 文件不为空
            if os.path.getsize(source_file):
                list_file.append(source_file)
            else:
                print("空文件:%s" % source_file)
                continue
    return list_file


def scan_dir(source_dir):
    list_result = []
    # print(source_dir)
    list_file = get_file_by_suffix(source_dir)

    for source_file in list_file:
        (filepath, filename) = os.path.split(source_file)
        (shotname, suffix) = os.path.splitext(filename)

        # list_except_suffix = [".jpg", ".ini"]
        if suffix == ".jpg" or suffix == ".ini":
            pass
        else:
            list_temp = str(shotname).split("-")
            if len(list_temp) == 0:
                print("没有名称：", source_file)
            elif len(list_temp) == 1:
                list_temp.append("")
                print("文件名没有-：", source_file)
                # new_music_name =str(source_file).replace("纯音乐", "待命名")
                # shutil.move(source_file, new_music_name)
            # elif len(list_temp) > 2:
            #     for i, item in enumerate(list_temp):
            #         if i > 1:
            #             list_temp[1] += list_temp[i]

            music = str(list_temp[0]).strip()
            author = str(list_temp[1]).strip()
            remark = "-".join(list_temp[2:]) + " "
            list_result.append([filepath, filename, music, author, remark, suffix, source_file])

    return list_result


def music_classification(source_file, sheet_name, dest_dir):
    list_sheet = numpy.array(pandas.read_excel(source_file, sheet_name=sheet_name)).tolist()
    list_author = Excel_Pandas(source_file).get_list1D_from_list2D(list_sheet, 3)
    dict_author = dict(Excel_Pandas(source_file).count_usage_frequency_by_dict(list_author))
    # pprint.pprint(dict_author)
    for i, fileLine in enumerate(list_sheet):
        # 移动歌曲
        work_dir = os.path.join(dest_dir, str(fileLine[3]).strip())
        new_music_name = os.path.join(work_dir, fileLine[1])

        if not os.path.exists(work_dir):
            if dict_author.get(str(fileLine[3]).strip(), 0) > 1:
                os.makedirs(work_dir)

        if os.path.exists(work_dir):
            print(fileLine[6], "----------------->", new_music_name)
            shutil.move(fileLine[6], new_music_name)


def rename_music(source_file, dest_file, sheet_name, music_name_rule=False):
    def count_author(list_sheet):
        list_1D = []
        for i, fileLine in enumerate(list_sheet):
            list_1D.append(fileLine[3])
        return dict(collections.Counter(list_1D))

    dict_sheet_new = {}
    list_sheet = numpy.array(pandas.read_excel(source_file, sheet_name=sheet_name)).tolist()

    list_sheet_new = []
    for i, fileLine in enumerate(list_sheet):
        list_temp = []
        if music_name_rule:
            # 作者 - 歌名 - 备注 - 后缀
            music_name = str(fileLine[3]).strip() + " - " + str(fileLine[2]).strip()
        else:
            # 歌名 - 作者 - 备注 - 后缀
            music_name = str(fileLine[2]).strip() + " - " + str(fileLine[3]).strip()
        if len(str(fileLine[4]).replace("nan", "").strip()) > 0:
            music_name += " - " + str(fileLine[4]).replace("nan", "").strip()
        music_name += fileLine[5]

        new_music_name = os.path.join(fileLine[0], music_name)
        if os.path.exists(fileLine[6]):
            # 重命名歌名
            print(fileLine[6], "----------------->", new_music_name)
            os.rename(fileLine[6], new_music_name)
        list_temp.extend(fileLine)
        list_temp.append(new_music_name)
        list_sheet_new.append(list_temp)
    # print(count_author(list_sheet_new))
    dict_sheet_new[sheet_name] = list_sheet_new

    # Excel_Pandas(dest_file).create_excel_pandas_by_dict(dict_sheet_new, header_flag=False)
    return dict_sheet_new


def main():
    root_dir = os.path.join(r"D:/", os.environ['USERNAME']).replace('/', '\\')
    dict_source_dir = {
        "local_music": os.path.join(root_dir, r"Music\Music\歌曲"),
        # "MV": os.path.join(root_dir, r"Music\MV"),
        "待整理": os.path.join(root_dir, r"Music\Music\歌曲\待整理"),
    }
    dest_file = r"music.xlsx"
    str_header = "目录 文件名 歌名 作者 备注 后缀 路径"
    Excel_Pandas(dest_file).create_excel_by_function(dict_source_dir, str_header, scan_dir)

    dict_source_dir = {
        "待整理": os.path.join(root_dir, r"Music\Music\歌曲\待整理"),
    }
    for key in dict_source_dir:
        # rename_music(dest_file, r"rename.xlsx", key)
        music_classification(dest_file, key, os.path.join(root_dir, r"Music\Music\歌曲\专辑"))

    # list_sheet = commpare_music(dest_file, "local_music", "MV")
    list_sheet = commpare_music(dest_file, "local_music", "listen1_music")
    dict_sheet = {}
    dict_sheet["listen1_music"] = list_sheet
    dest_file = r"..\..\..\..\..\YaVipCore\FishMusic\bin\Debug\configMusic.xlsx"
    Excel_Pandas(dest_file).create_excel_pandas_by_dict(dict_sheet)


if __name__ == "__main__":
    main()
