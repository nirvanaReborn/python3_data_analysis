#!/usr/bin/env python
# -*- coding:utf-8 -*-
# https://golangnote.com/topic/105.html
# python 里 ujson 和 msgpack 表现差不多,都比json要快，ujson 稍快一点点。

from time import time
import msgpack
import json
import ujson 

def exampleJson(ts):
    t1 = time()

    for i in xrange(100000):
        b = json.dumps(ts)
        out = json.loads(b)
    print 'json', time() - t1, 's'


def exampleUjson(ts):
    t1 = time()

    for i in xrange(100000):
        b = ujson.dumps(ts)
        out = ujson.loads(b)
    print 'ujson', time() - t1, 's'


def exampleMsgpack(ts):
    t1 = time()

    for i in xrange(100000):
        b = msgpack.packb(ts)
        out = msgpack.unpackb(b)
    print 'msgpack', time() - t1, 's'


if __name__ == "__main__":
    
    ts = {
        "C":   "LOCK",
        "K":   "31uEbMgunupShBVTewXjtqbBv5MndwfXhb",
        "T":   1000,
        "Max": 200,
        "Cn":  "中文",
        "Cn1": "中文",
        "Cn2": "中文",
        "Cn3": "中文",
        "Cn4": "中文",
        "Cn5": "中文",
        "Cn6": "中文",
        "Cn7": "中文",
        "Cn8": "中文",
        "Cn9": "中文",
    }
    exampleJson(ts)
    exampleUjson(ts)
    exampleMsgpack(ts)
