#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# 将listen1_backup.json文件中的歌曲写入到一个sheet页中

import logging

logging.captureWarnings(True)  # 通过捕获警告到日志的方式忽略警告
import requests
import json
import csv
import colorama
import pandas
import numpy
import sys
sys.path.append('../..')
from public_function import Excel_Pandas

dict_plat = {
    "netease": "网易音乐",
    "qq": "QQ音乐",
    "kugou": "酷狗音乐",
    "kuwo": "酷我音乐",
    "bilibili": "哔哩哔哩",
    "migu": "咪咕音乐",
    "xiami": "虾米音乐",
    "qianqian": "千千音乐",
}


def is_equal_keyword(source, target):
    if isinstance(source, str):
        if source == target:
            return True
    elif isinstance(source, list):
        for item in source:
            if item == target:
                return True
    elif isinstance(source, dict):
        for key in source:
            if source[key] == target:
                return True
    return False


# 获取相同名称的歌曲
def get_same_music(list_music):
    list_2D = []
    list_temp = []
    # ['song_sheet', 'id', 'title', 'artist', 'source', 'source_url']
    for i, fileLine in enumerate(list_music):
        if is_equal_keyword(list_temp, fileLine[2]):
            # print("重名歌曲：", i, fileLine)
            list_2D.append(fileLine)
        else:
            list_temp.append(fileLine[2])
    return list_2D


def data_mining(sheet_name, dict_item):
    str_title = "id	title artist artist_id album album_id img_url source source_url url disabled"
    list_title = str_title.split()
    list_1D = []
    list_head = []
    for key, value in dict_item.items():
        # print(key, '---------->', value)
        if key in list_title:
            list_head.append(key)
            if key == "source":
                list_1D.append(dict_plat[value])
            else:
                list_1D.append(value)
    list_1D.insert(0, sheet_name)
    list_head.insert(0, "song_sheet")
    return list_1D, list_head


def load_json(source_file):
    list_dict_musicList = []
    list_playe_class = []
    # 设置以utf-8解码模式读取文件，encoding参数必须设置，
    # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
    fd = open(source_file, encoding='utf-8')
    dict_music = json.load(fd)
    # print(type(dict_music))
    for key, value in dict_music.items():
        # print(key, '---------->', value)
        if key == "playerlists":
            list_playe_class = value

    for key, value in dict_music.items():
        # print(key, '---------->', value)
        if key in list_playe_class:
            dict_musicList = dict_music[key]
            list_dict_musicList.append(dict_musicList)
    return list_dict_musicList


def get_excel_date(source_file):
    list_id = []
    list_head = []
    list_sheet = []
    dict_sheet = {}

    list_dict_musicList = load_json(source_file)
    for i, dict_musicList in enumerate(list_dict_musicList):
        # for key, value in dict_musicList.items():
        #     print(key, '---------->', value)
        list_music = []
        sheet_name = dict_musicList['info']['title']
        for dict_item in dict_musicList['tracks']:
            list_1D, list_head = data_mining(sheet_name, dict_item)
            if list_1D[1] in list_id:
                print(colorama.Fore.RED + "重名歌曲id：", list_1D)
            else:
                list_id.append(list_1D[1])
            list_music.append(list_1D)
        list_sheet.extend(list_music)
    list_same_music = get_same_music(list_sheet)
    list_same_music.insert(0, list_head)
    # dict_sheet["listen1_same_music"] = list_same_music
    list_sheet.insert(0, list_head)
    dict_sheet["listen1_music"] = list_sheet
    return dict_sheet


def test_1(source_url):
    response = requests.get(source_url)
    html = response.text
    print(html)


# 检查歌曲地址是否有效
def check_url(dest_file):
    dataframe = pandas.read_excel(dest_file, sheet_name='listen1_music')
    list_sheet = list(numpy.array(dataframe).tolist())
    for i, fileLine in enumerate(list_sheet):
        if fileLine[0] == "我的歌单" and fileLine[2] == "纷飞(live版)":
            # http://music.163.com/#/song?id=28258197
            source_url = fileLine[5]
            print(source_url)
            test_1(source_url)


def create_csv(dict_sheet):
    try:
        for key, value in dict_sheet.items():
            dest_file = str(key) + '.csv'
            if value != None and len(value) > 0:
                with open(dest_file, 'w', encoding='utf-8') as csvfile:
                    spamwriter = csv.writer(csvfile)
                    spamwriter.writerows(value)
                print(colorama.Fore.GREEN + "生成CSV文档:", dest_file + colorama.Style.RESET_ALL)
            else:
                print(colorama.Fore.BLUE + dest_file, "数据为空" + colorama.Style.RESET_ALL)
    except Exception as e:
        print(e)


def main():
    source_file = r"listen1_backup.json"
    dest_file = r"music.xlsx"
    dict_sheet = get_excel_date(source_file)
    create_csv(dict_sheet)
    Excel_Pandas(dest_file).create_excel_pandas_by_dict(dict_sheet)

    # check_url(dest_file)
    # test_1("http://music.163.com/#/song?id=28258197")


if __name__ == "__main__":
    main()
