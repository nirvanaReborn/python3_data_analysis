#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 我们可以使用pickle 模块中的函数，实现序列化和反序列操作。
# pickle.dump(obj, file) obj 就是要被序列化的对象，file 指的是存储的文件
# pickle.load(file) 从file 读取数据，反序列化成对象
'''
pickle协议和JSON（JavaScript Object Notation）的区别 ：
1. JSON是一种文本序列化格式（它输出unicode文本，虽然大部分时间它被编码utf-8），而pickle是二进制序列化格式;
2. JSON是人类可读的，而pickle则不是;
3. JSON是可互操作的，并且在Python生态系统之外广泛使用，而pickle是特定于Python的;
'''
import pickle


# 将对象序列化到文件中
def test_1():
    with open(r"d:\data.dat", "wb") as f:
        a1 = "高淇"
        a2 = 234
        a3 = [20, 30, 40]
        pickle.dump(a1, f)
        pickle.dump(a2, f)
        pickle.dump(a3, f)


# 将获得的数据反序列化成对象
def test_2():
    with open(r"d:\data.dat", "rb") as f:
        a1 = pickle.load(f)
        a2 = pickle.load(f)
        a3 = pickle.load(f)
        print(a1)
        print(a2)
        print(a3)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
