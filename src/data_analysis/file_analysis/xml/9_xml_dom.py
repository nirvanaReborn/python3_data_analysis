#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_39008941/article/details/76037730
'''python获取xml文件方法集合
引入模块处理xml文件
from xml.dom.minidom import parse

打开xml文档
DOMTree = xml.dom.minidom.parse(data_path)

根据xml文档，得到文档元素的对象
data = DOMTree.documentElement

获取节点列表
nodelist = data.getElementsByTagName(大类名称)

获取第一个节点的子节点列表
childlist = nodelist[0].childNodes

获取XML节点属性值
node.getAttribute(AttributeName)

获取XML节点对象集合
node.getElementsByTagName(TagName)

返回子节点列表
node.childNodes

获取XML节点值
node.childNodes[index].nodeValue

访问第一个节点
node.firstChild ,等价于pagexml.childNodes[0]

访问元素属性 例如：
Node.attributes["id"]
a.name #就是上面的 "id"
a.value #属性的值
'''

import codecs
import os
import sys
import xml.dom.minidom


# 获得标签对之间的数据:方法二
# getchildren方法按照文档顺序返回所有子标签。并输出标签名（child.tag）和标签的数据（child.text）
# 其实，方法二的作用不在于此，它核心功能是可以遍历某一级标签下的所有子标签。


# 获得标签对之间的数据:方法一
def read_test_5(dom):
    # noinspection PyProtectedMember
    print("当前文件名:", sys._getframe().f_code.co_filename)  # 当前文件名，可以通过__file__获得
    # noinspection PyProtectedMember
    print("当前函数名:", sys._getframe().f_code.co_name)  # 当前函数名
    # noinspection PyProtectedMember
    print("当前的行号:", sys._getframe().f_lineno)  # 当前行号

    itemlist = dom.getElementsByTagName('plugins')
    for item in itemlist:
        child_list = item.getElementsByTagName('plugin')
        for child in child_list:
            print(child.getAttribute("lib"))


# 获得子标签
def read_child_tag(root):
    # noinspection PyProtectedMember
    print("当前文件名:", sys._getframe().f_code.co_filename)  # 当前文件名，可以通过__file__获得
    # noinspection PyProtectedMember
    print("当前函数名:", sys._getframe().f_code.co_name)  # 当前函数名
    # noinspection PyProtectedMember
    print("当前的行号:", sys._getframe().f_lineno)  # 当前行号

    itemlist = root.getElementsByTagName('monsvr')
    item = itemlist[0]
    print(item.nodeName, item.nodeValue)

    itemlist = root.getElementsByTagName('mainsvr')
    item = itemlist[0]
    print(item.nodeName, item.nodeValue)


def read_root(dom):
    # 得到文档元素对象
    root = dom.documentElement
    print(root.nodeName)  # 结点名字
    print(root.nodeValue)  # 结点的值，只对文本结点有效
    print(root.nodeType)  # 结点的类型
    print(root.ELEMENT_NODE)
    print(dict(root.attributes))  # 获取XML节点所有属性值
    print(root.getAttribute("name"))  # 获取XML节点某个属性值
    return root


# https://blog.csdn.net/whzhcahzxh/article/details/33735293
# 先将xml解析成utf8格式，然后替代第一行的encoding格式，然后保存一个新文件，提供后续解析，在解析完之后将这个新文件删除。
def ConverEncoding(source_file):
    with codecs.open(source_file, 'rb', 'GBK') as f:
        text = f.read()

    text = str(text).replace('<?xml version="1.0" encoding="GBK"?>', '<?xml version="1.0" encoding="utf-8"?>')

    tempfilename = source_file.split('.xml')[0] + 'temp.xml'
    with codecs.open(tempfilename, 'w', 'utf-8') as f:
        f.write(text)

    dom = xml.dom.minidom.parse(tempfilename)
    root = dom.documentElement
    print(root)

    os.remove(tempfilename)


def main():
    source_file = "9.xml"
    dom = xml.dom.minidom.parse(source_file)
    root = read_root(dom)
    print("*" * 40)
    read_child_tag(root)
    print("*" * 40)
    read_test_5(dom)
    print("*" * 40)


if __name__ == "__main__":
    main()
