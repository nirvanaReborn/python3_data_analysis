#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import zipfile
import xml.etree.ElementTree as ET


def main():
    # 打开XPS文档
    source_file= r'D:\SVN\PBOX-FLY\trunk\Sources\fly_IQ_trunk\fly_client\HSUCF_FLY\Help\量化运维监控说明v1.0.xps'
    # 解压缩XPS文件
    with zipfile.ZipFile(source_file, 'r') as zip_ref:
        zip_ref.extractall('extracted')

    # 解析和修改XML
    tree = ET.parse('extracted/some_file.xml')
    root = tree.getroot()

    # 对XML进行操作，如修改某个元素的文本
    element = root.find('SomeElement')
    element.text = 'Modified Text'
    # 保存
    tree.write('extracted/some_file.xml')

    # 重新压缩回XPS
    with zipfile.ZipFile('modifiedfile.xps', 'w') as zip_ref:
        for foldername, subfolders, filenames in os.walk('extracted'):
            for filename in filenames:
                filepath = os.path.join(foldername, filename)
                zip_ref.write(filepath, os.path.relpath(filepath, 'extracted'))


if __name__ == '__main__':
    main()
