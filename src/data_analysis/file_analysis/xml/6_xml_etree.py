#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
https://www.cnblogs.com/hupeng1234/p/7262371.html
# http://blog.csdn.net/yueguanghaidao/article/details/7265246
python有三种方法解析XML: SAX，DOM，以及ElementTree
这里主要介绍ElementTree。
"""

import codecs

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


def print_node(node):
    '''''打印结点基本信息'''
    print("=" * 40)
    # print(node)
    print("node.attrib:%s" % node.attrib)
    if "age" in node.attrib:
        print("node.get('age', None):%s" % node.get('age', None))
    print("node.tag:%s" % node.tag)
    print("node.text:%s" % node.text)


# 加载XML文件内容（两种方法：二是加载指定字符串）
def test_2(file_content):
    root = ET.fromstring(file_content)

    # 获取element的方法
    # 1 通过getiterator
    list_node = root.getiterator("person")
    for node in list_node:
        print_node(node)
        print_node(node.getchildren()[0])

    # 2通过 getchildren
    # lst_node_child = lst_node[0].getchildren()[0]
    # print_node(lst_node_child)

    # 3 .find方法
    node_find = root.find('person')
    print_node(node_find)

    # 4. findall方法
    node_findall = root.findall("person/name")[1]
    print_node(node_findall)


# 加载XML文件（两种方法：一是加载指定文件）
def test_1(source_file):
    # 要找出所有人的年龄
    tree = ET.parse(source_file)
    root = tree.getroot()
    print(root.tag, ":", root.attrib)  # 打印根元素的tag和属性

    list_node = tree.findall('person')
    # print(list_node)
    for node in list_node:  # 找出person节点
        print(node.attrib)  # 数据字典
        # print(node.tag, ':', node.text)
        # print("age:", node.attrib["age"])
        # print('age:', node.get('age', None))
        for child in node.getchildren():  # 找出当前节点的子节点
            print(child.tag, ':', child.text)
        print('############')


def read_xml(source_file, type):
    if type == "1":
        # 加载XML文件（两种方法：一是加载指定文件）
        test_1(source_file)
    elif type == "2":
        # 加载XML文件内容（两种方法：二是加载指定字符串）
        test_2(codecs.open(source_file, 'r', 'utf-8').read())
    else:
        print("未识别:", type)


def main():
    source_file = "6.xml"
    read_xml(source_file, "1")
    # read_xml(source_file, "2")


if __name__ == "__main__":
    main()
