#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
# http://blog.csdn.net/yueguanghaidao/article/details/7265246
python有三种方法解析XML: SAX，DOM，以及ElementTree

# http://blog.csdn.net/jerry_1126/article/details/43733889
1.SAX (Simple API for XML )
Pyhton标准库包含SAX解析器，SAX用事件驱动模型，通过在解析XML的过程中触发一个个的事件并调用用户定义的回调函数来处理XML文件。
SAX是一种基于事件驱动的API。利用SAX解析XML文档牵涉到两个部分:解析器和事件处理器。
解析器负责读取XML文档,并向事件处理器发送事件,如元素开始及结束事件;而事件处理器则负责对事件作出处理。
优点:SAX流式读取XML文件，比较快，占用内存少。
缺点:需要用户实现回调函数（handler）。

2.DOM(Document Object Model)
将XML数据在内存中解析成一个树，通过对树的操作来操作XML。
一个DOM的解析器在解析一个XML文档时，一次性读取整个文档，把文档中所有元素保存在内存中的一个树结构里，
之后你可以利用DOM提供的不同的函数来读取或修改文档的内容和结构，也可以把修改过的内容写入xml文件。
优点:使用DOM的好处是你不需要对状态进行追踪，因为每一个节点都知道谁是它的父节点，谁是子节点.
缺点:DOM需要将XML数据映射到内存中的树，一是比较慢，二是比较耗内存，使用起来也比较麻烦！

3.ElementTree(元素树)
ElementTree就像一个轻量级的DOM，具有方便友好的API。代码可用性好，速度快，消耗内存少。
相比而言，第三种方法，即方便，又快速，我们一直用它！
"""

import os

from public_function import GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
from public_function import dict_choice


def test_1():
    try:
        import xml.etree.cElementTree as ET
    except ImportError:
        import xml.etree.ElementTree as ET

    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "xml_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "xml_test.py")
    print(call_file)
    os.popen(call_file)


def test_3():
    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "xml_test.py")
    print(call_file)
    os.popen(call_file)


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
