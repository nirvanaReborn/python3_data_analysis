#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
# http://blog.csdn.net/jerry_1126/article/details/43733889
ElementTree生来就是为了处理XML ，它在Python标准库中有两种实现:
一种是纯Python实现，例如: xml.etree.ElementTree
另外一种是速度快一点的:   xml.etree.cElementTree
尽量使用C语言实现的那种，因为它速度更快，而且消耗的内存更少! 在程序中可以这样写:
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

加载XML :
方法一:加载文件
root = ET.parse('book.xml')
方法二:加载字符串
root = ET.fromstring(xmltext)

获取节点:
方法一:获得指定节点->getiterator()方法
book_node = root.getiterator('list')
方法二:获得指定节点->findall()方法
book_node = root.findall('list')
方法三:获得指定节点->find()方法
book_node = root.find('list')
方法四:获得儿子节点->getchildren()
for node in book_node:
    book_node_child = node.getchildren()[0]
    print(book_node_child.tag, '=> ', book_node_child.text)

常用方法:
# 当要获取属性值时，用attrib方法。
# 当要获取节点名时，用tag方法。
# 当要获取节点值时，用text方法。
'''

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


def call_xml_etree(source_file):
    root = ET.parse(source_file)
    print(root.attrib, root.tag, root.text)
    collection = root.findall('./movie')
    # print(collection)
    for node in collection:  # 找出movie节点
        # print(node.attrib) # 数据字典
        # print("Title:", node.attrib["title"])
        print('Title:', node.get('title'))
        # 找出movie节点的子节点
        for child in node.getchildren():
            print(child.tag, ':', child.text)
        print('#' * 20)


def main():
    source_file = r"1.xml"
    call_xml_etree(source_file)


if __name__ == "__main__":
    main()
