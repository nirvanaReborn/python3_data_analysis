#!/usr/bin/env python
# -*- coding:utf-8 -*-


'''
http://www.cnblogs.com/fnng/p/3581433.html
现在有以下几种：

NodeType	Named Constant
1	        ELEMENT_NODE
2	        ATTRIBUTE_NODE
3	        TEXT_NODE
4	        CDATA_SECTION_NODE
5	        ENTITY_REFERENCE_NODE
6	        ENTITY_NODE
7	        PROCESSING_INSTRUCTION_NODE
8	        COMMENT_NODE
9	        DOCUMENT_NODE
10	        DOCUMENT_TYPE_NODE
11	        DOCUMENT_FRAGMENT_NODE
12	        NOTATION_NODE        

http://www.w3school.com.cn/xmldom/dom_nodetype.asp
'''
import xml.dom.minidom


# 获得标签对之间的数据:方法二
# getchildren方法按照文档顺序返回所有子标签。并输出标签名（child.tag）和标签的数据（child.text）
# 其实，方法二的作用不在于此，它核心功能是可以遍历某一级标签下的所有子标签。
def read_test_6(file):
    try:
        import xml.etree.cElementTree as ET
    except ImportError:
        import xml.etree.ElementTree as ET

    root = ET.parse(file)
    p = root.findall('./login/item')
    for oneper in p:
        for child in oneper.getchildren():
            print(child.tag, ':', child.text)

    p = root.findall('./item')
    for oneper in p:
        for child in oneper.getchildren():
            print(child.tag, ':', child.text)


# 获得标签对之间的数据:方法一
def read_test_5(dom):
    itemlist = dom.getElementsByTagName('caption')
    for item in itemlist:
        print(item.firstChild.data)


# 获得标签属性值
# getAttribute方法可以获得元素的属性所对应的值。
def read_test_4(root):
    itemlist = root.getElementsByTagName('login')
    item = itemlist[0]
    un = item.getAttribute("username")
    print(un)
    pd = item.getAttribute("passwd")
    print(pd)

    ii = root.getElementsByTagName('item')
    i1 = ii[0]
    i = i1.getAttribute("id")
    print(i)

    i2 = ii[1]
    i = i2.getAttribute("id")
    print(i)


# 如何区分相同标签名字的标签
def read_test_3(root):
    # 获得的是标签为caption一组标签
    bb = root.getElementsByTagName('caption')
    b = bb[2]
    print(b.nodeName, b.nodeValue, b.firstChild, b.firstChild.data)

    bb = root.getElementsByTagName('item')
    b = bb[1]
    print(b.nodeName, b.nodeValue, b.firstChild, b.firstChild.data)


# 获得子标签
def read_child_tag(root):
    bb = root.getElementsByTagName('maxid')
    b = bb[0]
    print(b.nodeName, b.nodeValue)

    bb = root.getElementsByTagName('login')
    b = bb[0]
    print(b.nodeName, b.nodeValue)


def read_root(root):
    print(root.nodeName)  # 结点名字
    print(root.nodeValue)  # 结点的值，只对文本结点有效
    print(root.nodeType)  # 结点的类型
    print(root.ELEMENT_NODE)


def main():
    source_file = "6.xml"
    dom = xml.dom.minidom.parse(source_file)
    # 得到文档元素对象
    root = dom.documentElement

    dict_choice = {
        "1": "read_root(root)",
        "2": "read_child_tag(root)",
        "3": "read_test_3(root)",
        "4": "read_test_4(root)",
        "5": "read_test_5(dom)",
        "6": "read_test_6(source_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
