#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/hupeng1234/p/7262371.html
# python 使用ElementTree解析xml

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
from public_function import dict_choice


# ***************************************解析xml文件***************************************************
# 调用parse()方法，返回解析树
def test_1():
    source_file = "5.xml"
    tree = ET.parse(source_file)
    print(tree)
    root = tree.getroot()
    print(root)  # 获取根节点
    print(root.attrib, root.tag, root.text)


# 调用ElementTree类ElementTree(self, element=None, file=None)，返回解析树  # 这里的element作为根节点
def test_2():
    source_file = "5.xml"
    tree = ET.ElementTree(file=source_file)
    print(tree)
    root = tree.getroot()
    print(root)  # 获取根节点
    print(root.attrib, root.tag, root.text)


# 调用from_string(),返回解析树的根元素
def test_3():
    source_file = "5.xml"
    data = open(source_file).read()
    root = ET.fromstring(data)
    print(root)  # 获取根节点
    print(root.attrib, root.tag, root.text)


# *******************************************************************************************
# ***************************************遍历xml文件***************************************************
def test_4():
    source_file = "5.xml"
    tree = ET.parse(source_file)
    root = tree.getroot()
    print("第1层节点:", root.tag, ":", root.attrib)  # 打印根元素的tag和属性

    # 遍历xml文档的第二层
    for child in root:
        # 第二层节点的标签名称和属性
        print("第2层节点:", child.tag, ":", child.attrib)
        # 遍历xml文档的第三层
        for children in child:
            # 第三层节点的标签名称和属性
            print("第3层节点:", children.tag, ":", children.attrib)

    # 可以通过下标的方式直接访问节点
    # 访问根节点下第一个country的第二个节点year,获取对应的文本
    year = root[0][1].text  # 2008
    print(year)


'''ElementTree提供的方法
find(match)                                       # 查找第一个匹配的子元素， match可以时tag或是xpaht路径
findall(match)                                    # 返回所有匹配的子元素列表
findtext(match, default=None)                     # 
iter(tag=None)                                    # 以当前元素为根节点 创建树迭代器,如果tag不为None,则以tag进行过滤
iterfind(match)                                   # 
'''


def test_5():
    source_file = "5.xml"
    tree = ET.parse(source_file)
    root = tree.getroot()

    # 过滤出所有neighbor标签
    for neighbor in root.iter("neighbor"):
        print(neighbor.tag, ":", neighbor.attrib)

    # 遍历所有的counry标签
    for country in root.findall("country"):
        # 查找country标签下的第一个rank标签
        rank = country.find("rank").text
        # 获取country标签的name属性
        name = country.get("name")
        print(name, rank)


# *******************************************************************************************
# ***************************************修改xml文件**************************************************
'''关于class xml.etree.ElementTree.Element 属性相关
attrib                 　　 # 为包含元素属性的字典
keys()                      # 返回元素属性名称列表
items()                     # 返回(name,value)列表
get(key, default=None)      # 获取属性
set(key, value)             # 更新/添加  属性
del xxx.attrib[key]         # 删除对应的属性
'''


# 修改相关属性
def test_6():
    source_file = "5.xml"
    tree = ET.parse(source_file)
    root = tree.getroot()

    # 将所有的rank值加1,并添加属性updated为yes
    for rank in root.iter("rank"):
        new_rank = int(rank.text) + 1
        rank.text = str(new_rank)  # 必须将int转为str
        rank.set("updated", "yes")  # 添加属性

    # 在终端显示整个xml
    ET.dump(root)
    # 注意 修改的内容存在内存中 尚未保存到文件中，所以需要保存修改后的内容
    tree.write("output.xml", encoding="utf-8", xml_declaration=True)

    tree = ET.parse("output.xml")
    root = tree.getroot()

    for rank in root.iter("rank"):
        # attrib为属性字典，删除对应的属性updated
        del rank.attrib['updated']

    ET.dump(root)
    tree.write("output2.xml", encoding="utf-8", xml_declaration=True)


# 修改相关节点/元素
def test_7():
    source_file = "5.xml"
    tree = ET.parse(source_file)
    root = tree.getroot()

    # 删除rank大于50的国家
    for country in root.iter("country"):
        rank = int(country.find("rank").text)
        if rank > 50:
            # remove()方法 删除子元素
            root.remove(country)

    ET.dump(root)
    tree.write("output3.xml", encoding="utf-8", xml_declaration=True)


'''添加子元素方法总结:
append(subelement) 
extend(subelements)
insert(index, element)
'''


# 添加子元素
def test_8():
    source_file = "5.xml"
    tree = ET.parse(source_file)
    root = tree.getroot()

    country = root[0]
    last_element = country[len(list(country)) - 1]
    last_element.tail = '\n\t\t'

    # 创建新的元素, tag为test_append
    elem1 = ET.Element("test_append")
    elem1.text = "elem 1"
    # elem.tail = '\n\t'
    country.append(elem1)

    # SubElement() 其实内部调用的是append()
    elem2 = ET.SubElement(country, "test_subelement")
    elem2.text = "elem 2"

    # extend()
    elem3 = ET.Element("test_extend")
    elem3.text = "elem 3"
    elem4 = ET.Element("test_extend")
    elem4.text = "elem 4"
    country.extend([elem3, elem4])

    # insert()
    elem5 = ET.Element("test_insert")
    elem5.text = "elem 5"
    country.insert(5, elem5)

    ET.dump(country)
    tree.write("output4.xml", encoding="utf-8", xml_declaration=True)


# *******************************************************************************************
# ***************************************创建xml文件**************************************************
'''
想创建root Element,然后创建SubElement,最后将root element传入ElementTree(element),创建tree，调用tree.write()方法写入文件
对于创建元素的3个方法: 使用ET.Element、Element对象的makeelement()方法以及ET.SubElement
'''


def test_9():
    def subElement(root, tag, text):
        ele = ET.SubElement(root, tag)
        ele.text = text
        ele.tail = '\n'

    root = ET.Element("note")

    to = root.makeelement("to", {})
    to.text = "peter"
    to.tail = '\n'
    root.append(to)

    subElement(root, "from", "marry")
    subElement(root, "heading", "Reminder")
    subElement(root, "body", "Don't forget the meeting!")

    tree = ET.ElementTree(root)
    tree.write("note.xml", encoding="utf-8", xml_declaration=True)


# 由于原生保存的XML时默认无缩进，如果想要设置缩进的话， 需要修改保存方式
def test_10():
    def subElement(root, tag, text):
        ele = ET.SubElement(root, tag)
        ele.text = text

    def saveXML(root, filename, indent="\t", newl="\n", encoding="utf-8"):
        import xml.dom.minidom
        rawText = ET.tostring(root)
        dom = xml.dom.minidom.parseString(rawText)
        with open(filename, 'w') as f:
            dom.writexml(f, "", indent, newl, encoding)

    root = ET.Element("note")

    to = root.makeelement("to", {})
    to.text = "peter"
    root.append(to)

    subElement(root, "from", "marry")
    subElement(root, "heading", "Reminder")
    subElement(root, "body", "Don't forget the meeting!")

    # 保存xml文件
    saveXML(root, "note.xml")


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
