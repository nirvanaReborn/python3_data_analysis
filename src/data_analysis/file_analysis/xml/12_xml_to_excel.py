#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/qinbaby/article/details/23201841
# 一个python 脚本将XML文件转换到excel

import sys
import xml.dom.minidom

import xlwt


def handle_xml_report(xml_report, excel):
    problems = xml_report.getElementsByTagName("problem")
    handle_problems(problems, excel)


def handle_problems(problems, excel):
    for problem in problems:
        handle_problem(problem, excel)


def handle_problem(problem, excel):
    global row
    global col
    code = problem.getElementsByTagName("code")
    file = problem.getElementsByTagName("file")
    line = problem.getElementsByTagName("line")
    message = problem.getElementsByTagName("message")
    for node in code:
        excel.write(row, col, node.firstChild.data)
        col = col + 1
    for node in file:
        excel.write(row, col, node.firstChild.data)
        col = col + 1
    for node in line:
        excel.write(row, col, node.firstChild.data)
        col = col + 1
    for node in message:
        excel.write(row, col, node.firstChild.data)
        col = col + 1
    row = row + 1
    col = 0


def main():
    col = 0
    row = 0
    if (len(sys.argv) <= 1):
        print("usage: xml2xls src_file [dst_file]")
        exit(0)
    # the 1st argument is XML report ; the 2nd is XLS report
    if (len(sys.argv) == 2):
        xls_report = sys.argv[1][:-3] + 'xls'
    # if there are more than 2 arguments, only the 1st & 2nd make sense
    else:
        xls_report = sys.argv[2]
    xmldoc = xml.dom.minidom.parse(sys.argv[1])
    wb = xlwt.Workbook()
    ws = wb.add_sheet('MOLint')
    ws.write(row, col, 'Error Code')
    col = col + 1
    ws.write(row, col, 'file')
    col = col + 1
    ws.write(row, col, 'line')
    col = col + 1
    ws.write(row, col, 'Description')
    row = row + 1
    col = 0
    handle_xml_report(xmldoc, ws)
    wb.save(xls_report)


if __name__ == "__main__":
    main()
