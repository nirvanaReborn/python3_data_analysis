#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 打印xml文件中的所有节点

import codecs

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


def print_node(node):
    if node.tag is not None:
        print(node.tag, ':', node.text)  # 当前节点名

        if node.attrib:
            print(node.attrib)  # 当前节点属性(数据字典)
        print("*" * 40)

    if node.getchildren():
        for child in node.getchildren():  # 子节点
            print_node(child)


def main():
    source_file = r"D:\SVN\PBOX\trunk\Sources\workspace\MS\auto_sett_step_config.xml"
    file_content = codecs.open(source_file, 'r', 'GBK').read()
    root = ET.fromstring(file_content)
    print_node(root)


if __name__ == "__main__":
    main()
