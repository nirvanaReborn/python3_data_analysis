#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import xml.dom.minidom


def create_xml_dom(dest_file):
    list_1D = ['dede', '1234', '汉子', 'msfsd  dfsd']
    impl = xml.dom.minidom.getDOMImplementation()
    domTree = impl.createDocument(None, 'root', None)
    root = domTree.documentElement
    secondNode = domTree.createElement('secondNode')
    # secondNode.setAttribute('id', 1)
    root.appendChild(secondNode)
    for item in list_1D:
        nameE = domTree.createElement('third')
        nameT = domTree.createTextNode(item)
        nameE.appendChild(nameT)
        secondNode.appendChild(nameE)

    fw = open(dest_file, 'w', encoding='utf-8')
    domTree.writexml(fw, addindent='  ', newl='\n', encoding='utf-8')
    fw.close()


def call_xml_dom(source_file):
    domTree = xml.dom.minidom.parse(source_file)
    collection = domTree.documentElement
    if collection.hasAttribute("shelf"):
        print("Root element : %s" % collection.getAttribute("shelf"))

    # 在集合中获取所有电影
    movies = collection.getElementsByTagName("movie")

    # 打印每部电影的详细信息
    for movie in movies:
        print("*****Movie*****")
        if movie.hasAttribute("title"):
            print("Title: %s" % movie.getAttribute("title"))

        type = movie.getElementsByTagName('type')[0]
        print("Type: %s" % type.childNodes[0].data)
        format = movie.getElementsByTagName('format')[0]
        print("Format: %s" % format.childNodes[0].data)
        rating = movie.getElementsByTagName('rating')[0]
        print("Rating: %s" % rating.childNodes[0].data)
        description = movie.getElementsByTagName('description')[0]
        print("Description: %s" % description.childNodes[0].data)


def main():
    source_file = r"1.xml"
    call_xml_dom(source_file)


if __name__ == "__main__":
    main()
