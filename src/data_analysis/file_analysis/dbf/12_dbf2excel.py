#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import time

import dbfpy
# http://www.iplaypy.com/code/c2656.html
# Python方法将DBF文件导出到Excel代码示例
import dbfread
from win32com import client


def dbfread2xls(dbfilename, exfilename):
    table = dbfread.DBF(dbfilename, encoding='GB2312')
    excel = client.Dispatch('Excel.Application')
    wb = excel.Workbooks.Add()
    workSheet = wb.ActiveSheet
    excel.Visible = True
    time.sleep(1)

    row = 1
    col = 1
    for i, field in enumerate(table.field_names):
        workSheet.Cells(row, col + i).Value = field

    row = 2
    for record in table:
        col = 1
        for j, field in enumerate(table.field_names):
            workSheet.Cells(row, col + j).Value = record[field]
        row = row + 1

    wb.SaveAs(exfilename)
    wb.Close(False)
    excel.Application.Quit()


def dbfpy2xls(dbfilename, exfilename):  # 定义方法
    db = dbfpy.dbf.Dbf(dbfilename, True)
    excel = client.Dispatch('Excel.Application')
    wb = excel.Workbooks.Add()
    workSheet = wb.ActiveSheet
    excel.Visible = True
    time.sleep(1)
    r = 1
    c = 1

    for field in db.fieldNames:
        workSheet.Cells(r, c).Value = field
        c = c + 1
    r = 2

    for record in db:
        c = 1
        for field in db.fieldNames:
            workSheet.Cells(r, c).Value = record[field]
            c = c + 1
        r = r + 1

    wb.SaveAs(exfilename)
    wb.Close(False)
    excel.Application.Quit()
    db.close()


def main():
    dbffilename = "test.dbf"

    # 默认生成的excel文件在 C:\Users\%username%\Documents
    xlsfilename = "text.xls"

    dict_choice = {
        "1": "dbfread2xls(dbfilename, exfilename)",
        "2": "dbfpy2xls(dbffilename, xlsfilename)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
