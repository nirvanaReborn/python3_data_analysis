#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
http://www.cnblogs.com/mayi0312/p/6841201.html
https://github.com/zycool/dbfread
'''
import os
import sys

from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE
from public_function import dict_choice


def test_1():
    import dbfread
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "dbfread_test.py")
    print(call_file)
    os.popen(call_file)


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
