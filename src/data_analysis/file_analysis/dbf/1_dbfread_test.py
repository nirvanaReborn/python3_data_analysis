#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
http://www.cnblogs.com/mayi0312/p/6841201.html
https://github.com/zycool/dbfread
处理dbf文件
'''
import sys

# 导入模块
import dbfread

from public_function import GLOBAL_XLSX_FILE


def test_help():
    help(dbfread)


def test_dir():
    for i in dir(dbfread):
        print(i)


# 遍历数据表中（没加删除标志）的记录
def get_record(table):
    for record in table:
        for field in record:
            print(field, "=", record[field], end=",")
        print()


# 遍历数据表中（加了删除标志）的记录
def get_record_deleted(table):
    for record in table.deleted:
        for field in record:
            print(field, "=", record[field], end=",")
        print()


def main():
    dbffilename = r"test.dbf"

    # 默认生成的excel文件在 C:\Users\%username%\Documents
    xlsfilename = GLOBAL_XLSX_FILE

    # oracle数据库表文件名
    table = dbfread.DBF(dbffilename, encoding='GB2312')

    dict_choice = {
        "1": "get_record(table)",
        "2": "get_record_deleted(table)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
