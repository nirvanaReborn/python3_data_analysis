#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qcyfred/article/details/79925099
# python-docx-template 文档链接:https://docxtpl.readthedocs.io/en/latest/
# github上的示例:https://github.com/elapouya/python-docx-template
'''
docxtpl会使用2个包:
    python-docx用于读取，编写和创建子文档
    jinja2用于管理插入到模板docx中的标签
'''
import docx
# pip install docxtpl
import docxtpl
from docxtpl import DocxTemplate, InlineImage, RichText
from docx.shared import Mm, Pt


# https://blog.csdn.net/sinat_37005367/article/details/77855359
def test_1():
    doc = DocxTemplate("1.docx")  # 对要操作的docx文档进行初始化
    context = {'company_name': "World company"}  # company_name 是存在于1.docx文档里面的变量，就像这样{{company_name}}，直接放在1.docx文件的明确位置就行
    doc.render(context)  # 这里是有jinjia2的模板语言进行变量的替换，然后便可以在1.docx文档里面看到{{company_name}}变成了World company
    doc.save("generated_doc.docx")  # 保存

    # 当然，这个包的功能远远不止上面例子中的一些，可以包含图片
    myimage = InlineImage(doc, 'test_files/python_logo.png', width=Mm(20))  # tpl便是上面例子中的doc对象

    # 也可以包含另一个docx文档，
    sub = doc.new_subdoc()
    sub.subdocx = docx.Document('d:\\2.docx')
    doc.render({'sub': sub})


# -------------------------------------------------------------------------------
# https://blog.csdn.net/weixin_42670653/article/details/81503294
def test_2():
    tpl = DocxTemplate('test_files/richtext_tpl.docx')

    rt = RichText('an exemple of ')
    rt.add('a rich text', style='myrichtextstyle')
    rt.add('some violet', color='#ff00ff')

    context = {
        'example': rt,
    }

    tpl.render(context)
    tpl.save('test_files/richtext.docx')


# 1. 在设置文字颜色时，rt.add("some violet",color='#ff00ff')这种设置方法，WPS不兼容，因此按照上述自定义类型的方法来设置文字颜色比较好
def test_3():
    tpl = DocxTemplate('test_files/richtext_tpl.docx')

    rt = RichText('an exemple of ')
    rt.add(' with ')
    rt.add('some italic', italic=True)  # 斜体
    rt.add(' and ')
    rt.add('some violet', color='#ff00ff')  # 颜色
    rt.add(' and ')
    rt.add('some striked', strike=True)  # 删除线
    rt.add(' and ')
    rt.add('some small', size=14)  # 字体大小
    rt.add(' or ')
    rt.add('big', size=60)
    rt.add(' text.')
    rt.add(' Et voilà ! ')
    rt.add('\n1st line')
    rt.add('\n2nd line')  # 换行
    rt.add('\n3rd line')
    rt.add('\n\n<cool>')

    context = {
        'example': rt,
    }

    tpl.render(context)
    tpl.save('test_files/richtext.docx')


# -------------------------------------------------------------------------------
# https://blog.csdn.net/weixin_44872198/article/details/120332224
def test_4():
    tpl = DocxTemplate('demo.docx')
    # 对要插入的图片进行处理，这里的docxtpl，默认只有InlineImage（嵌入式插入图片，参数也只有width和height设置图片宽高）
    # AnchorImage是自己处理之后的可以插入浮动图片的类pos_x和pos_y用于定位图片位置。
    img2_path = docxtpl.AnchorImage(tpl, '2.png', width=Mm(15), height=Mm(10), pos_x=Pt(480), pos_y=Pt(490))
    img3_path = docxtpl.AnchorImage(tpl, '3.png', width=Mm(15), height=Mm(10), pos_x=Pt(460), pos_y=Pt(530))
    img4_path = docxtpl.AnchorImage(tpl, '4.png', width=Mm(15), height=Mm(10), pos_x=Pt(460), pos_y=Pt(570))
    # 定义模板的替换数据
    content = {
        "arr_year": 2021, "arr_month": 4, "arr_day": 15,
        "test_year": 2021, "test_month": 4, "test_day": 22,
        "img2": img2_path,
        "img3": img3_path,
        "img4": img4_path
    }

    tpl.render(content)
    tpl.save('save.docx')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
