#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#
import os
from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE


def test_1():
    # pip install docx2txt
    import docx2txt  # 只能读取docx格式文件

    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "docx2txt_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    # 如果是创建新文档，用python-docx
    # 如果只是简单的写入和读取word文档，没有太复杂的格式或者样式要求，推荐使用python-docx
    # pip install python-docx
    import docx

    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "python-docx_test.py")
    print(call_file)
    os.popen(call_file)


def test_3():
    # https://www.zhihu.com/column/c_1365074188886163456
    # 如果对格式有比较复杂的要求或者有很刁钻的要求，推荐使用pywin32
    # pip install pypiwin32
    from win32com.client import Dispatch, DispatchEx

    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "pywin32_test.py")
    print(call_file)
    os.popen(call_file)


def test_4():
    # 如果是修改文档用python-docx-template
    # pip install docxtpl
    import docxtpl

    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "8_docxtpl.py")
    print(call_file)
    os.popen(call_file)


def test_5():
    # 这个库可以从Office 格式的文件中抽取数据。
    import oletools

    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "oletools_test.py")
    print(call_file)
    os.popen(call_file)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
