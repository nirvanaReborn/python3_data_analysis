#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/hupeng1234/p/6920129.html
# 只对windows平台有效

import os
import win32com.client
from public_function import GLOBAL_WORK_DIR


# 读取word文档
def read_docx(source_file):
    word = win32com.client.Dispatch('Word.Application')  # 打开word应用程序
    # word = DispatchEx('Word.Application') #启动独立的进程
    word.Visible = 0  # 后台运行,不显示
    word.DisplayAlerts = 0  # 不警告

    doc = word.Documents.Open(FileName=source_file, Encoding='gbk')
    # content = doc.Range(doc.Content.Start, doc.Content.End)
    # content = doc.Range()
    print('----------------')
    print('段落数: ', doc.Paragraphs.count)

    # 利用下标遍历段落
    # for i in range(len(doc.Paragraphs)):
    #     para = doc.Paragraphs[i]
    #     print(para.Range.text)
    # print('-------------------------')

    # 直接遍历段落
    for i, para in enumerate(doc.paragraphs):
        file_content = str(para.Range.text).strip()
        if file_content:
            print("第" + str(i + 1) + "段的内容是：" + file_content)
        # print para  #只能用于文档内容全英文的情况

    doc.Close()  # 关闭word文档
    word.Quit()  # 关闭word程序


# 读取表格(不推荐)
def read_table(source_file):
    dict_table = {}
    word = win32com.client.Dispatch('Word.Application')  # 打开word应用程序
    # word = DispatchEx('Word.Application') #启动独立的进程
    word.Visible = 0  # 后台运行,不显示
    word.DisplayAlerts = 0  # 不警告

    doc = word.Documents.Open(FileName=source_file, Encoding='gbk')
    for i, table in enumerate(doc.Tables):  # 遍历所有表格

        # print(type(table)) # <class 'win32com.client.CDispatch'>
        list_table = str(table).split('')  # 一维列表
        # print(list_2D) # 打印整个表格的所有单元格
        for cell in list_table:  # 遍历每个表格的所有单元格
            print(cell.replace("\r", ""))

        if list_table:
            dict_table[i] = list_table
    return dict_table


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, "demo.doc")  # word文件路径
    read_docx(source_file)
    # read_table(source_file)


if __name__ == "__main__":
    main()
