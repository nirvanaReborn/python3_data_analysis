#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_44872198/article/details/120332224
# reportlab和docx2pdf，用于word转pdf格式

import os
from reportlab.pdfgen import canvas
from docx2pdf import convert


def main():
    # 已有的word文件
    docx_path = 'demo.docx'
    # 定义生成的pdf文件路径
    pdf_path = 'demo.pdf'
    # 先创建pdf文件
    c = canvas.Canvas(pdf_path)
    c.showPage()
    c.save()

    if os.path.exists(docx_path) and os.path.exists(pdf_path):
        convert(docx_path, pdf_path)
    else:
        print('路径不存在^_^。')


if __name__ == "__main__":
    main()
