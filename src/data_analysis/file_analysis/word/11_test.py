#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/anpengapple/p/8372958.html
# 用python解析word文件（一）：段落篇（paragraph）
import codecs
import os

import docx
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc import coreprops


def test_1():
    doc = docx.Document()
    doc.add_paragraph(u'第一段', style=None)
    doc.add_paragraph(u'第二段', style='Heading 2')
    p = doc.add_paragraph('')
    p.add_run('第三段', style=None)
    p.add_run('123', style="Heading 1 Char")
    p.add_run('456')
    p.add_run('789', style="Heading 2 Char")


class Paragraph_html():
    # 初始化函数，类似于构造函数
    def __init__(self, source_file):
        self.file_name = os.path.abspath(source_file)
        self.doc = docx.Document(self.file_name)
        self.paragraph = self.doc.paragraph

    def word_to_html(self):
        html = ""
        title = docx.opc.coreprops.title
        if title in self.paragraph.text and len(self.paragraph.text) < 15:
            html += " style=\"text-align: center; font-size: 30px\">"
        elif self.paragraph.alignment == docx.enum.text.WD_PARAGRAPH_ALIGNMENT.CENTER:
            html += " style=\"text-align: center; font-size: 15px\">"
        elif self.paragraph.alignment == docx.enum.text.WD_PARAGRAPH_ALIGNMENT.RIGHT:
            html += " style=\"text-align: right; font-size: 15px\">"
        else:
            html += " style=\"font-size: 15px\">"

        # html += "%s</p>" % p
        return html

    def create_file(self, file_content):
        with codecs.open(self.file_name, 'w', "gbk") as fw:
            fw.write(file_content)
            print("生成超文本文件:", self.file_name)


def main():
    pass


if __name__ == "__main__":
    main()
