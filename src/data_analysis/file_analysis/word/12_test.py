#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/anpengapple/p/8372987.html
# 用python解析word文件（一）：表格篇（table）

import codecs
import os
import sys

import docx

from public_function import GLOBAL_WORK_DIR


def read_table(table):
    _table_list = []
    for i, row in enumerate(table.rows):  # 读每行
        row_content = []
        for cell in row.cells:  # 读一行中的所有单元格
            c = cell.text
            if c not in row_content:
                row_content.append(c)
        # print(row_content)
        _table_list.append(row_content)

    return _table_list


def _fill_blank(table):
    cols = max([len(i) for i in table])

    new_table = []
    for i, row in enumerate(table):
        new_row = []
        [new_row.extend([i] * int(cols / len(row))) for i in row]
        print(new_row)
        new_table.append(new_row)

    return new_table


def _table_matrix(table):
    if not table:
        return ""

    # 处理同一行的各列
    temp_matrix = []
    for row in table:
        if not row:
            continue

        col_last = [row[0], 1, 1]
        line = [col_last]
        for i, j in enumerate(row):
            if i == 0:
                continue

            if j == col_last[0]:
                col_last[2] += 1
                line.append(["", 0, 0])
            else:
                col_last = [j, 1, 1]
                line.append(col_last)

        temp_matrix.append(line)

    # 处理不同行
    matrix = [temp_matrix[0]]
    last_row = []
    for i, row in enumerate(temp_matrix):
        if i == 0:
            last_row.extend(row)
            continue

        new_row = []
        for p, r in enumerate(row):
            if p >= len(last_row):
                break

            last_pos = last_row[p]

            if r[0] == last_pos[0] and last_pos[0] != "":
                last_row[p][1] += 1
                new_row.append(["", 0, 0])
            else:
                last_row[p] = row[p]
                new_row.append(r)

        matrix.append(new_row)

    return matrix


def table2html(t):
    table = _fill_blank(t)
    matrix = _table_matrix(table)

    html = "<html>"
    html += "<head>"
    html += "</head>"
    html += "<body>"
    html += '<table border="1" align="center">'
    for row in matrix:
        tr = "<tr>"
        for col in row:
            if col[1] == 0 and col[2] == 0:
                continue

            td = ["<td"]
            if col[1] > 1:
                td.append(" rowspan=\"%s\"" % col[1])
            if col[2] > 1:
                td.append(" colspan=\"%s\"" % col[2])
            td.append(">%s</td>" % col[0])

            tr += "".join(td)
        tr += "</tr>"
        html += tr
    html += "</table>"
    html += "</body>"
    html += "</html>"
    return html


def create_file(file_name, file_content):
    with codecs.open(file_name, 'w', "gbk") as fw:
        fw.write(file_content)
        print("生成超文本文件:", file_name)


def main():
    # source_file = os.path.join(GLOBAL_WORK_DIR, r"demo.docx")
    # doc = docx.Document(source_file)
    # for i, table in enumerate(doc.tables):  # 遍历所有表格
    #     read_table(table)

    list_2D = [
        ['我们来插入一个表格'],
        ['这是一级标题1', '这是二级标题1.1', '这是三级标题1.1.1', '总结'],
        ['这是一级标题1', '这是二级标题1.1', '这是三级标题1.1.2', '总结'],
        ['这是一级标题1', '这是二级标题1.1', '这是三级标题1.1.3', '总结'],
        ['这是一级标题1', '这是二级标题1.2', '这是三级标题1.2.1', '总结'],
        ['这是一级标题1', '这是二级标题1.3', '这是三级标题1.3.1', '总结'],
        ['这是一级标题1', '这是二级标题1.3', '这是三级标题1.3.2', '总结'],
        ['别忙，还有内容'],
        ['内容', '另一段内容'],
    ]

    dest_file = os.path.join(GLOBAL_WORK_DIR, r"table2html.html")
    create_file(dest_file, table2html(list_2D))


if __name__ == "__main__":
    main()
