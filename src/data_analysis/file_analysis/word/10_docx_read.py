#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/hupeng1234/p/6920129.html
# 使用docx扩展包
# 优点:不依赖操作系统,跨平台
# pip install python-docx
# 参考文档: https://python-docx.readthedocs.io/en/latest/index.html

# https://blog.csdn.net/woshisangsang/article/details/75221723
# 1，Document对象，表示一个word文档。
# 2，Paragraph对象，表示word文档中的一个段落
# 3，Paragraph对象的text属性，表示段落中的文本内容。

import os
import docx
# from docx.shared import Inches
from public_function import Excel_Pandas
from public_function import GLOBAL_WORK_DIR


# 读取word文档
def read_docx(source_file):
    doc = docx.Document(source_file)
    # content = '\n'.join([para.text for para in doc.paragraphs])

    print("段落数:" + str(len(doc.paragraphs)))

    # 输出每一段的内容
    for i, para in enumerate(doc.paragraphs):
        print(para)
        file_content = str(para.text).strip()
        if file_content:
            print("第" + str(i + 1) + "段的内容是：" + file_content)

    # 输出段落编号及段落内容
    # for i in range(len(doc.paragraphs)):
    #     print("第" + str(i + 1) + "段的内容是：" + doc.paragraphs[i].text)

    # return content


# 读取表格
def read_table(source_file):
    dict_sheet = {}
    doc = docx.Document(source_file)
    for i, table in enumerate(doc.tables):  # 遍历所有表格
        list_2D = []
        print("***************table***************")
        print(i, len(table.rows), len(table.columns))
        # print(table) # <docx.table.Table object at 0x0000020C8E9EC278>
        for row in table.rows:  # 遍历每个表格的所有行
            print("----------row--------------")
            # print(row.part)
            # print(row.table)

            row_str = '\t'.join([cell.text for cell in row.cells])  # 一行数据
            print(row_str)

            list_1D = []
            for cell in row.cells:  # 遍历每行的单元格
                list_1D.append(str(cell.text))
                # print(cell.text)

            if list_1D:
                list_2D.append(list_1D)

        if list_2D:
            dict_sheet[i] = list_2D
    return dict_sheet


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"demo.docx")
    read_docx(source_file)
    dict_sheet = read_table(source_file)
    dest_file = os.path.join(GLOBAL_WORK_DIR, r"test.xlsx")
    Excel_Pandas(dest_file).create_excel_pandas_by_dict(dict_sheet, header_flag=False)


if __name__ == "__main__":
    main()
