#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/hupeng1234/p/6920129.html
# 使用docx扩展包
# 优点:不依赖操作系统,跨平台
# pip install python-docx
# 参考文档: https://python-docx.readthedocs.io/en/latest/index.html

'''
https://blog.csdn.net/woshisangsang/article/details/75221723
1，Document对象，表示一个word文档。
2，Paragraph对象，表示word文档中的一个段落
3，Paragraph对象的text属性，表示段落中的文本内容。

https://www.cnblogs.com/zknublx/p/7613810.html
'''
import os
import docx
from docx.shared import Inches
from public_function import GLOBAL_WORK_DIR


# 创建表格
def create_table(dest_file):
    doc = docx.Document()
    table = doc.add_table(rows=1, cols=3, style='Table Grid')  # 创建带边框的表格
    # 添加标题行
    hdr_cells = table.rows[0].cells  # 获取第0行所有所有单元格
    hdr_cells[0].text = 'Name'
    hdr_cells[1].text = 'Id'
    hdr_cells[2].text = 'Desc'

    # 添加三行数据
    data_lines = 3
    for i in range(data_lines):
        cells = table.add_row().cells
        cells[0].text = 'Name%s' % i
        cells[1].text = 'Id%s' % i
        cells[2].text = 'Desc%s' % i
    # ----------------------------------------------------------------
    rows = 2
    cols = 4
    table = doc.add_table(rows=rows, cols=cols)  # 创建不带边框的表格
    val = 1
    for i in range(rows):
        cells = table.rows[i].cells
        for j in range(cols):
            cells[j].text = str(val * 10)
            val += 1

    doc.save(dest_file)


def create_word(dest_file):
    document = docx.Document()

    document.add_heading('Document Title', 0)

    p = document.add_paragraph('A plain paragraph having some ')
    p.add_run('bold').bold = True  # 加粗
    p.add_run(' and some ')
    p.add_run('italic.').italic = True  # 斜体

    document.add_heading('Heading, level 1', level=1)
    document.add_paragraph('Intense quote', style='Intense Quote')

    document.add_paragraph(
        'first item in unordered list', style='List Bullet'
    )
    document.add_paragraph(
        'first item in ordered list', style='List Number'
    )

    source_pic = os.path.join(GLOBAL_WORK_DIR, '1.png')
    document.add_picture(source_pic, width=Inches(6.25))

    records = (
        (3, '101', 'Spam'),
        (7, '422', 'Eggs'),
        (4, '631', 'Spam, spam, eggs, and spam')
    )

    table = document.add_table(rows=1, cols=3)
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = 'Qty'
    hdr_cells[1].text = 'Id'
    hdr_cells[2].text = 'Desc'
    for qty, id, desc in records:
        row_cells = table.add_row().cells
        row_cells[0].text = str(qty)
        row_cells[1].text = id
        row_cells[2].text = desc

    document.add_page_break()

    document.save(dest_file)
    print("生成word文档:", dest_file)


def create_word2():
    document = docx.Document()  # 创建一个Document对象，同时也会创建一个空白文档

    # 添加段落和标题
    document.add_heading('标题', 0)
    p = document.add_paragraph('一个段落里可以有')
    p.add_run('粗体').bold = True
    p.add_run('和')
    p.add_run('斜体').italic = True
    document.add_heading('标题1', level=1)
    document.add_paragraph('设置Intense quote段落样式', style='Intense Quote')
    document.add_paragraph('设置成项目符号中的第一项的段落样式', style='List Bullet')
    document.add_paragraph('设置成编号里的第一项的段落样式', style='List Number')

    # 添加图片
    document.add_picture('白敬亭.png', width=Inches(1.25))

    # 表格数据
    records = (
        (1, '山月', '学Python'),
        (2, '小红', '唱歌'),
        (3, '小兰', '跳舞')
    )

    # 添加表格
    table = document.add_table(rows=1, cols=3)
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = '编号'
    hdr_cells[1].text = '姓名'
    hdr_cells[2].text = '爱好'
    for number, name, hobby in records:
        row_cells = table.add_row().cells
        row_cells[0].text = str(number)
        row_cells[1].text = name
        row_cells[2].text = hobby

    table.style = 'Light Shading Accent 1'  # 设置表格样式
    document.add_page_break()  # 添加换页符
    document.save('实例.docx')  # 保存文件


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"demo.docx")
    create_table(source_file)
    create_word(os.path.join(GLOBAL_WORK_DIR, 'demo.docx'))


if __name__ == "__main__":
    main()
