#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qq_45464895/article/details/123173742
# http://blog.csdn.net/kevinelstri/article/details/61921812
# 使用python读取数据科学最常用的文件格式
# 微软Word的docx文件是另一种文件格式，它是基于文本的数据组织的。
# 它有许多特点，如表格的内联加法，图像、超链接等，这有助于在一个非常重要的文件格式。
# docx文件对于PDF文件，具有的优点就是docx文件是可编辑的。
# 你也可以把docx文件转换成任何其他格式。

'''
ImportError: DLL load failed: 找不到指定的程序。
# https://stackoverflow.com/questions/26404228/python-docx-cannot-be-imported-to-python

'''
# pip install python-docx
import docx  # 只处理docx格式文件


# 读取word
def test_1(source_file):
    fullText = []
    doc = docx.Document(source_file)
    paragraphs = doc.paragraphs
    print(len(paragraphs))
    for paragraph in paragraphs:
        print(paragraph.text)
        print(paragraph.style)
        for run in paragraph.runs:
            print(run.text)
        fullText.append(paragraph.text)
    return '\n'.join(fullText)


# 读取word中的表格
def test_2(source_file):
    # 方案一：输入外层表格table，输出为内层表格
    def get_nested_tables_solu1(table):
        for table_row in table.rows:
            for table_cell in table_row.cells:
                return table_cell.tables[0]

    # 方案二：输入外层表格，以及内嵌表格在外层表格的行列位置row,column，默认为第一行第一列
    def get_nested_tables_solu2(table, row=0, column=0):
        try:
            return table.cell(row, column).tables[0]
        except:
            return ""

    doc = docx.Document(source_file)
    for table in doc.tables:
        if len(table.columns) == 4 and table.cell(0, 1).text == "修改原因":
            return table


# ---------------------------------------------------------------
def test_4(dest_file):
    # 新建文档：
    document = docx.Document()  # 创建一个Document对象，同时也会创建一个空白文档
    document.save('实例.docx')  # 保存文档

    # 打开已有文档：
    document = docx.Document('数据.docx')  # 打开名为数据的word文档
    document.save('实例.docx')  # 保存文档


# 添加段落
def test_5(dest_file):
    doc = docx.Document()
    paragraph_1 = doc.add_paragraph('Hello world!', style='Title')  # 添加段落
    # 也可以把一个段落作为"光标"，在其正上方插入一个新段落,这样就可以在文档中间插入段落了。
    prior_paragraph = paragraph_1.insert_paragraph_before('我是山月')
    print(prior_paragraph)

    paragraph_2 = doc.add_paragraph('This is a second paragraph.', style='List Bullet')
    # 在已有段落的末尾添加文本
    run = paragraph_2.add_run(' This text is being added to the second paragraph.')
    # run对象的.bold和.italic属性，可以用来设置加粗或斜体
    run.bold = True
    run.italic = True
    # 设置字体样式
    run.style = 'Emphasis'

    doc.add_paragraph('This is a yet another paragraph.')
    doc.save(dest_file)


# 添加标题
def test_6(dest_file):
    '''
    add_heading(text="", level=1)的参数，是一个标题文本的字符串，以及一个从0 到4 的整数。
        整数0 表示标题是Title 样式，这用于文档的顶部。
        整数1 到4 是不同的标题层次，1是主要的标题，4 是最低层的子标题。
    add_heading()返回一个Paragraph 对象，让你不必多花一步从Document 对象中提取它。
    '''
    doc = docx.Document()
    doc.add_heading('Header 0', level=0)
    doc.add_heading('Hello world!', level=3)
    doc.save(dest_file)


# 添加换行符和换页符
def test_7(dest_file):
    '''
    要添加换行符（而不是开始一个新的段落），可以在Run 对象上调用add_break()方法，换行符将出现在它后面。
    如果希望添加换页符，可以将docx.text.WD_BREAK.PAGE作为唯一的参数，传递给add_break()
    '''
    doc = docx.Document()
    doc.add_paragraph('This is on the first page!')
    doc.paragraphs[0].runs[0].add_break(docx.text.WD_BREAK.PAGE)
    doc.add_paragraph('This is on the second page!')
    # 添加分页符
    doc.add_page_break()
    doc.add_paragraph('This is on the third page!')
    doc.save(dest_file)


# 添加表格
def test_8(dest_file):
    doc = docx.Document()
    # 创建表格：
    table = doc.add_table(rows=2, cols=2)
    # 通过行和列的索引（从零开始）来确定单元格：
    cell = table.cell(0, 1)
    # 往单元格里写入数据
    cell.text = '单元格'

    # 通过表的.rows属性获取第一行，通过行的.cells属性获取单元格
    row = table.rows[0]
    row.cells[0].text = '1'
    row.cells[1].text = '2'

    # for i, row in enumerate(table.rows):
    #     for j, cell in enumerate(row.cells):
    #         cell.text = list_2D[i][j]

    # 利用循环获取所有单元格数据
    for i, row in enumerate(table.rows):
        for j, cell in enumerate(row.cells):
            print(i, j, cell.text)

    # 使用 len()来计算表中的行数或列数：
    row_count = len(table.rows)
    col_count = len(table.columns)

    # 在表的下方插入一行：
    row = table.add_row()

    # 设计表格样式：
    table.style = 'Light Shading Accent 1'
    doc.save(dest_file)


# 添加图片
def test_9(dest_file):
    '''
    第一个参数是一个字符串，表示图像的文件名。
    可选的width 和height 关键字参数，将设置该图像在文档中的宽度和高度。
    如果省略，宽度和高度将采用默认值，即该图像的正常尺寸。
    你可能愿意用熟悉的单位来指定图像的高度和宽度，诸如英寸或厘米。
    所以在指定 width 和 height 关键字参数时，可以使用 docx.shared.Inches()和docx.shared.Cm()函数。
    单位可以选择英寸inches 或厘米 centimeters：
    '''
    doc = docx.Document()
    doc.add_picture('zophie.png', width=docx.shared.Inches(1), height=docx.shared.Cm(4))
    doc.save(dest_file)


def main():
    source_file = r"PBOX项目PACK10包性能测试报告.docx"
    dest_file = r'helloworld.docx'
    dict_choice = {
        "1": "test_1(source_file)",
        "2": "test_2(dest_file)",
        "3": "test_3(dest_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
