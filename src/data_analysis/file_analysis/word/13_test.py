#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/anpengapple/p/8372991.html
# 样式篇（style）

import os
import sys

import docx
from docx.enum.style import WD_STYLE_TYPE

from public_function import GLOBAL_WORK_DIR


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"demo.docx")
    document = docx.Document(source_file)
    for baseStyle in docx.styles.style:
        print(baseStyle.type)
        # if style.type == WD_STYLE_TYPE.PARAGRAPH:
        #     print(style.text)
        # elif style.type == WD_STYLE_TYPE.TABLE:
        #     for row in style.rows:
        #         for cell in row.cells:
        #             # print(cell.text)
        #             pass
        # else:
        #     print(style.type)


if __name__ == "__main__":
    main()
