#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
'''
最后便是pandoc了，我在这次用到的功能主要是，用来把html文件直接转换为markdow格式文件，
然后再转换为txt文件，最后将txt文件内容和格式一同插入到docx文档中，
当然，也可以直接把html文件转换为docx文件，格式基本一致.

由于使用pandoc是在控制台下cmd或者shell使用的，因此要用到python的另一个包subprocess.
还有一个问题，若使用过程中，出现打开docx文档报错，那么极有可能便是初始化模板格式错误，因此需要替换一个新的初始化模板.
而关于更多例子，可以去官网的在线转换器https://pandoc.org/try/
或者官网的例子https://pandoc.org/demos.html
'''
import subprocess


def main():
    # shell=True相当于新开了一个shell或者cmd控制台
    subprocess.call('pandoc --latex-engine=xelatex temp.html -o temp.text', cwd='d:\\python', shell=True)
    subprocess.call('pandoc --latex-engine=xelatex temp.text -o t1.text', cwd='d:\\python', shell=True)
    subprocess.call('pandoc temp.html -o temp.docx', cwd='d:\\python', shell=True)


if __name__ == "__main__":
    main()
