#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_43798170/article/details/104384750

import csv  # 导出的时候用，导入用pandas最好
import pandas as pd  # 导入csv文件
import math


def main():
    df = pd.read_csv("passingevents.csv")

    matchid = df["MatchID"]
    teamid = df["TeamID"]
    beginplayer = df['OriginPlayerID']
    endplayer = df['DestinationPlayerID']
    beginx = df['EventOrigin_x']
    beginy = df['EventOrigin_y']
    endx = df['EventDestination_x']
    endy = df['EventDestination_y']
    ev = df['EventSubType']
    daoru = [[] for i in range(3)]
    print(beginplayer)

    for i in range(len(matchid)):
        # print(matchid[i],ev[i],teamid[i],endplayer[i])
        if matchid[i] != 1:
            break
        else:
            if ev[i] == 'Simple pass':
                # print(i)
                tepb = beginplayer[i][-2:]
                tepe = endplayer[i][-2:]
                print(tepb)
                daoru[0].append(tepb)
                daoru[1].append(tepe)
                daoru[2].append(math.sqrt((beginx[i] - endx[i]) ** 2 + (beginy[i] - endy[i]) ** 2))
    # rint(daoru)

    label = ['begin', 'to', 'val']
    f = open('tt.csv', 'w', encoding='utf-8', newline='')
    writer = csv.writer(f)
    writer.writerow(label)
    writer.writerows(daoru)


if __name__ == "__main__":
    main()
