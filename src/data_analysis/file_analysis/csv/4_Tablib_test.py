#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os

# https://www.toutiao.com/a6525191832400298509/
# Tablib 是一个用来处理与表格格式数据有关的 Python 库，允许导入、导出、管理表格格式数据，并具备包括切片、动态列、标签和过滤，以及格式化导入和导出等高级功能。
# Tablib 支持导出/导入的格式包括：Excel 、JSON 、YAML 、HTML 、TSV 和 CSV ，暂不支持 XML 。
import tablib


# http://blog.csdn.net/calling_wisdom/article/details/41447791
def test_1():
    # 创建dataset，方法1
    dataset1 = tablib.Dataset()
    header1 = ('ID', 'Name', 'Tel', 'Age')
    dataset1.headers = header1
    dataset1.append([1, 'zhangsan', 13711111111, 16])
    dataset1.append([2, 'lisi', 13811111111, 18])
    dataset1.append([3, 'wangwu', 13911111111, 20])
    dataset1.append([4, 'zhaoliu', 15811111111, 25])
    print('dataset1:', os.linesep, dataset1, os.linesep)

    # 创建dataset，方法2
    header2 = ('ID', 'Name', 'Tel', 'Age')
    data2 = [
        [1, 'zhangsan', 13711111111, 16],
        [2, 'lisi', 13811111111, 18],
        [3, 'wangwu', 13911111111, 20],
        [4, 'zhaoliu', 15811111111, 25]
    ]
    dataset2 = tablib.Dataset(*data2, headers=header2)
    print('dataset2: ', os.linesep, dataset2, os.linesep)

    # 增加行
    dataset1.append([5, 'sunqi', 15911111111, 30])  # 添加到最后一行的下面
    dataset1.insert(0, [0, 'liuyi', 18211111111, 35])  # 在指定位置添加行
    print('增加行后的dataset1: ', os.linesep, dataset1, os.linesep)

    # 删除行
    dataset1.pop()  # 删除最后一行
    dataset1.lpop()  # 删除第一行
    del dataset1[0:2]  # 删除第[0,2)行数据
    print('删除行后的dataset1:', os.linesep, dataset1, os.linesep)

    # 增加列
    # 现在dataset1就剩两行数据了
    dataset1.append_col(('beijing', 'shenzhen'), header='city')  # 增加列到最后一列
    dataset1.insert_col(2, ('male', 'female'), header='sex')  # 在指定位置添加列
    print('增加列后的dataset1: ', os.linesep, dataset1, os.linesep)

    # 删除列
    del dataset1['Tel']
    print('删除列后的dataset1: ', os.linesep, dataset1, os.linesep)

    # 获取各种格式的数据
    print('csv format: ', os.linesep, dataset1.csv, os.linesep)
    # print('yaml format: ', os.linesep, dataset1.yaml, os.linesep)
    # print('tsv format: ', os.linesep, dataset1.tsv, os.linesep)

    # 导出到Excel表格中
    dataset1.title = 'dataset1'  # 设置Excel中表单的名称
    dataset2.title = 'dataset2'
    myfile = open('mycsv.csv', 'wb')
    myfile.write(dataset1.csv)
    myfile.close()


def main():
    test_1()


if __name__ == "__main__":
    main()
