#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 当数据数据特别大，还可以使用 Mars 分布式，Mars 很容易部署(通过几个命令)，也支持在 k8s 集群拉起服务

# pip install pymars
import mars.dataframe as md


def main():
    df = md.read_csv('ratings.csv').execute()
    print(df)


if __name__ == '__main__':
    main()
