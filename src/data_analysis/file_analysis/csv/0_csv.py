#!/usr/bin/env python
# -*- coding: utf-8 -*-


# http://blog.csdn.net/hk2291976/article/details/41545881
'''
CSV是一种广泛使用的文件格式，所谓“CSV”，是Comma Separated Value（逗号分隔值）的英文缩写，通常都是纯文本文件。
出现在档案总管中的档案类型是「逗号分格」。
这里注意，如果文件是一个对象则要加入b属性，读出来的数据可以看作一个二维列表，每一行就是csv文件的一行，
而每个item，就是逗号分隔的每个数据（即excel中的一个单元格）
'''
import os
import sys

from public_function import GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE
from public_function import dict_choice


def test_1():
    import csv
    call_dir = GLOBAL_RELATIVE_PATH_BUILTIN_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "1_csv_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    import pandas
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "2_pandas_test.py")
    print(call_file)
    os.popen(call_file)


def test_3():
    import mars.dataframe as md
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "3_mars_test.py")
    print(call_file)
    os.popen(call_file)


def test_4():
    import tablib
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "4_Tablib_test.py")
    print(call_file)
    os.popen(call_file)


def test_5():
    pass
    # https://blog.csdn.net/xilu_data/article/details/123619140
    # pip install csvkit
    # 它是专门处理CSV文件的命令行工具，可以实现文件互转、数据处理、数据统计等，十分便捷。
    '''
    in2csv：将Excel转为CSV
    csvjson：将CSV文件转换为Json格式
    csvcut：对数据进行索引切片
    csvgrep：对数据进行过滤，可按照正则表达式规则
    csvjoin：对不同数据表按键进行连接
    csvsort：对数据进行排序
    csvstack：将多个数据表进行合并
    csvlook：以 Markdown 兼容的固定宽度格式将 CSV 呈现到命令行
    csvstat：对数据进行简单的统计分析
    
    # in2csv DoubanMovie.xlsx > DoubanMovie.csv
    # csvjson test.csv > test.json
    
    # 对SQL数据库进行读写和查询操作
    # 从MySQL数据库中读取一张表存到本地CSV文件中，使用csvsql命令实现。
    csvsql --db "mysql://user:pass@host/database?charset=utf8" --tables "test1" --insert test1.csv
    # 直接对MySQL数据库进行数据查询，使用sql2csv命令实现
    sql2csv --db "mysql://user:pass@host/database?charset=utf8" --query "select * from test2"
    '''


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == '__main__':
    main()
