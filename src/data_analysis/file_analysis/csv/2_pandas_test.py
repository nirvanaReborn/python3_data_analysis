#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://blog.csdn.net/cxmscb/article/details/54729191
# https://github.com/cxmscb/python_data_analysis_read_save_csv_txt_file


import numpy as np
import pandas as pd
from public_function import dict_choice


# 读取csv文件:当列索引存在时
def test_1():
    x = pd.read_csv("./res/data1.csv")
    print(x)
    '''
       A  B  C  D
    0  1  2  3  a
    1  4  5  6  b
    2  7  8  9  c
    '''


# 读取csv文件:当列索引不存在时,默认从0开始索引
def test_2():
    x = pd.read_csv('./res/data2.csv', header=None)
    print(x)
    '''
       0  1  2  3
    0  1  2  3  a
    1  4  5  6  b
    2  7  8  9  c
    '''


# 设置列索引
def test_3():
    x = pd.read_csv('./res/data2.csv', names=['A', 'B', 'C', 'D'])
    print(x)
    '''
       A  B  C  D
    0  1  2  3  a
    1  4  5  6  b
    2  7  8  9  c
    '''


# 将一(多)列的元素作为行(多层次)索引
def test_4():
    x = pd.read_csv('./res/data2.csv', names=['A', 'B', 'C', 'D'], index_col='D')
    print(x)
    '''
       A  B  C
    D         
    a  1  2  3
    b  4  5  6
    c  7  8  9
    '''

    x = pd.read_csv('./res/data2.csv', names=['A', 'B', 'C', 'D'], index_col=['D', 'C'])
    print(x)
    '''
         A  B
    D C      
    a 3  1  2
    b 6  4  5
    c 9  7  8
    '''


# 一般NULL, nan, 空格 等自动转换为NaN
def test_5():
    x = pd.read_csv('./res/data3.csv', na_values=[])
    print(x)
    '''
         A    B    C    D
    0  1.0  2.0    3  NaN
    1  NaN  5.0    6    b
    2  7.0  NaN  Nan    c
    '''


# 将某个元素值设置为NaN
def test_6():
    x = pd.read_csv('./res/data3.csv', na_values=['Nan'])
    print(x)
    '''
         A    B    C    D
    0  1.0  2.0  3.0  NaN
    1  NaN  5.0  6.0    b
    2  7.0  NaN  NaN    c
    '''


# 在对应列上设置元素为NaN
def test_7():
    setNaN = {'C': ['Nan'], 'D': ['b', 'c']}
    x = pd.read_csv("./res/data3.csv", na_values=setNaN)
    print(x)
    '''
         A    B    C   D
    0  1.0  2.0  3.0 NaN
    1  NaN  5.0  6.0 NaN
    2  7.0  NaN  NaN NaN
    '''


# 读取数据
def test_8():
    x = pd.read_table('./res/data4.txt', sep='\s+')  # sep:分隔的正则表达式
    print(x)
    '''
       A  B  C
    a  1  2  3
    b  4  5  6
    '''

    # 使用numpy读取txt
    x = np.loadtxt('./res/data5.txt', delimiter='\t')  # 分隔符
    print(x)
    '''
    [[ 1.176813  3.16702 ]
     [-0.566606  5.749003]
     [ 0.931635  1.589505]
     [-0.036453  2.690988]]
    '''


def test_9():
    pass


# 保存数据到csv文件
def test_10():
    x = pd.read_csv('./res/data3.csv')
    x.to_csv('./res/data3out.csv', encoding='utf-8-sig')
    '''
    data3out:
    ,A,B,C,D
    0,1.0,2.0,3.0,
    1,,5.0,6.0,
    2,7.0,,,
    '''

    # 保存数据到csv文件,设置NaN的表示,去掉行索引，去掉列索引(header)
    x.to_csv('./res/data3out.csv', na_rep='NaN', index=False, header=False, encoding='utf-8-sig')
    '''
    data3out:
    1.0,2.0,3.0,NaN
    NaN,5.0,6.0,NaN
    7.0,NaN,NaN,NaN
    '''

    x = pd.read_csv("./res/data3out.csv", names=['W', 'X', 'Y', 'Z'])
    print(x)
    '''
         W    X    Y   Z
    0  1.0  2.0  3.0 NaN
    1  NaN  5.0  6.0 NaN
    2  7.0  NaN  NaN NaN
    '''


# 写入csv文件
def test_11():
    cont_list = [{'name': 'zs', 'age': 14}, {'name': 'sd', 'age': 15}]
    df = pd.DataFrame(cont_list, columns=['name', 'age'])
    print(df)
    # index=False表示csv文件不加行序号
    df.to_csv("test.csv", index=False, encoding='utf-8-sig')


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
