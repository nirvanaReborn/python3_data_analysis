#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/hzxpyjq/article/details/50686983
# .strings文件与excel互转、 xml文件与excel互转
import sys

from public_function import dict_choice


def test_1():
    # iOS国际化： 将.strings文件中对应的字符串解析到excel中,每一个文件单独建一个sheet
    from tempfile import TemporaryFile
    from xlwt import Workbook
    import re, sys, os

    dir = sys.path[0]
    print(dir)

    book = Workbook(encoding='utf-8')
    pattern = re.compile(r'".+?"')  # 匹配 "xxx" = "xxx",得到两个字符串

    for file in os.listdir(dir):  # 遍历当前目录
        fileName = os.path.splitext(file)[0]
        fileExt = os.path.splitext(file)[1]

        if os.path.isfile(file) and fileExt == '.strings':
            #        print 'processing file: ', file
            sheet = book.add_sheet(fileName)
            f = open(file, "r")
            if fileName == 'InfoPlist':
                row_index = 0
                for line in f:
                    match = re.match(r'\w+=', line)  # 匹配 xxx="xxx"，得到两个字符串
                    if match:
                        sheet.write(row_index, 0, match.group()[:-1])
                    match2 = pattern.search(line)
                    if match2:
                        sheet.write(row_index, 1, match2.group()[1:-1])
                row_index = row_index + 1
            else:
                row_index = 0
                for line in f:
                    if line.startswith('"'):
                        # TODO：分割字符串，"占位符" = "内容";
                        col_index = 0
                        for m in pattern.finditer(line):
                            sheet.write(row_index, col_index, m.group()[1:-1])
                            col_index = col_index + 1
                        row_index = row_index + 1

                pass
            f.close()

    book.save('simple.xls')
    book.save(TemporaryFile())


def test_2():
    # iOS国际化： 将excel中的内容转化成对应的.strings文件,一个sheet是一个单独的.strings文件

    from xlrd import open_workbook
    import codecs

    workbook = open_workbook('simple.xls')
    for sheet in workbook.sheets():
        print('Sheet name: ', sheet.name)

        # 按照sheet的name生成.strings文件
        file = codecs.open(sheet.name + '.strings', 'w+', encoding='utf-8')
        for row_index in range(sheet.nrows):
            result_placeholder = sheet.cell(row_index, 0).value
            result_content = sheet.cell(row_index, 1).value

            if sheet.name == 'InfoPlist':
                file.write(result_placeholder + '="' + result_content + '";\n')
            else:
                file.write('"' + result_placeholder + '" = "' + result_content + '";\n')
        file.close()


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == '__main__':
    main()
