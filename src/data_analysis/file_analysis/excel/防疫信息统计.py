#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import os
import pandas as pd


def test_1(source_file):
    '''统一更改部分人员的核查结果'''
    df = pd.read_excel(source_file, sheet_name='重点人员名单')
    list_head = df.columns.tolist()  # 标题行
    list_sheet = df.values.tolist()  # 已经跳过标题行
    for i, fileLine in enumerate(list_sheet):
        try:
            is_update = False
            if ("太阳国际" in str(fileLine[list_head.index("具体地址")])
                    # 单元格内容为空
                    and pd.isna(fileLine[list_head.index("核查结果")])):
                identity_card = str(fileLine[list_head.index("主证件号码")])
                if len(identity_card) == 18:
                    # 身份证中出生年份
                    if int(identity_card[6:10]) >= 2009:
                        is_update = True
                    else:
                        print(i+1, "出生年份:", identity_card[6:10])
                else:
                    print(i+1, "不是身份证:", identity_card)
                    is_update = True

                if is_update:
                    remark = "自述18号考试完放假后再也没有返校，与确诊病例无交集，核酸已做，均为阴性。"
                    list_sheet[i][list_head.index("核查结果")] = remark
        except Exception as e:
            print(i + 1, e, fileLine)
    dest_file = os.path.join(os.path.dirname(source_file), "处理后.xlsx")
    pd.DataFrame(list_sheet, columns=list_head).to_excel(dest_file, sheet_name="重点人员名单", index=False, header=True)


def main():
    source_file = r"D:\2022.1.30第五批重点人员18151人.xlsx"
    test_1(source_file)


if __name__ == "__main__":
    main()
