#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import sys

import numpy
import pandas

from public_function import GLOBAL_WORK_DIR


# 读取excel文件
def test_1():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xls")
    dataframe = pandas.read_excel(source_file, sheet_name='登录信息')
    print(dataframe)
    # print([[c, *dataframe[c].tolist()] for c in dataframe.columns])
    list_sheet = list(numpy.array(dataframe).tolist())
    list_sheet.insert(0, dataframe.columns.values.tolist())
    print(list_sheet)


def main():
    test_1()


if __name__ == "__main__":
    main()
