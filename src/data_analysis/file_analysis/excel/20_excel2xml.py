#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/w174504744/article/details/50926197
# Python 将Excel转为Xml

import os
import sys

import numpy
import pandas

from public_function import GLOBAL_WORK_DIR


def export_xml(list_sheet, group="group", item="item"):
    xml_file = u'<?xml version="1.0" encoding="gb2312"?>\n'
    xml_file += u'<%s>\n' % group
    for i in range(1, len(list_sheet)):
        xml_file += u'\t<%s ' % item
        tmp = [u'%s = "%s"' % (str(list_sheet[0][j]), str(list_sheet[i][j]))
               for j in range(len(list_sheet[i]))]
        xml_file += u' '.join(map(str, tmp))
        xml_file += u' />\n'

    xml_file += u'</%s>' % group
    return xml_file


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xlsx")

    # 获取生成的xml文件路径
    dest_file = os.path.splitext(source_file)[0] + '.xml'

    dataframe = pandas.read_excel(source_file)
    # print([[c, *dataframe[c].tolist()] for c in dataframe.columns])
    list_sheet = list(numpy.array(dataframe).tolist())
    list_sheet.insert(0, dataframe.columns.values.tolist())
    # print(list_sheet)

    with open(dest_file, 'w', encoding='UTF-8') as fw:
        fw.write(export_xml(list_sheet))


if __name__ == "__main__":
    main()
