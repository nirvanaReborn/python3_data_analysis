#!/usr/bin/env python
# -*- coding: utf-8 -*-


# http://www.cnblogs.com/lhj588/archive/2012/01/06/2314181.html

import operator
import os
import sys

import numpy
import pandas

from public_function import GLOBAL_WORK_DIR


def compare_file(source_file_1, source_file_2):
    try:
        # 如果我们想读取excel中的所有值时，可以将“sheet_name”属性指定为None，这样会得到一个字典变量，字典的key就是sheet名，value就是对应sheet里的数据。
        dict_sheet_1 = pandas.read_excel(source_file_1, sheet_name=None)
        dict_sheet_2 = pandas.read_excel(source_file_2, sheet_name=None)
        if operator.eq(dict_sheet_1, dict_sheet_2):
            print("两个excel文件内容相同")
        else:
            for key in dict_sheet_1:
                if key in dict_sheet_2.keys():
                    # if len(dict_sheet_1[key]) > len(dict_sheet_2[key]):
                    #     min_row = len(dict_sheet_2[key])
                    # else:
                    #     min_row = len(dict_sheet_1[key])
                    # print(key, min_row)
                    # list indices must be integers or slices, not tuple

                    for i, fileLine in enumerate(dict_sheet_1[key]):
                        if operator.eq(dict_sheet_1[key][i], dict_sheet_2[key][i]):
                            # print(key, "两个excel文件该sheet页的第%d行内容相同" % (i + 1))
                            pass
                        else:
                            print(key, "两个excel文件该sheet页的第%d行内容不同" % (i + 1))
                else:
                    print("[%s]比[%s]中多了sheet页：%s" % (source_file_1, source_file_2, key))
    except Exception as e:
        print(e)


def compare_cell(source_file):
    list_sheet = numpy.array(pandas.read_excel(source_file)).tolist()
    col = 14
    for row, fileLine in enumerate(list_sheet):
        cell_value_1 = list_sheet[row][col]
        cell_value_2 = list_sheet[row + 1][col]
        cell_value_1 = float(cell_value_1)
        cell_value_2 = float(cell_value_2)

        # 与excel表格的行列数保持一致
        tuple = ([row + 1, col + 1, cell_value_1], [row + 2, col + 1, cell_value_2])
        print(tuple)

        if (cell_value_1 == cell_value_2):
            # print("%s和%s的值相等" % row, row + 1)
            pass
        elif (cell_value_1 + cell_value_2 == 0):
            # print("%s和%s的值相反" % row, row + 1)
            pass
        else:
            print("%d和%d的值不一致" % (row, row + 1))
            print(row, cell_value_1)
            print(row + 1, cell_value_2)


def main():
    source_file_1 = os.path.join(GLOBAL_WORK_DIR, "视频.xlsx")
    source_file_2 = os.path.join(GLOBAL_WORK_DIR, "(冲突)视频.xlsx")

    dict_choice = {
        "1": "compare_cell(source_file_1)",
        "2": "compare_file(source_file_1, source_file_2)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
