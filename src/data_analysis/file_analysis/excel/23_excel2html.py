#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.python100.com/html/GK66T8420JZG.html
import numpy
import pandas
# 使用jinja2模板生成HTML表格
import jinja2
# 将HTML代码保存为文件
import io


def test_1():
    source_file = r"D:\SVN\PBOX-FLY\trunk\Documents\D5.Others\Python版本升级\PTrade量化三方库开源协议-py3.11.xlsx"
    df = pandas.read_excel(source_file, sheet_name='开源协议')
    data = numpy.array(df).tolist()
    # print(data)
    # 设置模板
    template = '''

        {% for row in data %}

            {% for item in row %}

            {% endfor %}

        {% endfor %}

        '''

    # 生成HTML表格
    t = jinja2.Template(template)
    html = t.render(data=data)

    print(html)

    # 将HTML代码保存到文件
    with io.open('table.html', 'w', encoding='utf-8') as f:
        f.write(html)


def main():
    html_str = """<table class="dataframe" id="pip_table">
    <thead>
    <tr style="text-align: right;">
        <th>序号</th>
        <th>三方库名称</th>
        <th>Python3.11三方库版本号</th>
        <th>开源协议</th>
    </tr>
    </thead>
    <tbody>
    """
    source_file = r"D:\SVN\PBOX-FLY\trunk\Documents\D5.Others\Python版本升级\PTrade量化三方库开源协议-py3.11.xlsx"
    df = pandas.read_excel(source_file, sheet_name='开源协议')
    data = numpy.array(df).tolist()
    for row in data:
        html_str += """    <tr>
        <td></td>
        <td>{0}</td>
        <td>{1}</td>
        <td>{2}</td>
    </tr>
""".format(row[0], row[1], row[2])

    html_str += """
    <script>
        <!--自增序号-->
        const table = document.querySelector("#pip_table");
        const rows = table.querySelectorAll("tbody tr");
        for (let i = 0; i < rows.length; i++) {
            const cell = rows[i].querySelector("td:first-child");
            cell.textContent = i + 1;
        }
    </script>
</table>
    """

    dest_file = r"D:\SVN\PBOX-FLY\trunk\Sources\fly_IQ_trunk\IQWeb\templates\pip_list_py311.html"
    with open(dest_file, 'w', encoding='UTF-8') as f:
        f.write(html_str)


if __name__ == '__main__':
    main()
