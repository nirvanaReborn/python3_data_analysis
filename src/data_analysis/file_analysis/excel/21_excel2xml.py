#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/hzxpyjq/article/details/50686983
# .strings文件与excel互转、 xml文件与excel互转
import os
import sys

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
import xml.dom.minidom

from public_function import dict_choice
from public_function import GLOBAL_WORK_DIR


def test_1():
    # Android国际化： 将xml文件中对应的字符串解析到excel中
    from xlwt import Workbook

    source_file = os.path.join(GLOBAL_WORK_DIR, 'strings.xml')
    dest_file = os.path.join(GLOBAL_WORK_DIR, 'new_strings.xls')
    # 新建一个workbook
    book = Workbook(encoding='utf-8')
    sheet = book.add_sheet('Android')

    # 打开xml
    xmldoc = xml.dom.minidom.parse(source_file)
    code = xmldoc.getElementsByTagName('string')
    row = 0
    for node in code:
        for item in node.childNodes:
            sheet.write(row, 0, node.getAttribute('name'))
            sheet.write(row, 1, item.data)
        row = row + 1
        # 保存workbook
    book.save(dest_file)


def test_2():
    # Android国际化： 将excel中的内容转化到xml中
    from xlrd import open_workbook
    import codecs

    source_file = os.path.join(GLOBAL_WORK_DIR, 'strings.xls')
    dest_file = os.path.join(GLOBAL_WORK_DIR, 'new_strings.xml')
    # 打开excel
    workbook = open_workbook(source_file)

    # 新建xml
    doc = xml.dom.minidom.Document()
    # 添加根元素
    root = doc.createElement('resources')
    doc.appendChild(root)

    # 添加字符串
    for sheet in workbook.sheets():
        for row_index in range(sheet.nrows):
            result_placeholder = str(sheet.cell(row_index, 0).value)
            result_content = str(sheet.cell(row_index, 1).value)
            # 新建一个文本元素
            text_element = doc.createElement('string')
            # 设置该文本元素的一个属性
            text_element.setAttribute('name', result_placeholder)
            # 在指定元素节点的最后一个子节点后添加节点，该方法返回新的子节点。
            text_element.appendChild(doc.createTextNode(result_content))
            root.appendChild(text_element)

    f = codecs.open(dest_file, 'w', encoding='utf-8')
    # doc.writexml(f)
    f.write(doc.toprettyxml(indent='    '))
    f.close()


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
