#!/usr/bin/env python
# -*- coding: utf-8 -*-

# http://www.python-excel.org/
# http://www.cnblogs.com/lhj588/archive/2012/01/06/2314181.html

import datetime
import sys

import xlrd
import xlwt

from public_function import GLOBAL_XLS_FILE


# ---------------------------------------------------------------------------------------------------
def open_excel(source_file='file.xls'):
    try:
        data = xlrd.open_workbook(source_file)
        return data
    except Exception as e:
        print(str(e))


# 根据索引获取Excel表格中的数据   参数:file：Excel文件路径     colnameindex：表头列名所在行的索引  ，by_index：表的索引
def excel_table_byindex(file='file.xls', colnameindex=0, by_index=0):
    data = open_excel(file)
    sheet = data.sheets()[by_index]
    nrows = sheet.nrows  # 行数
    ncols = sheet.ncols  # 列数
    colnames = sheet.row_values(colnameindex)  # 某一行数据
    list_2D = []
    for rownum in range(1, nrows):
        row = sheet.row_values(rownum)
        if row:
            app = {}
            for i in range(len(colnames)):
                app[colnames[i]] = row[i]
            list_2D.append(app)
    return list_2D


# 根据名称获取Excel表格中的数据   参数:file：Excel文件路径     colnameindex：表头列名所在行的索引  ，by_name：Sheet1名称
def excel_table_byname(source_file='file.xls', colnameindex=0, by_name=u'Sheet1'):
    data = open_excel(source_file)
    sheet = data.sheet_by_name(by_name)
    nrows = sheet.nrows  # 行数
    colnames = sheet.row_values(colnameindex)  # 某一行数据
    list_2D = []
    for rownum in range(1, nrows):
        row = sheet.row_values(rownum)
        if row:
            app = {}
            for i in range(len(colnames)):
                app[colnames[i]] = row[i]
            list_2D.append(app)
    return list_2D


def test_4(source_file='file.xls'):
    data = xlrd.open_workbook(source_file)
    for sheet in data.sheets():
        for row in range(sheet.nrows):
            for col in range(sheet.ncols):
                print(sheet.cell(row, col).value)


def test_3(source_file):
    tables = excel_table_byname(source_file)
    for row in tables:
        print(row)


def test_2(source_file):
    tables = excel_table_byindex(source_file)
    for row in tables:
        print(row)


# ---------------------------------------------------------------------------------------------------
def test_1(source_file='file.xls'):
    # 打开Excel文件读取数据
    data = xlrd.open_workbook(source_file)

    # 获取所有工作表的个数
    sheets = data.sheets()
    count = len(sheets)
    print(count)

    # 获得所有sheet名称
    sheet_names = data.sheet_names()
    print('sheet names :' + str(sheet_names))

    # 遍历sheet名称
    for sheet in data.sheets():
        print(sheet.name)

    # 获取一个工作表
    sheet = data.sheets()[0]  # 通过索引顺序获取
    # sheet = data.sheet_by_index(0) #通过索引顺序获取
    # sheet = data.sheet_by_name(u'Sheet1') #通过名称获取

    # 获取行数和列数
    allrows = sheet.nrows
    allcols = sheet.ncols
    print(allrows)
    print(allcols)

    # 获取单元格的值(从第0行第0列开始)
    cell_A1 = sheet.cell(0, 0).value
    list_2D = [0 + 1, 0 + 1, cell_A1]
    print(list_2D)

    # 使用行列索引
    cell_A1 = sheet.row(0)[0].value
    print(cell_A1)
    cell_B1 = sheet.col(1)[0].value
    print(cell_B1)

    # 获取整行和整列的值（数组）
    list_row = sheet.row_values(0)
    print(list_row)
    list_col = sheet.col_values(0)
    print(list_col)

    # 循环行列表数据
    for row in range(allrows):
        print(sheet.row_values(row))

    # 简单的写入
    row = 0
    col = 0

    # 类型 0 empty,1 string, 2 number, 3 date, 4 boolean, 5 error
    ctype = 1
    value = '单元格的值'

    xf = 0  # 扩展的格式化

    sheet.put_cell(row, col, ctype, value, xf)

    cell_A1 = sheet.cell(0, 0)  # 单元格的值'
    print(cell_A1)

    cell_A1_value = sheet.cell(0, 0).value  # 单元格的值'
    print(cell_A1_value)


# ---------------------------------------------------------------------------------------------------
def test_write_excel(dest_file):
    wb = xlwt.Workbook()
    ws = wb.add_sheet('mysheet')
    ws.write(0, 0, 1.01)  # 把表格想象成二维表，前2各参数是行列
    ws.write(0, 1, 'haha')
    ws.write(1, 0, 'A')
    ws.write(1, 1, 'B')
    ws.write(1, 2, 'SUM')
    ws.write(2, 0, 123)  # A3
    ws.write(2, 1, 456)  # B3
    ws.write(2, 2, xlwt.Formula("A3+B3"))
    wb.save(dest_file)


# 在写入文件的时候还可以加入格式
def test_write_excel_style(dest_file):
    style0 = xlwt.easyxf('font: name Times New Roman, color-index red, bold on', num_format_str='#,##0.00')
    style1 = xlwt.easyxf(num_format_str='D-MM-YYYY')

    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')
    ws.write(0, 0, 1234.56, style0)
    ws.write(1, 0, datetime.datetime.now(), style1)
    ws.write(2, 0, 1)
    ws.write(2, 1, 1)
    ws.write(2, 2, xlwt.Formula("A3+B3"))
    wb.save(dest_file)


# ---------------------------------------------------------------------------------------------------


def main():
    source_file = GLOBAL_XLS_FILE
    dest_file = GLOBAL_XLS_FILE
    dict_choice = {
        "1": "test_1(source_file)",
        "2": "test_2(source_file)",
        "3": "test_3(source_file)",
        "4": "test_4(source_file)",
        "5": "test_5(source_file)",
        "6": "test_6(source_file)",

        "A": "test_write_excel(dest_file)",
        "B": "test_write_excel_style(dest_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
