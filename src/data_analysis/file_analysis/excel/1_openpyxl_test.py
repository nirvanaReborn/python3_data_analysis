#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.python-excel.org/
# Openpyxl 是一个用于读写 Excel 2010 xlsx / xlsm / xltx / xltm 文件的 Python 库。
# http://www.cnblogs.com/guanfuchang/p/5970435.html
# openpyxl 操作 excel

import datetime
import os

import openpyxl
from openpyxl.styles import Color,PatternFill, Font, Border, Side
from openpyxl.utils import column_index_from_string
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.formatting import Rule
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule, FormulaRule

from public_function import GLOBAL_WORK_DIR, GLOBAL_XLSX_FILE


def test_0():
    # 打开文件
    workbook = openpyxl.load_workbook('example.xlsx')

    # 获取工作表
    sheet = workbook.active

    # 按列读取Excel文件数据
    for column in sheet.columns:
        for cell in column:
            print(cell.value)

    # 按行读取Excel文件数据
    for row in sheet.rows:
        for cell in row:
            print(cell.value)


def test_1(source_file=GLOBAL_XLSX_FILE):
    wb = openpyxl.load_workbook(source_file)  # 读文件
    sheetnames = wb.get_sheet_names()  # 获取读文件中所有的sheet，通过名字的方式
    worksheet = wb.get_sheet_by_name(sheetnames[0])  # 获取第一个sheet内容
    rows = worksheet.max_row  # 获取读取的excel的文件的总行数
    cols = worksheet.max_column  # 获取读取的excel的文件的总列数
    print(rows, cols)

    for i in range(rows):
        cell_4 = worksheet.cell(row=i + 1, column=4).value  # 读文件i行4列的表格值
        cell_5 = worksheet.cell(row=i + 1, column=5).value  # 读文件i行5列的表格值
        result = [cell_4, cell_5]
        print(i, result)


def test_2(dest_file=GLOBAL_XLSX_FILE):
    wb = openpyxl.Workbook()
    worksheet = wb.active  # 激活 worksheet
    worksheet['A1'] = 42  # 数据可以直接分配到单元格中
    worksheet.append([1, 2, 3])  # 可以附加行，从第一列开始附加
    worksheet['A3'] = datetime.datetime.now().strftime("%Y-%m-%d")  # Python 类型会被自动转换
    worksheet.row_dimensions[3].height = 70  # 设置行高
    worksheet.column_dimensions['A'].width = 20  # 设置列宽
    wb.save(dest_file)  # 保存文件


# http://cn.voidcc.com/question/p-vjzwheqh-bhn.html
# https://www.jianshu.com/p/3ca14e923a45
def test_3(dest_file=os.path.join(GLOBAL_WORK_DIR, r"demo.xlsx")):
    wb = openpyxl.load_workbook(dest_file)
    ws = wb['全部功能号']
    # FFFF0000 相当于 #FF0000
    # HTML颜色代码表: https://www.917118.com/tool/color_3.html
    red_fill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
    pink_fill = PatternFill(start_color='FFFF5050', end_color='FFFF5050', fill_type='solid')
    light_pink_fill = PatternFill(start_color='FFFF9B99', end_color='FFFF9B99', fill_type='solid')
    blue_fill = PatternFill(start_color='FF0000FF', end_color='FF0000FF', fill_type='solid')
    light_blue_fill = PatternFill(start_color='FF0066FF', end_color='FF0066FF', fill_type='solid')
    v_light_blue_fill = PatternFill(start_color='FF9BC2E6', end_color='FF9BC2E6', fill_type='solid')

    side = Side(style='thin', color='FF000000')
    myBorder = Border(left=side, right=side, bottom=side, top=side)
    my_white_font = Font(color='FFFFFFFF', size=12)
    my_black_font = Font(size=12)
    p = 'A3'
    num = 'A8'
    denom = 'A9'
    ws['{0}'.format(p)] = '=IF({0}/{1} >= 2, {0}/{1}, IF(AND({0}/{1}>=0, {0}/{1} <2),{0}/{1}-1, 1-{0}/{1}))'.format(num, denom)

    ws.conditional_formatting.add('{0}'.format(p),
                                  FormulaRule(formula=['{0} < -0.5'.format(p)], fill=red_fill, font=my_black_font))

    ws.conditional_formatting.add('{0}'.format(p),
                                  FormulaRule(formula=['AND({0} < -0.25, {0} >=-0.5)'.format(p)], fill=pink_fill,
                                              font=my_black_font))

    ws.conditional_formatting.add('{0}'.format(p),
                                  FormulaRule(formula=['AND({0} < 0, {0} >=-0.25)'.format(p)], fill=light_pink_fill,
                                              font=my_black_font))

    ws.conditional_formatting.add('{0}'.format(p),
                                  FormulaRule(formula=['AND({0} >=0, {0} < 1)'.format(p)], fill=v_light_blue_fill,
                                              font=my_black_font))

    ws.conditional_formatting.add('{0}'.format(p),
                                  FormulaRule(formula=['AND({0} >=1, {0} <= 3)'.format(p)], fill=light_blue_fill,
                                              font=my_white_font))

    ws.conditional_formatting.add('{0}'.format(p),
                                  FormulaRule(formula=['{0} > 3'.format(p)], fill=blue_fill,
                                              font=my_white_font))
    wb.save(dest_file)


def test_4(dest_file=os.path.join(GLOBAL_WORK_DIR, r"demo.xlsx")):
    wb = openpyxl.load_workbook(dest_file)
    ws = wb['全部功能号']
    # solid 表示纯色填充， none 表示无色
    red_fill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
    green_fill = PatternFill(start_color='FF00EC00', end_color='FF00EC00', fill_type='solid')
    none_fill = PatternFill(fill_type='none')
    my_white_font = Font(color='FFFFFFFF', size=12)
    my_black_font = Font(size=12)

    dict_color = {
        "LS": red_fill,
        "LF": green_fill,
    }

    # ws['A1'].fill = red_fill
    # ws['A2'].fill = green_fill
    # ws['A3'].fill = none_fill

    for i in range(1, ws.max_row + 1):  # 遍历行号
        fill_color = dict_color.get(ws.cell(row=i, column=5).value, none_fill)
        for j in range(1, ws.max_column + 1):  # 遍历当前行的所有表格
            # 将当前行的每一个表格填充颜色
            ws.cell(row=i, column=j).fill = fill_color

    # Worksheet 对象有 row_dimensions 和column_dimensions 属性，控制行高和列宽。
    wb.save(dest_file)


# 原理new Color(97,100,255);这个是32位存储, 参数依次是 r、g、b。省略了a，alpha通道的值 rgba;
def int32Color(r, g, b):
    return (((r) & 0xff) >> 16) | (((g) & 0xff) >> 8) | (((b) & 0xff) >> 0)


def _32ConvertTo16(color):
    b = (color >> 3) & 0x1f  # 取32位颜色b的前五位  0x1f 二进制表示如下 00011111
    g = (color >> (8 + 2)) & 0x3f
    r = (color >> (16 + 3)) & 0x1f
    return ((r << 11) | (g << 5) | (b << 0))  # (r<<11) + (g<<5) + (b<<0))


# https://blog.csdn.net/masmq/article/details/107154871
# openpyxl3.0官方文档（27）—— 条件格式
def test_5(dest_file=os.path.join(GLOBAL_WORK_DIR, r"123.xlsx")):
    wb = openpyxl.load_workbook(dest_file)
    ws = wb.active
    # solid 表示纯色填充， none 表示无色
    red_fill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
    green_fill = PatternFill(start_color='FF00EC00', end_color='FF00EC00', fill_type='solid')
    none_fill = PatternFill(fill_type='none')

    dict_color = {
        "持仓": red_fill,
        "清仓": green_fill,
    }

    column_letter = openpyxl.utils.get_column_letter(ws.max_column - 1)
    diff_range = 'A2' + ':' + column_letter + str(ws.max_row)
    print(diff_range)

    ws.conditional_formatting.add(diff_range, FormulaRule(formula=['$C2="持仓"'], fill=dict_color.get("持仓", none_fill)))
    ws.conditional_formatting.add(diff_range, FormulaRule(formula=['$C2="清仓"'], fill=dict_color.get("清仓", none_fill)))
    wb.save(dest_file)


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
