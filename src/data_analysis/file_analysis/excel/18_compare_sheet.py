#!/usr/bin/env python
# -*- coding:utf-8 -*-

import collections
import pprint

import numpy
import pandas


# 获取重复项
def get_repeat_data(list_1D):
    list_repeat = []
    dict_count = dict(collections.Counter(list_1D))
    for key in dict_count:
        if dict_count[key] > 1:
            list_repeat.append(key)
    print(list_repeat)
    return list_repeat


def get_sheet(source_file, sheet_name):
    list_url = []
    list_sheet = numpy.array(pandas.read_excel(source_file, sheet_name=sheet_name)).tolist()
    for i, fileLine in enumerate(list_sheet):
        list_url.append(fileLine[1])

    print(source_file, sheet_name, "重复数据有：")
    pprint.pprint(get_repeat_data(list_url))
    return set(list_url)


def main():
    source_file = r"..\html\bookmarks\bookmarks.xlsx"
    set_1 = get_sheet(source_file, "UC书签备份_2018_1_7")
    set_2 = get_sheet(source_file, "UC书签备份_2018_2_25")

    list_1 = set_1 - set_2
    if list_1:
        pprint.pprint(list_1)
    print("*" * 40)
    list_2 = set_2 - set_1
    if list_2:
        pprint.pprint(list_2)


if __name__ == "__main__":
    main()
