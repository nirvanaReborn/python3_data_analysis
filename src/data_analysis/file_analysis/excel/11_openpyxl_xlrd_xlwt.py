#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
python中使用xlrd、xlwt库以及openpyxl库读写excel浅析
http://blog.csdn.net/u013816144/article/details/53317577

1、Python中使用xlrd、xlwt库读写excel（xls）浅析 
2、python中使用openpyxl库读写excel（xlsx）浅析（亲测推荐使用openpyxl库，解决了问题：ValueError: row index was 65536, not allowed by .xls format ） 

# 坐标系直接的坐标互转 gcj02towgs84
'''
import os
import sys

import openpyxl
import xlrd
import xlwt

from public_function import GLOBAL_XLSX_FILE


def create_excel_xlsx(dest_file, list_choice, list_sheet, dict_sheet_name, dict_head):
    if os.path.exists(dest_file):
        os.remove(dest_file)
    wb = openpyxl.Workbook()
    for i in range(len(list_sheet)):
        list_2D = list_sheet[i]
        # 处理标题
        head = dict_head[list_choice[i]]
        header = head.split()
        list_2D.insert(0, header)
        # 新增sheet页
        sheet_name = dict_sheet_name[list_choice[i]]
        worksheet = wb.create_sheet(sheet_name)
        row = 1
        for fileLine in list_2D:
            if len(str(fileLine)) > 0:
                # ws.append(fileLine)
                col = 1
                for item in fileLine:
                    worksheet.cell(row=row, column=col).value = str(item)
                    col += 1
                row += 1
    wb.save(dest_file)


def create_excel_xls(dest_file, list_choice, list_sheet, dict_sheet_name, dict_head):
    if os.path.exists(dest_file):
        os.remove(dest_file)
    wb = xlwt.Workbook()
    for i in range(len(list_sheet)):
        list_2D = list_sheet[i]
        # 处理标题
        head = dict_head[list_choice[i]]
        header = head.split()
        list_2D.insert(0, header)
        # 新增sheet页
        sheet_name = dict_sheet_name[list_choice[i]]
        worksheet = wb.add_sheet(sheet_name)
        row = 0
        for fileLine in list_2D:
            if len(str(fileLine)) > 0:
                col = 0
                for item in fileLine:
                    worksheet.write(row, col, str(item))  # 把表格想象成二维表，前2个参数是行列
                    col += 1
                row += 1
    wb.save(dest_file)


def read_excel_write_xls(source_file, dest_file):
    readbook = xlrd.open_workbook(source_file)  # 读文件
    table = readbook.sheets()[0]  # 获取读入的文件的第一个sheet
    nrows = table.nrows  # 获取sheet的行数
    print(nrows)

    writebook = xlwt.Workbook()  # 打开一个将写的文件
    worksheet = writebook.add_sheet('test')  # 在打开的excel中添加一个sheet
    for i in range(nrows):
        if i == 0:  # 我处理的数据第一行是属性名，所以去掉
            continue
        lng = table.cell(i, 0).value  # 获取i行1列的表格值
        lat = table.cell(i, 2).value  # 获取i行3列的表格值
        # 坐标系直接的坐标互转 gcj02towgs84
        # result = gcj02towgs84(lng,lat)#引用转换函数
        result = [lng, lat]
        print(i)
        worksheet.write(i, 0, result[0])  # 写入excel第i行1列
        worksheet.write(i, 1, result[1])  # 写入excel第i行2列
    writebook.save(dest_file)  # 一定要记得保存


def read_excel_write_xlsx(source_file, dest_file):
    wb = openpyxl.load_workbook(source_file)  # 读文件
    sheetnames = wb.get_sheet_names()  # 获取读文件中所有的sheet，通过名字的方式
    ws = wb.get_sheet_by_name(sheetnames[0])  # 获取第一个sheet内容
    rows = ws.max_row  # 获取读取的excel的文件的行数
    print(rows)

    outwb = openpyxl.Workbook()  # 打开一个将写的文件
    worksheet = outwb.create_sheet("test", 0)  # 在将写的文件创建sheet,插入到最开始的位置
    # outws = outwb.create_sheet(title="test")  # 在将写的文件创建sheet,默认插入到最后
    for i in range(rows):
        lng = ws.cell(row=i + 1, column=4).value  # 读文件i行4列的表格值
        lat = ws.cell(row=i + 1, column=5).value  # 读文件i行5列的表格值
        # 坐标系直接的坐标互转
        # result = gcj02towgs84(lng,lat)#引用函数
        result = [lng, lat]
        print(i)
        worksheet.cell(row=i + 1, column=1).value = result[0]  # 写文件第i行1列
        worksheet.cell(row=i + 1, column=2).value = result[1]  # 写文件第i行2列
    outwb.save(dest_file)  # 一定要记得保存


def get_file_suffix(source_file):
    (filepath, filename) = os.path.split(source_file)
    (shotname, extension) = os.path.splitext(filename)
    return (filepath, filename, shotname, extension)[3]


def main():
    source_file = r'13.xlsx'
    dest_file = GLOBAL_XLSX_FILE
    dict_sheet_name = {
        "0": "code",
        "1": "repeated_function",
        "2": "ls_code",
    }

    dict_head = {
        "0": "功能号 功能名称 类别 行号 调用函数 被调用次数 路径",
        "1": "冲突的功能号",
        "2": "功能号",
    }

    file_suffix = get_file_suffix(dest_file)
    if ".xlsx" == file_suffix:
        read_excel_write_xlsx(source_file, dest_file)
    elif ".xls" == file_suffix:
        read_excel_write_xls(source_file, dest_file)
    else:
        print("未识别的文件:", dest_file)


if __name__ == "__main__":
    main()
