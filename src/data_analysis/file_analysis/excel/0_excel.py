#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#
import os
from public_function import GLOBAL_RELATIVE_PATH_OTHER_MODULE


def test_1():
    import openpyxl
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "openpyxl_test.py")
    print(call_file)
    os.popen(call_file)


def test_2():
    import xlrd
    import xlwt
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "xlrd_test.py")
    print(call_file)
    os.popen(call_file)

    call_file = os.path.join(os.path.abspath(call_dir), "xlwt_test.py")
    print(call_file)
    os.popen(call_file)


def test_3():
    # xlutils（依赖于xlrd和xlwt）提供复制excel文件内容和修改文件的功能。
    # 其实际也只是在xlrd.Book和xlwt.Workbook之间建立了一个管道而已
    import xlutils
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "xlutils_test.py")
    print(call_file)
    os.popen(call_file)


def test_4():
    import pandas
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "pandas_test.py")
    print(call_file)
    os.popen(call_file)


def test_5():
    import xlsxwriter
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "XlsxWriter_test.py")
    print(call_file)
    os.popen(call_file)


def test_6():
    # 这个库可以从Office 格式的文件中抽取数据。
    import oletools
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "oletools_test.py")
    print(call_file)
    os.popen(call_file)


def test_7():
    import win32com
    call_dir = GLOBAL_RELATIVE_PATH_OTHER_MODULE
    call_file = os.path.join(os.path.abspath(call_dir), "pywin32_test.py")
    print(call_file)
    os.popen(call_file)


def main():
    from public_function import dict_choice
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
