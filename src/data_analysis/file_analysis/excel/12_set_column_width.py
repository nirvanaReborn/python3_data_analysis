#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 调整单元格的宽度和高度
# https://stackoverflow.com/questions/13197574/openpyxl-adjust-column-width-size

import openpyxl

try:
    from openpyxl.cell import get_column_letter
except ImportError:
    from openpyxl.utils import get_column_letter

from public_function import GLOBAL_XLSX_FILE


def test_1(workSheet, data):
    column_widths = []
    for row in data:
        for i, cell in enumerate(row):
            if len(column_widths) > i:
                if len(cell) > column_widths[i]:
                    column_widths[i] = len(cell)
            else:
                column_widths += [len(cell)]

    for i, column_width in enumerate(column_widths):
        workSheet.column_dimensions[get_column_letter(i + 1)].width = column_width
    return workSheet


def test_2(workSheet, data):
    dict_column_width = {}
    for row in workSheet.rows:
        for cell in row:
            if cell.value:
                dict_column_width[cell.column] = max((dict_column_width.get(cell.column, 0), len(cell.value)))
    for col, value in dict_column_width.items():
        workSheet.column_dimensions[col].width = value
    return workSheet


def test_3(workSheet, data):
    def as_text(value):
        if value is None:
            return ""
        return str(value)

    for column_cells in workSheet.columns:
        length = max(len(as_text(cell.value)) for cell in column_cells)
        workSheet.column_dimensions[column_cells[0].column].width = length
    return workSheet


def test_4(workSheet, data):
    column_widths = []
    for row in workSheet.iter_rows():
        for i, cell in enumerate(row):
            try:
                column_widths[i] = max(column_widths[i], len(cell.value))
            except IndexError:
                column_widths.append(len(cell.value))

    for i, column_width in enumerate(column_widths):
        workSheet.column_dimensions[get_column_letter(i + 1)].width = column_width
    return workSheet


def test_5(workSheet, data):
    for col in workSheet.columns:
        max_length = 0
        column = col[0].column  # Get the column name
        for cell in col:
            if cell.coordinate in workSheet.merged_cells:  # not check merge_cells
                continue
            try:  # Necessary to avoid error on empty cells
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass
        adjusted_width = (max_length + 2) * 1.2
        workSheet.column_dimensions[column].width = adjusted_width
    return workSheet


def read_excel(source_file):
    list_2d = []
    wb = openpyxl.load_workbook(source_file)
    workSheet = wb.get_sheet_by_name("Report4.html")
    for i in range(workSheet.max_row):
        list_1d = []
        for j in range(workSheet.max_column):
            list_1d.append(workSheet.cell(row=i + 1, column=j + 1).value)
        list_2d.append(list_1d)
    return list_2d


def write_excel(source_file, dest_file, function):
    data = read_excel(source_file)
    wb = openpyxl.load_workbook(source_file)
    workSheet = wb.get_sheet_by_name("Sheet")
    workSheet = function(workSheet, data)
    for i, fileLine in enumerate(data):
        if len(str(fileLine)) > 0:
            workSheet.append(fileLine)
    wb.save(dest_file)


def make_choice(dict_choice, source_file, dest_file):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        write_excel(source_file, dest_file, dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    source_file = GLOBAL_XLSX_FILE
    dest_file = GLOBAL_XLSX_FILE
    dict_choice = {
        "1": "test_1",
        "2": "test_2",
        "3": "test_3",
        "4": "test_4",
        "5": "test_5",
    }

    make_choice(dict_choice, source_file, dest_file)


if __name__ == '__main__':
    main()
