#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import pandas as pd


def main():
    source_file = r'D:\BaiduNetdiskDownload\新闻.xlsx'
    df = pd.read_excel(source_file, sheet_name= '其他', dtype=str).fillna('')
    str_md = '# 其他\n'
    dest_file = r'D:\BaiduNetdiskDownload\news.md'
    for index, row in df.iterrows():
        # 去除NBSP（Non-Breaking Space，非断行空格）
        row['标题'] = row['标题'].replace('\u00A0', ' ').strip()
        row['网址'] = row['网址'].replace('\u00A0', ' ').strip()

        str_md += '\n[{0}]({1})\n'.format(row['标题'], row['网址'])

    with open(dest_file, 'w', encoding='utf-8') as f:
        f.write(str_md)


if __name__ == '__main__':
    main()
