#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.python-excel.org/
# 用于编写数据，格式化信息的替代软件包，特别是Excel 2010格式的图表（即：.xlsx）

# https://www.cnblogs.com/fkissx/p/5617630.html

# pip install XlsxWriter
import xlsxwriter


def test_1():
    workbook = xlsxwriter.Workbook('hello.xlsx')  # 建立文件
    worksheet = workbook.add_worksheet()  # 建立sheet， 可以work.add_worksheet('employee')来指定sheet名，但中文名会报UnicodeDecodeErro的错误
    worksheet.write('A1', 'Hello world')  # 向A1写入
    workbook.close()


def main():
    test_1()


if __name__ == "__main__":
    main()
