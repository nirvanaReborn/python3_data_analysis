#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 数据去重

import os
import sys

import numpy
import pandas

from public_function import GLOBAL_WORK_DIR


# 找出小说作者为空的情况
def get_value_null(list_sheet):
    for i, fileLine in enumerate(list_sheet):
        if str(fileLine[1]).strip() == "":
            print("作者为空：", i + 1, fileLine[0])


# 找出小说重复的情况
def get_value_repeat(list_sheet):
    dict_count = {}
    for i, fileLine in enumerate(list_sheet):
        key = str(fileLine[0]).strip()
        if key in dict_count.keys():
            if str(fileLine[1]).strip() == dict_count[key]:
                print("小说重复：", i + 1, key, fileLine[1])
        else:
            dict_count[key] = str(fileLine[1]).strip()


# 找出带有空格的情况
def get_value_space(list_sheet):
    for i, fileLine in enumerate(list_sheet):
        for j, cell in enumerate(fileLine):
            if str(cell).strip() != cell:
                print("带有空格", i + 1, j + 1, cell)


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, "小说.xlsx")
    dataframe = pandas.read_excel(source_file, sheet_name=r"网络小说")
    # print([[c, *dataframe[c].tolist()] for c in dataframe.columns])
    list_sheet = list(numpy.array(dataframe).tolist())
    list_sheet.insert(0, dataframe.columns.values.tolist())
    # print(list_sheet)
    get_value_repeat(list_sheet)
    get_value_null(list_sheet)
    get_value_space(list_sheet)


if __name__ == "__main__":
    main()
