#!/usr/bin/env python
# -*- coding:utf-8 -*-


import os
import sys

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))
# print(PROJECT_ROOT)


call_dir = os.path.join(PROJECT_ROOT, r"../../../python3_course/src/common")
GLOBAL_RELATIVE_PATH_OTHER_MODULE = os.path.join(call_dir, r"./module/other_module")
GLOBAL_RELATIVE_PATH_BUILTIN_MODULE = os.path.join(call_dir, r"./module/built-in_module")

call_dir = os.path.join(PROJECT_ROOT, r"../../../python3_public_resource/src/public_resource")
# print(os.path.abspath(call_dir))
sys.path.append(call_dir)
# from public_function_summary import *


# 全局常量
IMAGEIO_FFMPEG_EXE = r"D:\ProgramFiles\1_FreeInstallation\ffmpeg\ffmpeg.exe"
GLOBAL_EMPTY_STR = ''
GLOBAL_SPACE_CHAR = ' '

# GLOBAL_WORK_DIR = r"D:\share\test"
GLOBAL_WORK_DIR = __import__('os').path.join(PROJECT_ROOT, "resource")
GLOBAL_FILE_NAME = r"%s" % __import__('time').strftime("%Y%m%d")
GLOBAL_TXT_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".txt")
GLOBAL_LOG_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".log")
GLOBAL_ERROR_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".error")
GLOBAL_CSV_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".csv")
GLOBAL_XLS_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".xls")
GLOBAL_XLSX_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".xlsx")
GLOBAL_DOCX_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".docx")
# GLOBAL_MP3_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".mp3")
GLOBAL_MP3_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, "demo.mp3")
GLOBAL_MP4_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".mp4")
GLOBAL_JPG_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".jpg")
GLOBAL_PNG_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".png")
GLOBAL_GIF_FILE = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".gif")

dict_choice = {}
for i in range(0, 100):
    dict_choice[str(i)] = "test_{}()".format(str(i))


class GetConfigInfo():
    # 初始化函数，类似于构造函数
    def __init__(self, file_name, filter_tag=r"登录信息"):
        self.source_file = os.path.abspath(file_name)
        self.filter_tag = filter_tag
        self.list_unique_index = []

    def analysis_xml(self) -> list:
        try:
            import xml.etree.cElementTree as ET
        except ImportError:
            import xml.etree.ElementTree as ET

        list_config = []
        tree = ET.parse(self.source_file)
        root = tree.getroot()
        for child in root:  # 第二层节点
            if child.tag == self.filter_tag:
                if "unique_index" in child.keys():
                    unique_index = child.get("unique_index", "")
                    if unique_index:
                        self.list_unique_index = str(unique_index).split(',')

                for subchild in child:  # 第三层节点
                    list_config.append(subchild.attrib)
        return list_config

    def get_list_config(self) -> list:
        filename, suffix = os.path.splitext(self.source_file)
        # print(suffix)
        if ".xml" == suffix:
            return self.analysis_xml()
        # elif suffix in (".xlsx", ".xls"):
        #     return self.analysis_excel()
        elif ".ini" == suffix:
            pass
        else:
            print("未知的文件格式:", self.source_file)
            return []

    def get_dict_config(self) -> dict:
        def get_list_unique_index(dict_info):
            if self.list_unique_index:
                if len(self.list_unique_index) == 1:
                    return str(dict_info[self.list_unique_index[0]])
                else:
                    list_unique_index = []
                    for item in self.list_unique_index:
                        list_unique_index.append(str(dict_info[item]))
                    return tuple(list_unique_index)
            else:
                print("没有指定唯一索引字段列表")
                return None

        dict_config = {}
        list_dict_info = self.get_list_config()
        for dict_info in list_dict_info:
            dict_config[get_list_unique_index(dict_info)] = dict_info

        return dict_config


class Excel_Pandas():
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_XLSX_FILE):
        super(Excel_Pandas, self).__init__()
        self.file_name = os.path.abspath(file_name)

    def create_excel_pandas(self, dict_sheet_name, dict_head, list3D_sheet, append=False):
        import pandas

        if list3D_sheet != None and len(list3D_sheet) > 0:
            # if_sheet_exists = 'replace'   # 写入Sheet页方式为覆盖
            # if_sheet_exists = 'overlay' # 写入Sheet页方式为追加
            if os.path.exists(self.file_name):
                kargs = {'mode': 'a', 'if_sheet_exists': 'replace'}
            else:
                kargs = {'mode': 'w'}
            with pandas.ExcelWriter(self.file_name, engine='openpyxl', **kargs) as writer:
                for i, key in enumerate(list(dict_sheet_name.keys())):
                    df = pandas.DataFrame(list3D_sheet[i], columns=list(dict_head[key]))
                    df.to_excel(writer, sheet_name=dict_sheet_name[key], index=False, header=True)

    def create_excel_pandas_by_dict(self, dict_sheet, header_flag=True, append=False):
        dict_head = {}
        dict_sheet_name = {}
        list3D_sheet = []

        if dict_sheet:
            for key in dict_sheet:
                list_2D = dict_sheet[key]
                if header_flag:
                    dict_head[key] = list_2D.pop(0)
                else:
                    dict_head[key] = "无" * len(list_2D[0])

                dict_sheet_name[key] = key
                list3D_sheet.append(list_2D)

            self.create_excel_pandas(dict_sheet_name, dict_head, list3D_sheet, append)

    def create_excel_by_function(self, dict_source_file, str_header, function_name, arg=(), append=False):
        dict_head = {}
        dict_sheet_name = {}
        list3D_sheet = []

        for key in dict_source_file:
            if arg:
                list_sheet = function_name(dict_source_file[key], arg)
            else:
                list_sheet = function_name(dict_source_file[key])

            if list_sheet != None and len(list_sheet) > 0:
                dict_head[key] = str_header.split()
                dict_sheet_name[key] = key
                list3D_sheet.append(list_sheet)

        self.create_excel_pandas(dict_sheet_name, dict_head, list3D_sheet, append)

    def get_list1D_from_list2D(self, list_2D, index=0):
        list_1D = []
        for i, fileLine in enumerate(list_2D):
            list_1D.append(fileLine[index])
        return list_1D

    # 统计使用频率
    def count_usage_frequency_by_dict(self, object):
        # object可以是list或字符串
        dict_count = {}
        for item in object:
            dict_count[item] = dict_count.get(item, 0) + 1
        list_count = sorted(dict_count.items(), key=lambda d: d[1], reverse=True)
        # dict(collections.Counter(list_1D))
        return list_count


def show_PrettyTable(list_2D, list_head=None, title=None):
    import prettytable
    if not list_2D:
        return

    if title:
        print(title)

    if isinstance(list_2D, dict):
        list_2D = [list_2D]

    table = prettytable.PrettyTable(border=True, header=True)
    # noinspection PyProtectedMember
    table.align = "l"  # 水平对齐方式（None，“l”（左对齐），“c”（居中），“r”右对齐）
    for i, fileLine in enumerate(list_2D):
        if fileLine:
            if isinstance(fileLine, dict):
                table._set_field_names(fileLine.keys())
                table.add_row(fileLine.values())
            else:
                table.add_row(fileLine)
    if list_head:
        table._set_field_names(list_head)  # Field names must be unique!
    print(table)
