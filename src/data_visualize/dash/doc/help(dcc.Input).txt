Help on class Input in module builtins:

class Input(dash.development.base_component.Component)
 |  A Input component.
 |  A basic HTML input control for entering text, numbers, or passwords.
 |  
 |  Note that checkbox and radio types are supported through
 |  the Checklist and RadioItems component. Dates, times, and file uploads
 |  are also supported through separate components.
 |  
 |  Keyword arguments:
 |  - id (string; optional): The ID of this component, used to identify dash components
 |  in callbacks. The ID needs to be unique across all of the
 |  components in an app.
 |  - value (string; optional): The value of the input
 |  - style (dict; optional): The input's inline styles
 |  - className (string; optional): The class of the input element
 |  - type (a value equal to: "text", 'number', 'password', 'email', 'range', 'search', 'tel', 'url', 'hidden'; optional): The type of control to render.
 |  - autocomplete (string; optional): This attribute indicates whether the value of the control can be automatically completed by the browser.
 |  - autofocus (string; optional): The element should be automatically focused after the page loaded.
 |  - inputmode (a value equal to: "verbatim", "latin", "latin-name", "latin-prose", "full-width-latin", "kana", "katakana", "numeric", "tel", "email", "url"; optional)
 |  - list (string; optional): Identifies a list of pre-defined options to suggest to the user.
 |  The value must be the id of a <datalist> element in the same document.
 |  The browser displays only options that are valid values for this
 |  input element.
 |  This attribute is ignored when the type attribute's value is
 |  hidden, checkbox, radio, file, or a button type.
 |  - max (string; optional): The maximum (numeric or date-time) value for this item, which must not be less than its minimum (min attribute) value.
 |  - maxlength (string; optional): If the value of the type attribute is text, email, search, password, tel, or url, this attribute specifies the maximum number of characters (in UTF-16 code units) that the user can enter. For other control types, it is ignored. It can exceed the value of the size attribute. If it is not specified, the user can enter an unlimited number of characters. Specifying a negative number results in the default behavior (i.e. the user can enter an unlimited number of characters). The constraint is evaluated only when the value of the attribute has been changed.
 |  - min (string; optional): The minimum (numeric or date-time) value for this item, which must not be greater than its maximum (max attribute) value.
 |  - minlength (string; optional): If the value of the type attribute is text, email, search, password, tel, or url, this attribute specifies the minimum number of characters (in Unicode code points) that the user can enter. For other control types, it is ignored.
 |  - multiple (string; optional): This Boolean attribute indicates whether the user can enter more than one value. This attribute applies when the type attribute is set to email or file, otherwise it is ignored.
 |  - name (string; optional): The name of the control, which is submitted with the form data.
 |  - pattern (string; optional): A regular expression that the control's value is checked against. The pattern must match the entire value, not just some subset. Use the title attribute to describe the pattern to help the user. This attribute applies when the value of the type attribute is text, search, tel, url, email, or password, otherwise it is ignored. The regular expression language is the same as JavaScript RegExp algorithm, with the 'u' parameter that makes it treat the pattern as a sequence of unicode code points. The pattern is not surrounded by forward slashes.
 |  - placeholder (string; optional): A hint to the user of what can be entered in the control . The placeholder text must not contain carriage returns or line-feeds. Note: Do not use the placeholder attribute instead of a <label> element, their purposes are different. The <label> attribute describes the role of the form element (i.e. it indicates what kind of information is expected), and the placeholder attribute is a hint about the format that the content should take. There are cases in which the placeholder attribute is never displayed to the user, so the form must be understandable without it.
 |  - readonly (string; optional): This attribute indicates that the user cannot modify the value of the control. The value of the attribute is irrelevant. If you need read-write access to the input value, do not add the "readonly" attribute. It is ignored if the value of the type attribute is hidden, range, color, checkbox, radio, file, or a button type (such as button or submit).
 |  - required (string; optional): This attribute specifies that the user must fill in a value before submitting a form. It cannot be used when the type attribute is hidden, image, or a button type (submit, reset, or button). The :optional and :required CSS pseudo-classes will be applied to the field as appropriate.
 |  - selectionDirection (string; optional): The direction in which selection occurred. This is "forward" if the selection was made from left-to-right in an LTR locale or right-to-left in an RTL locale, or "backward" if the selection was made in the opposite direction. On platforms on which it's possible this value isn't known, the value can be "none"; for example, on macOS, the default direction is "none", then as the user begins to modify the selection using the keyboard, this will change to reflect the direction in which the selection is expanding.
 |  - selectionEnd (string; optional): The offset into the element's text content of the last selected character. If there's no selection, this value indicates the offset to the character following the current text input cursor position (that is, the position the next character typed would occupy).
 |  - selectionStart (string; optional): The offset into the element's text content of the first selected character. If there's no selection, this value indicates the offset to the character following the current text input cursor position (that is, the position the next character typed would occupy).
 |  - size (string; optional): The initial size of the control. This value is in pixels unless the value of the type attribute is text or password, in which case it is an integer number of characters. Starting in, this attribute applies only when the type attribute is set to text, search, tel, url, email, or password, otherwise it is ignored. In addition, the size must be greater than zero. If you do not specify a size, a default value of 20 is used.' simply states "the user agent should ensure that at least that many characters are visible", but different characters can have different widths in certain fonts. In some browsers, a certain string with x characters will not be entirely visible even if size is defined to at least x.
 |  - spellcheck (string; optional): Setting the value of this attribute to true indicates that the element needs to have its spelling and grammar checked. The value default indicates that the element is to act according to a default behavior, possibly based on the parent element's own spellcheck value. The value false indicates that the element should not be checked.
 |  - step (string; optional): Works with the min and max attributes to limit the increments at which a numeric or date-time value can be set. It can be the string any or a positive floating point number. If this attribute is not set to any, the control accepts only values at multiples of the step value greater than the minimum.
 |  
 |  Available events: 'blur', 'change'
 |  
 |  Method resolution order:
 |      Input
 |      dash.development.base_component.Component
 |      collections.abc.MutableMapping
 |      collections.abc.Mapping
 |      collections.abc.Sized
 |      collections.abc.Iterable
 |      collections.abc.Container
 |      object
 |  
 |  Methods defined here:
 |  
 |  __init__(self, **kwargs)
 |  
 |  __repr__(self)
 |  
 |  ----------------------------------------------------------------------
 |  Data and other attributes defined here:
 |  
 |  __abstractmethods__ = frozenset()
 |  
 |  ----------------------------------------------------------------------
 |  Methods inherited from dash.development.base_component.Component:
 |  
 |  __delitem__(self, id)
 |      Delete items by ID in the tree of children.
 |  
 |  __getitem__(self, id)
 |      Recursively find the element with the given ID through the tree
 |      of children.
 |  
 |  __iter__(self)
 |      Yield IDs in the tree of children.
 |  
 |  __len__(self)
 |      Return the number of items in the tree.
 |  
 |  __setitem__(self, id, item)
 |      Set an element by its ID.
 |  
 |  to_plotly_json(self)
 |  
 |  traverse(self)
 |      Yield each item in the tree.
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors inherited from dash.development.base_component.Component:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)
 |  
 |  ----------------------------------------------------------------------
 |  Methods inherited from collections.abc.MutableMapping:
 |  
 |  clear(self)
 |      D.clear() -> None.  Remove all items from D.
 |  
 |  pop(self, key, default=<object object at 0x00218030>)
 |      D.pop(k[,d]) -> v, remove specified key and return the corresponding value.
 |      If key is not found, d is returned if given, otherwise KeyError is raised.
 |  
 |  popitem(self)
 |      D.popitem() -> (k, v), remove and return some (key, value) pair
 |      as a 2-tuple; but raise KeyError if D is empty.
 |  
 |  setdefault(self, key, default=None)
 |      D.setdefault(k[,d]) -> D.get(k,d), also set D[k]=d if k not in D
 |  
 |  update(*args, **kwds)
 |      D.update([E, ]**F) -> None.  Update D from mapping/iterable E and F.
 |      If E present and has a .keys() method, does:     for k in E: D[k] = E[k]
 |      If E present and lacks .keys() method, does:     for (k, v) in E: D[k] = v
 |      In either case, this is followed by: for k, v in F.items(): D[k] = v
 |  
 |  ----------------------------------------------------------------------
 |  Methods inherited from collections.abc.Mapping:
 |  
 |  __contains__(self, key)
 |  
 |  __eq__(self, other)
 |      Return self==value.
 |  
 |  get(self, key, default=None)
 |      D.get(k[,d]) -> D[k] if k in D, else d.  d defaults to None.
 |  
 |  items(self)
 |      D.items() -> a set-like object providing a view on D's items
 |  
 |  keys(self)
 |      D.keys() -> a set-like object providing a view on D's keys
 |  
 |  values(self)
 |      D.values() -> an object providing a view on D's values
 |  
 |  ----------------------------------------------------------------------
 |  Data and other attributes inherited from collections.abc.Mapping:
 |  
 |  __hash__ = None
 |  
 |  ----------------------------------------------------------------------
 |  Class methods inherited from collections.abc.Sized:
 |  
 |  __subclasshook__(C) from abc.ABCMeta
 |      Abstract classes can override this to customize issubclass().
 |      
 |      This is invoked early on by abc.ABCMeta.__subclasscheck__().
 |      It should return True, False or NotImplemented.  If it returns
 |      NotImplemented, the normal algorithm is used.  Otherwise, it
 |      overrides the normal algorithm (and the outcome is cached).