# coding=UTF-8
import dash
import pandas
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 
"""
df = pandas.read_csv("ptbkstock.csv", dtype=str)


app = dash.Dash()

app.layout = html.Div([
    html.H2(children="饼状图_查看投资账户持仓分布"),
    dcc.Dropdown(
        id='id_acct_select',
        options=[{'label': str(i), 'value': str(i), 'disabled': False} for i in df["PTINVEST_ACCOUNT"].unique()], # disabled 该选项是否不可选
        value=[df["PTINVEST_ACCOUNT"].unique()[0]],
        multi=True,         # 是否可以多选
        searchable=True,    # 是否支持模糊匹配
        clearable=True,     # 是否有清除选择按钮
        placeholder="请选择一个账户...",
        disabled=False      # 控件是否可用
        # setProps # ???
    ),
    dcc.Graph(
        id='id_pie'
    )
])

# dmoain x 方向分布
def get_domain_X(p_count): # 生成iter
    if isinstance(p_count, int):
        for i in range(p_count):
            middle_value = float('%.2f' % (1 / p_count)) # 保留两位小数
            yield [i*middle_value+0.01, (i+1)*middle_value-0.01]

@app.callback(
    Output(component_id='id_pie', component_property='figure'),
    [
        Input(component_id='id_acct_select', component_property='value'),
    ],
)
def updateGraph(p_acct_select):
    if p_acct_select:
        select_count = len(p_acct_select)
        traces = []
        for acct_no, domain_X in zip(p_acct_select, get_domain_X(select_count)):
            df_acct = df[df["PTINVEST_ACCOUNT"] == str(acct_no)]
            df_acct = df_acct[df_acct["PTINVEST_ACCOUNT"] == df_acct["TRADE_UNIT"]]
            stkcode_type = list(map(lambda x, y: '代码:'+str(x)+'市场:'+str(y), df_acct["STOCK_CODE"], df_acct["EXCHANGE_TYPE"]))
            traces.append(
                go.Pie(
                    visible=True,                       # 决定该trace是否显示。default: True
                    sort=False,                         # 确定扇区是否从最大到最小被重新排序。default: True
                    direction='counterclockwise',       # 扇区中text的显示方向 default: "counterclockwise"
                    opacity=1,                          # 不透明度
                    labels=stkcode_type,                # 每项的标签
                    values=df_acct["ENABLE_AMOUNT"],    # 每项的数值
                    pull=['0.2', '0.3'],                # 将扇区拉离圆心
                    hoverinfo='all',                    # hover时显示内容 默认为："all"。Any combination of "label", "text", "value", "percent", "name" joined with a "+" OR "all" or "none" or "skip". 例如: "label", "text", "label+text", "label+text+value", "all",这里text指的是hovertext属性的内容
                    hovertext='hovertext',              # 设置与每个扇区关联的悬停文本元素。 如果一个字符串，所有数据点出现相同的字符串。 如果是一个字符串数组，这些项目按照这个跟踪的扇区顺序映射。 default: ""
                    textinfo='percent+value+text',      # 显示在饼图的数值 默认为:"percent"。Any combination of "label", "text", "value", "percent" joined with a "+" OR "none". 例如: "label", "text", "label+text", "label+text+value", "none"，这里text指的是text属性的内容
                    text=['text'] * len(df_acct),       # 扇区上显示的自定义文本
                    textposition='auto',                # 指定textinfo的显示位置 default: "auto"  例如: "inside" | "outside" | "auto" | "none"
                    marker=go.Marker(                   # 编辑每项样式
                        # color=[],                     # 每项的颜色
                        line=go.Line(                   # marker的轮廓样式
                            color="#000000",
                            width=2,
                        ),
                    ),
                    domain={                            # 在traces中有多个饼图时，调整饼图的位置
                        'x': domain_X,                  # 在x轴方向 默认值为[0, 1]表示占据全部
                        # 'y': []                       # 在y轴方向 默认值为[0, 1]
                    },
                    hole='0.35',                         # 设置切出馅饼的半径的分数。 用它来制作甜甜圈图表。default: 0
                    name='投资账户:' + str(acct_no)      # trace名
                )
            )
        return {
            'data': traces,
            'layout':
                {
                    'title': "查看投资账户的持仓数量占比",
                    'annotations': [go.Annotations( # 注释 若图形为甜甜圈，可以再圆心中显示信息
                        text='annotations_text',
                        x=0.5,
                        y=0.5,
                        showarrow=False,
                    )]
                }
        }


if __name__ == "__main__":
    app.run_server(debug=True)