import pandas
import plotly

df = pandas.read_csv("ptbkstock.csv", dtype=str)
df_acct = df[df["PTINVEST_ACCOUNT"] == "2288001301"]
df_acct = df_acct[df_acct["PTINVEST_ACCOUNT"] == df_acct["TRADE_UNIT"]]
# print(df_acct)
a = 1
b = 3
print(a/b)
print(type(a/b))
#方法一：
print(round(a/b, 2))
print(type(round(a/b, 2)))
#方法二：
print(format(float(a)/float(b), '.2f'))
print(type(format(float(a)/float(b), '.2f')))
#方法三：
print('%.2f' % (a/b))
print(type('%.2f' % (a/b)))

print(['text']*2)