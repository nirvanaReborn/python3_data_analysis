# coding=UTF-8
import dash
import pandas as pd
import dash_html_components as html
import dash_core_components as dcc
import plotly.graph_objs as go

"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:使用dash创建散点图
"""

app = dash.Dash()

df = pd.read_csv("data.csv")

app.layout = html.Div(
    dcc.Graph(
        id='id_scatter_plot',
        figure={
            # 获取数据
            'data': [
                go.Scatter(
                    x=df[df['continent'] == i]['gdp per capita'],# x 轴数值
                    y=df[df['continent'] == i]['life expectancy'],# y 轴数值
                    text=df[df['continent'] == i]['country'],# 散点tip显示数值
                    mode='markers',# 用散点表示
                    opacity=0.7, # 不透明度
                    marker={ # 点属性
                        'size': 15, # 大小
                        'line': {'width': 0.5, 'color': 'black'} # 点轮廓样式
                    },
                    name=i,
                ) for i in df.continent.unique()
            ],
            'layout': go.Layout(
                xaxis={'type': 'log', 'title': 'GDP Per Capita'},
                yaxis={'title': 'Life Expectancy'},
                margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
                legend={'x': 0, 'y': 1},
                hovermode=False
            )
        }
    )
)

if __name__ == "__main__":
    app.run_server(debug=True)