# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from dash.dependencies import Output, Input
import pandas
import numpy
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 利用Graph的selectedData属性,动态突出显示所选数据在其他Graph中的位置
"""
# 生成一个50行4列的随机数据表格
df = pandas.DataFrame({
    'Column {}'.format(i): numpy.random.rand(50) + i*10
    for i in range(4)
})

app = dash.Dash()

app.layout = html.Div([
    html.H2(children="演示Graph的selectedData属性,动态突出显示所选数据在其他Graph中的位置"),
    dcc.Graph(
        id='g1',
    ),
    dcc.Graph(
        id='g2',
    ),
])

def create_figure(p_g1_Data, p_g2_Data ,p_graph):
    if p_graph == "g1":
        x_colum = "Column 0"
        y_colum = "Column 1"
    elif p_graph == "g2":
        x_colum = "Column 2"
        y_colum = "Column 3"
    if p_g1_Data is None and p_g2_Data is None:
        return {
            'data': [
                go.Scatter( # 该trace是在散点图上面显示text
                    x=df[x_colum],
                    y=df[y_colum],
                    mode='markers+text',
                    textposition='top',
                    text=df.index,
                    marker={
                        'size': 15,  # 大小
                        'opacity': 0.5,  # 不透明度
                        'color': 'rgb(125, 58, 235)' # 颜色
                    },
                    name="Country",# trace名,
                    hoverinfo='all' # hover时显示内容
                )
            ],
            'layout': go.Layout(
                title=p_graph+"_Graph",
                yaxis={
                    'title': "Value",
                },
            ),
        }
    elif not (p_g1_Data is None):
        select_data = p_g1_Data
    elif not (p_g2_Data is None):
        select_data = p_g2_Data
    if select_data:
        select_text = [i['text'] for i in select_data["points"]]
        print(select_text)
        df_select = df.loc[select_text]
        return {
            'data': [
                go.Scatter(  # 该trace是在散点图上面显示text
                    x=df[x_colum],
                    y=df[y_colum],
                    mode='markers',
                    text=df.index,
                    marker={
                        'size': 15,  # 大小
                        'opacity': 0.1,  # 不透明度
                        'color': 'rgb(125, 58, 235)'  # 颜色
                    },
                    name="Country_Not",# trace名,
                    hoverinfo='all',  # hover时显示内容
                ),
                go.Scatter(
                    x=df_select[x_colum],
                    y=df_select[y_colum],
                    mode='marker+text',
                    textposition='top',
                    text=select_text,
                    marker={
                        'size': 15,  # 大小
                        'opacity': 1,  # 不透明度
                        'color': 'rgb(125, 58, 235)'  # 颜色
                    },
                    name="Country_Select",# trace名
                    hoverinfo='all',
                )
            ],
            'layout': go.Layout(
                title=p_graph + "_Graph",
                yaxis={
                    'title': "Value",
                },
                # dragmode='select',
                hovermode='closest',
                # showlegend=False,
            ),
        }

@app.callback(
    Output(component_id='g1', component_property='figure'),
    [
        Input(component_id='g1', component_property='selectedData'),
        Input(component_id='g2', component_property='selectedData'),
    ]
)
def updateG1(p_g1_selectedData, p_g2_selectedData):
    print(type(p_g1_selectedData))
    print("p_g1_selectedData: %s" % str(p_g1_selectedData))
    print("p_g2_selectedData: %s" % str(p_g2_selectedData))
    return create_figure(p_g1_Data=p_g1_selectedData, p_g2_Data=p_g2_selectedData, p_graph='g1')

@app.callback(
    Output(component_id='g2', component_property='figure'),
    [
        Input(component_id='g1', component_property='selectedData'),
        Input(component_id='g2', component_property='selectedData'),
    ]
)
def updateG2(p_g1_selectedData, p_g2_selectedData):
    print("p_g1_selectedData: %s" % str(p_g1_selectedData))
    print("p_g2_selectedData: %s" % str(p_g2_selectedData))
    return create_figure(p_g1_Data=p_g1_selectedData, p_g2_Data=p_g2_selectedData, p_graph='g2')

if __name__ == "__main__":
    app.run_server(debug=True)