# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input

"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:交互式Dash app写法(3): 
             (2个Dropdown 2个RadioItems 1个Slider)-Graph
@Note:
    1、 如果一个操作需要相应多个组件的属性或一个组件的多个属性
        需要写多个回调函数(装饰器@app.callback())来实现，因为一个装饰器只能实现更新一个组件的一个属性
"""

app = dash.Dash()

app.layout = html.Div([
    html.H2(children='单输入多输出'),
    dcc.RadioItems(
        id='id_countries',
        options=[
            {'label': 'America', 'value': 'America'},
            {'label': 'Canada', 'value': 'Canada'}
        ],
        value='America',
    ),
    dcc.RadioItems(
        id='id_cities',
        options=[
            {'label': 'New York City', 'value': 'New York City'},
            {'label': 'San Francisco', 'value': 'San Francisco'},
            {'label': 'Cincinnati', 'value': 'Cincinnati'}
        ],
        value='New York City',
    ),
    html.H3(
        id='id_h3',
        children='New York City is a city in America'
    ),
])

# 选择国家时，更新城市的选择项(options)
@app.callback(
    Output(component_id='id_cities', component_property='options'),
    [Input(component_id='id_countries', component_property='value'),]
)
def updateRadioItems(p_input_value):
    if str(p_input_value) == 'America':
        options = [
            {'label': 'New York City', 'value': 'New York City'},
            {'label': 'San Francisco', 'value': 'San Francisco'},
            {'label': 'Cincinnati', 'value': 'Cincinnati'}
        ]
    if str(p_input_value) == 'Canada':
        options = [
            {'label': 'Montréal', 'value': 'Montréal'},
            {'label': 'Toronto', 'value': 'Toronto'},
            {'label': 'Ottawa', 'value': 'Ottawa'}
        ]
    return options

# 选择国家时，更新城市的默认值(value)
@app.callback(
    Output(component_id='id_cities', component_property='value'),
    [Input(component_id='id_countries', component_property='value'),]
)
def updateRadioItems(p_input_value):
    if str(p_input_value) == 'America':
        return 'New York City'
    if str(p_input_value) == 'Canada':
        return 'Montréal'

# 选择国家或城市时，更新输出内容(children)
@app.callback(
    Output(component_id='id_h3', component_property='children'),
    [
        Input(component_id='id_cities', component_property='value'),
        Input(component_id='id_countries', component_property='value'),
    ]
)
def updateH3(p_cities_value, p_countries_value):
    return ("%s城市在%s") % (p_cities_value, p_countries_value)


if __name__ == "__main__":
    app.run_server(debug=True)