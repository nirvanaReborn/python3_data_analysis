# coding=UTF-8
import dash
import pandas as pd
import dash_html_components as html
import dash_core_components as dcc

"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:使用dash创建table视图
"""
# 读取csv文件数据
df = pd.read_csv("data.csv") # ReturnType <class 'pandas.core.frame.DataFrame'>

def generate_table():
    return html.Table([
        # 表头 Tr标签表示一行内容
        html.Tr([
            html.Th("第一列"),
            html.Th("第二列"),
        ]),
        # 表内容 Tr标签表示一行内容，Td标签表示单元格内容
        html.Tr([
            html.Td("1-1"),
            html.Td("1-2"),
        ]),
        html.Tr([
            html.Td("2-1"),
            html.Td("2-2"),
        ])
    ])

def generate_table_2(dataframe, max_rows=10):
    return html.Div(
        [html.Tr([
            html.Th(col) for col in dataframe.columns
        ])]
        +
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

app = dash.Dash()

app.layout = html.Div([
    html.H4(children="我的第一个Dash表格"),
    generate_table(),
    generate_table_2(dataframe=df)
    ],
    style={'background': 'gray'}
)

if __name__ == "__main__":
    app.run_server(debug=True)



