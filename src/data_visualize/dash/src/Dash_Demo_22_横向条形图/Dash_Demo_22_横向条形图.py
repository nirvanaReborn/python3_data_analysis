# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍 横向条形图
"""

top_labels = ['Strongly<br>agree', 'Agree', 'Neutral', 'Disagree',
              'Strongly<br>disagree']

colors = ['rgba(38, 24, 74, 0.8)', 'rgba(71, 58, 131, 0.8)',
          'rgba(122, 120, 168, 0.8)', 'rgba(164, 163, 204, 0.85)',
          'rgba(190, 192, 213, 1)']

x_data = [[21, 30, 21, 16, 12],
          [24, 31, 19, 15, 11],
          [27, 26, 23, 11, 13],
          [29, 24, 15, 18, 14]]

y_data = ['The course was effectively<br>organized',
          'The course developed my<br>abilities and skills ' +
          'for<br>the subject', 'The course developed ' +
          'my<br>ability to think critically about<br>the subject',
          'I would recommend this<br>course to a friend']

new_x_data = []
for i in range(0, len(x_data[0])):
    new_xd = []
    for xd in x_data:
        new_xd.append(xd[i])
    new_x_data.append(new_xd)

traces = []
for x, color in zip(new_x_data, colors):
    traces.append(
        go.Bar(
            x=x,
            y=y_data,
            orientation='h',
            marker=go.Marker(
                color=color,
                line=go.Line(
                    color='rgb(248, 248, 249)',
                    width=1)),
            # text=list(map(lambda x: str(x) + "%", x)),
            # textposition="inside",
        )
    )

annotations = []

# y轴注释
for y in y_data:
    annotations.append(
        go.Annotation(
            text=str(y),
            font=go.Font(
                family='Arial', size=14,
                color='rgb(67, 67, 67)',),
            xref="paper",
            x=0,
            yref="y",
            y=y,
            xanchor="right",
            showarrow=False,
        )
    )

# x轴注释
x_count = 0
x_value = 0
for x, x_label in zip(x_data[-1], top_labels):
    x_value = x_count + x/2
    x_count = x_count + x
    annotations.append(
        go.Annotation(
            text=x_label,
            font=go.Font(
                family='Arial', size=14,
                color='rgb(67, 67, 67)', ),
            xref="x",
            x=x_value,
            yref="paper",
            y=1,
            yanchor="bottom",
            showarrow=False,
        )
    )

# 条形图单个元素(数据)注释
for xd, y in zip(x_data, y_data):
    x_count = 0
    x_value = 0
    for x in xd:
        x_value = x_count + x / 2
        x_count = x_count + x
        annotations.append(
            go.Annotation(
                text=str(x) + "%",
                font=go.Font(
                    family='Arial', size=14,
                    color='rgb(248, 248, 255)'),
                yref="y",
                y=y,
                xref="x",
                x=x_value,
                showarrow=False,
            )
        )


app = dash.Dash()

app.layout = html.Div([
    html.H2(children="横向条形图"),
    dcc.Graph(
        id="id_graph",
        figure=go.Figure(
            data=go.Data(
                traces
            ),
            layout=go.Layout(
                title="调查问卷意见反馈",
                barmode="stack",
                margin=go.Margin(
                    l=200,
                ),
                showlegend=False,
                xaxis=go.XAxis(
                    visible=False,
                ),
                yaxis=go.YAxis(
                    visible=False,
                ),
                paper_bgcolor='rgb(248, 248, 255)',
                plot_bgcolor='rgb(248, 248, 255)',
                annotations=go.Annotations(annotations)
            ),
        ),
    ),
])

if __name__ == "__main__":
    app.run_server(debug=True, port=8050)