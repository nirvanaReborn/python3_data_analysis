# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html


"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:介绍dash_core_components的核心组件
"""

app = dash.Dash()

app.layout = html.Div([
    html.H1(children='下拉框展示'),
    html.H2(children='下拉框-单选'),
    # 下拉框
    dcc.Dropdown(
        # 下拉框选项
        # label 下拉框中元素类型 value 元素值
        options=[
            {'label': '杭州', 'value': 'HZ'}, # 选项1
            {'label': '郑州', 'value': 'ZZ'}, # 选项2
            {'label': '洛阳', 'value': 'LY'}, # 选项3
        ],
        value='ZZ' # 初始值
    ),
    html.H2(children='下拉框-多选'),
    dcc.Dropdown(
        options=[
            {'label': '杭州', 'value': 'HZ'},  # 选项1
            {'label': '郑州', 'value': 'ZZ'},  # 选项2
            {'label': '洛阳', 'value': 'LY'},  # 选项3
        ],
        value=['HZ', 'ZZ'],
        multi=True # 是否多选
    ),
    html.H1(children='滑动条'),
    html.H2(children='一般滑动条'),
    dcc.Slider(
        min=-5, # 最小值
        max=10, # 最大值
        step=0.5, # 步长
        value=1, # 初始值
        # 标记 key表示位置(value值) value表示显示内容
        marks={i: '{0}'.format(i) for i in range(-5, 10)}
    ),
    html.H2(children='可选范围滑动条'),
    dcc.RangeSlider(
        min=-5,
        max=10,
        step=0.5,
        value=[-2, 5], # 初始值
        # 标记 key表示位置 value表示显示内容
        marks={i: '{0}'.format(i) for i in range(-5, 10)}
    ),
    html.H1(children='输入框'),
    dcc.Input(
        placeholder='请输入...', # 输入框提示内容
        type='text',
        value='',
    ),
    html.H1(children='复选框'),
    dcc.Checklist(
        options=[
            {'label': '杭州', 'value': 'HZ'},  # 选项1
            {'label': '郑州', 'value': 'ZZ'},  # 选项2
            {'label': '洛阳', 'value': 'LY'},  # 选项3
        ],
        values=['HZ', 'LY']
    ),
    dcc.Checklist(
        options=[
            {'label': '杭州', 'value': 'HZ'},  # 选项1
            {'label': '郑州', 'value': 'ZZ'},  # 选项2
            {'label': '洛阳', 'value': 'LY'},  # 选项3
        ],
        values=['HZ', 'LY'],
        labelStyle={'color': 'blue'} # 设置label的CSS
    ),
    html.H1(children='单选框'),
    dcc.RadioItems(
        options=[
            {'label': '杭州', 'value': 'HZ'},  # 选项1
            {'label': '郑州', 'value': 'ZZ'},  # 选项2
            {'label': '洛阳', 'value': 'LY'},  # 选项3
        ],
        value='LY',
        labelStyle={'border-style': 'inset'} # 设置label的CSS
    ),
])

if __name__ == "__main__":
    app.run_server(debug=True)