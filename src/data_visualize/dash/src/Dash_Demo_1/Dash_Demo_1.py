# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_1.py
@Date:2018-01-20
@Description:介绍dash库简单用法，便于初学者上手
"""
# 实例化一个Dash对象，每个dash应用必须步骤
app = dash.Dash()

# 对页面布局
#     整个页面的布局结构为：
#     1.整个页面是个div
#     2.div内设定一个h1型号的标题
#     3.div包含一个子div，内容为一行文本
#     4.最后为我们画的图

# 注意：
# 使用dash_html_components.html对象来构造HTML时
#   创建标签使用html.标签()
#   同级标签之间在一个列表中
#       html.上级标签([
#           html.同级标签(),
#           html.同级标签(),
#       ])
# 创建标签时 属性 children 可不写


app.layout = html.Div(children = [
    html.H1(children = 'Hello Dash'),
    html.Div(children =
        '''
        Dash : '这是我的第一幅Dash图形'
        '''),
    # 画图型
    dcc.Graph(
        # 唯一标识该图形
        id='id_graph1',
        #具体内容
        figure = {
            # data是key，也可以理解为属性。值为字典的列表，即为图形的具体数据
            # 其中x为横轴数据链，y为与x相对应的纵轴数据链
            # type为图形的类型，name为图形的标识
            # 'data': [
            #     {'x': [1, 2, 3], 'y': [10, 15, 20], 'type': 'bar', 'name': '张三'},
            #     {'x': [1, 2, 3], 'y': [20, 23, 25], 'type': 'bar', 'name': '李四'},
            # ],
            'data': [
                go.Bar(
                    x=[1, 2, 3],
                    y=[10, 15, 20],
                    name='张三',
                ),
                go.Bar(
                    x=[1, 2, 3],
                    y=[20, 23, 25],
                    name='李四',
                ),
            ],
            'layout': {
                'title': 'My Graph Title'
            }
        }
    )
])

if __name__ == "__main__":
    # 端口默认为8050
    app.run_server(debug=True)
