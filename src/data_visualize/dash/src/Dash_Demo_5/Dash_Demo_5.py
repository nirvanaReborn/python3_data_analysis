# coding=UTF-8
import dash
import dash_html_components as html
import dash_core_components as dcc

"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:使用dash创建MackDown文本
"""

app = dash.Dash()

# Markdown 语法文本
markdown_text = '''
### Dash and Markdown

Dash apps can be written in Markdown.
Dash uses the [CommonMark](http://commonmark.org/)
specification of Markdown.
Check out their [60 Second Markdown Tutorial](http://commonmark.org/help/)
if this is your first introduction to Markdown!
'''

app.layout = html.Div([
    dcc.Markdown(children=markdown_text),
])

if __name__ == "__main__":
    app.run_server(debug=True)