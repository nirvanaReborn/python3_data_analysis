# coding=UTF-8
import dash
import pandas
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input
import plotly.graph_objs as go


"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:交互式Dash app写法(3): 
             多个输入(2个Dropdown 2个RadioItems 1个Slider)
             一个输出(Graph)
"""

app = dash.Dash()

df = pandas.read_csv("data.csv")

app.layout = html.Div([
    html.H2(
        children='2个下拉框、2个单选框、1个滑动条',
    ),
    html.Div([
        html.Div([
            dcc.Dropdown(
                id='xaxis-column',
                options=[{'label': i, 'value': i} for i in df['Indicator Name'].unique()],
                value=df['Indicator Name'].unique()[0],
            ),
            dcc.RadioItems(
                id='xaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                # labelStyle={'display': 'inline-block'}
            ),
        ]),
        html.Div([
            dcc.Dropdown(
                id='yaxis-column',
                options=[{'label': i, 'value': i} for i in df['Indicator Name'].unique()],
                value=df['Indicator Name'].unique()[0],
            ),
            dcc.RadioItems(
                id='yaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                # labelStyle={'display': 'inline-block'}
            ),
        ]),
    ]),
    dcc.Graph(
        id='indicator-graphic',
    ),
    dcc.Slider(
        id='year-slider',
        step=None,
        min=df['Year'].min(),
        max=df['Year'].max(),
        value=df['Year'].min(),
        marks={str(year): str(year) for year in df['Year'].unique()},
    ),
])

@app.callback(
    Output(component_id='indicator-graphic', component_property='figure'),
    [
        Input(component_id='xaxis-column', component_property='value'),
        Input(component_id='xaxis-type', component_property='value'),
        Input(component_id='yaxis-column', component_property='value'),
        Input(component_id='yaxis-type', component_property='value'),
        Input(component_id='year-slider', component_property='value'),
    ]
)
def updateGraph(x_value, x_type, y_value, y_type, slider_value):
    df_year = df[df['Year'] == int(slider_value)]
    traces = []
    for Country_Name in df_year['Country Name'].unique():
        df_year_country = df_year[df_year['Country Name'] == Country_Name]
        traces.append(
            go.Scatter(
                x=df_year_country[df_year_country['Indicator Name'] == x_value]['Value'],  # x,y,text 是一个集合
                y=df_year_country[df_year_country['Indicator Name'] == y_value]['Value'],  # x,y,text 是一个集合
                text=df_year_country['Country Name'],
                mode='markers',
                marker={
                    'size': 15,
                    'opacity': 0.5,
                    'line': {'width': 0.5, 'color': 'white'}
                },
                name=Country_Name
            )
        )


    figure = {
        'data': traces,
        'layout': go.Layout(
            xaxis={
                'title': x_value,
                'type': 'linear' if x_type == 'Linear' else 'log'
            },
            yaxis={
                'title': y_value,
                'type': 'linear' if y_type == 'Linear' else 'log'
            },
            margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
            hovermode='closest',
        )
    }
    return figure


if __name__ == "__main__":
    app.run_server(debug=True)