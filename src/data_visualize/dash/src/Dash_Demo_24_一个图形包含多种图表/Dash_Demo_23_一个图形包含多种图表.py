# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍 一个图形包含多种图表
@Note：其实就是在traces中添加多个不同类型的trace
"""
app = dash.Dash()

app.layout = html.Div([
    html.H2("一个图形包含多种图表"),
    dcc.Graph(
        id="id_graph",
        figure=go.Figure(
            data=go.Data([
                go.Scatter(
                    x=[0, 1, 2, 3, 4, 5],
                    y=[1.5, 1, 1.3, 0.7, 0.8, 0.9],
                ),
                go.Bar(
                    x=[0, 1, 2, 3, 4, 5],
                    y=[1, 0.5, 0.7, -1.2, 0.3, 0.4],
                ),
            ]),
            layout=go.Layout(
            ),
        ),
    ),
])

if __name__ == "__main__":
    app.run_server(debug=True, port=8050)