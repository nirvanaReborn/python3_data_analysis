# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍 点图 dot plots
"""
app = dash.Dash()

app.layout = html.Div([
    html.H2(children="点图"),
    dcc.Graph(
        id="id_graph",
        figure=go.Figure(
            data=go.Data([
                go.Scatter(
                    x=[72, 67, 73, 80, 76, 79, 84, 78, 86, 93, 94, 90, 92, 96, 94, 112],
                    y=["Brown", "NYU", "Notre Dame", "Cornell", "Tufts", "Yale",
                       "Dartmouth", "Chicago", "Columbia", "Duke", "Georgetown",
                       "Princeton", "U.Penn", "Stanford", "MIT", "Harvard"],
                    name="Women",
                    mode="markers",
                    marker=go.Marker(
                        color='pink',
                        size=12,
                    ),
                ),
                go.Scatter(
                    mode="markers",
                    name="Man",
                    x=[92, 94, 100, 107, 112, 114, 114, 118, 119, 124, 131, 137, 141, 151, 152, 165],
                    y=["Brown", "NYU", "Notre Dame", "Cornell", "Tufts", "Yale",
                        "Dartmouth", "Chicago", "Columbia", "Duke", "Georgetown",
                        "Princeton", "U.Penn", "Stanford", "MIT", "Harvard"],
                    marker=go.Marker(
                        color="blue",
                        size=12,
                    ),
                ),
            ]),
            layout=go.Layout(
                title="Gender Earnings Disparity",
                xaxis=go.XAxis(
                    title="Annual Salary(in thousands)",
                    showgrid=False,
                    showline=True,
                    linecolor='rgb(102, 102, 102)',
                    titlefont=dict(
                        color='rgb(204, 204, 204)'
                    ),
                    tickfont=dict(
                        color='rgb(102, 102, 102)',
                    ),
                    autotick=False,
                    ticks='outside',
                    dtick=10,
                    tickcolor='rgb(102, 102, 102)',
                ),
                yaxis=go.YAxis(
                    title="School",
                ),
                margin=dict(
                        l=140,
                        r=40,
                        b=50,
                        t=80
                ),
                legend=dict(
                    font=dict(
                        size=10,
                    ),
                    yanchor='middle',
                    xanchor='right',
                ),
                width=1000,
                height=600,
                paper_bgcolor='rgb(254, 247, 234)',
                plot_bgcolor='rgb(254, 247, 234)',
                hovermode='closest',
            ),
        ),
    ),
])

if __name__ == "__main__":
    app.run_server(debug=True, port=8050)