# coding=UTF-8
import dash
from textwrap import dedent as d # ???
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍dcc.Graph 四个属性hoverData, clickData, selectedData, relayoutData
              这些属性可以作为交互的参数(Input)来调用
"""

app = dash.Dash()
styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}

app.layout = html.Div([
    html.H2(
        children='介绍dcc.Graph中四个属性hoverData, clickData, selectedData, relayoutData'
    ),
    dcc.Graph(
        id='basic-interactions',
        figure={
            'data': [
                go.Scatter(
                    x=[1, 2, 3, 4],
                    y=[4, 1, 3, 5],
                    text=['a', 'b', 'c', 'd'],
                    name='Trace 1',
                    mode='markers', # 这里的mode参数默认是由点连接起来的折线图，我们需要绘制散点图，所以将其设置为'markers'
                    marker={        # 定义每个点的性质
                        'size': 12, # 点的大小
                    }
                ),
                go.Scatter(
                    x=[1, 2, 3, 4],
                    y=[9, 4, 1, 4],
                    text=['w', 'x', 'y', 'z'],
                    name='Trace 2',
                    mode='markers',
                    marker={        # 定义每个点的性质
                        'size': 12, # 点的大小
                    }
                ),
            ]
        }
    ),
    # html.Div(className='row', children=[
    #     html.Div([
    #         dcc.Markdown(d("""
    #            **Hover Data**
    #
    #            Mouse over values in the graph.
    #        """)),
    #         html.Pre(id='hover-data', style=styles['pre'])
    #     ], className='three columns'),
    #
    #     html.Div([
    #         dcc.Markdown(d("""
    #            **Click Data**
    #
    #            Click on points in the graph.
    #        """)),
    #         html.Pre(id='click-data', style=styles['pre']),
    #     ], className='three columns'),
    #
    #     html.Div([
    #         dcc.Markdown(d("""
    #            **Selection Data**
    #
    #            Choose the lasso or rectangle tool in the graph's menu
    #            bar and then select points in the graph.
    #        """)),
    #         html.Pre(id='selected-data', style=styles['pre']),
    #     ], className='three columns'),
    #
    #     html.Div([
    #         dcc.Markdown(d("""
    #            **Zoom and Relayout Data**
    #
    #            Click and drag on the graph to zoom or click on the zoom
    #            buttons in the graph's menu bar.
    #            Clicking on legend items will also fire
    #            this event.
    #        """)),
    #         html.Pre(id='relayout-data', style=styles['pre']),
    #     ], className='three columns')
    # ])
    html.H3(children='hoverData效果演示'),
    html.H5(id='hover-data', children=' '),
    html.H3(children='clickData效果演示'),
    html.H5(id='click-data', children=' '),
    html.H3(children='selectedData效果演示'),
    html.H5(id='selected-data', children=' '),
    html.H3(children='relayoutData效果演示'),
    html.H5(id='relayout-data', children=' '),
])

@app.callback(
    Output(component_id='hover-data', component_property='children'),
    [
        Input(component_id='basic-interactions', component_property='hoverData'),
    ]
)
def display_hover_data(p_hoverData):
    print("hover后响应"+str(p_hoverData))
    return "hover后响应"+str(p_hoverData)

@app.callback(
    Output(component_id='click-data', component_property='children'),
    [
        Input(component_id='basic-interactions', component_property='clickData'),
    ]
)
def display_click_data(p_clickData):
    print("click后响应"+str(p_clickData))
    return "click后响应"+str(p_clickData)

# 使用Box Select或Lasso Select
@app.callback(
    Output(component_id='selected-data', component_property='children'),
    [
        Input(component_id='basic-interactions', component_property='selectedData'),
    ]
)
def display_click_data(p_selectedData):
    print("selected后响应"+str(p_selectedData))
    return "selected后响应"+str(p_selectedData)

# 使用鼠标划范围时触发
# 应答的数据为范围坐标(对角坐标(x0, y0)(x1, y1))
@app.callback(
    Output(component_id='relayout-data', component_property='children'),
    [
        Input(component_id='basic-interactions', component_property='relayoutData'),
    ]
)
def display_click_data(p_relayoutData):
    print("relayout后响应"+str(p_relayoutData))
    return "relayout后响应"+str(p_relayoutData)


if __name__ == "__main__":
    app.run_server(debug=True)