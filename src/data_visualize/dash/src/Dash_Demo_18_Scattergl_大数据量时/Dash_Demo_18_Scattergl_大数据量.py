# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍WebGL
@Note: 如果是大数据量时建议使用go.Scattergl绘图，加载图形和交互时性能提升非常明显。
       属性使用和go.Scatter几乎一样
"""
N=100000 # 生成10W个点 # 数据太多可能造成内存不足
point_num=5000
trace_num=10
traces=[]
for i in range(trace_num):
    traces.append(
        go.Scattergl(
            x=numpy.linspace(0, 1, point_num), # 生成范围在0-1 point_num 个的等差数列 type list
            y=numpy.random.randn(point_num) + 10*i, # 生成一维N个正态分布的数据 type list
            mode='lines',
        )
    )


app = dash.Dash()

app.layout = html.Div([
    html.H2(children="介绍WebGL"),
    dcc.Graph(
        id="id_graph",
        figure=go.Figure(
            data=go.Data([
                go.Scattergl(
                    x=numpy.random.randn(N), # 生成一维N个正态分布的数据 type list
                    y=numpy.random.randn(N), # 生成一维N个正态分布的数据 type list
                    mode='markers',
                    marker=go.Marker(
                        line=go.Line(
                            color="black",
                            width=1,
                        )
                    ),
                ),
            ]),
        ),
    ),
    dcc.Graph(
        id="id_graph_2",
        figure=go.Figure(
            data=traces,
        ),
    ),
])



if __name__ == "__main__":
    app.run_server(debug=True)