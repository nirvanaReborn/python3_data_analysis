# coding=UTF-8
import dash
import dash_html_components as html
import dash_core_components as dcc
import plotly.graph_objs as go

"""
@Author: zhangzheng
@File: Dash_Demo_2.py
@Date:2018-01-20
@Description:在dash中使用css对应用进行美化
"""

app = dash.Dash()
# css 属性字典
color = {'background': '#111111', 'text': '#7FDBFF'}

app.layout = html.Div(style={'background': color['background']}, children=[
    html.H1(children='Hello Dash',
            style={
                'textAlign': 'center',
                'color': color['text']
            }),
    html.Div(children='这是我的第一幅Dash图形',
             style={
                 'textAlign': 'center',
                 'color': color['text']
             }),
    dcc.Graph(
        id = 'id_graph1',
        figure = {
            # 'data': [
            #     {'x': [1, 2, 3], 'y': [10, 15, 20], 'type': 'bar', 'name': '张三'},
            #     {'x': [1, 2, 3], 'y': [20, 23, 25], 'type': 'bar', 'name': '李四'},
            # ],
            'data': [
                go.Bar(
                    x=[1, 2, 3],
                    y=[10, 15, 20],
                    name='张三',
                ),
                go.Bar(
                    x=[1, 2, 3],
                    y=[20, 23, 25],
                    name='李四',
                ),
            ],
            'layout': {
                'title': 'My Graph Title',
                # plot_bgcolor Graph中图形主体的背景颜色
                'plot_bgcolor': color['background'],
                # paper_bgcolor Graph中非图形主体的背景颜色
                'paper_bgcolor': color['background'],
                'font': {
                    'color': color['text']
                }

            }
        }
    )
])
# 附加css样式 不清楚干嘛
# app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"})
if __name__ == "__main__":
    app.run_server(debug=True)
