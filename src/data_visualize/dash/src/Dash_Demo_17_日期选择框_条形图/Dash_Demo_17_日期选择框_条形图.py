# coding=UTF-8
import dash
import pandas
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 详细介绍Dash 日期选择框、条形图

String Token	Example	            Description
YYYY            2014                4 or 2 digit year
YY              14                  2 digit year
Y               -25                 Year with any number of digits and sign
Q               1..4                Quarter of year. Sets month to first month in quarter.
M MM            1..12               Month number
MMM MMMM        Jan..December       Month name
D DD            1..31               Day of month
Do              1st..31st           Day of month with ordinal
DDD DDDD        1..365              Day of year
X               1410715640.579      Unix timestamp
x               1410715640579       Unix ms timestamp
"""

df_sh = pandas.read_csv("sh.csv")
df_sz = pandas.read_csv("sz.csv")
df_hs300 = pandas.read_csv("hs300.csv")
df_sz50 = pandas.read_csv("sz50.csv")
df_zxb = pandas.read_csv("zxb.csv")
df_cyb = pandas.read_csv("cyb.csv")

app = dash.Dash()

app.layout = html.Div([
    html.H2(children="显示所选日期的各指数交易量"),
    dcc.Graph(
        id="id_graph",
    ),
    html.H4(children="选择日期..."),
    dcc.DatePickerRange(
        id="id_datepickerange",
        min_date_allowed="2015-06-26",                  # 日历可选最小日期 格式 'YYYY-MM-DD'
        max_date_allowed="2018-02-03",                  # 日历可选最大日期 格式 'YYYY-MM-DD'
        initial_visible_month="2018-01-01",             # 打开日历窗口默认显示月份 格式 'YYYY-MM-DD' 必须设置？？？
        start_date="2018-01-01",                        # 起始日期 格式 'YYYY-MM-DD'
        end_date="2018-01-10",                          # 结束日期 格式 'YYYY-MM-DD'
        display_format="YYYY/MM/DD",                    # 显示格式
        start_date_placeholder_text="选择开始日期...",   # 开始日期选择框placeholder
        end_date_placeholder_text="选择结束日期...",     # 结束日期选择框placeholder
        calendar_orientation='horizontal',              # 日期选择窗口的水平或垂直排列方式 'vertical' | 'horizontal'
        with_portal=True,                               # 弹出日期选择窗口 如果为True 则calendar_orientation应选择为'horizontal'
        with_full_screen_portal=False,                  # 弹出日期选择窗口全屏显示 默认False
        minimum_nights=2,                               # 指定日期之间最少的夜晚数 默认1
        clearable=True,                                 # 是否在日期选择框右侧出现一个小“x”，清楚所选日期。默认为 False
        is_RTL=False,                                   # 确定日期是从左到右还是从右到左操作 默认为 False 从左到右
        first_day_of_week=1,                            # 指定哪一天是一周中的第一天，值必须是[0，...，6]，其中0代表星期天，6代表星期六 默认0
        day_size=40,                                    # 日期选择框的大小 默认39
        disabled=False,                                 # 是否不可用  默认 False
        month_format="YYYY年MM月",                      # 打开日历窗口中显示年月的格式
        reopen_calendar_on_clear=True,                  # 如果为True，日历将在清除时自动打开 默认 False
        # setProps ？？？
        show_outside_days=False,                        # 如果为真，日历将显示其他月份的日子 默认 False
        stay_open_on_select=False,                      # 如果为True，则当用户选择一个值时，日历将不会关闭，并将等待直到用户单击关闭日历 默认 False
    ),
])

@app.callback(
    Output(component_id='id_graph', component_property='figure'),
    [
        Input(component_id='id_datepickerange', component_property='start_date'),
        Input(component_id='id_datepickerange', component_property='end_date'),
    ]
)
def updateGraph(p_start_date, p_end_date):
    print("p_start_date:", p_start_date)
    print("p_end_date:", p_end_date)

    if p_start_date and p_end_date:
        df_sh_startDate=df_sh[p_start_date <= df_sh["date"]]
        df_sh_selectDate=df_sh_startDate[df_sh_startDate["date"] <= p_end_date]
        df_sz_startDate = df_sz[p_start_date <= df_sz["date"]]
        df_sz_selectDate = df_sz_startDate[df_sz_startDate["date"] <= p_end_date]
        return go.Figure(
            data=go.Data([
                go.Bar(
                    visible=True,                           # 是否显示该trace
                    x=df_sh_selectDate["date"],
                    y=df_sh_selectDate["volume"],
                    name="上证指数",
                    text="text",                            # 如果hovertext未设置，text显示在hoverlabel中
                    textposition="outside",                 # ( enumerated or array of enumerateds : "inside" | "outside" | "auto" | "none" ) default: "none"
                    hovertext="hovertext",                  # hover时 hoverlable显示的数据
                    hoverinfo="all",                        # Any combination of "x", "y", "z", "text", "name" joined with a "+" OR "all" or "none" or "skip". examples: "x", "y", "x+y", "x+y+z", "all"
                    legendgroup="a",                        # 不同trace设置相同的legendgroup后，点击legend中的元素会一起隐藏或显示
                    opacity=1,
                    # selectedpoints=[1,2],                 # 报错没有该属性，但官方文档有该属性的说明 https://plot.ly/python/reference/#bar-selectedpoints
                    hoverlabel=dict(                        # hoverlabel样式
                        bgcolor='yellow',
                        bordercolor='green',                # hoverlabel边框颜色
                        # font=dict(
                        #    color="",                      # 默认以bordercolor边框颜色显示
                        #    family="",                     # 字体
                        #    size=1,                        # 字号
                        # ),
                        namelength=5,                       # 显示trace名的长度 设置此跟踪悬停标签中跟踪名称的长度（字符数）。 -1显示整个名称，不管长度。 0-3表示第一个0-3个字符，如果小于多个字符，整数> 3将显示整个名称，但如果长度过长，将截断为“namelength-3”字符并添加省略号。
                    ),
                    # stream=dict(token="", maxpoints=1) ??? 待研究
                    # textfont=dict(family="", size="", color=""), # text字体
                    # insidetextfont=(family="", size="", color=""),
                    # outsidetextfont=(family="", size="", color=""),
                    constraintext="both",                   # 限制条形内部或外部文本的大小不能大于条形本身。default: "both" ( enumerated : "inside" | "outside" | "both" | "none" )
                    orientation="v",                        # 设置条形的方向 default "v"
                    # base=5,??? 待研究
                    # offset=5, ??? 待研究
                    # selected=dict()                       # bar无此用法，但官方文档有该属性的说明 https://plot.ly/python/reference/#bar-selected
                    # unselected=dict()                     # bar无此用法，但官方文档有该属性的说明  https://plot.ly/python/reference/#bar-unselected
                ),
                go.Bar(
                    visible=True,
                    x=df_sz_selectDate["date"],
                    y=df_sz_selectDate["volume"],
                    name="深圳成指",
                    marker=go.Marker(                       # 条形样式
                        color='rgb(158,202,225)',           # 可以使用列表来表示每个条形的颜色 [rgb(), rgb(), rgb()]
                        line=go.Line(
                            width=1,
                            color='rgb(8,48,107)'
                        ),
                        # colorbar="",
                    ),
                    opacity=0.6,                            # 条形不透明度 默认1
                    # width=[1, 2, 3, 1, 1]                 # 设置条形宽度
                    legendgroup="b",
                    orientation="v",                        # 条形展示方式 默认"v"垂直，"h"-水平
                ),
            ]),
            layout=go.Layout(
                barmode="group",                            # traces在条形图展示方式
                xaxis=go.XAxis(
                    tickangle=-45,                          # ticklabel的旋转角度
                ),
                legend=dict(                                # legend图例样式设置
                    x=0,                                    # 图例x位置
                    y=1.2,                                  # 图例y位置
                    bgcolor='red',                          # 背景色 默认rgb(255, 255, 255)
                    bordercolor='green',                    # 边框色
                    borderwidth=3,                          # 边框宽度 默认0
                    orientation='v',                        # legend元素(trace名)排列方式 默认'v'垂直
                    traceorder="reversed",                  # legend元素(trace名)排序方式
                    # tracegroupgap=10,
                ),
                bargap=0.4,
                bargroupgap=0.1,
            ),
        )



if __name__ == "__main__":
    app.run_server(debug=True)