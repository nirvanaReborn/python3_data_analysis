# coding=UTF-8
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:交互式Dash app写法(2): 
             一个输入(Slider)
             一个输出(Graph)
"""

app = dash.Dash()
df = pandas.read_csv("data.csv")

app.layout = html.Div([
    html.H2(children='滑动条-散点图'),
    dcc.Graph(
        id='graph-with-slider',
        animate=True, # If true, animate between updates using plotly.js's `animate` function
    ),
    dcc.Slider(
        id='year_slider',
        min=df['year'].min(),
        max=df['year'].max(),
        value=df['year'].min(),
        marks={str(year): str(year)+'年' for year in df['year'].unique()},
        updatemode='drag', # value值更新方式 'mouseup', 'drag'
        #step=None, # ??? 如果是None则只能选择滑动条的位置点
    ),
    html.H4(children='', id='id_h4'),
])

@app.callback(
    Output(component_id='id_h4', component_property='children'),
    [Input(component_id='year_slider', component_property='value')],
)
def sliderUpdateDiv(p_input_value):
    return '您当前选则的年份是%s' % str(p_input_value)

@app.callback(
    Output(component_id='graph-with-slider', component_property='figure'),
    [Input(component_id='year_slider', component_property='value')],
)
def sliderUpdateGraph(Input_Vaule):
    year_df = df[df.year == Input_Vaule]
    traces = []
    for i in year_df.continent.unique():
        continent_year_df = year_df[year_df['continent'] == i]
        traces.append(
            go.Scatter(
                x=continent_year_df['gdpPercap'],
                y=continent_year_df['lifeExp'],
                text = continent_year_df['country'],
                mode='markers', # ???
                opacity=0.7, # ???
                marker={
                    'size': 15,
                    'line': {'width': 0.5, 'color': 'white'}  # ???
                },
                name=i  # ???
            )
        )
    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'type': 'log', 'title': 'GDP Per Capita'},
            yaxis={'title': 'Life Expectancy', 'range': [20, 90]},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            legend={'x': 0, 'y': 1},
            hovermode='closest')
    }

if __name__ == "__main__":
    app.run_server(debug=True)
