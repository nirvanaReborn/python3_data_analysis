# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from plotly import tools
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍 在一个绘图区中绘制多个图形
@Note: 
    1.重点是通过tools.make_subplots() 方法构造一个多图形的绘图区域 fig
    2.fig.append_trace(trace0, 1, 1) 方法添加数据(figure.data)
    3.fig['layout'].update(layout) 设置布局
        在设置布局时注意每个图形都要规定其坐标范围xaxis1 xaxis2 yaxis1 yaxis2 的 domain 属性
"""

y_saving = [1.3586, 2.2623000000000002, 4.9821999999999997, 6.5096999999999996,
            7.4812000000000003, 7.5133000000000001, 15.2148, 17.520499999999998]
y_net_worth = [93453.919999999998, 81666.570000000007, 69889.619999999995,
               78381.529999999999, 141395.29999999999, 92969.020000000004,
               66090.179999999993, 122379.3]
x_saving = ['Japan', 'United Kingdom', 'Canada', 'Netherlands',
            'United States', 'Belgium', 'Sweden', 'Switzerland']
x_net_worth = ['Japan', 'United Kingdom', 'Canada', 'Netherlands',
               'United States', 'Belgium', 'Sweden', 'Switzerland']

# 图1 条形图
trace0 = go.Bar(
        name="Household savings, percentage of household disposable income",
        x=y_saving,
        y=x_saving,
        orientation='h',
)
# 图2 折线图
trace1 = go.Scatter(
    name="Household net worth, Million USD/capita",
    mode="lines+markers",
    x=y_net_worth,
    y=x_net_worth,
    text="text",
    textposition="middle left",
    hovertext="hovertext",
)

annotations = []

for x, y in zip(y_net_worth, x_net_worth):
    annotations.append(go.Annotation(
        xref="x2",
        # xref="paper",
        yref="y2",
        x=x-20000,
        # x=0.5,
        y=y,
        text='{:,}'.format(x) + 'M',
        font=dict(family='Arial', size=12,
                  color='rgb(50, 171, 96)'),
        showarrow=False,
    ))

for x, y in zip(y_saving, x_saving):
    annotations.append(go.Annotation(
        xref="x1",
        yref="y1",
        x=x,
        y=y,
        text=str(x) + "%",
        font=dict(family='Arial', size=12,
                  color='rgb(50, 171, 96)'),
        showarrow=False,
        xanchor="left",
    ))

layout = go.Layout(
            title="Household savings & net worth for eight OECD countries",
            xaxis1=go.XAxis(
                domain=[0, 0.49],
            ),
            yaxis1=go.YAxis(
                domain=[0, 0.85],
            ),
            xaxis2=go.XAxis(
                domain=[0.51, 1],
                side="top",
            ),
            yaxis2=go.YAxis(
                domain=[0, 0.85],
                visible=True,
                showticklabels=False,
                showgrid=False,
            ),
            paper_bgcolor='rgb(248, 248, 255)',
            plot_bgcolor='rgb(248, 248, 255)',
            annotations=annotations
        )
# tools.make_subplots 返回一个plotly.graph_objs.Figure实例
fig = tools.make_subplots(rows=1, cols=2, specs=[[{}, {}]], shared_xaxes=True, shared_yaxes=False, vertical_spacing=0.001)
fig.append_trace(trace0, 1, 1)
fig.append_trace(trace1, 1, 2)
fig['layout'].update(layout)


app = dash.Dash()

app.layout = html.Div([
    html.H2("一个绘图区中绘制多个图形"),
    dcc.Graph(
        id="id_graph",
        figure=fig
    )
])

if __name__ == "__main__":
    app.run_server(debug=True, port=8050)