# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input, State
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍dash.dependencies的State类
              两个输入框，一个按钮
              当点击按钮时，打印两个输入框的值。
@Note: Dash通过监听组件的属性来触发回调函数
"""
app = dash.Dash()

app.layout = html.Div([
    html.H2(children="介绍dash.dependencies的State类使用，点击按钮时打印两个输入框的值"),
    dcc.Input(id='id_input1', type='text', value='杭州'),
    dcc.Input(id='id_input2', type='text', value='郑州'),
    html.Button(id='id_button', n_clicks=0, children='Submit'),
    html.Div(id='id_output'),
])

@app.callback(
    Output(component_id='id_output', component_property='children'),
    [Input(component_id='id_button', component_property='n_clicks'),],
    [
        State(component_id='id_input1', component_property='value'),
        State(component_id='id_input2', component_property='value'),
    ],
)
def updateDiv(p_clicks, p_input1_value, p_input2_value):
    return ("按钮已点击%s次，Input1为%s，Input2为%s。") % (p_clicks, p_input1_value, p_input2_value)

if __name__ == "__main__":
    app.run_server(debug=True)