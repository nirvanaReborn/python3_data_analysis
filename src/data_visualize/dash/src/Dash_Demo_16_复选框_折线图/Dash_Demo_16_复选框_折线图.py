# coding=UTF-8
import dash
import pandas
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 详细介绍Dash 复选框、折线图使用
"""
df_sh = pandas.read_csv("sh.csv")
df_sz = pandas.read_csv("sz.csv")
df_hs300 = pandas.read_csv("hs300.csv")
df_sz50 = pandas.read_csv("sz50.csv")
df_zxb = pandas.read_csv("zxb.csv")
df_cyb = pandas.read_csv("cyb.csv")

sh_line_style = {
    'color': '#AAB8A3',
    'dash': 'dot',
    'shape': 'linear',
}
sz_line_style = {
    'color': '#EBEDF4',
    'dash': 'solid',
    'shape': 'spline',
}
hs300_line_style = {
    'color': '#FF5983',
    'dash': 'dash',
    'shape': 'hv',
}
sz50_line_style = {
    'color': '#C11941',
    'dash': 'longdash',
    'shape': 'vh',
}
zxb_line_style = {
    'color': '#7A023C',
    'dash': 'dashdot',
    'shape': 'hvh',
}
cyb_line_style = {
    'color': '#5ED5D1',
    'dash': '5px,10px,2px,2px',
    'shape': 'vhv',
}

app = dash.Dash()

app.layout = html.Div([
    html.H2(children="各个指数板块成交量近3年变化情况"),
    dcc.Checklist( # Checklist和RadioItems用法基本相同,有点区别是单选框RadioItems的默认值属性为value type str
        id='id_radio',
        options=[
            {'label': '上证指数', 'value': 'sh', 'disabled ': False}, # disabled 该选项是否不可选
            {'label': '深圳成指', 'value': 'sz', 'disabled ': False},
            {'label': '沪深300指数', 'value': 'hs300', 'disabled ': False},
            {'label': '上证50', 'value': 'sz50', 'disabled ': False},
            {'label': '中小板', 'value': 'zxb', 'disabled ': False},
            {'label': '创业板', 'value': 'cyb', 'disabled ': False},
        ],
        values=['sh', 'sz'] # 默认值 type list
    ),
    dcc.Graph(
        id='id_graph',
    ),
])


@app.callback(
    Output(component_id="id_graph", component_property="figure"),
    [
        Input(component_id="id_radio", component_property="values")
    ]
)
def updateGraph(p_radio_value):
    if p_radio_value:
        traces = []
        if 'sh' in p_radio_value:
            traces.append(creatData(p_df=df_sh, p_name="上证指数", p_line=sh_line_style))
        if 'sz' in p_radio_value:
            traces.append(creatData(p_df=df_sz, p_name="深圳成指", p_line=sz_line_style))
        if 'hs300' in p_radio_value:
            traces.append(creatData(p_df=df_hs300, p_name="沪深300指数", p_line=hs300_line_style))
        if 'sz50' in p_radio_value:
            traces.append(creatData(p_df=df_sz50, p_name="上证50", p_line=sz50_line_style))
        if 'zxb' in p_radio_value:
            traces.append(creatData(p_df=df_zxb, p_name="中小板", p_line=zxb_line_style))
        if 'cyb' in p_radio_value:
            traces.append(creatData(p_df=df_cyb, p_name="创业板", p_line=cyb_line_style))
        return {
            'data': traces,
            'layout': go.Layout(
                xaxis=go.XAxis(
                    visible=True,                   # 是否可见轴
                    title="日期",                   # x标题
                    color="black",                  # 字体颜色
                    linecolor='rgb(204, 204, 204)', # 轴线条颜色
                    linewidth=2,                    # 轴线条宽度
                    showline=True,                  # 展示轴线条
                    showgrid=False,                 # 展示网格
                    showticklabels=True,            # 展示轴上的值(tick)
                    ticks='outside',                # 展示ticks的方式
                    tickcolor='rgb(204, 204, 204)', # tick的颜色
                    tickwidth=2,                    # tick的宽度
                    ticklen=5,                      # tick的长度
                    tickfont=dict(                  # 展示轴上的字体样式
                        family='Arial',
                        size=15,
                        color='rgb(82, 82, 82)',
                    ),
                    type='date',                    # 设置轴类型 举例: "-" | "linear" | "log" | "date" | "category"
                ),
                yaxis=go.YAxis(
                    visible=True,   # 是否可见轴
                    title="成交量", # y标题
                    type='linear',  # 设置轴类型 举例: "-" | "linear" | "log" | "date" | "category"
                    showline=False, # 展示轴线
                    zeroline=False, # 展示0线
                    showgrid=False, # 展示网格
                    showticklabels=False, # 展示轴上的值(tick)
                ),
                title="指数折线图",
                autosize=False,         # 自动调整图片比例使其铺满
                width=1300,             # 图片 宽
                height=500,             # 图片 高
                margin=dict(            # 外边距
                    autoexpand=False,
                    l=150,
                    r=0,
                    b=80,
                    t=110,
                    pad=0,
                ),
                showlegend=True,                # 是否标识所有trace
                annotations=[ # 注释
                    go.Annotation(
                        visible=True,           # 可见
                        xref='paper',           # 确定x轴位置方式
                        x=0.00,                 # 注释的x轴位置
                        y=565217874,            # 注释的y轴位置
                        text='上证指数 565217874',         # 注释文本
                        xanchor='right',        # 注释锚(anchor)水平方向
                        yanchor='middle',       # 注释锚(anchor)垂直方向
                        font=dict(family='Arial', # 字体
                                  size=12,
                                  color="rgba(67,67,67,1)",),
                        showarrow=False,),      # 显示箭头
                    go.Annotation(
                        visible=True,
                        xref='paper',
                        x=0.00,
                        y=294665860,
                        text='深圳成指 294665860',
                        xanchor='right',
                        yanchor='middle',
                        font=dict(family='Arial',
                                  size=12,
                                  color="rgba(67,67,67,1)", ),
                        showarrow=False,),
                    go.Annotation(
                        visible=True,
                        xref='paper',
                        yref='paper',
                        x=0.00,
                        y=1.1,
                        text='指数折线图',
                        xanchor='center',
                        yanchor='middle',
                        font=dict(family='Arial',
                                  size=30,
                                  color='rgb(37,37,37)'),
                        showarrow=False,),
                    go.Annotation(
                        visible=True,
                        xref='paper',
                        yref='paper',
                        x=0.5,
                        y=-0.2,
                        text='Source: Design By <b>zhangzheng</b>',
                        xanchor='center',
                        yanchor='top',
                        font=dict(family='Arial',
                                  size=12,
                                  color='rgb(150,150,150)'),
                        showarrow=False, ),
                ],
            )
        }

def creatData(p_df, p_name, p_line):
    return go.Scatter(                  # 默认为折线图
        mode="lines",                   # 如果少于20个点 default "lines+markers" 折线+点 否则 default "lines" 折线图。Any combination of "lines", "markers", "text" joined with a "+" OR "none". examples: "lines" 折线图, "markers" 散点图, "lines+markers" 折线+点, "lines+markers+text", "none"
        x=p_df["date"],
        y=p_df["volume"],
        name=p_name,                    # trace名
        line=go.Line(                   # 设置折线的样式
            color=p_line["color"],      # 颜色 type str-color default plotly自动配色(不丑)
            width=3,                    # 宽度(px) type number default 2
            dash=p_line["dash"],        # 设置线条虚线形状 例子："solid", "dot", "dash", "longdash", "dashdot", or "longdashdot"。还可以使用 "5px,10px,2px,2px"来自定义线条形状
            shape=p_line["shape"],      # 设置线条的波浪风格 有曲线spline 折线linear 矩形线hv
        ),
        connectgaps=True,               # 如果一点某坐标值为None，折线在此中断
        showlegend=True,                # 是否标识trace(默认显示在图例的右侧)
        opacity=1,                      # 不透明度
        hoverinfo="all",                # Any combination of "x", "y", "z", "text", "name" joined with a "+" OR "all" or "none" or "skip". examples: "x", "y", "x+y", "x+y+z", "all" default: "all"
        hovertext='hovertext',          # hover时label中显示的内容
        # hoverlabel=dict(              # hoverlabel样式
        #     bgcolor='yellow',
        #     bordercolor='green',      # hoverlabel边框颜色
        #     # font=dict(
        #     #    color="",              # 默认以bordercolor边框颜色显示
        #     #    family="",             # 字体
        #     #    size=1,                # 字号
        #     # ),
        #     namelength=5,             # 显示trace名的长度 设置此跟踪悬停标签中跟踪名称的长度（字符数）。 -1显示整个名称，不管长度。 0-3表示第一个0-3个字符，如果小于多个字符，整数> 3将显示整个名称，但如果长度过长，将截断为“namelength-3”字符并添加省略号。
        # ),
        # stream=???
        # text="在图中显示具体值",       # 设置与每个（x，y）对相关联的文本元素。 如果一个字符串，所有数据点上都会出现相同的字符串。 如果是一个字符串数组，这些项目将被映射为这个轨迹的（x，y）坐标。
        # textfont=dict(family="", size="", color=""), # text字体
        # textposition="top center"     # 设置text相对应（x，y）点的位置 ( enumerated or array of enumerateds : "top left" | "top center" | "top right" | "middle left" | "middle center" | "middle right" | "bottom left" | "bottom center" | "bottom right" )  default: "middle center"
        # xcalendar="gregorian",        # 日期数据显示格式 default: "gregorian" ( enumerated : "gregorian" | "chinese" | "coptic" | "discworld" | "ethiopian" | "hebrew" | "islamic" | "julian" | "mayan" | "nanakshahi" | "nepali" | "persian" | "jalali" | "taiwan" | "thai" | "ummalqura" )
        # ycalendar="gregorian",        # 日期数据显示格式 default: "gregorian" ( enumerated : "gregorian" | "chinese" | "coptic" | "discworld" | "ethiopian" | "hebrew" | "islamic" | "julian" | "mayan" | "nanakshahi" | "nepali" | "persian" | "jalali" | "taiwan" | "thai" | "ummalqura" )


    )

if __name__ == "__main__":
    app.run_server(debug=True)
