# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍 气泡图使用 以大洲为集合，展示各个国家平均每人GDP和人均寿命，并通过气泡大小表示国家人口规模。
"""
df = pd.read_csv("data.csv")
contient_list = df["continent"].unique() # 5大洲
df_year = df[df["year"] == 2007]


traces=[]

for continent in contient_list:
    hovertext = []
    for country in df_year[df_year["continent"] == continent]["country"].unique():
        hovertext.append(
            "Country:%(country)s <br>Pop:%(pop)s <br>GdpPercap:%(gdbpercap)s <br>LifeExp:%(lifeexp)s <br>Year:2007"
            %
            {
                "country": country,
                "pop": list(df_year[df_year["country"] == country]["pop"])[0],
                "gdbpercap": list(df_year[df_year["country"] == country]["gdpPercap"])[0],
                "lifeexp": list(df_year[df_year["country"] == country]["lifeExp"])[0],
            }
        )
    size=df_year[df_year["continent"] == continent]["pop"]
    traces.append(
        go.Scatter(
            mode="markers",
            x=df_year[df_year["continent"] == continent]["gdpPercap"],
            y=df_year[df_year["continent"] == continent]["lifeExp"],
            hovertext=hovertext,
            hoverinfo="all",
            name=continent,
            marker=go.Marker(
                size=list(map(lambda x:x/10000000, df_year[df_year["continent"] == continent]["pop"])),
                sizemin=7,
                line=go.Line(
                    color="white",
                    width=2,
                ),
            ),
        )
    )

app = dash.Dash()

app.layout = html.Div([
    html.H2(children="气泡图-02"),
    dcc.Graph(
        id='id_graph',
        figure=go.Figure(
            data=go.Data(traces),
            layout=go.Layout(
                title='Life Expectancy v. Per Capita GDP, 2007',
                yaxis=go.YAxis(
                    title="LifeExp",
                    zeroline=False,
                    gridcolor='rgb(255, 255, 255)',
                    gridwidth=2,
                    ticklen=5,
                ),
                xaxis=go.XAxis(
                    title="GdpPercap",
                    gridcolor='rgb(255, 255, 255)',
                    gridwidth=2,
                    type='log',
                    ticklen=5,
                ),
                paper_bgcolor='rgb(243, 243, 243)',
                plot_bgcolor='rgb(243, 243, 243)',
            ),
        ),

    ),
])


if __name__ == "__main__":
    app.run_server(debug=True)