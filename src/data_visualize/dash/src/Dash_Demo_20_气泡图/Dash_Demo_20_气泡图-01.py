# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍 气泡图
"""
app = dash.Dash()

size = [40, 60, 80, 100]

app.layout = html.Div([
    html.H2(children="气泡图-01"),
    dcc.Graph(
        id="id_graph",
        figure=go.Figure(
            data=go.Data([
                go.Scatter(
                    x=[1, 2, 3, 4],
                    y=[1, 2, 3, 4],
                    mode="markers",
                    marker=go.Marker(
                        line=go.Line(
                            color="black",
                            width=1,
                        ),
                        size=size,                  # 通过控制散点的大小来制作气泡图
                        sizeref=2*max(size)/100**2,  # 设置用于确定标记点渲染大小的比例因子。仅当`marker.size`设置为数字数组时才有效果。(number) default: 1
                        sizemode='area',            # 设置“尺寸”中的数据转换为像素的规则。仅当`marker.size`设置为数字数组时才有效果。 ( enumerated : "diameter"直径 | "area"面积 ) default: "diameter"
                        sizemin=4,                  # 设置渲染标记点的最小尺寸（以像素为单位）。仅当`marker.size`设置为数字数组时才有效果。(number greater than or equal to 0) default: 0
                        # color=['rgb(93, 164, 214)', 'rgb(255, 144, 14)',
                        #        'rgb(44, 160, 101)', 'rgb(255, 65, 54)'],  # 气泡颜色 设置标记颜色。 它接受一个特定的颜色或者一组数字，这些数字映射到相对于数组的最大值和最小值的颜色比例，或者相对于'cmin'和'cmax'（如果设置）。
                        color=[120, 125, 130, 135],                         # 气泡颜色
                        showscale=True,                                     # 显示颜色范围 确定是否显示一个颜色条。仅当marker.color被设置为数字数组时才有效果。
                        opacity=[1, 0.8, 0.6, 0.4],                         # 气泡透明度
                    ),
                    # hoverinfo="all",
                    # hovertext="hovertext", # 如果设置了hovertext则在hover操作元素时会显示hovertext内容(string or array of strings)
                    # hoverlabel=dict(bgcolor="", bordercolor="", font=dict()),
                    text=["A<br>Size 40", "B<br>Size 60", "C<br>Size 80", "D<br>Size 100"], # 如果没有设置hovertext时，hover操作元素会优先显示text的值 (string or array of strings)

                )
            ]),
            layout=go.Layout(
                hovermode="closest",
            ),
        ),
    ),
])

if __name__ == "__main__":
    app.run_server(debug=True, port=8051)