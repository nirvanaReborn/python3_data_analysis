# coding=UTF-8
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import numpy
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 介绍 散点图
"""
app = dash.Dash()

app.layout = html.Div([
    html.H2(children="散点图"),
    dcc.Graph(
        id="id_graph",
        figure=go.Figure(
            data=go.Data([
                go.Scatter(
                    x=numpy.linspace(0, 10, 100), # 生成0-10 共100个数据的等差数列
                    y=numpy.random.randn(100), # 生成 一个一维共100个正太数组
                    mode="markers",
                    marker=go.Marker(
                        line=go.Line(
                            color="black",
                            width=1,
                        ),
                        size=15,
                    ),
                    opacity=0.6,
                    visible=True,
                    showlegend=True,
                    legendgroup="group_a",
                    name="name",
                    hoverinfo="all",
                    # hoverlabel=dict(bgcolor="", bordercolor="", font=dict()),
                    text="text",
                    hovertext="hovertext",
                )
            ])
        )
    ),
])

if __name__ == "__main__":
    app.run_server(debug=True)