# coding=UTF-8
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html

"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description:交互式Dash app写法(1): Input-Div
"""

app = dash.Dash()

app.layout = html.Div([
    html.H2('输入框'),
    dcc.Input(
        id='id_input_1',
        placeholder='请输入...',
        value='苹果',
        type='text'
    ),
    html.Div(
        id='id_div_2',
        children='我是div标签', # children属性表示标签内的文本
    ),
])

# 回调函数 装饰器（装饰update_output_div）
@app.callback(
    Output(component_id='id_div_2', component_property='children',),
    [Input(component_id='id_input_1', component_property='value')]
)
# 更新div
def update_output_div(input_value):
    return "您输入了“{}”".format(input_value)

if __name__ == "__main__":
    app.run_server(debug=True)