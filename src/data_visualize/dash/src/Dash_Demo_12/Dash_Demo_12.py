# coding=UTF-8
import dash
# from textwrap import dedent as d # ???
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas
"""
@Author: zhangzheng
@File: Dash_Demo_3.py
@Date:2018-01-20
@Description: 利用Graphd的hoverData属性 动态显示横纵坐标的数据(折线图)
"""
df = pandas.read_csv("data.csv")

app = dash.Dash()

app.layout = html.Div([
    html.H2(children="Graphd的hoverData属性演示"),
    dcc.Dropdown(
        id="id_x_dropdown",
        options=[{'label': i, 'value': i} for i in df["Indicator Name"].unique()],
        value=df["Indicator Name"].unique()[0],
    ),
    dcc.RadioItems(
        id="id_x_radioitems",
        options=[{'label': 'Linear', 'value': 'Linear'}, {'label': 'Log', 'value': 'Log'}],
        value='Linear',
    ),
    dcc.Dropdown(
        id="id_y_dropdown",
        options=[{'label': i, 'value': i} for i in df["Indicator Name"].unique()],
        value=df["Indicator Name"].unique()[0],
    ),
    dcc.RadioItems(
        id="id_y_radioitems",
        options=[{'label': 'Linear', 'value': 'Linear'}, {'label': 'Log', 'value': 'Log'}],
        value='Linear',
    ),
    dcc.Graph(
        id='id_graph_scatter', # 初始时不要将属性figure置为"{}"空字典，可能会导致页面生成失败
    ),
    dcc.Graph(
        id='id_x_graph_line',
    ),
    dcc.Graph(
        id='id_y_graph_line',
    ),
    dcc.Slider(
        id='id_slider_year',
        min=df['Year'].min(),
        max=df['Year'].max(),
        step=None,
        value=1997,
        marks={str(i): str(i) for i in df['Year'].unique()},
    ),
])

@app.callback(
    Output(component_id="id_graph_scatter", component_property="figure"),
    [
        Input(component_id="id_x_dropdown", component_property="value"),
        Input(component_id="id_x_radioitems", component_property="value"),
        Input(component_id="id_y_dropdown", component_property="value"),
        Input(component_id="id_y_radioitems", component_property="value"),
        Input(component_id="id_slider_year", component_property="value"),
    ]
)
def updateScatter(p_x_dropdown, p_x_radioitems, p_y_dropdown, p_y_radioitems, p_slider_year):
    df_year = df[df["Year"] == int(p_slider_year)]
    return {
        'data': [
            go.Scatter(
                x=df_year[df_year["Indicator Name"] == str(p_x_dropdown)]["Value"],
                y=df_year[df_year["Indicator Name"] == str(p_y_dropdown)]["Value"],
                text=df_year[df_year["Indicator Name"] == str(p_y_dropdown)]["Country Name"],
                mode='markers',
                marker={
                    'size': 15, # 大小
                    'opacity': 0.5, # 不透明度
                },
            )
        ],
        'layout': go.Layout(
                    xaxis={
                        'title': str(p_x_dropdown),
                        'type': 'linear' if str(p_x_radioitems) == "Linear" else 'log',
                    },
                    yaxis={
                        'title': str(p_y_dropdown),
                        'type': 'linear' if str(p_y_radioitems) == "Linear" else 'log',
                    },
        ),
    }


def creat_time_series(p_data, p_title, p_type, p_country_name):
    return {
        'data': [go.Scatter(
            y=p_data["Value"],
            x=p_data["Year"],
        )],
        'layout': go.Layout(
            title = p_country_name + "的" + p_title,
            xaxis={
                'title': "Year",
            },
            yaxis={
                'title': p_title,
                'type': p_type
            }
        )
    }


@app.callback(
    Output(component_id="id_x_graph_line", component_property="figure"),
    [
        Input(component_id="id_graph_scatter", component_property="hoverData"),
        Input(component_id="id_x_dropdown", component_property="value"),
        Input(component_id="id_x_radioitems", component_property="value"),
    ]
)
def updateXLineGraph(p_graph_scatter, p_x_dropdown, p_x_radioitems):
    country_name = p_graph_scatter["points"][0]["text"]
    df_country = df[df["Country Name"] == str(country_name)]
    df_country_title = df_country[df_country["Indicator Name"] == str(p_x_dropdown)]
    return creat_time_series(p_data=df_country_title, p_title=p_x_dropdown, p_type=p_x_radioitems, p_country_name=country_name)

@app.callback(
    Output(component_id="id_y_graph_line", component_property="figure"),
    [
        Input(component_id="id_graph_scatter", component_property="hoverData"),
        Input(component_id="id_y_dropdown", component_property="value"),
        Input(component_id="id_y_radioitems", component_property="value"),
    ]
)
def updateYLineGraph(p_graph_scatter, p_y_dropdown, p_y_radioitems):
    country_name = p_graph_scatter["points"][0]["text"]
    df_country = df[df["Country Name"] == str(country_name)]
    df_country_title = df_country[df_country["Indicator Name"] == str(p_y_dropdown)]
    return creat_time_series(p_data=df_country_title, p_title=p_y_dropdown, p_type=p_y_radioitems, p_country_name=country_name)



if __name__ == "__main__":
    app.run_server(debug=True)