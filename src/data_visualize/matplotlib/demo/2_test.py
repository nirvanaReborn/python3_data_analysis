#!/usr/bin/env python
# -*- coding: utf-8 -*-

# http://www.toutiao.com/a6411453746781634817/
# Python绘图——山楂树之恋


import numpy as np
import matplotlib.pylab as plt
from matplotlib.font_manager import FontProperties


def main():
    font_set = FontProperties(fname=r"c:\windows\fonts\simsun.ttc", size=15)
    # 以上两行为最后面title输出汉字的第一部分
    N = 1000
    # 如果需要手动输入N 则选择下面这个
    # N=int(input("Enter the number N: "))
    X = np.zeros(N)
    Y = np.zeros(N)
    X[0] = 0.5
    Y[0] = 0.0
    for i in range(N - 1):
        a = np.random.random()
        # 产生一个0到1之间的随机数（浮点数）
        if a < 0.1:
            X[i + 1] = 0.05 * X[i]
            Y[i + 1] = 0.6 * Y[i]
        elif a >= 0.1 and a < 0.2:
            X[i + 1] = 0.05 * X[i]
            Y[i + 1] = -0.5 * Y[i] + 1.0
        elif a >= 0.2 and a < 0.4:
            X[i + 1] = 0.46 * X[i] - 0.32 * Y[i]
            Y[i + 1] = 0.39 * X[i] + 0.38 * Y[i] + 0.6
        elif a >= 0.4 and a < 0.6:
            X[i + 1] = 0.47 * X[i] - 0.15 * Y[i]
            Y[i + 1] = 0.17 * X[i] + 0.42 * Y[i] + 1.1
        elif a >= 0.6 and a < 0.8:
            X[i + 1] = 0.43 * X[i] + 0.28 * Y[i]
            Y[i + 1] = -0.25 * X[i] + 0.45 * Y[i] + 1.0
        else:
            X[i + 1] = 0.42 * X[i] + 0.26 * Y[i]
            Y[i + 1] = -0.35 * X[i] + 0.31 * Y[i] + 0.7
    plt.plot(X, Y, 's', markersize=1)
    plt.title(u'山楂树之恋', fontproperties=font_set)
    # title输出为汉字的第二部分
    plt.show()


if __name__ == '__main__':
    main()
