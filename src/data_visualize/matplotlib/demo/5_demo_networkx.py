#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/brightyuxl/p/9994634.html
# Python3画图系列——NetworkX初探
# https://blog.csdn.net/newbieMath/article/details/54632713

import networkx as nx
import matplotlib.pyplot as plt


def test_1():
    G = nx.Graph()
    G.add_node(1)
    G.add_edge(2, 3)
    # G.add_edge(3, 2)
    print("输出全部节点：{}".format(G.nodes()))
    print("输出全部边：{}".format(G.edges()))
    print("输出全部边的数量：{}".format(G.number_of_edges()))
    nx.draw(G)
    plt.show()


def test_2():
    G = nx.DiGraph()
    G.add_node(1)
    G.add_node(2)
    G.add_nodes_from([3, 4, 5, 6])
    G.add_cycle([1, 2, 3, 4])
    G.add_edge(1, 3)
    G.add_edges_from([(3, 5), (3, 6), (6, 7)])
    print("输出全部节点：{}".format(G.nodes()))
    print("输出全部边：{}".format(G.edges()))
    print("输出全部边的数量：{}".format(G.number_of_edges()))
    nx.draw(G)
    plt.show()


def test_3():
    G = nx.cubical_graph()
    plt.subplot(121)
    nx.draw(G)
    plt.subplot(122)
    nx.draw(G, pos=nx.circular_layout(G), nodecolor='r', edge_color='b')
    plt.show()


def test_4():
    G = nx.path_graph(8)
    nx.draw(G)
    plt.show()


def test_5():
    G = nx.cycle_graph(24)
    pos = nx.spring_layout(G, iterations=200)
    nx.draw(G, pos, node_color=range(24), node_size=800, cmap=plt.cm.Blues)
    plt.show()


def test_6():
    G = nx.petersen_graph()
    plt.subplot(121)
    nx.draw(G, with_labels=True, font_weight='bold')
    plt.subplot(122)
    nx.draw_shell(G, nlist=[range(5, 10), range(5)], with_labels=True, font_weight='bold')
    plt.show()


def main():
    test_6()


if __name__ == "__main__":
    main()
