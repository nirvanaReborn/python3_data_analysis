#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/moady/p/5734058.html

# 计算TF-IDF词语权重，测试scikit-learn数据分析
def test_5():
    import os
    import sys
    from sklearn import feature_extraction
    from sklearn.feature_extraction.text import TfidfTransformer
    from sklearn.feature_extraction.text import CountVectorizer

    if __name__ == "__main__":
        corpus = ["我 来到 北京 清华大学",  # 第一类文本切词后的结果 词之间以空格隔开
                  "他 来到 了 网易 杭研 大厦",  # 第二类文本的切词结果
                  "小明 硕士 毕业 与 中国 科学院",  # 第三类文本的切词结果
                  "我 爱 北京 天安门"]  # 第四类文本的切词结果

        # 该类会将文本中的词语转换为词频矩阵，矩阵元素a[i][j] 表示j词在i类文本下的词频
        vectorizer = CountVectorizer()

        # 该类会统计每个词语的tf-idf权值
        transformer = TfidfTransformer()

        # 第一个fit_transform是计算tf-idf，第二个fit_transform是将文本转为词频矩阵
        tfidf = transformer.fit_transform(vectorizer.fit_transform(corpus))

        # 获取词袋模型中的所有词语
        word = vectorizer.get_feature_names()

        # 将tf-idf矩阵抽取出来，元素a[i][j]表示j词在i类文本中的tf-idf权重
        weight = tfidf.toarray()

        # 打印每类文本的tf-idf词语权重，第一个for遍历所有文本，第二个for便利某一类文本下的词语权重
        for i in range(len(weight)):
            print(u"-------这里输出第", i, u"类文本的词语tf-idf权重------")
            for j in range(len(word)):
                print(word[j], weight[i][j])


# 矩阵数据集，测试sklearn
def test_4():
    from sklearn import datasets
    iris = datasets.load_iris()
    digits = datasets.load_digits()
    print(digits.data)


# 显示Matplotlib强大绘图交互功能
def test_3():
    import numpy as np
    import matplotlib.pyplot as plt

    N = 5
    menMeans = (20, 35, 30, 35, 27)
    menStd = (2, 3, 4, 1, 2)

    ind = np.arange(N)  # the x locations for the groups
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()  # 创建一个子图
    rects1 = ax.bar(ind, menMeans, width, color='r', yerr=menStd)

    womenMeans = (25, 32, 34, 20, 25)
    womenStd = (3, 5, 2, 3, 3)
    rects2 = ax.bar(ind + width, womenMeans, width, color='y', yerr=womenStd)

    # add some
    ax.set_ylabel('Scores')
    ax.set_title('Scores by group and gender')
    ax.set_xticks(ind + width)
    ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

    ax.legend((rects1[0], rects2[0]), ('Men', 'Women'))

    def autolabel(rects):
        # attach some text labels
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width() / 2., 1.05 * height,
                    '%d' % int(height),
                    ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)

    plt.show()


# 桃心程序，测试numpy和matplotlib
def test_2():
    import numpy as np
    import matplotlib.pyplot as plt

    X = np.arange(-5.0, 5.0, 0.1)
    Y = np.arange(-5.0, 5.0, 0.1)

    x, y = np.meshgrid(X, Y)
    f = 17 * x ** 2 - 16 * np.abs(x) * y + 17 * y ** 2 - 225

    fig = plt.figure()
    cs = plt.contour(x, y, f, 0, colors='r')
    plt.show()


# 斜线坐标，测试matplotlib
def test_1():
    import matplotlib
    import numpy
    import scipy
    import matplotlib.pyplot as plt
    plt.plot([1, 2, 3])
    plt.ylabel('some numbers')
    plt.show()


def main():
    test_1()
    test_2()
    test_3()
    test_4()
    test_5()


if __name__ == "__main__":
    main()
