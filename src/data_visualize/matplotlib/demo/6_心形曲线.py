#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


def main():
    # 设置参数方程
    t = np.linspace(0, 2 * np.pi, 1000)
    x = 16 * np.sin(t) ** 3
    y = 13 * np.cos(t) - 5 * np.cos(2 * t) - 2 * np.cos(3 * t) - np.cos(4 * t)

    # 创建图像和坐标轴
    plt.figure(figsize=(8, 8))
    ax = plt.gca()

    # 绘制心形曲线
    ax.plot(x, y, 'r')

    # 设置坐标轴范围
    ax.set_xlim(-20, 20)
    ax.set_ylim(-20, 20)

    # 隐藏坐标轴
    ax.axis('off')

    # 保存图像，移除了 fontproperties 参数
    plt.savefig('./heart_curve.png', dpi=300, bbox_inches='tight', pad_inches=0)


if __name__ == '__main__':
    main()
