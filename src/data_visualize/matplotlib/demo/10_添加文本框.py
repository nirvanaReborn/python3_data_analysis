#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6919325798402884109/
# 总结了16个常用的matlibplot画图技巧


import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec

plt.rcParams['font.sans-serif'] = ['MSYH', 'SimHei']  # 用来正常显示中文标签


# 为文本添加添加背景框
def test_1():
    fig = plt.figure()  # 创建一个图形实例
    fig, axe = plt.subplots()  # 创建一个子图
    axe.text(0.5, 0.5, '我是文本框', bbox={'facecolor': 'cyan', 'alpha': 0.5, 'pad': 0.7})  # 添加文本框
    plt.show()


# 添加指示箭头
def test_2():
    plt.rcParams['axes.unicode_minus'] = False
    fig = plt.figure()  # 创建一个图形实例
    fig, axe = plt.subplots()  # 创建一个子图
    t = np.arange(0.0, 2.0, 0.01)
    s = np.sin(2 * np.pi * t)
    axe.plot(t, s, linestyle='-', label='line1')
    axe.annotate('我是正弦函数', xy=(1.25, 1), xytext=(1.9, 1),
                 arrowprops=dict(facecolor='red', shrink=0.2),
                 horizontalalignment='center', verticalalignment='center')
    plt.show()


# 改变折线形状
def test_3():
    fig, axe = plt.subplots()  # 创建一个子图
    np.random.seed(100)
    x = np.arange(0, 10, 1)
    y1 = np.random.rand(10)
    axe.plot(x, y1, '--o')
    plt.show()


# 柱状图横置
def test_4():
    fig, axe = plt.subplots()  # 创建一个子图
    data_m = (40, 60, 120, 180, 20, 200)
    index = np.arange(6)
    width = 0.4
    axe.barh(index, data_m, width, align='center', alpha=0.8, label='men')
    plt.show()


# 移动坐标轴位置
def test_5():
    fig = plt.figure()
    fig, axe = plt.subplots()
    axe.spines['right'].set_color('none')
    axe.spines['top'].set_color('none')
    axe.spines['bottom'].set_position(('data', 1))
    axe.spines['left'].set_position(('data', 1))
    plt.show()


# 设置坐标轴范围
def test_6():
    fig = plt.figure()
    fig, axe = plt.subplots()
    plt.xlim(0, 10)
    plt.ylim(0, 8000)
    plt.show()


# 改变坐标轴颜色
def test_7():
    fig = plt.figure()
    fig, axe = plt.subplots()
    axe.spines['right'].set_color('yellow')
    axe.spines['top'].set_color('red')
    plt.show()


# 设置坐标轴刻度
def test_8():
    fig = plt.figure()
    fig, axe = plt.subplots()
    axe.set_xticks([0, 1, 2, 3, 4, 5])
    plt.show()


# 改变刻度
def test_9():
    fig = plt.figure()
    fig, axe = plt.subplots()
    axe.set_xticks([0, 1, 2, 3, 4, 5])
    axe.set_xticklabels(['Taxi', 'Metro', 'Walk', 'Bus', 'Bicycle', 'Driving'])
    plt.show()


# 坐标倾斜
def test_10():
    fig = plt.figure()
    fig, axe = plt.subplots()
    axe.set_xticks([0, 1, 2, 3, 4, 5])
    axe.set_xticklabels(['Taxi', 'Metro', 'Walk', 'Bus', 'Bicycle', 'Driving'], rotation=45)
    plt.show()


# 绘制子图
def test_11():
    fig = plt.figure()
    fig, axe = plt.subplots(4, 4, figsize=(10, 10))
    plt.show()


# 加网格线
def test_12():
    fig = plt.figure()
    fig, axe = plt.subplots()
    axe.grid(True)
    plt.show()


# 改变图形颜色
def test_13():
    fig, axe = plt.subplots()
    data_m = (40, 60, 120, 180, 20, 200)
    index = np.arange(6)
    axe.bar(index, data_m, color='y')
    plt.show()


# 改变样式
def test_14():
    '''plt.style.available
    ['bmh',
    'classic',
    'dark_background',
    'fast',
    'fivethirtyeight',
    'ggplot',
    'grayscale',
    'seaborn-bright',
    'seaborn-colorblind',
    'seaborn-dark-palette',
    'seaborn-dark',
    'seaborn-darkgrid',
    'seaborn-deep',
    'seaborn-muted',
    'seaborn-notebook',
    'seaborn-paper',
    'seaborn-pastel',
    'seaborn-poster',
    'seaborn-talk',
    'seaborn-ticks',
    'seaborn-white',
    'seaborn-whitegrid',
    'seaborn',
    'Solarize_Light2',
    'tableau-colorblind10',
    '_classic_test']
    '''
    fig, axe = plt.subplots()
    data_m = (40, 60, 120, 180, 20, 200)
    index = np.arange(6)
    axe.bar(index, data_m)
    plt.style.use('dark_background')
    plt.show()


# 添加表格
def test_15():
    fig, axe = plt.subplots()
    data_m = (40, 60, 120, 180, 20, 200)
    data_f = (30, 100, 150, 30, 20, 50)
    index = np.arange(6)
    width = 0.4
    # bar charts
    axe.bar(index, data_m, width, color='c', label='men')
    axe.bar(index, data_f, width, color='b', bottom=data_m, label='women')
    axe.set_xticks([])
    axe.legend()  # 创建图例
    data = (data_m, data_f)  # 表格每一行数据
    rows = ('male', 'female')  # 表格行名
    columns = ('Taxi', 'Metro', 'Walk', 'Bus', 'Bicycle', 'Driving')  # 表格列名
    table = axe.table(cellText=data, rowLabels=rows, colLabels=columns,
                      loc='best')
    # table.auto_set_font_size(False)
    # table.set_fontsize(10)  # 字体大小
    # table.scale(1, 0.6)  # 表格缩放
    plt.show()


def test_17():
    a = list(range(10))
    b = np.random.randint(1, 100, 10)
    plt.figure(figsize=(7, 7))
    gs = gridspec.GridSpec(4, 1)
    # 画图表
    ax1 = plt.subplot(gs[:3, 0])
    ax1.bar(a, b)
    plt.xlabel('x Label')
    plt.ylabel('y Label')
    plt.title('title')
    # 插入表格
    ax2 = plt.subplot(gs[3, 0])
    plt.axis('off')
    rowLabels = ['第一行', '第二行', '第三行']  # 表格行名
    cellText = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]  # 表格每一行数据
    table = plt.table(cellText=cellText, rowLabels=rowLabels,
                      loc='center', cellLoc='center', rowLoc='center')
    table.auto_set_font_size(False)
    table.set_fontsize(10)  # 字体大小
    table.scale(1, 1.5)  # 表格缩放
    plt.show()


# 饼状图分离
def test_16():
    fig, axe = plt.subplots()
    labels = 'Taxi', 'Metro', 'Walk', 'Bus', 'Bicycle', 'Drive'
    sizes = [10, 30, 5, 25, 5, 25]
    explode = (0.1, 0.1, 0.5, 0.1, 0.1, 0.1)  # 控制分隔距离
    axe.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    axe.axis('equal')
    plt.show()
    plt.savefig('temp.png', dpi=fig.dpi)  # 保存绘制的图片


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
