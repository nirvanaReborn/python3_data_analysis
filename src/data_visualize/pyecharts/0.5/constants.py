#!/usr/bin/env python3
# coding=utf-8

# 对于str类型的字符串，调用len()和遍历时，其实都是以字节为单位的，这个太坑爹了，同一个字符使用不同的编码格式，长度往往是不同的。
# 对于unicode类型的字符串调用len()和遍历才是以字符为单位，这是我们所要的。
# 将模块中显式出现的所有字符串转为unicode类型，不过，对于必须使用str字符串的地方要加以注意。
from __future__ import unicode_literals
# 经开发团队决定，0.5.x 版本将不再进行维护，0.5.x 版本代码位于 05x 分支，文档地址:
# https://05x-docs.pyecharts.org/#/zh-cn/prepare
# pip install pyecharts_snapshot==0.0.3 pyecharts==0.5.11
import pyecharts


if int(str(pyecharts.__version__).split('.')[0]) > 0:
    print("该脚本需要v0.5.x版本")
    print("[pyecharts v1.0.0 停止对 Python2.7，3.4~3.5 版本的支持和维护，仅支持 Python3.6+，向下不兼容的。](https://pyecharts.org/#/zh-cn/release-note/v100)")
    exit(0)

RANGE_COLOR = [
    '#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8',
    '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026'
    ]

X_TIME = [
    "12a", "1a", "2a", "3a", "4a", "5a", "6a", "7a", "8a", "9a", "10a", "11a",
    "12p", "1p", "2p", "3p", "4p", "5p", "6p", "7p", "8p", "9p", "10p", "11p"
    ]

Y_WEEK = [
    "Saturday", "Friday", "Thursday", "Wednesday",
    "Tuesday", "Monday", "Sunday"
    ]

CLOTHES = ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"]

WEEK = ['周一', '周二', '周三', '周四', '周五', '周六', '周日']