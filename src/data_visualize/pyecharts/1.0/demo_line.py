#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_42241770/article/details/108393930
# pyecharts折线图全解


import pyecharts.options as opts
from pyecharts.charts import Line


def test_1():
    '''基本折线图
    series_name：图形名称
    y_axis：数据
    symbol：标记的图形，pyecharts提供的类型包括’circle’, ‘rect’, ‘roundRect’, ‘triangle’, ‘diamond’, ‘pin’, ‘arrow’, ‘none’，也可以通过 ‘image://url’ 设置为图片，其中 URL 为图片的链接。
    is_symbol_show：是否显示 symbol
    '''
    x = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期七', '星期日']
    y = [100, 200, 300, 400, 500, 400, 300]
    map_data = dict(zip(x, y))
    dest_file = '基本折线图'
    line = (
        Line(init_opts=opts.InitOpts(width="1800px", height="800px"))
            .set_global_opts(tooltip_opts=opts.TooltipOpts(is_show=False),
                             xaxis_opts=opts.AxisOpts(type_="category"),
                             yaxis_opts=opts.AxisOpts(
                                 type_="value",
                                 axistick_opts=opts.AxisTickOpts(is_show=True),
                                 splitline_opts=opts.SplitLineOpts(is_show=True),
                             ),
                             )
            .add_xaxis(xaxis_data=map_data.keys())
            .add_yaxis(series_name=dest_file,
                       y_axis=map_data.values(),
                       symbol="emptyCircle",
                       is_symbol_show=True,
                       label_opts=opts.LabelOpts(is_show=False),
                       )
    )
    # 渲染到html页面
    line.render(dest_file + '.html')


def test_2():
    '''连接空数据（折线图）'''
    # is_connect_nones
    x = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期七', '星期日']
    y = [100, 200, 300, 400, None, 400, 300]

    line = (
        Line()
            .add_xaxis(xaxis_data=x)
            .add_yaxis(series_name="连接空数据（折线图）",
                       y_axis=y,
                       is_connect_nones=True
                       )
            .set_global_opts(title_opts=opts.TitleOpts(title="Line-连接空数据"))
    )
    line.render()


def test_3():
    '''多条折线重叠'''
    # add_yaxis
    x = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期七', '星期日']
    y1 = [100, 200, 300, 400, 100, 400, 300]
    y2 = [200, 300, 200, 100, 200, 300, 400]
    line = (
        Line()
            .add_xaxis(xaxis_data=x)
            .add_yaxis(series_name="y1线", y_axis=y1, symbol="arrow", is_symbol_show=True)
            .add_yaxis(series_name="y2线", y_axis=y2)
            .set_global_opts(title_opts=opts.TitleOpts(title="Line-多折线重叠"))
    )
    line.render()


def test_4():
    '''平滑曲线折线图'''
    # is_smooth：平滑曲线标志
    x = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期七', '星期日']
    y1 = [100, 200, 300, 400, 100, 400, 300]
    y2 = [200, 300, 200, 100, 200, 300, 400]
    line = (
        Line()
            .add_xaxis(xaxis_data=x)
            .add_yaxis(series_name="y1线", y_axis=y1, is_smooth=True)
            .add_yaxis(series_name="y2线", y_axis=y2, is_smooth=True)
            .set_global_opts(title_opts=opts.TitleOpts(title="Line-多折线重叠"))
    )
    line.render()


def test_5():
    '''阶梯图'''
    # is_step：阶梯图参数
    x = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期七', '星期日']
    y1 = [100, 200, 300, 400, 100, 400, 300]
    line = (
        Line()
            .add_xaxis(xaxis_data=x)
            .add_yaxis(series_name="y1线", y_axis=y1, is_step=True)
            .set_global_opts(title_opts=opts.TitleOpts(title="Line-阶梯图"))
    )
    line.render()


def test_6():
    '''变换折线的样式'''
    # linestyle_opts:折线样式配置，color设置颜色，width设置宽度，type设置类型，有’solid’, ‘dashed’, 'dotted’三种类型
    # itemstyle_opts：图元样式配置，border_width设置描边宽度，border_color设置描边颜色，color设置纹理填充颜色
    from pyecharts.faker import Faker
    x = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期七', '星期日']
    y1 = [100, 200, 300, 400, 100, 400, 300]
    line = (
        Line()
            .add_xaxis(xaxis_data=x)
            .add_yaxis("y1",
                       y1,
                       symbol="triangle",
                       symbol_size=30,
                       linestyle_opts=opts.LineStyleOpts(color="red", width=4, type_="dashed"),
                       itemstyle_opts=opts.ItemStyleOpts(border_width=3, border_color="yellow", color="blue"),
                       )
            .set_global_opts(title_opts=opts.TitleOpts(title="Line-ItemStyle"))
    )
    line.render()


def test_7():
    '''折线面积图'''
    # areastyle_opts
    x = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期七', '星期日']
    y1 = [100, 200, 300, 400, 100, 400, 300]
    y2 = [200, 300, 200, 100, 200, 300, 400]
    line = (
        Line()
            .add_xaxis(xaxis_data=x)
            .add_yaxis(series_name="y1线", y_axis=y1, areastyle_opts=opts.AreaStyleOpts(opacity=0.5))
            .add_yaxis(series_name="y2线", y_axis=y2, areastyle_opts=opts.AreaStyleOpts(opacity=0.5))
            .set_global_opts(title_opts=opts.TitleOpts(title="Line-多折线重叠"))
    )
    line.render()


def test_8():
    '''双横坐标折线图'''
    # extend_axis
    from pyecharts.commons.utils import JsCode
    js_formatter = """function (params) {
            console.log(params);
            return '降水量  ' + params.value + (params.seriesData.length ? '：' + params.seriesData[0].data : '');
        }"""

    line = (
        Line()
            .add_xaxis(
            xaxis_data=[
                "2016-1",
                "2016-2",
                "2016-3",
                "2016-4",
                "2016-5",
                "2016-6",
                "2016-7",
                "2016-8",
                "2016-9",
                "2016-10",
                "2016-11",
                "2016-12",
            ]
        )
            .extend_axis(
            xaxis_data=[
                "2015-1",
                "2015-2",
                "2015-3",
                "2015-4",
                "2015-5",
                "2015-6",
                "2015-7",
                "2015-8",
                "2015-9",
                "2015-10",
                "2015-11",
                "2015-12",
            ],
            xaxis=opts.AxisOpts(type_="category",
                                axistick_opts=opts.AxisTickOpts(is_align_with_label=True),
                                axisline_opts=opts.AxisLineOpts(
                                    is_on_zero=False, linestyle_opts=opts.LineStyleOpts(color="#6e9ef1")
                                ),
                                axispointer_opts=opts.AxisPointerOpts(
                                    is_show=True, label=opts.LabelOpts(formatter=JsCode(js_formatter))
                                ),
                                ),
        )
            .add_yaxis(
            series_name="2015 降水量",
            is_smooth=True,
            symbol="emptyCircle",
            is_symbol_show=False,
            color="#d14a61",
            y_axis=[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
            label_opts=opts.LabelOpts(is_show=False),
            linestyle_opts=opts.LineStyleOpts(width=2),
        )
            .add_yaxis(
            series_name="2016 降水量",
            is_smooth=True,
            symbol="emptyCircle",
            is_symbol_show=False,
            color="#6e9ef1",
            y_axis=[3.9, 5.9, 11.1, 18.7, 48.3, 69.2, 231.6, 46.6, 55.4, 18.4, 10.3, 0.7],
            label_opts=opts.LabelOpts(is_show=False),
            linestyle_opts=opts.LineStyleOpts(width=2),
        )
            .set_global_opts(
            legend_opts=opts.LegendOpts(),
            tooltip_opts=opts.TooltipOpts(trigger="none", axis_pointer_type="cross"),
            xaxis_opts=opts.AxisOpts(
                type_="category",
                axistick_opts=opts.AxisTickOpts(is_align_with_label=True),
                axisline_opts=opts.AxisLineOpts(
                    is_on_zero=False, linestyle_opts=opts.LineStyleOpts(color="#d14a61")
                ),
                axispointer_opts=opts.AxisPointerOpts(
                    is_show=True, label=opts.LabelOpts(formatter=JsCode(js_formatter))
                ),
            ),
            yaxis_opts=opts.AxisOpts(
                type_="value",
                splitline_opts=opts.SplitLineOpts(
                    is_show=True, linestyle_opts=opts.LineStyleOpts(opacity=1)
                ),
            ),
        )
    )
    line.render()


def test_9():
    '''用电量随时间变化'''
    # 这里给大家介绍几个关键参数：
    # ①visualmap_opts：视觉映射配置项，可以将折线分段并设置标签（is_piecewise），将不同段设置颜色（pieces）；
    # ②markarea_opts：标记区域配置项，data参数可以设置标记区域名称和位置。
    x_data = [
        "00:00",
        "01:15",
        "02:30",
        "03:45",
        "05:00",
        "06:15",
        "07:30",
        "08:45",
        "10:00",
        "11:15",
        "12:30",
        "13:45",
        "15:00",
        "16:15",
        "17:30",
        "18:45",
        "20:00",
        "21:15",
        "22:30",
        "23:45",
    ]
    y_data = [
        300,
        280,
        250,
        260,
        270,
        300,
        550,
        500,
        400,
        390,
        380,
        390,
        400,
        500,
        600,
        750,
        800,
        700,
        600,
        400,
    ]

    line = (
        Line()
            .add_xaxis(xaxis_data=x_data)
            .add_yaxis(
            series_name="用电量",
            y_axis=y_data,
            is_smooth=True,
            label_opts=opts.LabelOpts(is_show=False),
            linestyle_opts=opts.LineStyleOpts(width=2),
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(title="一天用电量分布", subtitle="纯属虚构"),
            tooltip_opts=opts.TooltipOpts(trigger="axis", axis_pointer_type="cross"),
            xaxis_opts=opts.AxisOpts(boundary_gap=False),
            yaxis_opts=opts.AxisOpts(
                axislabel_opts=opts.LabelOpts(formatter="{value} W"),
                splitline_opts=opts.SplitLineOpts(is_show=True),
            ),
            visualmap_opts=opts.VisualMapOpts(
                is_piecewise=True,
                dimension=0,
                pieces=[
                    {"lte": 6, "color": "green"},
                    {"gt": 6, "lte": 8, "color": "red"},
                    {"gt": 8, "lte": 14, "color": "yellow"},
                    {"gt": 14, "lte": 17, "color": "red"},
                    {"gt": 17, "color": "green"},
                ],
                pos_right=0,
                pos_bottom=100
            ),
        )
            .set_series_opts(
            markarea_opts=opts.MarkAreaOpts(
                data=[
                    opts.MarkAreaItem(name="早高峰", x=("07:30", "10:00")),
                    opts.MarkAreaItem(name="晚高峰", x=("17:30", "21:15")),
                ]
            )
        )
    )
    line.render()


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
