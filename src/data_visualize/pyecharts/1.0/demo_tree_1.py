#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://pyecharts.org/#/zh-cn/tree_charts?id=tree%ef%bc%9a%e6%a0%91%e5%9b%be
# http://gallery.pyecharts.org/#/Tree/tree_base
import pyecharts
import webbrowser


def draw_tree_base(dest_file):
    data = [
        {
            "name": "A",
            "children": [{
                "name": "B"
            },
                {
                    "name": "C",
                    "children": [{
                        "children": [{
                            "name": "I"
                        }],
                        "name": "E"
                    },
                        {
                            "name": "F"
                        }
                    ],

                },
                {
                    "name": "D",
                    "children": [{
                        "name": "G",
                        "children": [{
                            "name": "J"
                        }, {
                            "name": "K"
                        }]
                    },
                        {
                            "name": "H"
                        },
                    ],
                },
                {
                    "name": "F"
                },
            ],
        }
    ]
    # https://blog.csdn.net/wusefengye/article/details/79030900
    c = (
        pyecharts.charts.Tree(init_opts=pyecharts.options.InitOpts(width="1366px", height="700px"))
            .add("", data,
                 collapse_interval=2,  # 折叠节点间隔，当节点过多时可以解决节点显示过杂间隔。
                 # layout= 'radial', # 树图的布局，有 正交(水平和垂直方向) 和 径向(以根节点为圆心，每一层节点为环) 两种。
                 )
            .set_global_opts(title_opts=pyecharts.options.TitleOpts(title="Tree-基本示例"))
            .render(dest_file)
    )
    webbrowser.open(dest_file, new=1)


def main():
    dest_file = "树型图表.html"
    draw_tree_base(dest_file)


if __name__ == "__main__":
    main()
