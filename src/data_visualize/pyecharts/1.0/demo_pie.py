#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/95625996

# 先导入要绘图的函数模组
from pyecharts import options as opts
from pyecharts.charts import Page, Pie
from pyecharts.commons.utils import JsCode
from pyecharts.faker import Collector, Faker


# 使用绘图函数对表进行设置，并设置表的各种参数
def pie_rosetype() -> Pie:
    v = Faker.choose()
    c = (
        Pie()
            .add(
            "",
            [list(z) for z in zip(v, Faker.values())],
            radius=["30%", "75%"],
            center=["25%", "50%"],
            rosetype="radius",
            label_opts=opts.LabelOpts(is_show=False),
        )
            .add(
            "",
            [list(z) for z in zip(v, Faker.values())],
            radius=["30%", "75%"],
            center=["75%", "50%"],
            rosetype="area",
        )
            .set_global_opts(title_opts=opts.TitleOpts(title="Pie-玫瑰图示例"))
    )
    return c


def main():
    # 调用函数，并对图表进行渲染输出
    figure = pie_rosetype()
    figure.render()


if __name__ == "__main__":
    main()
