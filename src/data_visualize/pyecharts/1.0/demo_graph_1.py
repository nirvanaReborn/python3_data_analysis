#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://pyecharts.org/#/zh-cn/basic_charts?id=graph%ef%bc%9a%e5%85%b3%e7%b3%bb%e5%9b%be
# http://gallery.pyecharts.org/#/Graph/graph_base
'''
# https://www.cnblogs.com/xy3719/articles/7258784.html
# 生成网络拓扑图
滚轮还可以放大缩小, 还可以自定义图标。

唯一的缺点就是无法在连接线的两端显示端口好、两个节点之间只能有一条线相连。
缺点可以通过补充一个excel端口接线表来补充说明。
'''
from pyecharts.charts import Graph  # 引用之前要先pip install pyecharts及相关的支持模块
from pyecharts import options


def main():
    nodes = [{"name": "外网", "symbolSize": 50, 'symbol': 'image://D://yun.png'},  # 注意图标的位置要用绝对地址，否则会认不到图片
             {"name": "防火墙", "symbolSize": 40},  # 设置节点及节点图标大小
             {"name": "核心交换", "symbolSize": 50},
             {"name": "9-A1-1", "symbolSize": 20},
             {"name": "9-A1-2", "symbolSize": 20},
             {"name": "9-A1-3", "symbolSize": 20},
             {"name": "9-A3-1", "symbolSize": 20},
             {"name": "9-A3-2", "symbolSize": 20},
             {"name": "9-A3-3", "symbolSize": 20}, ]

    links = [{"source": "防火墙", "target": "外网"},  # 设置节点连接关系
             {"source": "防火墙", "target": "核心交换"},
             {"source": "核心交换", "target": "9-A1-1"},
             {"source": "核心交换", "target": "9-A1-2"},
             {"source": "核心交换", "target": "9-A1-3"},
             {"source": "核心交换", "target": "9-A3-1"},
             {"source": "核心交换", "target": "9-A3-2"},
             {"source": "核心交换", "target": "9-A3-3"}, ]

    # graph = Graph()
    # graph.add("", nodes, links, repulsion=8000)  # is_label_show=True表示节点名字为一直显示
    # # graph.show_config()#表示将生成的文件打印出来，我们只要结果的HTML不需要知道代码
    # graph.render(r'./tb.html')  # 生成html文件并保存到当前路劲下。
    c = (
        Graph()
            .add("", nodes, links, repulsion=8000)
            .set_global_opts(title_opts=options.TitleOpts(title="Graph-基本示例"))
            .render("graph_base.html")  # 生成html文件并保存到当前路劲下。
    )


if __name__ == "__main__":
    main()
