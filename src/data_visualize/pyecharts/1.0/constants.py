#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 官方教程: https://pyecharts.org/#/zh-cn/intro
# pyecharts 分为 v0.5.X 和 v1 两个大版本，v0.5.X 和 v1 间不兼容，v1 是一个全新的版本,仅支持 Python3.6+.
# pyecharts.org 不做版本管理，您所看到的当前文档为最新版文档，若文档与您使用的版本出现不一致情况，请及时更新 pyecharts。
# pip install echarts-themes-pypkg==0.0.3 pyecharts-jupyter-installer==0.0.3 pyecharts==1.7.1
import pyecharts

print(pyecharts.__version__)
if int(str(pyecharts.__version__).split('.')[0]) < 1:
    print("该脚本需要v1.x版本")
    print("[pyecharts v1.0.0 停止对 Python2.7，3.4~3.5 版本的支持和维护，仅支持 Python3.6+，向下不兼容的。](https://pyecharts.org/#/zh-cn/release-note/v100)")
    exit(0)


# Note: 在使用 Pandas&Numpy 时，请确保将数值类型转换为 python 原生的 int/float。比如整数类型请确保为 int，而不是 numpy.int32

# 使用 Notebook
# 当然你也可以采用更加酷炫的方式，使用 Notebook 来展示图表，matplotlib 有的，pyecharts 也会有的。
# pyecharts 支持 Jupyter Notebook / Jupyter Lab / Nteract / Zeppelin 四种环境的渲染。
# 具体内容请参考:https://pyecharts.org/#/zh-cn/notebook